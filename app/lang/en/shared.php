<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

/**
 * Created by PhpStorm.
 * User: mxgel
 * Date: 01/02/2018
 * Time: 14:00
 */
$messages = [
    "home"        => "Home",
    "top_games"   => "Top Games",
    "live"        => "Live",
    "jackpot"     => "Jackpot",
    "how-to-play" => "How to play",
    'title'       => "Bet Palace - The #1 Online & SMS sports Betting Website In Tanzania",
];