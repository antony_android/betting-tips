<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

/**
 * Created by PhpStorm.
 * User: mxgel
 * Date: 01/02/2018
 * Time: 14:00
 */
$messages = [
    "home"                 => "Home",
    "top_games"            => "Highlights",
    "today"                => "Today",
    "virtuals"             => "Virtuals",
    "autobet"              => "Autobet Big Wins",
    "results"              => "Results",
    "rafiki"               => "Bet na Beste",
    "tomorrow"             => "Tomorrow",
    "live"                 => "Live Betting",
    "jackpot"              => "Jackpot",
    "how-to-play"          => "How to Play",
    "my-bets"              => "My Bets",
    "my-transactions"      => "Transactions",
    "deposit"              => "Deposit",
    "withdraw"             => "Withdraw",
    "logout"               => "Logout",
    "register"             => "Register",
    "login"                => "Login",
    "football"             => "Football",
    "sports"               => "Sports",
    "betslip"              => "Betslip",
    "bal"                  => "Bal",
    "balance"              => "Balance",
    "win"                  => "Win",
    "phone_number"         => "Phone Number",
    "remember_me"          => "Remember Me",
    "password"             => "Password",
    "forgot_password"      => "Forgot or Have no Password",
    "forgot_password_mobi"      => "No Password?",
    "top_football"         => "Top Football",
    "how_to_enter"         => "How to Join",
    "customer_care"        => "Customer Care",
    "terms_and_conditions" => "Terms and Conditions",
    "company_number"       => "Paybill Numbers",
    "place_bet"            => "Place Bet",
    "remove_all"           => "Remove All",
    "possible_winnings"    => "Possible Winnings",
    "today_games"          => "Today's Games",
    "how_to_bet"           => "How to Bet",
    "shop"                 => "Be our Shop Partner",
    "upcoming"             => "Upcoming Games",
    "tomorrow"             => "Tomorrow",
    'title'                => "Bet Palace – Where Kings are Made",
    'description'          => "Bet Palace – Where Kings are Made - Kenya's number one sports betting platform",
    'keywords'             => "Bet Palace – Where Kings are Made, betin.co.ke, betin, sport, pesa, soccer, betting, kenya, autobet",
    'play-via-ussd'        => "Play via USSD",
    'ussd-code'            => '#',
];
