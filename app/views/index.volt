<!DOCTYPE html>
<html>
<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131026068-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-131026068-1');
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description"
          content="<?php echo $t->_('description'); ?>">
    <meta name="keywords" content="<?php echo $t->_('keywords'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:site_name" content="betpalace"/>
    <link  rel="shortcut icon" type="image/x-icon" sizes="16x16" href="{{ url('favicon.ico') }}">
    <link rel="preload" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700"></noscript>
    <!-- <link href='' rel='stylesheet' type='text/css'> -->
    {{ get_title() }}

    {{ partial("partials/header-scripts") }}

    <?php $divid = 'http://'.$_SERVER['HTTP_HOST'].'/betslip'; ?>

    {#<script type="text/javascript">#}
        {#var divid = "<?= $divid ?>";#}
        {#var url = "<?= $divid ?>";#}

        {#var divj = "betslipJ";#}
        {#var urlj = "matches/jackpot";#}

        {#var divb = "betslipB";#}
        {#var urlb = "matches/correct";#}

        {#var thediv = "matches";#}
        {#var dataUrl = "matches";#}
    {#</script>#}


    <script>
   /**  window.addEventListener('load', function(){
         var time = new Date().getTime();
         document.body.mousemove = function(e) {
             time = new Date().getTime();
         };

         document.body.keypress = function(e) {
             time = new Date().getTime();
         };

         function refresh() {
             if(new Date().getTime() - time >= 60000) 
                 window.location.reload(true);
             else 
                 setTimeout(refresh, 60000);
         }
         setTimeout(refresh, 60000);
         });
     **/
    </script>
    <meta name="google-site-verification" content="sonfKMuxyCx2xRl_uQAhzL8Sp-es--8tGrzgKmyTeT4" />


</head>
<body>
{{ content() }}

</body>
</html>
{{ partial("partials/footer-scripts") }}