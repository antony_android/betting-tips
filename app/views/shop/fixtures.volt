<html>

<style type="text/css" media="print">
    @page 
    {
        size:  auto;   /* auto is the initial value */
        margin: 2mm;  /* this affects the margin in the printer settings */
    }

    html
    {
        background-color: #FFFFFF; 
        margin: 0px;  /* this affects the margin on the html before sending to printer */
    }

    td{
        padding: 2px;
        border: 1px solid;
    }
</style>

<body>
<div style="padding: 20px;">
{{ partial("partials/shop/fixtures") }}
</div>

</body>
</html>
<script>
window.print();
</script>