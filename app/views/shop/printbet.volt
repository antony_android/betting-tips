<html>

<style type="text/css" media="print">
    @page 
    {
        size:  auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }

    html
    {
        background-color: #FFFFFF; 
        margin: 0px;  /* this affects the margin on the html before sending to printer */
    }
</style>

<body>
<div class="container">
{{ partial("partials/shop/printbet") }}
</div>
</body>
</html>
<script>
window.print();
</script>