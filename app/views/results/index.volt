<div class="container">
{{ partial("partials/top") }}
{{ partial("partials/results") }}
{{ partial("partials/bottom") }}
</div>
{{ partial("partials/footer") }}

<script>
	$(document).ready( function(){
	var date = new Date();
		$("#date_picked").datepicker({ dateFormat: 'dd-mm-yy'});
	});
</script>