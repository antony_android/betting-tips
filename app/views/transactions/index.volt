<div class="container">
{{ partial("partials/top") }}
<div class="">
    <div class="panel-header ">
        <h2>My Transactions</h2>
    </div>
    <div class="table-responsive inner-content whitebg">
        {{ this.flashSession.output() }}
        <table class="table mybets">
            <thead class="panel-header">
            <tr>
                <th>Date</th>
                <th>Reference</th>
                <th>Type</th>
                <th>Amount</th>
            </tr>
            </thead>
            <tbody>
            {% for transaction in transactions %}
                <tr>
                    <td>{{ transaction['created'] }}</td>
                    <td>{{ transaction['reference'] }}</td>
                    <td>{{ transaction['DB_CR'] }}</td>
                    <td>{{ transaction['amount'] }}</td>
                </tr>
            {% endfor %}
            </tbody>
        </table>
    </div>
</div>
{% if pages is defined %}
    <div class="paginator">
        <a href="?page=<?= $page-1; ?>">Prev </a>
        <?php for ($x = 0; $x <= $pages; $x++): ?>
        <a href="?page=<?= $x; ?>" class="
    <?php
            if($x==$page){
                echo 'current-page';
            }
            ?>
    "><?= $x+1; ?> </a>
        <?php endfor; ?>
        <a href="?page=<?= $page+1; ?>">Next </a>
    </div>
{% endif %}
{{ partial("partials/bottom") }}
</div>
{{ partial("partials/footer") }}