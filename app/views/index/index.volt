{{ partial("partials/header") }}

<div class="cd fade" id="msgModal" tabindex="-1" role="dialog" aria-labelledby="msgModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="d">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <button type="button" class="cg fx fp eg k js-newMsg">New message</button>
        <h4 class="modal-title">Messages</h4>

      </div>
    </div>
  </div>
</div>

<div class="cd fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="userModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
     {{ partial("forms/signup") }}
    </div>
  </div>
</div>


<div class="by amt">
  <div class="gc">

  {{ partial("partials/sidebar") }}

    <div class="gz home">
   
    	{{ image("img/banner.jpg") }}
    	<div class="panel-header">

    	<h4>Today's Matches</h4>
    	
    </div>

    


    <div class="matches">
    		<?php foreach($today as $day): ?>

			<div class="col-sm-12 top-matches">
            <div class="col-sm-1">
            <?php echo date('d/m', strtotime($day['start_time'])); ?>
            </div>
            <div class="col-sm-1">
            <?php echo date('H:i', strtotime($day['start_time'])); ?>
            </div>
            <div class="col-sm-1 odds">
             <span class="bold">ID<br/> <span class="label label-warning game-id"><?php echo $day['game_id']; ?></span></span>
            </div>

            <div class="col-sm-5 highlited" style="">
            <span class="bold"><?php echo $day['home_team']; ?></span> &nbsp; v  &nbsp; <span class="bold"><?php echo $day['away_team']; ?></span>
            </div>
            <div class="col-sm-3">
            <div class="col-sm-12">
	            <div class="col-sm-4 odds">
	            H<br/>
	            <span class="label label-inverse"> <?php echo $day['home_odd']; ?></span>
	            </div>
	            <div class="col-sm-4 odds">
	            X<br/>
	            <span class="label label-inverse"> <?php echo $day['neutral_odd']; ?></span>
	            </div>
	            <div class="col-sm-4 odds">
	            A<br/>
	            <span class="label label-inverse"> <?php echo $day['away_odd']; ?></span>
	            </div>
            </div>
            </div>
            </div>

            <?php endforeach; ?>
    	</div>

    </div>

{{ partial("partials/right") }}

  </div>
</div>

<div class="modal fade" id="mModal" role="dialog" >
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <a href="https://betpalace.co.ke/virtual-sports">
    <div class="modal-content">
        <?php

        $number = date('Ymd');

     
      $remainder = $number % 2;
      
      
      if($remainder != 0){
          $squa = 1;
      }else{
          $squa = 0;
      }

        $date = date('Ymd');

        
        ?>

        <?php

        if( $squa != 1 ){ ?>
      <div class="modal-header" style="padding: 0px;">
        <button type="button" class="close" data-dismiss="modal" style="position: absolute; top:10px; right:10px; color: #fff;">&times;</button>
        <img src="img/mfl.webp">
      </div>
      <?php 
          }else{ ?>
      <div class="modal-header" style="padding: 0px;">
          <button type="button" class="close" data-dismiss="modal" style="position: absolute; top:10px; right:10px; color: #fff;">&times;</button>
          <img src="img/mfls.webp">
        </div>

        <?php } ?>
      
    </div>
  </a>
  </div>
</div>


<div id="window-resizer-tooltip" style="display: none;"><a href="#" title="Edit settings"></a><span class="tooltipTitle">Window size: </span><span class="tooltipWidth" id="winWidth">1390</span> x <span class="tooltipHeight" id="winHeight">825</span><br><span class="tooltipTitle">Viewport size: </span><span class="tooltipWidth" id="vpWidth">1390</span> x <span class="tooltipHeight" id="vpHeight">192</span></div>


<div class="modal align-middle" id="mModal" role="dialog" style="">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <a href="https://betpalace.co.ke/virtual-sports">
    <div class="modal-content">
        <?php

        $number = date('Ymd');

     
      $remainder = $number % 2;
      
      
      if($remainder != 0){
          $squa = 1;
      }else{
          $squa = 0;
      }

        $date = date('Ymd');

        
        ?>

        <?php

        if( $squa != 1 ){ ?>
      <div class="modal-header" style="padding: 0px;">
        <button type="button" class="close" data-dismiss="modal" style="position: absolute; top:10px; right:10px; color: #fff;">&times;</button>
        <img src="img/mfl.webp">
      </div>
      <?php 
          }else{ ?>
      <div class="modal-header" style="padding: 0px;">
          <button type="button" class="close" data-dismiss="modal" style="position: absolute; top:10px; right:10px; color: #fff;">&times;</button>
          <img src="img/mfls.webp">
        </div>

        <?php } ?>
      
    </div>
  </a>
  </div>
</div>
 