<div class="container">
{{ partial("partials/top") }}
{{ partial("partials/rafiki") }}
{{ partial("partials/right_peer_new") }}
</div>
{{ partial("partials/footer") }}

<script>
	$(document).ready( function(){
		$("#date_put").datepicker({ dateFormat: 'dd-mm-yy'});
	});
</script>