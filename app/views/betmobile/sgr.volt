<div class="container">
	{{ partial("partials/top") }}
	{{ partial("partials/betmobile_sgr") }}
	{{ partial("partials/bottom") }}
</div>
{{ partial("partials/footer") }}

<script>
    $(document).ready(function () {
        var amount = $('#bet-mobile-success').val();
        if (amount) {
            betSuccessAnalytics(amount);
        }
    });
</script>