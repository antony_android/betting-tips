<div class="panel-header"><h2><?php echo $t->_('terms_and_conditions'); ?></h2></div>
<div class="odd-wrapper">

    <div class="nav-side">

      <button class="accordion acc-large" data-toggle="collapse" data-target="#definitions">1. Definitions</button>
	  <div id="definitions" class="collapse">
	    <div class="bigger-list">

		<p> In these rules the following words have the following meanings unless the context requires otherwise</p>

		<p> 1.1 We, Us, Our means Bet Palace (product) and our offices. </p>


		<p> 1.2 Bet means the staking of money on the prediction of the outcome of a sporting event offered by us. A bet can include one or more predictions. A bet on one outcome is a single. A bet on many different outcomes is a multiple.</p>


		<p> 1.3 Odds or Price means the return on the money staked should the prediction recorded in a Bet be correct. A winning price of 2 would return double to the stake to the customer.</p>


		<p> 1.4 Customer means anyone placing a bet with Bet Palace or any other services hosted or sponsored by said company.</p>

		<p> 1.5 User means anyone using this service.</p>

		<p> 1.6 VOID means that for one reason or another, the bet is not acceptable to Bet Palace. In these cases this bet is settled at the price of 1. In the case of a single bet this would mean the stake is returned to the customer.</p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#bets">2. Bets</button>
	  <div id="bets" class="collapse">
	    <div class="bigger-list">

		<p> Bets include all bets placed on any of our remote channels including our www.betpalace.co.ke  website, mobile apps and via SMS. In the case of such bets, the bet is struck when a bet id or receipt exists in our back end systems or database. We may refuse to take bets placed if they are deemed wrong or mistaken.</p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#identity-customers">3. Identity of Customers</button>
	  <div id="identity-customers" class="collapse">
	    <div class="bigger-list">

		<p> 3.1 Your mobile phone number is your identity. The identity of customers is determined by the mobile phone number. We strongly encourage all customers to sign up for identity programs administered by the mobile phone companies. We use mobile phone numbers to uniquely identify customers and their account details </p>


		<p> 3.2 If transactions are made via your phone by someone who is not the registered user, the registered user will remain liable for their transactions. In addition we cannot do account transactions for accounts which are not identical to the phone number.</p>


		<p> 3.3 This means that you cannot, for example, ask to be paid out on a different number other than the number you have placed a bet with; also you cannot send money to us and request that this is placed in a different account. In addition to your phone number you initial transaction will initiate the creation of a virtual account to which all monies added to the cause of betting will be held.</p>

		<p> 3.4 Note: This is not a standard banking account and it shall not be used to keep money other than the use to place bets.</p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#age-restriction">4. Age Restriction</button>
	  <div id="age-restriction" class="collapse">
	    <div class="bigger-list">

		<p> 4.1 The Law prohibits under 18 year olds from betting. We may at any time ask you to confirm your age. You will indemnify us of any damages if you deceive us or misrepresent your age. </p>


		<p> 4.2 Underage clients will have their accounts closed immediately and any balances forfeit. We may also suspend ANY account for any reason and ask you to come with ID to one of our offices or agents to prove age, and identity. Individuals suspecting underage gambling are encouraged to report this to us immediately on our customer service number or via email to <b>info@betpalace.co.ke</b></p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#account-security">4. Account Security</button>
	  <div id="account-security" class="collapse">
	    <div class="bigger-list">

		<p> 4.1 Customers are the registered user of their mobile number and Account. Customers are responsible for the security of their phone numbers and account on the Bet Palace platforms. </p>

		<p> 4.2 We strongly recommend that customers do not disclose their PINs or any other security sensitive information to any other person. In the event that an unauthorized person uses a customer's phone for betting or money transactions to us, the registered user remains responsible.</p>

		<p> 4.3 We may also ask you to identify yourself and collect your winnings at one of our premises if we so require.</p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#pricing">5. Pricing</button>
	  <div id="pricing" class="collapse">
	    <div class="bigger-list">

		<p> 5.1 Customers may incur charges when using SMS services. The pricing will be in 3 categories of single bet, multi bet and jackpot bets. Bet Palace will retain the right to change the prices as when deemed necessary. </p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#bets-validation">6. Acceptance and Validation of Bets</button>
	  <div id="bets-validation" class="collapse">
	    <div class="bigger-list">

		<p> 6.1 All bets will be settled at the end of the game (also referred to as Full-time and normal time). This denotes the period of play which includes time added by the Referee for injuries and other stoppages. This does not include scheduled extra time, or Penalty shoot-outs, if played. </p>

		<p> 6.2 In matches where penalty shoot outs or extra time are due to take place, all bets are settled at full time unless an Outright price is specifically requested and confirmed at the time the bet is placed. </p>

		<p> 6.3 Where Bet Palace have not quoted prices on a match, any single bet on that match will be void and treated as a non-runner in multiple bets. </p>

		<p> 6.4 Where Bet Palace have not quoted prices on a match, any single bet on that match will be void and treated as a non-runner in multiple bets. </p>

		<p> 6.5 If any team starts a 90 minute competitive game with less than 11 players, all bets on that match will be made void. </p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#latebets">7. Late Bets</button>
	  <div id="latebets" class="collapse">
	    <div class="bigger-list">

		<p> 7.1 Bets can only be placed on the outcome of a future event, unless we are offering special 'betting in-running prices' for that event. If Bet Palace mistakenly accepts a bet for an event that has already started then the bet will be void and your stake returned, except in the following circumstances: </p>

		<ul>
		<li>Where bets are taken after the start of an event, bets will stand if nothing has happened which would significantly alter the chances of the selection winning or losing.</li>

		</ul>

		<p> 7.2 All bets placed after the result of an event is known will be void win or lose. </p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#bets-cancellation">8. Cancellation of Bets</button>
	  <div id="bets-cancellation" class="collapse">
	    <div class="bigger-list">

		<p> 8.1 Once a bet is submitted, one will not be able to cancel the bet. Before submission of the bet, Bet Palace will offer the customer the option of canceling the selections. </p>

		<p> 8.2 Bet Palace may cancel bets or individual selections within bets, at any time and in that event, will return the stake to you. </p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#account-correction">9. Account Corrections</button>
	  <div id="bets-cancellation" class="collapse">
	    <div class="bigger-list">

		<p> 9.1 Bet Palace has the right to recover from you any amount overpaid and has your authority to adjust your account to reflect the true outcome and rectify the mistake. An example of such a mistake might be where a price is incorrect, a bet is late (see Acceptance and Validation of Bets) or where we enter a result of an event incorrectly.</p>

		</div>
	  </div>

	   <button class="accordion acc-large" data-toggle="collapse" data-target="#disputes">10. Disputes</button>
	  <div id="disputes" class="collapse">
	    <div class="bigger-list">

		<p> 10.1 If you have a complaint; you can email customer support on info@betpalace.co.ke and if your query or complaint is not resolved, you can ask for the matter to be escalated to the Head of Betting Operations. The Head of Betting Operations will investigate and contact you back with a resolution within 48 hours. You will be given the name and status of the people to whom your query/complaint has been referred. If you remain dissatisfied we may agree with you to refer the matter to an independent third body for arbitration.</p>

		<p> 10.2 Bet Palace will at all times apply best efforts to resolve a reported matter promptly.</p>

		<p> 10.3 If you have a query with regard to any transaction, you may also contact Bet Palace at info@betpalace.co.ke with details of the query. Bet Palace will review any queried or disputed transactions.</p>

		<p> 10.4 If for some reason you are not satisfied with the resolution of the complaint by the Company, you can complain to the Betting Control and Licensing Board.</p>

		<p> 10.5 Kindly note that by contacting the Betting Control and Licensing Board you are confirming that you have not breached any of the Bet Palace Terms and Conditions as agreed to by you upon registration to our service.</p>

		<p> 10.6 In all other times where a dispute arises, parties shall refer the matter for Arbitration by a single arbitrator agreed by the parities where the chosen venue shall be Bet Palace premises.</p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#errors">11. Errors</button>
	  <div id="errors" class="collapse">
	    <div class="bigger-list">

		<p> 11.1 Bet Palace makes every effort to ensure that we do not make errors in accepting bets. However, if as a result of human error or system problems a bet is accepted at a price (which includes the odds, handicap provisions, and other terms and conditions or details of the bet) that is:</p>

		<ul>

		<li>Materially different from those available in the general market at the time the bet was made; or</li>

		<li>Clearly incorrect given the chance of the event occurring at the time the bet was made then Bet Palace will pay winnings at the correct price.</li>

		</ul>

		<p> 11.2 Examples of circumstances where this would apply are:</p>

		<ul>

		<li>the price is recorded as 100-1 when the price on offer in the general market is 10-1</li>

		<li>the margins for handicap betting have been reversed</li>

		</ul>

		<p> 11.3 If a bet is accepted in error by Bet Palace on an event or outcome for which no Bet Palace prices are available the bet will be void and your stake returned.</p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#bet-displays">12. Bet Displays</button>
	  <div id="bet-displays" class="collapse">
	    <div class="bigger-list">

		<p> 12.1 A bet displayed on the website and or any other media channels (mobile app and sms) does not constitute a legally binding contract between the customer and us. The prices we offer on all outcomes on display on our website take the form of an advertisement rather than a contractual term.</p>

		<p> 12.2 Any results or scores displayed on the site – for example during betting in play – are for guidance purposes only.</p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#fraud">13. Fraud</button>
	  <div id="fraud" class="collapse">
	    <div class="bigger-list">

		<p> 13.1 Bet Palace will seek criminal and contractual sanctions against any customer involved in fraud, dishonesty or criminal acts. Bet Palace will withhold payment to any customer where any of these are suspected.</p>

		<p> 13.2 The customer shall indemnify and shall be liable to pay to Bet Palace, on demand, all costs, charges or losses sustained or incurred by Bet Palace (including any direct, indirect or consequential losses, loss of profit and loss of reputation) arising directly or indirectly from the customer’s fraud, dishonesty or criminal act.</p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#price-changes">14. Deposits, Withdrawals, Price & Price Changes</button>
	  <div id="price-changes" class="collapse">
	    <div class="bigger-list">
                <p> 14.1 For all deposits, the Number of the depositor must be identical to the mobile phone number registered on the Company account receiving the funds i.e. the registered account. Any charges levied by the Mobile Operators payment gateways (MPesa and Airtel Money) will be deducted from the player's account. You should only deposit money in your account for the purposes of you using such money to place bets/stakes on our platforms. We shall be entitled to suspend or close your account if we reasonably consider or have reason to believe that you are depositing money without any intention to place bets/stakes. You may only bet/stake if you have sufficient funds in your account. </p>
                <p>14.2 You can withdraw your money any time in accordance with the terms herein. We have a number of controls and checks that take place before any withdrawal request is processed. Withdrawals may be subject to limits from time to time at the sole descretion of betpalace. These checks are part of our ongoing commitment to maintaining the security and ensuring the safety of our customers' monies</p>
		<p> 14.3 All prices are subject to change and may, on occasion, be restricted to certain stake levels.</p>

		</div>
	  </div>

	   <button class="accordion acc-large" data-toggle="collapse" data-target="#results">15. Results</button>
	  <div id="results" class="collapse">
	    <div class="bigger-list">

		<p> 15.1 In the case of Football and other Sports, bets are settled on the official result published by the relevant event managers or result handlers 3 to 24 hours after the match/event is finished.</p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#results">16. Voids</button>
	  <div id="voids" class="collapse">
	    <div class="bigger-list">

		<p> 16.1 If a selection in a single bet is made void the stake will be returned. Void selections in multiple bets will be treated as non-runners and the stake will run onto the remaining selections in the bet.</p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#withholding-payment">17. Withholding of Payment</button>
	  <div id="withholding-payment" class="collapse">
	    <div class="bigger-list">

		<p> 17.1 We reserve the right to withhold payment and to declare bets on an event void if we have evidence that the following has occurred:</p>

		<ul>

			<li>the integrity of the event has been called into question</li>

			<li>the price(s) or pool has been manipulated</li>

			<li>match rigging has taken place</li>

		</ul>

		<p> 17.2 Evidence of the above may be based on the size, volume or pattern of bets placed with us across any or all of our betting channels. A decision given by the relevant governing body of the sport in question (if any) will be conclusive.</p>

		<p> 17.3 If any customer owes any money to Bet Palace for any reason, we have the right to take that into account before making any payments to that customer.</p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#full-normalbets">18. Full Time and Normal Time</button>
	  <div id="full-normalbets" class="collapse">
	    <div class="bigger-list">

		<p><strong>What is meant by 'Full Time' and 'Normal Time'?</strong></p>

		<p> 18.1 Adult matches are played over a period of time, and this period can be called 'Full Time' or 'Normal Time'. All bets unless specifically stating they are for Outright, To Qualify, Extra Time or Penalties markets, will be settled on the result after Normal Time</p>

		<p> 18.2 During the game, the referee may stop the game for short periods for substitutions, or if a player gets injured, and at the end of each half the referee adds however many minutes he feels is required to account for any of these stoppages. This additional time is called "Injury time".</p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#extra-time">19. Extra Time</button>
	  <div id="extra-time" class="collapse">
	    <div class="bigger-list">

		<p> 19.1 Extra Time is the added extra time that a game ending in a draw has to be played to determine the winner. Full Time and Normal Time are terms for the full game period, plus any 'Injury Time' that the referee chooses to add.</p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#abandoned-matches">20. Abandoned Matches</button>
	  <div id="abandoned-matches" class="collapse">
	    <div class="bigger-list">

		<p> 20.1 If a match is abandoned after it has commenced, all bets on that match will be made void except where settlement has already been determined. For example, where the first goal has been scored by a named player the First Goal scorer and Time of First Goal markets, amongst others, will stand.</p>

		<p> 20.1 If any team starts a competitive game with less than 11 players, all bets on that match will be made void.</p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#postponed-matches">21. Postponed/Re-arranged Matches</button>
	  <div id="postponed-matches" class="collapse">
	    <div class="bigger-list">

		<p> 21.1 A postponed match is void unless it is re-scheduled to commence within 24 hours of the original start time and this is confirmed within 12 hours of the original start time. In such circumstances where (a) void match(es) is/are included in an accumulator the bet will be settled on the remaining selections.</p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#price-fluctuation">22. Prices Subject to Fluctuation</button>
	  <div id="price-fluctuation" class="collapse">
	    <div class="bigger-list">

		<p> 22.1 All prices are subject to fluctuation up to the kick-off. All bets will be settled using Bet Palace online (www.betpalace.co.ke) prices at the time the bet is placed.</p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#contingencies">22. Related Contigencies in One Match</button>
	  <div id="contingencies" class="collapse">
	    <div class="bigger-list">

		<p> 22.1 Where we offer various betting opportunities from the same match (e.g. correct score, first player to score etc) these cannot be combined in accumulative bets where the outcome is related (except where special fixed prices are available).</p>

		<p> 22.2 Where an accumulative bet of this type has been accepted in error it will be settled by equally dividing the stake unit where the related outcomes clash.</p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#results">23. Results</button>
	  <div id="results" class="collapse">
	    <div class="bigger-list">

		<p> 23.1 If the result is not immediately available from an official channel then the result published by the official governing body immediately the match/event is finished will be used for settling purposes. Any subsequent corrections or amendments to this result will not apply for settlement purposes.</p>

		<p> 23.2 Bet Palace will settle all bets rapidly but because of time differences, for some events, bets may be settled overnight or with some delay.</p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#payouts">24. Maximum Payouts</button>
	  <div id="payouts" class="collapse">
	    <div class="bigger-list">

		<p> 24.1 Unless otherwise authorized by Bet Palace, the maximum payout of any winning bet shall be KES 50,000.</p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#account-closure">25. Account Closure</button>
	  <div id="account-closure" class="collapse">
	    <div class="bigger-list">

		<p> 25.1 We reserve the right to close any customer account or refuse to take bets from any customer. In this event of account closure, the full value of any customer funds will be returned as soon as applicable.</p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#betting-procedures">26. Betting Procedures</button>
	  <div id="betting-procedures" class="collapse">
	    <div class="bigger-list">

	    <strong>Website (www.betpalace.co.ke)</strong>

		<p> 26.1 User initiates registration process by sending the word GAME to the SMS short code 29085 (Charging: MO-On Demand). That information is relayed to the mobile carrier then to the Bet Palace systems.</p>

		<p> 26.2 An automated confirmation response is generated; Welcome to Bet Palace. Your account is 07XX XXX XXX. Visit www.betpalace.co.ke for T & C. SMS cost KES 2.00.</p>

		<ul>
			<li>The SMS cost will be subject to change as deemed necessary by Bet Palace.</li>
		</ul>

		<p> 26.3 The first user interaction would be the registration process. The expected procedure would be on SMS, sending the word GAME(S) to 29085 and in turn receive a message confirming registration. The registration process involves creating the user profile and account.</p>

		<ul>

		<p><strong>Bet Placement</strong></p>

		<li>Bets can be placed through 3 ways;</li>

		<ol>
			<li>Through www.betpalace.co.ke</li>
			<li>Through Mobile App</li>
			<li>Through SMS</li>
		</ol>

		<p> 26.4 Once the user has placed bets, then after the match(es) have ended, the user will receive an automated notification of scores/results of the specific match(es) the user placed bets on and whether they have won or not and by how much.</p>

		<p> 26.5 This information will automatically initiate the accounting protocol of the betting system and credit/debit the users account and also be able to generate statements for that specific match/event.</p>

		<p> 26.6 This information can be accessed by the end user anytime through checking their account statement.</p>
		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#responsible-gaming">27. Responsible Gaming</button>
	  <div id="responsible-gaming" class="collapse">
	    <div class="bigger-list">

		<p> 27.1 Bet Palace is dedicated to increasing awareness among its customers on responsible betting. Besides, Bet Palace affirms its commitment to, at all times, provide a good and enjoyable betting experience to all involved parties at any time. This call for responsible betting is not aimed at denying the players the fun they derive from betting, but is a mature way of ensuring that one stays in control.</p>

		</div>
	  </div>

	  <button class="accordion acc-large" data-toggle="collapse" data-target="#staying-control">28. Staying in Control</button>
	  <div id="staying-control" class="collapse">
	    <div class="bigger-list">

		<p> 28.1 The majority of our customers enjoy the betting entertainment experience. Whilst the majority of people do wager within their means, for some, wagering may become a problem. It may help you to keep control to remember the following:</p>

		<ul>

			<li>Only place bets with amounts that you can afford to lose;</li>

			<li>Keep track of the time and amount you spend on betting;</li>

			<li>If you want to have a break, you may use our self-exclusion option by emailing us at info@betpalace.co.ke with details of the account you have opened with us. Our representative will call you to confirm your instructions and thereafter the company will close your account(s) for a period of 3 months, during which time it will not be possible for the account(s) to be re-opened for any reason.</li>

			<li>Sports betting should be entertaining and not seen as a way of making money; Avoid chasing losses.</li>

			<li>If you are concerned that betting may have taken over your life (or someone else's life) then the following questions may help you find out:</li>

			<ol>

			<li>Have others ever criticized your betting?</li>

			<li>Have you ever lied to cover up the amount you have used to place bets or time you have spent doing it?</li>

			<li>Do arguments, frustrations or disappointments make you want to wager?</li>

			<li>Do you wager alone for long periods?</li>

			<li>Do you stay away from work or college to wager?</li>

			<li>Do you place wagers to escape from a boring or unhappy life?</li>

			<li>Are you reluctant to spend 'wagering money' on anything else?</li>

			<li>Have you lost interest in your family, friends or pastimes due to wagering?</li>

			<li>After losing, do you feel you must try and win back your losses as soon as possible?</li>

			<li>When wagering and you run out of money, do you feel lost and in despair, and need to place wagers again as soon as possible?</li>

			<li>Do you place bets using all your finances?</li>

			<li>Have you lied, stolen or borrowed just to get money to place bets or to pay wagering debts?</li>

			<li>Do you feel depressed or even suicidal because of your wagering?</li>

			</ol>

			</ul>

			<li>These questions are provided to help an individual decide if he or she has a problem and needs to seek help.</li>

		</ul>

			<p style="text-decoration: underline;">Getting Help</p>

		<ul>

			<li>There are many organizations that can provide support and assistance to individuals who develop a problem with wagering. If you feel this has happened to you, we highly recommend contacting a self-help organization for guidance.</li>

			</ul>

			<p style="text-decoration: underline;">Restriction of Minor/Under-age Betting</p>

			<ul>

			<li>Betting for persons below the age of 18 is prohibited.</li>

			</ul>

			</ul>

			<p style="text-decoration: underline;">How to protect minors from betting:</p>

			<ol>

			<li>Your details like deposit, password and username should not be shared with minors.</li>

			<li>Your machine should never be left unattended when logged in. Avoid using the “Remember My Password” option on your computer.</li>

			<li>Lock your computer with a password so as to limit or deny incidental or accidental access to it.</li>

			<li>Enlighten your kids on the risks and dangers of underage betting.</li>

			<li>Alternatively, use child protection software.</li>

			</ol>

			<p style="text-decoration: underline;">Measures we have put in place to curb under-age betting:</p>

			<ol>

			<li>Every individual signing up for a new account at www.betpalace.co.ke must tick a box indicating they are 18 years and above. There is a notification that players under 18 are not accepted.</li>

			<li>Once an individual has created an account using his/her date of birth, address and name, such bio-data is used to ascertain that the person is above 18 years.</li>

			<li>The use of passport or original National ID or other documents to proof age and identity of person when collecting winnings from any of our offices or appointed agents.</li>

			<li>Our payment services are based on mobile money services, cash and cheque. For cheque or cash payments, we require proof of age through the Identity card.</li>

			<li>If you suspect or know of any child betting on our platforms or using someone’s account, notify us immediately via info@betpalace.co.ke for necessary steps to be taken. Your details will be confidential and your effort will be highly appreciated.</li>

			</ol>

			<p style="text-decoration: underline;">Adhering to Limits</p>

			<ol>

			<li>

			In a day, a user will be allowed a maximum of;

				<ul>

					<li>15 single bets</li>

					<li>10 multi bets</li>

					<li>6 jackpot bets</li>

				</ul>

			</li>

			</ol>

			<p style="text-decoration: underline;">Constant Review of Customer History</p>

			<ul>

			<li>
			To keep tabs on your betting activities on Bet Palace, we provide you with easy access to the history of your monthly transactions. Click on “Bet History” on your www.betpalace.co.ke account, to monitor the bets placed, amount spent and the outcomes (win or lose). 
			</li>

			</ul>

			<p style="text-decoration: underline;">Self-Exclusion Option</p>

			<ul>

			<li>The Bet Palace Self-Exclusion facility allows you to temporarily close your account for a particular period.</li>

			<li>During self-exclusion, you will not be allowed to re-open your account or any new account.</li>

			<li>Monitoring Mechanisms will be put in place for any activity related to your account or profile on Bet Palace betting platforms. Any new accounts detected will be closed immediately.</li>

			<li>

			<b>How to request for Bet Palace Self-Exclusion?</b>

			<ul>
				<li>Write to us on info@betpalace.co.ke to request for temporary closure of your account for your preferred time duration.</li>

				<li>Once you send your request our representatives will get in touch with you to confirm your request and you will thereafter receive an email acknowledging receipt. This is typically within 48 hours of sending a Self-Exclusion request.</li>

			</ul>

			</li>

			<li>

			<b>What does the Account Closure process entail?</b>

			<ul>
				<li>Once we receive a Self-Exclusion request, the process of closing the account begins. It mainly involves checking account balances or any pending bets and deactivation of the account; as well as setting alerts for any activity related to the particular account or profile on Bet Palace betting platforms.</li>

			</ul>

			</li>

			<li>

			<b>How soon after requesting a self-exclusion will it be activated?</b>

			<ul>
				<li>This process requires reasonable working period to implement. However, we will endeavor to effect your self-exclusion as soon as possible. Typically, it will take a maximum of 48 hours to implement after you receive an email acknowledging receipt of self-exclusion request on your account from a Bet Palace Agent. Once the account closing process is complete, you will receive a confirmation email alerting you of the temporary closure. The self-exclusion period is considered as having commenced when it is communicated to you.</li>

			</ul>

			</li>

			<li>

			<b>What happens to the balance in my Bet Palace account after temporary closure?</b>

			<ul>
				<li>If you have any balance, we will reach out to you to withdraw or get consent that you would want the money to remain in your account.</li>
			</ul>

			<b>What happens to bets that I have placed which have not yet reached a conclusion?</b>

			<ul>
				<li>Any unsettled bets will be settled on the result of the event. If your bet is a win, you can contact our Customer Support team to arrange for the payment of your winnings to be made.</li>

			</ul>

			</li>

			<li>

			<b>What do I do if I can still access my Bet Palace Account after start of Self-Exclusion period?</b>

			<ul>
				<li>If you can still access any of our Bet Palace services after having received communication of activation of your Self-Exclusion period, kindly notify us immediately. Write to us at info@betpalace.co.ke informing us of your continued access to Bet Palace services.</li>

				<li>Failure to communicate to us, we will not be held liable to you or any third party if you are able to continue to bet via Bet Palace website or SMS or our other platforms after self-exclusion.</li>

			</ul>

			</li>

			<li>

			<b>What should I do during the Self-Exclusion period?</b>

			<ul>
				<li>We recommend that you seek additional support and advice during the self-exclusion period. Once the period expires, we will automatically re-open your account.</li>

			</ul>

			</li>

		</div>
	  </div>

     </div>

</div>
