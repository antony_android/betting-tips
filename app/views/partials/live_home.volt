   {{ javascript_include('js/live_home.js') }} 
   <div class="live-matches-header">
      <div class="row  row-mg-0">
      <div class="col-md-4">
      <span class="strip">
       LIVE GAMES</span>
       </div>
       <div class="col-md-4"></div>
       </div>
   </div>

    <div class="col-sm-12 nopadding left-text">
      <div id="live_matches" class="live-match"> 
      
        <?php if(count($liveMatches) > 0): ?>
        <?php foreach($liveMatches as $match): ?>
        <a href="/livematch?id=<?= $match['match_id']; ?>">
        <div class="one-homelive">
        <div class="live-header-wrapper">
          <span class="live-header">Live</span>
          <!-- <span class="live-match-header">
          <img class="side-icon" src="<?= 'svg/'. $sp['sport_name'].'.svg' ?>"> </span> -->
          <span class="live-competition"><?= $match['sport_name']; ?></span> - 
          <span><?= $match['category']; ?></span> - 
          <span><?= $match['competition_name']; ?></span>
        </div>
        <div class="row row-mg-0 live-home-content">
         <div class="row row-mg-0">
          <div class="col-sm-3 col-xs-4 col-pd-0">
            <span class="live-team"><?= $match['home_team']; ?></span>
          </div>
          <div class="col-sm-6 col-xs-4 live-goal col-pd-0">
            <div class="col-md-12 hidden-xs live-innerplaceholder" style="height:auto">
              <div class="col-md-12">
                <div class="col-md-2"></div>
                <?php $scores= $match['score']; ?>
                <?php $splitScores = explode (":", $scores); ?>
                <div class="col-md-1 counter-live-red-cards-left home_corner">
                <?php echo $splitScores[0]; ?></div>
                <?php $match_time= $match['match_time']; ?>
                <?php $splitTime = explode (":", $match_time); ?>
                <?php $displayTime = $splitTime[0]; ?>
                <?php if($match['match_status'] == "Halftime"){
                    $displayTime = "Halftime";
                }else{

                  $displayTime = $displayTime."'";
                }?>
                <div class="col-md-6 stats-bg live-scores">
                  Scores at <span style="font-weight:bold;">
                  <?php echo $displayTime; ?>
                  </span>
                </div>
                 <div class="col-md-2"></div>
                <div class="col-md-1 counter-live-red-cards-right away_corner">
                <?php echo $splitScores[1]; ?></div>
               </div>
              </div>
              <div class="mobi col-xs-12 align-center">
                <span class="live-goal"><?= $match['score']; ?></span>
              </div>
            </div>
            <div class="col-sm-3 col-xs-4 align-right col-pd-0">
              <span class="live-team"><?= $match['away_team']; ?></span>
            </div>
          </div>
          <div class="row row-mg-0">
          <?php $score = $match['score']; ?>
          <div class="col-sm-3 col-xs-4 live-goal col-pd-0">
            <?php for($i=0; $i < $match['home_yellow_card']; $i++): ?>
              <img class="yellow-cards-live" src="/img/yellow_card.png" alt="" />
            <?php endfor ?>
            <?php for($i=0; $i < $match['home_red_card']; $i++): ?>
              <img class="yellow-cards-live" src="/img/yellow_card.png" alt="" />
            <?php endfor ?>
          </div>
          <div class="col-sm-6 col-xs-4 live-goal col-pd-0">
            <div class="col-md-12 hidden-xs live-innerplaceholder" style="height:auto">
              <div class="col-md-12">
                <div class="col-md-2"></div>
                <div class="col-md-1 counter-live-left home_corner">
                <?php echo $match['home_corner']; ?></div>
                <div class="col-md-6 stats-bg">Corners</div>
                 <div class="col-md-2"></div>
                <div class="col-md-1 counter-live-right away_corner">
                <?php echo $match['away_corner']; ?></div>
               </div>
              </div>
              <div class="mobi col-xs-12 align-center">
                <span class="live-minutes">
                  <?php echo $displayTime; ?>
                </span>
              </div>
          </div>
          <div class="col-sm-3 col-xs-4 align-right col-pd-0">
          <?php for($i=0; $i < $match['away_red_card']; $i++): ?>
            <img class="yellow-cards-live" src="/img/yellow_card.png" alt="" />
          <?php endfor ?>
          <?php for($i=0; $i < $match['away_yellow_card']; $i++): ?>
            <img class="yellow-cards-live" src="/img/yellow_card.png" alt="" />
          <?php endfor ?>
          </div>
          </div>
        </div>
        </div>
        </a>
        <?php endforeach; ?>
        <?php else: ?>
        <div class="empty-row col-md-12">There are are no Live Games at the Moment. Please check with us in a moment!</div>
        <?php endif; ?>

      </div>
  </div>