{#<div id="fb-root"></div>#}

{#Carousel#}
{{ partial("partials/carousel") }}

<div class="homepage block-shadow">
   
      <div class="col-sm-12 top-matches hd">
        <div class="h">

            <!-- tabs menu -->
            
            <div class="ss col-sm-8 nopadding">
                <h3 class="competition">
                    {{title}}
                </h3> 
            </div> 

        <div class="ss col-sm-4 nopadding web-element">
         <form name="search" method="post" action="/competition?id={{competitionId}}&sp={{sportId}}">
            <div class="col-sm-8 nopadding submit-input">
                <input type="text" name="keyword" class="form-control" data-action="grow"
                                         placeholder="Your search criteria" value="{{keyword}}">
            </div>
            <div class="col-sm-4 nopadding search-button">
                <button type="submit" class="search-btn">Search</button>
            </div>
            </form>
        </div>
      </div> <!-- end class h -->
        
    </div> <!-- end top matches -->

    <!-- Tab panels -->
    <div class="tab-content">
       
            {{ partial("partials/match-list") }}
        
    </div> <!-- end tab content -->

</div> <!-- end home page -->
