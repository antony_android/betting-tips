<div class="gn">
    <!-- <div class="qv rc aog alu web-element">
    <header><div class="header-holder"><span class="col-sm-10">Live Betting</span><span class="col-sm-2 header-icon"><i class="fa fa-rss" aria-hidden="true"></i></span></div></header>
    </div> -->
    <div class="qv rc aog alu web-element block-shadow bottom-std-margin-spacing">
        <header>
            <div class="header-holder">
                <span class="col-sm-10"><?php echo $t->_('top_football'); ?></span><span class="col-sm-2 header-icon">
                <img height="14px" src="/svg/football.svg" alt="">
                </span>
            </div>
        </header>

        <ul class="aoi nav base-bg">
            <?php foreach($this->topCompetitions as $competition): ?>
            <li class="li-white-h">
                <a class="col-sm-12" href="{{url('/shop/competition')}}?id=<?php echo $competition['competition_id']?>&sp=<?= $competition['sport_id']?>">
                    <span class="col-sm-1" style="padding:0;"><img class="side-icon" src="<?php echo $this->categoryLogo->getFlag($competition['country_code']) ?>"/> </span>
                    <span class="col-sm-9 topl" style=""><?php echo $competition['competition_name']?></span>
                </a>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>

    <div class="web-element block-shadow bottom-std-margin-spacing">
        <header>
            <div class="header-holder"><span class="col-sm-10">Other Sports (A-Z)</span><span
                        class="col-sm-2 header-icon"><i class="icon-football fa fa-gamepad"
                                                        aria-hidden="true"></i></span></div>
        </header>
        <?php
            $id=''; 
            $categoryID='';
            $competitionID='';
        ?>
        <ul class="sidebar-menu aoi nav base-bg">
            <?php foreach($sports as $sp): ?>
            <?php if($id!=$sp['sport_id']): ?>
            <li class="treeview">
                <a href="#">
                    <span style="padding:0;"><img class="side-icon" src="<?= '/svg/'. $sp['sport_name'].'.svg' ?>"> </span>
                    <span class="topl"> {{ sp['sport_name'] }} </span>
                    <img class="down-arrow pull-right" src="{{ url('img/down-arrow.svg') }}">
                </a>
                <ul class="treeview-menu">
                    <?php $id=$sp['sport_id']; ?>


                    <?php foreach($sports as $s): ?>
                    <?php $url='/shop/'.$sportType[$s['sport_id']].'?id='.$s['competition_id']; ?>
                    <?php if($id == $s['sport_id'] && $categoryID != $s['category_id']): ?>
                    <li class="treeview"><a href="{{ url(url) }}">
                            <img class="side-icon" src="<?php echo $this->categoryLogo->getFlag($s['country_code']) ?>"/>
                            <span class="topl"> {{ s['category'] }} </span>
                        </a>

                        <ul class="treeview-menu second-child">
                            <?php $categoryID=$s['category_id']; ?>
                            <?php foreach($sports as $st): ?>
                            <?php
                                $url='/shop/'.$sportType[$s['sport_id']].'?id='.$st['competition_id'].'&sp='.$id; 
                                $competitionID=$st['competition_id'];
                                ?>
                            <?php if($categoryID == $st['category_id']): ?>
                            <li><a href="{{ url(url) }}">
                                    <span class="topl"> {{ st['competition_name'] }} </span>
                                </a></li>
                            <?php endif; ?>
                            <?php $categoryID=$s['category_id']; ?>
                            <?php endforeach; ?>
                        </ul>

                    </li>
                    <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </li>
            <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="web-element">
	<a href="/autobet"><img src="/img/autobet-left.jpg" alt="Bet Palace Auto Bet" /></a>
    </div>
</div>
