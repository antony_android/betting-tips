</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<?php
  $slipCount = $this->session->get('shopbetslip');
  $shopSlipCount = sizeof($slipCount);
?>
<div class="gn" id="right-generic">
    <div class='alert alert-success alert-dismissible betslip-success' role='alert'>
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
        </button>
        Bet successfully placed!
    </div>
    <div class="bet-option-list" id='slip-holder'>
        <div class="bet alu block-shadow">
            <header>
                <div class="betslip-header">
                    <span class="col-sm-7 slp">SELECTED SLIPS</span>
                    <span class="col-sm-4 slp">
                        <img src="{{ url('img/loader.gif') }}" class="loader"/>
                    </span>
                    <span class="col-sm-1 shop-slip-counter">
                        <?= $shopSlipCount ?>
                    </span>
                </div>
            </header>
            <img src="{{ url('img/loader.gif') }}" class="loader"/>
            <button id="slip-button-close" type="button" class="close mobi" aria-hidden="true">×</button>
            <div id="betslip-shop" class="betslip">

            </div>

            <div id="quick-login">


                <?php echo $this->tag->form("shop/authenticate"); ?>

                <p>
                    <label>Cashier Number *</label>
                    <?php echo $this->tag->textField(["mobile","class"=>"form-control msisdn-f"]) ?>
                </p>

                <p>
                    <label>Password *</label>
                    <input type="hidden" name="ref" value="{{ refURL }}">
                    <?php echo $this->
                    tag->passwordField(["password","name"=>"password","class"=>"form-control",
                    "placeholder"=>"Password"])
                    ?>
                </p>

                <div class="col-sm-12 zero-padding">
                    <div class="col-sm-4 zero-padding"><?php echo $this->tag->submitButton(["Login","class"=>"cg fm"])
                        ?>
                    </div>
                </div>

                </form>
            </div>

        </div>

        <div id="payScroller"></div>

        	{{ partial("partials/company-info") }}

    	</div>

  </div>
</div>

<div id="window-resizer-tooltip" style="display: none;"><a href="#" title="Edit settings"></a><span class="tooltipTitle">Window size: </span><span class="tooltipWidth" id="winWidth">1390</span> x <span class="tooltipHeight" id="winHeight">825</span><br><span class="tooltipTitle">Viewport size: </span><span class="tooltipWidth" id="vpWidth">1390</span> x <span class="tooltipHeight" id="vpHeight">192</span></div>