<div class="">
<div class="row panel-header mybets-bg">
  <div class="col-sm-8">
  	<h2>My Shop bets</h2>
  </div>
  <div class="col-sm-4">
  	<form name="search" method="post" action="/shop/searchbet">
	    <div class="col-sm-8 nopadding submit-input">
	        <input type="text" name="bet_id" class="form-control" data-action="grow"
	            placeholder="BET ID">
	    </div>
	    <div class="col-sm-4 nopadding search-button">
	        <button type="submit" class="search-btn">Search</button>
	    </div>
    </form>
  </div>
</div>
	<div class="table-responsive inner-content whitebg "> 
	{{ this.flashSession.output() }}
	<table class="table mybets"> 
	<thead class="panel-header"> 
	<tr> <th>Date</th> <th>BET ID</th> <th class="web-element">Bet Type</th> <th>Bet Amount</th> <th>Status</th> </tr> 
	</thead> 
	<tbody> 
	<?php foreach($myBets as $bet): ?>
	<tr class='clickable-row' data-href="mybets?id=<?= $bet['bet_id'] ?>"> 
	<td><a href="mybets?id=<?= $bet['bet_id'] ?>"><?= date('d/m H:i', strtotime($bet['created'])) ?></a></td>
		  <td><a href="mybets?id=<?= $bet['bet_id'] ?>"><?= $bet['bet_id'] ?></a></td>
		  <td class="web-element"><a href="mybets?id=<?= $bet['bet_id'] ?>"><?php 
		  if($bet['jackpot_bet_id']){
			echo "JACKPOT";
		  }else{
		  if($bet['total_matches']>1){
			echo "MULTIBET";
		  }else{
		  	echo "SINGLEBET";
		  }
		  }
		  ?></a></td> 
		  <td><a href="mybets?id=<?= $bet['bet_id'] ?>"><?= $bet['bet_amount'] ?></a></td> 
		  <td><a href="mybets?id=<?= $bet['bet_id'] ?>">

		  <?php if($bet['status']==200){
			echo 'Payment Pending';
		   }elseif($bet['xstatus']==5){
			    if($bet['sp_status']==50){
			    	echo 'Paid';
			    }else{
			    	echo 'Won';
			    }
		   }elseif($bet['xstatus']==3){
			echo 'Lost';
		   }elseif($bet['xstatus']==4){
			echo 'Cancelled';
		   }elseif($bet['xstatus']==9){
			echo 'Jackpot';
		   }elseif($bet['xstatus']==24){
			echo 'Cancelled';
		   }elseif($bet['xstatus']==1){
			echo 'Open';
   		   }
   		   elseif($bet['xstatus']==50){
                echo 'Paid'; 
		   }
		   else{
			echo 'Lost';
		   }
		   ?></a></td> 
	</tr> 
	<?php endforeach; ?>
	</tbody> 
	</table> </div>
</div>
