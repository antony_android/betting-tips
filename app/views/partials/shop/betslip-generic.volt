<tr class="hide-on-affix odds-bg">
    <td>TOTAL ODDS</td>

    <td class="pull-right">BET AMOUNT</td>
<tr class="odds-bg">
    <td><b><?php echo $totalOdds; ?> </b></td>
    <td class="pull-right">
        <div id="betting">
            <input class="bet-select" type="text" id="bet_amount" name="bet_amount" onkeyup="winnings()" value="50"
                   min="20" max="20000"/>
        </div>
    </td>
</tr>

<tr class="bet-win-tr hide-on-affix ">
    <td>POSSIBLE WIN</td>
    <td class="pull-right">
    <?php $winnings = $totalOdds*50; ?>
        KES. <span
                id="pos_win"><?php ($winnings<70000)?$winnings = $winnings:$winnings=70000; $taxable = $winnings-50;  echo $winnings; ?></span>
    </td>
</tr>

<tr class="bet-win-tr hide-on-affix">
    <td>Tax (20%)</td>
    <td class="pull-right">KES. <span id="tax"><?php  $tax = (20 * $taxable) / 100;  echo round($tax); ?></span></td>
</tr>
<tr class="bet-win-tr hide-on-affix possible-win-bg">
    <td>NET POSSIBLE WIN</td>
    <td class="pull-right">KES. <span id="net-amount"><?php echo round($winnings - $tax) ?></span></td>
</tr>
<tr><td colspan=2></td></tr>
