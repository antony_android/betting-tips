<div class="">
<div class="panel-header">
	<h2>Withdrawal</h2>
</div>
<div class="table-responsive inner-content whitebg ">
<p>Enter Amount Below:</p>

        {{ this.flashSession.output() }}

<?php echo $this->tag->form("/shop/withdrawal"); ?>

 <p>
    <label>Amount (KSh.) *</label>
    <?php echo $this->tag->numericField(["amount","placeholder"=>"KSh. ","class"=>"form-control","required"=>"required"]) ?>
 </p>

<p>
    <?php echo $this->tag->submitButton(["Withdraw Now","class"=>"cg fm"]) ?> 
 </p>

</form>
</div>
</div>