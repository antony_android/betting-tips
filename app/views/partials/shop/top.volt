<?php 

  $id = $this->session->get('auth')['id'];

  $phql = "SELECT ProfileBalance.balance,ProfileBalance.bonus_balance from ProfileBalance where ProfileBalance.profile_id='$id' limit 1";
  $checkUser = $this->modelsManager->executeQuery($phql);

  $checkUser=$checkUser->toArray();

 ?>

<div id="shrink-header" class="">

<?php
  $slipCount = $this->session->get('shopbetslip');
  $shopSlipCount = sizeof($slipCount);
?>

<nav class="ck pc os app-navbar top-nav navbar navbar-expand-lg navbar-light bg-light">

  <div class="by hidden-overflow header-pd">

    <div class="col-md-3 col-xs-3 hidden-overflow">

        <div>
          <a class="e logo" href="{{url('/shop/matches')}}" title="Bet Palace">
          <img src="{{url('/img/logo.png')}}" alt="Bet Palace" title="Bet Palace">
          </a>
      </div>

    </div>
    
    <div class="hidden-md col-xs-5">
    {% if session.get('auth') != null %}
      <span class="mobi balnc" href="">

        <?php echo $t->_('bal'); ?>: KSH.

          <?php
              echo floor($checkUser['0']['balance']);
          ?>
          </span>
      {% endif %}
      </div>

      <div class="col-xs-4">
        <div class="pull-right">

        <a href="{{url('betmobile')}}" class="mobi betslip-mobi url-link" title="Bet Palace Betslip">
          <?php echo $t->_('betslip'); ?> <span class="shop-slip-counter"><?= $shopSlipCount ?></span></a>
       </div>
      </div>

      <div class="col-md-4 hidden-xs">
        
        <div style="font-size: 26px !important;
                    color: #ffd632;
                    padding-top: 22px;
                    font-weight: 500;">

          Welcome to Bet Palace Sport Book
        </div>
      </div>
    
      <div class="col-md-4 col-xs-5 hidden-overflow pull-right pt-18" id="navbar-collapse-main">

        {% if session.get('auth') != null %} {# variable is not set #}
        
        <div class="og ale ss profile web-element">
          <i class="fa fa-user" aria-hidden="true"></i> {{session.get('auth')['mobile']}} <br> <?php echo $t->_('balance'); ?>: KShs.

          <?php
               if(isset($checkUser[0])){
               echo number_format(floor($checkUser[0]['balance']));
               }

          ?>
           <br/>
           <a href="{{url('/shop/logout')}}"> <?php echo $t->_('logout'); ?><i class="fa fa-sign-out" aria-hidden="true"></i> </a>
        </div>
        {% else %} {# variable is set #}

          <?php echo $this->tag->form(["shop/authenticate","class"=>"ow og i web-element"]); ?>

          <div class="col-sm-5 col-sm-padding">
            <input type="text" name="mobile" class="form-control phone-txt" data-action="grow" placeholder="<?php echo $t->_('phone_number'); ?>..."> 
            <div class="col-sm-12 nopadding">
              <div class="remember_me">
                <input type="checkbox" name="remember" value="1" />
              </div>
              <div class="remember_me">
                <label><?php echo $t->_('remember_me'); ?></label>
              </div>
            </div>
          </div>
          <div class="col-sm-5 col-sm-padding">
            <div class="col-sm-12 nopadding">
            </div>
            <input type="password" name="password" class="form-control phone-txt" data-action="grow" placeholder="<?php echo $t->_('password'); ?>..."><br/>
            <input type="hidden" name="ref" value="{{refURL}}">
          </div>
          <div class="col-sm-2 col-sm-padding">
              <button class="cg fp login-butt"><?php echo $t->_('login'); ?></button>
            </form>
          </div>
        {% endif %}

      </div>

  </div>
</nav>

<nav class="second-nav ck pc os app-navbar ">
  <div class="by f collapse " id="navbar-collapse-main">

        <ul class="nav navbar-nav og ale ss">
        <li>
            <a class="cg fm ox anl url-link not-selectable" href="{{url('/shop')}}" data-original-title="" title="Bet Palace Home">
              <?php echo $t->_('home'); ?>
            </a>
        </li> 

        {% if session.get('auth') != null %} {# variable is not set #}
        <li>
         <a class="url-link" href="{{url('/shop/mybets')}}" title="Bet Palace My Shop bets"> My Shop Bets </a>
        </li>
        <li>
         <a class="url-link" href="{{url('/shop/withdraw')}}" title="Withdraw"> Withdraw </a>
        </li>
        <li>
         <a class="url-link" href="{{url('/shop/pnl')}}" title="P&L Report"> P&L Report </a>
        </li>
        {% endif %}  

        <li>
          <a class="url-link" href="{{url('/shop/fixtures')}}" target="_blank" title="Print Today's Fixtures">
              Print Today's Fixtures
          </a>
        </li>

        </ul>

        <ul class="nav navbar-nav st su sv">

        </ul>

        <ul class="nav navbar-nav hidden">

        </ul>

      </div>
      <div class="mobi">

        {% if session.get('auth') != null %} {# variable is not set #}
        <ul class="nav in">
        <li>
         <a class="url-link" href="{{url('/shop/mybets')}}" title="My Shop Bets"><?php echo $t->_('my-bets'); ?></a>
         </li>
          <li>
         <a class="url-link" href="{{url('/shop/logout')}}" title="Bet Palace Logout"><?php echo $t->_('logout'); ?></a>
         </li>
         </ul>
          {% else %} {# variable is set #}
          <ul class="nav men top-mobi-menu">
            <li class="ml-mobi">
              <a data-toggle="modal" href="{{url('/shop/login')}}" title="Bet Palace login"><?php echo $t->_('login'); ?></a>
            </li>
          </ul>
          {% endif %}

        {% if session.get('auth') != null %}
        <div class="col-xs-12 pad-0">
        {% else %}
        <div class="col-xs-9 pad-0">
        {% endif %}

        <ul class="main-nav">

          <li>
            <a class="url-link-not-selectable" href="{{url('/shop')}}"><?php echo $t->_('home'); ?> |</a>
          </li>
         
        </ul>
      </div>

      </div>

</nav>

<div style="padding-top:19px" class="web-element">
  <div class="slim-banner" style="">
        <div id="main-carousel" class="carousel slide carousel-fade" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="flash-banner" src="/img/jackpot.png" alt="" height="120px">
            </div>
          </div>

        </div>
  </div>
</div>

</div>

<div class="by amt">
  <div class="gc">
{{ partial("partials/shop/sidebar") }}
<div class="gz home">