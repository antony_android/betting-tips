<style>

	.paybet-btn{

		width: 60%;
	    border: none;
	    padding: 2px;
	    background: #090909 !important;
	    text-transform: none;
	    color: #fff;
	    font-size: 14px;
	    font-weight: normal;
	    height: 29px;
	}

	.no-padding{
		padding:0px;
	}
</style>

<div class="">
<div class="panel-header mybets-bg">
  <h2><a href="{{url('/shop/mybets')}}"><i class="fa fa-chevron-left" aria-hidden="true"></i></a> Bet Details</h2>
</div>

<?php 
$created = strtotime($myBet['created']);
$nowT = date("Y-m-d H:i:s");
$nowTime = strtotime($nowT);
$interval  = abs($nowTime - $created);
$minutes   = round($interval / 60);
?>

<div class="table-responsive inner-content whitebg nopadding"> 
{{ this.flashSession.output() }}
<table class="table basic-table mybets"> 
	<thead class="black-bg"> 
	<tr> <th>Bet ID</th> <th>Time</th> <th>Amount</th> <th>Possible Win</th> <th>Status</th> </tr> 
	</thead> 
	<tbody>
	<tr> 
		  <a href="#"><td><?= $myBet['bet_id'] ?></td></a>
		  
		  <td><?= date('d/m H:i', strtotime($myBet['created'])) ?></td> 
		  <td><?= $myBet['bet_amount'] ?></td> 
		  <td><?= $myBet['is_void'] ? $myBet['revised_possible_win'] : $myBet['possible_win'] ?></td>
		  <td>
		   	<?php
		   if($myBet['status']==1){
			echo 'Open';
   		    }elseif($myBet['status']==5){

   		    	if($myBet['sp_status']==50){
			    	echo 'PAID';
			    }else{
			    	echo '<div class="col-sm-3 no-padding">';
	                echo 'Won'; 
	                echo '</div>';
	                echo '<div class="col-sm-8 no-padding">';
	                echo '<button class="paybet-btn" onClick="payShopBet('.$myBet['bet_id'].')">
	                Pay Bet</button>';
	                echo '</div>';
			    }
		   }elseif($myBet['status']==3){
			echo 'Lost';
		   }elseif($myBet['status']==4){
			echo 'Cancelled';
		   }elseif($myBet['status']==9){
			echo 'Jackpot';
		   }elseif($myBet['status']==24){
			echo 'Cancelled';
		   }else{
			echo 'View';
		   }
		   ?>
		   </td> 
	</tr> 
	</tbody> 
	</table>
</div>
	<div class="col-sm-12 nopadding">
		<?php if($minutes < 10 && $myBet['status'] != 24): ?>
		<a class="cancelb" href="{{url('/shop/cancelbet?id=')}}{{myBet['bet_id']}}"> Cancel Bet </a>
		<?php endif; ?>
	</div>

	<div class="table-responsive whitebg "> 

	<table class="table mybets"> 
	<thead class="table-h panel-header"> 
	<tr> <th>Name</th><th>Start Time</th><th>Odds</th> <th>Type</th><th>Mkt</th> <th>Pick</th> <th>Outcome</th> <th>Results</th> </tr> 
	</thead> 
	<tbody> 
	<?php foreach($betDetails as $bet): ?>
	<tr> 
		  <td class="web-element"><?= $bet['home_team']." v ".$bet['away_team'] ?></td> 
		  <td class="mobi"><?= $bet['home_team']." <br/> ".$bet['away_team'] ?></td>
		  <td><?= $bet['start_time'] ?></td> 
		  <td><?= $bet['odd_value'] ?></td> 
		  <td>
			  <?php if($bet['live_bet']==1){
			  	echo '<span style="color:red">LIVE</span>';
			  }else{
			  	echo '<span>PREMATCH</span>';
			  }?>
		  </td>
		  <td><?= $bet['bet_type'] ?></td> 
		  <td><?= $bet['bet_pick'] ?></td> 
		  <td><?php
		  		if($bet['void_factor']){
		  			echo "Void";
		  		}
				elseif(empty($bet['winning_outcome']))
					echo "Open";
				else
					echo $bet['winning_outcome'];

		 ?></td> 
		 <td><?= $bet['ft_score'] ?></td> 
	</tr> 
	<?php endforeach; ?>
	</tbody> 
	</table> 
	</div>
</div>
