<style type="text/css">
  img {
      vertical-align: middle;
      max-width: 60%;
  }

  td {
    border: 1px solid;
  }

</style>

<div class="row pc header-pd">

    <div class="col-md-3 col-xs-3 hidden-overflow">

      <div>
          <a class="e logo" href="{{url('/shop/matches')}}" title="Bet Palace">
          <img src="{{url('/img/logo_black.png')}}" alt="Bet Palace" title="Bet Palace">
          </a>
      </div>

    </div>
    
    <div class="hidden-md col-xs-5">
      <span class="mobi balnc" href="">

      </span>
    </div>

    <div class="col-xs-4">
        <div class="pull-right">

        <a href="{{url('betmobile')}}" class="mobi betslip-mobi url-link" title="Bet Palace Betslip">
          <span class="shop-slip-counter"></span></a>
       </div>
    </div>

      <div class="col-md-4 hidden-xs">
        
        <div style="font-size: 26px !important;
                    color: #ffd632;
                    padding-top: 22px;
                    font-weight: 500;">

          Bet Palace Sport Book Fixtures
        </div>
      </div>
    
      <div class="col-md-4 col-xs-5 hidden-overflow pull-right pt-18" id="navbar-collapse-main">
        
        <div class="og ale ss profile web-element">
           <span style="font-size: 14px;">
             <?php echo date("d-m-Y") ?>
           </span>
           <br/>
        </div>

      </div>

</div>

<div class="row">
  <table style="border: 1px solid;font-size: 11px;">
    <tr style="font-weight: bold;background: #444;color: #eee;">
      <td>Game ID</td>
      <td>Game</td>
      <td>Comp.</td>
      <td>Country</td>
      <td>Kick Off</td>
      <td>1</td>
      <td>X</td>
      <td>2</td>
      <td>GG</td>
      <td>NG</td>
      <td>OV 2.5</td>
      <td>UN 2.5</td>
      <!--
        <td>1X</td>
        <td>X2</td>
        <td>12</td>
      -->
    </tr>
    <?php foreach($fixturesDetails as $detail): ?>
        <tr>
            <td style="padding:1px;">
               <?php echo $detail["game_id"]; ?>
            </td>
            <td style="padding:1px;">
               <?php echo $detail["GAME"]; ?>
            </td>
            <td style="padding:1px;">
               <?php echo $detail["COMPETITION"]; ?>
            </td>
            <td style="padding:1px;">
               <?php echo $detail["COUNTRY"]; ?>
            </td>
            <td style="padding:1px;">
               <?php echo $detail["KICK_OFF"]; ?>
            </td>
            <td style="padding:1px;">
               <?php echo $detail["HOME"]; ?>
            </td>
            <td style="padding:1px;">
               <?php echo $detail["DRAW"]; ?>
            </td>
            <td style="padding:1px;">
               <?php echo $detail["AWAY_ODDS"]; ?>
            </td>
            <td style="padding:1px;">
               <?php echo $detail["GG"]; ?>
            </td>
            <td style="padding:1px;">
               <?php echo $detail["NG"]; ?>
            </td>
            <td style="padding:1px;">
               <?php echo $detail["OVER_25"]; ?>
            </td>
            <td style="padding:1px;">
               <?php echo $detail["UNDER_25"]; ?>
            </td>
            <!--
              <td style="padding:1px;">
                 <?php echo $detail["DC1X"]; ?>
              </td>
              <td style="padding:1px;">
                 <?php echo $detail["DCX2"]; ?>
              </td>
              <td style="padding:1px;">
                 <?php echo $detail["DC12"]; ?>
              </td>
            -->
        </tr>
    <?php endforeach; ?>
  </table>
</div>