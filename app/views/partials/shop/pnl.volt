<div class="">
<div class="row panel-header mybets-bg">
  <div class="col-sm-8">
  	<h2>P&L Report</h2>
  </div>
  <div class="col-sm-8">
  	<form name="search" method="post" action="/shop/pnl">
	    <div class="col-sm-4 nopadding submit-input">
	        <input type="text" name="date_from" class="form-control" data-action="grow"
	            placeholder="From">
	    </div>
	    <div class="col-sm-4 nopadding submit-input">
	        <input type="text" name="date_to" class="form-control" data-action="grow"
	            placeholder="To">
	    </div>
	    <div class="col-sm-4 nopadding search-button">
	        <button type="submit" class="search-btn">View Report</button>
	    </div>
    </form>
  </div>
</div>
<div class="table-responsive inner-content whitebg"> 
	{{ this.flashSession.output() }}
	<table class="table mybets"> 
	<thead class="panel-header"> 
	<tr> <th>Date</th> <th>No. of Bets</th> <th>Bet Amount</th>
	<th>Winnings</th><th>P&L</th> <th>% Resulted</th> </tr> 
	</thead> 
	<tbody> 
	<?php if(count($pnl) > 0): ?>
	<?php foreach($pnl as $detail): ?>
	<tr> 
		<td>
			<?php echo $detail['dc']; ?>
		</td>
		<td>
			<?php echo $detail['num']; ?>
		</td>
		<td>
			<?php echo $detail['bet_amount']; ?>
		</td> 
		<td>
			<?php echo $detail['won_possible_win']; ?>
		</td> 
		<td>
			<?php echo $detail['pnl']; ?>
		</td> 
		<td>
			<?php echo $detail['centage']; ?>
		</td> 
	</tr> 
	<?php endforeach; ?>
	<?php endif; ?>
	</tbody> 
	</table> 
   </div>
</div>