{#<div id="fb-root"></div>#}

<div class="web-element">

    <!-- {{ image("img/banner.png") }}  -->
</div>


<div class="homepage block-shadow">
   
      <div class="col-sm-12 top-matches hd">
        <div class="h">

            <!-- tabs menu -->
            
            <div class="ss col-sm-8 nopadding">
                <h3 class="competition"><?php echo $topSports[$sportId];?> &raquo;&#x000BB;&#187;</h3> 
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item active">
                        <a class="nav-link " data-toggle="tab" href="#panel0" role="tab" 
                        data-url="{{ url('tab') }}?t=0&sp={{sportId}}" >Highlights</a>
                    </li>

                    <!--
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#panel1" role="tab" 
                        data-url="{{ url('tab') }}?t=1&sp={{sportId}}">
                        <?php
                            echo 'Today';
                        ?>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#panel2" role="tab" 
                        data-url="{{ url('tab') }}?t=2&sp={{spo3rtId}}">
                        <?php
                           echo 'Tomorrow';
                        ?>
                        </a>
                    </li>
                    -->
                   
                </ul> 

            </div> 

        <div class="ss col-sm-4 nopadding web-element">
         <form name="search" method="post" action="/shop/matches?sp={{sportId}}">
            <div class="col-sm-8 nopadding submit-input">
                <input type="text" name="keyword" class="form-control" data-action="grow"
                                         placeholder="Your search criteria" value="{{keyword}}">
            </div>
            <div class="col-sm-4 nopadding search-button">
                <button type="submit" class="search-btn">Search</button>
            </div>
            </form>
        </div>
      </div> <!-- end class h -->
        
    </div> <!-- end top matches -->

    <!-- Tab panels -->
    <div class="tab-content">
        <!--Panel 1-->
        <div class="tab-pane active" id="panel0" >
            {{ partial("partials/shop/matches-list") }}
        </div>
         <!--/.Panel 1 -->
        <!--Panel 2-->
        <div class="tab-pane" id="panel1" >Loading ...
            
        </div>
        <!--/.Panel 2-->
        <!--Panel 3-->
        <div class="tab-pane " id="panel2">Loading ...
        </div>
        <!--/.Panel 3-->
    </div> <!-- end tab content -->

</div> <!-- end home page -->