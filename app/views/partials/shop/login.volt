<div class="panel-header">
	<h2>Login to SportBook</h2>
</div>
<div class="inner-content whitebg">


        {{ this.flashSession.output() }}

<?php echo $this->tag->form("shop/authenticate"); ?>

 <p>
    <label>Cashier Number *</label>
    <?php echo $this->tag->textField(["mobile","class"=>"form-control msisdn-f"]) ?>
 </p>

 <p>
    <label>Password *</label>
    <?php echo $this->tag->passwordField(["password","name"=>"password","class"=>"form-control","placeholder"=>"Password"]) ?>
 </p>

  <p>
    <?php echo $this->tag->submitButton(["Login","class"=>"cg fm"]) ?> 
 </p>

</form>
</div>