<div class="">
<div class="panel-header">
  <h2><a href="{{url('/shop/mybets')}}"><i class="fa fa-chevron-left" aria-hidden="true"></i></a> Bet Details</h2>
</div>
<div class="table-responsive"> <table class="table white-color"> 
	<thead> 
	<tr> <th>ID</th> <th>Type Bet</th> <th>Date & Time</th> <th>Description</th> <th>Bet Amount</th> <th>Possible Win</th> <th>Status</th> </tr> 
	</thead> 
	<tbody> 
	<tr class="cancel-header"> 
		  <a href="#"><td><?= $myBet['bet_id'] ?></td></a>
		  <td><?php 
		  if($myBet['total_matches']>1){
			echo "Multi Bet";
		  }else{
		  	echo "Single Bet";
		  }
		  ?></td> 
		  <td><?= date('d/m H:i', strtotime($myBet['created'])) ?></td> 
		  <td><?= $myBet['bet_message'] ?></td> 
		  <td><?= $myBet['bet_amount'] ?></td> 
		  <td><?= $myBet['possible_win'] ?></td> 
		  <td>
		   	<?php
		   if($myBet['status']==1){
			echo 'Pending results';
   		    }elseif($myBet['status']==5){
                if($bet['sp_status']==50){
			    	echo 'Paid';
			    }else{
			    	echo 'Won';
			    }
		   }elseif($myBet['status']==3){
			echo 'Lost';
		   }elseif($myBet['status']==4){
			echo 'Cancelled';
		   }elseif($myBet['status']==9){
			echo 'Pending Jackpot';
		   }elseif($myBet['status']==24){
			echo 'Bet cancelled';
		   }else{
			echo 'View';
		   }
		   ?>
		   </td> 
	</tr> 
	</tbody> 
	</table> </div>
	<div class="col-sm-12">
	<h4 class="text-center">Are you sure you want to cancel this bet?</h4>
	<a class="cancel" href="{{url('/shop/mybets')}}"> No </a>
	<?php echo $this->tag->form("/shop/cancel"); ?>
	<input type="hidden" name="bet_id" value="{{myBet['bet_id']}}" />
	 <button class="cancel" href="{{url('cancel')}}"> Yes </button> 
	</form>
	</div>
</div>