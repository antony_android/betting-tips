<div class="foot-wrapper">
    <div class="row footer-container">
        <div class="col-md-3">
            <span class="white-titles">BETPALACE</span>
            <div style="width:100%; border-bottom:1px solid #acacac"></div>
            <span class="white-p" > Need Help? Contact us <br> 
            <div>
                0705 290085, 0739 290085</a><!--, 0777 290085-->
            </div>
            <div class="">
                <img src="/img/whatsapp.png" width="30px"/>
                <span>0797 290 087</span>
            </div>
            <span style="font-weight:bold;"> support@betpalace.co.ke</span>
                </span><br>
                <span><a href="https://www.facebook.com/BetPalace-471643323624937/">
                <img height="24px" class="foot-icons" src="/svg/facebook-logo.svg" alt=""></a><a href="https://twitter.com/BetPalaceKE"><img height="24px" class="foot-icons" src="/svg/twitter-logo-on-black-background.svg" alt=""></a><a href=""><img height="24px" class="foot-icons" src="/svg/instagram.svg" alt=""></a></span>
        </div>
        <div class="col-md-6">
                <span class="white-titles">RESPONSIBLE GAMING</span>
                <div style="width:100%; border-bottom:1px solid #acacac"></div>
                <span class="white-p">
                        Betting may be addictive and dangerous if not taken in moderation. Please note that only persons above the legal age limit of 18 years are allowed to gamble. Bet responsibly!
                    </span>
        </div>
        <div class="col-md-3">
                <span class="white-titles">LICENCE</span>
                <div style="width:100%; border-bottom:1px solid #acacac"></div>
                <span class="white-p">Bet Palace Ltd, the copyright owner of this website, is licensed by BCLB (Betting Control and Licensing Board of Kenya) under the Betting, Lotteries and Gaming Act, Cap 131, Laws of Kenya under License number: 0000136.
                    </span>
        </div>
    </div>
</div>
<div class="copyright-section">
    <span class="copyright-section-span">
        &copy;<?php echo date("Y"); ?> BetPalace. All rights Reserved.
    </span>
</div>