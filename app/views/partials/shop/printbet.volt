<div style="padding: 10px;">
    <table style="border: none;">
        <tbody>
        <tr>
            <td>
                <img src="{{url('/img/logo_black.png')}}" alt="Bet Palace" width="200px" title="Bet Palace">
            </td>
        </tr>
        <tr>
            <td style="font-weight: 500;font-size: 14px;padding: 15px;">
             BET ID: <?php echo $bet_id; ?>
            </td>
        </tr>
        <tr>
            <td>
                =========================================
            </td>
        </tr>

        <?php $count = 1; ?>
        <?php $slipSize = count($betslipDetails); ?>

        <?php foreach($betslipDetails as $slipDetail): ?>
            <tr>
                <td style="padding: 1px;">
                    <span style="font-weight: bold;">Game </span>: <?php echo $slipDetail["home_team"]." vs ".$slipDetail["away_team"]; ?><br/>
                    <span style="font-weight: bold;">Pick </span>: &nbsp;&nbsp;&nbsp;<?php echo $slipDetail["bet_pick"]; ?><br/>
                    <span style="font-weight: bold;">Odds </span>: &nbsp;<?php echo $slipDetail["odd_value"]; ?>
                </td>
            </tr>
            <?php if($count != $slipSize): ?>
            <tr>
                <td>
                    -----------------------------------------------------------------------------
                </td>
            </tr>
            <?php $count++; ?>
        <?php endif; ?>

        <?php endforeach; ?>

        <?php foreach($betDetails as $detail): ?>
        <tr>
            <td>
                =========================================
            </td>
        </tr>
        <tr>
            <td colspan="3" style="padding: 5px;">
                <span style="font-weight: bold;">Bet Amount</span>: 
                KShs. <?php echo $detail["bet_amount"]; ?>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="padding: 5px;">
                <span style="font-weight: bold;">Total Odds</span>: 
                 <?php echo $detail["total_odd"]; ?>
            </td>
        </tr>
        
        <!--<tr>
            <td style="padding: 5px;">
                <span style="font-weight: bold;">Possible Win</span>: 
                KShs. <?php round($detail["raw_possible_win"]); ?>
            </td>
        </tr>
        -->
        <tr>
            <td colspan="3" style="padding: 5px;">
                <span style="font-weight: bold;">Tax</span>: 
                KShs. <?php echo round($detail["tax"]); ?>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="padding: 5px;">
                <span style="font-weight: bold;">Net Possible Win</span>: 
                KShs. <?php echo round($detail["possible_win"]); ?>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                ------------------------------------------------------------------------------
            </td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="3" style="padding: 5px;">
                <span>Bet Palace Limited</span><br/>
                <span>P.O. BOX 388-00507</span><br/>
                <span>Nairobi, Kenya</span><br/>
                <span>Tel: (+254) 705290085</span><br/>
                <span>Website: www.betpalace.co.ke</span><br/>
                <span>Email: info@betpalace.co.ke</span>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                ------------------------------------------------------------------------------
            </td>
        </tr>
        <tr>
            <td colspan="3" style="padding: 5px;">
                <span>SMS "JOIN" to 29085 to Register</span><br/>
                <span>MPESA Paybill No: 290085</span><br/>
            </td>
        </tr>
        <tr>
            <td>
                =========================================
            </td>
        </tr>
        </tbody>
    </table>
</div>