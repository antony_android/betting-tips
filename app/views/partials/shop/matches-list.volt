<?php
if (!function_exists('clean'))  
{ 
 function clean($string) {
        $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }
}
?>

<div class="matches full-width match-list-games">
    <?= empty($today)? '<div class="empty-list">
    No events found for selected category. Check different category to find more sports</div>':''?>

    <?php foreach($today as $key => $group):?>
    <header class="col-sm-12 top-matches match-header events-header web-element">
        <div class="col-sm-4 left-text p-0 date-header">
            <?php echo $key; ?>        
        </div>
        <div class="col-sm-2 mkts-desc-head mgr-5 p-0">3 WAY
        </div>
        <div class="col-sm-2 mkts-desc-head mgr-5 p-0">DOUBLE CHANCE</div>
        <div class="col-sm-2 mkts-desc-head over-under p-0">OVER/UNDER 2.5</div>
        <div class="col-sm-0 p-0">&nbsp;</div>
        <!-- markets description -->
        <div class="col-sm-4 left-text">&nbsp;</div>
        <div class="col-sm-2 mkts-desc mgr-5 p-0">
            <div class="col-sm-4 p-0">HOME</div>
            <div class="col-sm-4 p-0">DRAW</div>
            <div class="col-sm-4 p-0">AWAY</div>
        </div>
        <div class="col-sm-2 mkts-desc mgr-5 p-0">
            <div class="col-sm-4 p-0">1 OR X</div>
            <div class="col-sm-4 p-0">X OR 2</div>
            <div class="col-sm-4 p-0">1 OR 2</div>
        </div>
        <div class="col-sm-2 mkts-desc over-under mgr-5 p-0">
            <div class="col-sm-6 p-0">OVER</div>
            <div class="col-sm-6 p-0">UNDER</div>
        </div>
        <div class="col-sm-1 p-0 bets-zaidi">More</div>
        <!-- end markets description -->
    </header>

    <div class="middle-content">
        <?php foreach($group as $day): ?>
        <?php
        $theMatch = @$theBetslip[$day['match_id']];
        ?>
        <div class="col-sm-12 top-matches hidden-overflow">

            <div class="col-sm-4 col-xs-11 p-0">
                <div class="compt-detail mobile-view"> 
                    GAME ID: <span class="game-id"> <?php echo $day['game_id']; ?> </span><?php echo $day["category"].' '.$day["competition_name"]; ?> |  <?php echo date('d-m-Y H:i', strtotime($day['start_time'])); ?>
                </div>
                <div class="compt-detail web-element p-3-left">
                    <div class="col-sm-12 p-0"><?php echo $day["category"].' '.$day["competition_name"]; ?></div>
                    <div class="col-sm-3 p-0">ID: <span class="game-id"> <?php echo $day['game_id']; ?></div>
                    <div class="col-sm-9 web-games p-0"><?php echo $day['home_team']; ?></div>
                    <div class="col-sm-12 pt-5"></div>
                    <div class="col-sm-3 p-0"><?php echo date('H:i', strtotime($day['start_time'])); ?></div>
                    <div class="col-sm-9 web-games p-0"><?php echo $day['away_team']; ?></div>
                </div>
                <div class="compt-teams hidden-lg">
                    <?php echo $day['home_team']; ?> 
                    <span class="mobile-view">VS</span>
                    <?php echo $day['away_team']; ?>
                </div>
            </div>
             <div class="col-xs-1 events-odd pad
                <?php
                    if($theMatch && $theMatch['sub_type_id']!=1){
                        echo ' picked';
                    }
                ?>
                mobile-view">

                <a class="side" href="<?php echo 'match?id='.$day['match_id']; ?>">+<?php echo $day['side_bets']; ?> </a>
            </div>

            <div class="col-sm-2 col-xs-12 match-div-col mgr-5  p-0" >

                    <div class="col-sm-4 col-xs-4 p-0 match-left-col">
                        <?php if($day['home_odd'] > 0): ?>
                        <button class="home-team <?php echo $day['match_id']; ?> <?php
                            echo clean($day['match_id'].$day['sub_type_id'].$day['home_team']);
                            if($theMatch['bet_pick']==$day['home_team'] && $theMatch['sub_type_id']=='1'){
                            echo ' picked';
                        }
                        ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="1x2" bettype='prematch'
                        awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['home_odd']; ?>"
                        target="javascript:;" odd-key="<?php echo $day['home_team']; ?>" parentmatchid="<?php echo $day['parent_match_id']; ?>"
                        id="<?php echo $day['match_id']; ?>"
                        custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['home_team']); ?>" value="1" special-value-value="0"
                        onClick="addShopBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))">
                        <span class="odds-btn-label mobile-view">HOME<br/></span>
                        <span class="theodds">
                        <?php echo $day['home_odd']; ?>
                            
                        </span></button>
                        <?php endif ?>
                    </div>

                    <div class="col-sm-4 col-xs-4 events-odd match-div-col match-mid-col" 
                    style="">
                    <?php if($day['neutral_odd'] > 0): ?>
                    <button class="draw <?php echo $day['match_id']; ?> <?php
                        echo clean($day['match_id'].$day['sub_type_id']."draw
                        ");
                        if($theMatch['bet_pick']=='draw' && $theMatch['sub_type_id']=='1'){
                        echo ' picked';
                    }
                    ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="1x2" bettype='prematch'
                    awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['neutral_odd']; ?>"
                    custom="<?php echo clean($day['match_id'].$day['sub_type_id']."draw"); ?>" value="1" odd-key="draw"
                    target="javascript:;" parentmatchid="<?php echo $day['parent_match_id']; ?>"
                    id="<?php echo $day['match_id']; ?>" special-value-value="0"
                    onClick="addShopBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))"><span
                    class="label label-inverse"> 
                    <span class="odds-btn-label mobile-view">DRAW<br/></span>
                    <?php echo $day['neutral_odd']; ?></span></button>
                    <?php endif ?>
                </div>
                <div class="col-sm-4 col-xs-4 match-div-col match-right-col" style="padding:0;">
                    <?php if($day['away_odd'] > 0): ?>
                    <button class="away-team <?php echo $day['match_id']; ?> <?php echo clean($day['match_id'].$day['sub_type_id'].$day['away_team']);
                        if($theMatch['bet_pick']==$day['away_team'] && $theMatch['sub_type_id']=='1'){
                        echo ' picked';
                    }
                    ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="1x2" bettype='prematch'
                    awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['away_odd']; ?>" value="1"
                    custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['away_team']); ?>" odd-key="<?php echo $day['away_team']; ?>"
                    target="javascript:;" parentmatchid="<?php echo $day['parent_match_id']; ?>"
                    id="<?php echo $day['match_id']; ?>" special-value-value="0"
                    onClick="addShopBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))">
                    <span class="odds-btn-label mobile-view">AWAY<br/></span>
                    <span class="theodds"> <?php echo $day['away_odd']; ?> </span></button>
                    <?php endif ?>
                </div>
            </div> 
            <!-- end 3 way buttons -->

            <div class="col-sm-2 col-xs-12 match-div-col mgr-5  p-0" >
                <div class="col-sm-4 col-xs-4 p-0 match-left-col">
                    <?php if($day['double_chance_1x_odd'] > 0): ?>
                    <button class="home-team <?php echo $day['match_id']; ?> <?php
                        echo clean($day['match_id'].$day['sub_type_id'].$day['home_team'].' or draw');
                        if($theMatch['bet_pick']==$day['home_team'].' or draw' && $theMatch['sub_type_id']=='10'){
                        echo ' picked';
                    }
                    ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="Double Chance" bettype='prematch'
                    awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['double_chance_1x_odd']; ?>"
                    target="javascript:;" odd-key="<?php echo $day['home_team']. ' or draw'; ?>" parentmatchid="<?php echo $day['parent_match_id']; ?>"
                    id="<?php echo $day['match_id']; ?>"
                    custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['home_team']. ' or draw'); ?>" value="10" special-value-value="0"
                    onClick="addShopBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))">
                    <span class="odds-btn-label mobile-view">1 OR X<br/></span>
                    <span class="theodds"><?php echo $day['double_chance_1x_odd']; ?></span></button>
                    <?php endif ?>
                </div>

                <div class="col-sm-4 col-xs-4 events-odd match-div-col match-mid-col" style="">
                    <?php if($day['double_chance_x2_odd'] > 0): ?>
                    <button class="draw <?php echo $day['match_id']; ?> <?php
                        echo clean($day['match_id'].$day['sub_type_id']."draw or 
                        ".$day['away_team']);
                        if($theMatch['bet_pick']=='draw or '.$day['away_team'] && $theMatch['sub_type_id']=='10'){
                        echo ' picked';
                    }
                    ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="Double Chance" bettype='prematch'
                    awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['double_chance_x2_odd']; ?>"
                    custom="<?php echo clean($day['match_id'].$day['sub_type_id']."draw or ".$day['away_team']); ?>" value="10" odd-key="<?= 'draw or '.$day['away_team']; ?>"
                    target="javascript:;" parentmatchid="<?php echo $day['parent_match_id']; ?>"
                    id="<?php echo $day['match_id']; ?>" special-value-value="0"
                    onClick="addShopBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))">  
                    <span class="odds-btn-label mobile-view">X OR 2<br/></span>
                    <span class="label label-inverse"> <?php echo $day['double_chance_x2_odd']; ?></span></button>
                    <?php endif ?>
                </div>
                <div class="col-sm-4 col-xs-4 match-div-col match-right-col" style="padding:0;">
                    <?php if($day['double_chance_12_odd'] > 0): ?>
                    <button class="away-team <?php echo $day['match_id']; ?> <?php echo clean($day['match_id'].$day['sub_type_id'].$day['home_team'] .' or '.$day['away_team']);
                        if($theMatch['bet_pick']==$day['home_team'].' or '.$day['away_team'] && $theMatch['sub_type_id']=='10'){
                        echo ' picked';
                    }
                    ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="Double Chance" bettype='prematch'
                    awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['double_chance_12_odd']; ?>" value="10"
                    custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['home_team'] .' or '. $day['away_team']); ?>" odd-key="<?php echo $day['home_team'] .' or '. $day['away_team']; ?>"
                    target="javascript:;" parentmatchid="<?php echo $day['parent_match_id']; ?>"
                    id="<?php echo $day['match_id']; ?>" special-value-value="0"
                    onClick="addShopBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))">
                    <span class="odds-btn-label mobile-view">1 OR 2<br/></span>
                    <span class="theodds"> <?php echo $day['double_chance_12_odd']; ?> </span></button>
                    <?php endif ?>
                </div>
            </div> <!-- end Double chance buttons -->

            <div class="col-sm-2 col-xs-12 match-div-col over-under mgr-5  p-0" >
                <div class="col-sm-6 col-xs-6 p-0 match-left-col">
                    <?php if($day['over_25_odd'] > 0): ?>
                    <button class="home-team <?php echo $day['match_id']; ?> <?php
                        echo clean($day['match_id'].$day['sub_type_id'].'over 2.5');
                        if($theMatch['bet_pick']=='over 2.5' && $theMatch['sub_type_id']=='18'){
                        echo ' picked';
                    }
                    ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="Totals Over/Under" bettype='prematch'
                    awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['over_25_odd']; ?>"
                    target="javascript:;" odd-key="<?php echo 'over 2.5'; ?>" parentmatchid="<?php echo $day['parent_match_id']; ?>"
                    id="<?php echo $day['match_id']; ?>"
                    custom="<?php echo clean($day['match_id'].$day['sub_type_id'].'over 2.5'); ?>" value="18" special-value-value="2.5"
                    onClick="addShopBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))">
                    <span class="odds-btn-label mobile-view">OVER 2.5<br/></span>
                    <span class="theodds"><?php echo $day['over_25_odd']; ?></span></button>
                    <?php endif ?>
                </div>

                <div class="col-sm-6 col-xs-6 events-odd match-div-col match-right-col" style="padding:0;">
                    <?php if($day['under_25_odd'] > 0): ?>
                    <button class="draw <?php echo $day['match_id']; ?> <?php
                        echo clean($day['match_id'].$day['sub_type_id']."under 2.5");
                        if($theMatch['bet_pick']=='under 2.5' && $theMatch['sub_type_id']=='18'){
                        echo ' picked';
                    }
                    ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="Totals Over/Under" bettype='prematch'
                    awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['under_25_odd']; ?>"
                    custom="<?php echo clean($day['match_id'].$day['sub_type_id']."under 2.5"); ?>" value="18" odd-key="under 2.5"
                    target="javascript:;" parentmatchid="<?php echo $day['parent_match_id']; ?>"
                    id="<?php echo $day['match_id']; ?>" special-value-value="2.5"
                    onClick="addShopBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))">
                    <span class="odds-btn-label mobile-view">UNDER 2.5<br/></span>
                    <span class="label label-inverse"> <?php echo $day['under_25_odd']; ?></span></button>
                    <?php endif ?>
                </div>

            </div> <!-- end over under buttons -->

            <div class="col-sm-1 events-odd pad
                <?php
                    if($theMatch && $theMatch['sub_type_id']!=1){
                        echo ' picked';
                    }
                ?>
                web-element">

                <a class="side" href="<?php echo '/shop/match?id='.$day['match_id']; ?>">+<?php echo $day['side_bets']; ?> </a>
            </div>
    </div>

    <?php endforeach; ?>

</div>

<?php endforeach; ?>

</div>