<?php
if (!function_exists('clean'))  
{ 
 function clean($string) {
        $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }
}
?>

<div class="odd-wrapper3">
    <div class="row inner-wr3">
        <div class="col-md-12">
            <h3>Get the FUN out of Betting - AUTOBET!</h3>
        </div>
    </div>
</div>

<div class="matches full-width match-list-games">
    <?= empty($games) && empty($lastGames)? '<div class="empty-list">
    No events found for selected category. Check different category to find more sports</div>':''?>

    <?= !empty($games)?
    '<header class="col-sm-12 match-header peer-header">
        <div class="col-sm-1 mkts-desc-head pl-10 web-element"></div>
        <div class="col-sm-2 mkts-desc-head pl-14 web-element">Competition</div>
        <div class="col-sm-5 mkts-desc-head pl-13 web-element">Game</div>
        <div class="col-sm-3 mkts-desc-head pl-9 web-element">Start Time</div>
        <div class="col-sm-1 auto-selection pl-5 web-element">AUTO</div>
    </header>':''?>

    <?= !empty($lastGames)?
    '<header class="col-sm-12 match-header peer-header">
        <div class="col-sm-1 mkts-desc-head pl-10 web-element"></div>
        <div class="col-sm-2 mkts-desc-head pl-14 web-element">Competition</div>
        <div class="col-sm-5 mkts-desc-head pl-13 web-element">Game</div>
        <div class="col-sm-3 mkts-desc-head pl-9 web-element">Start Time</div>
        <div class="col-sm-1 auto-selection pl-5 web-element">Result</div>
    </header>':''?>

    <?php if(!empty($games)): ?>
    <div class="middle-content matches-detail">
        <?php $count =0; ?>
        <?php foreach($games as $day): ?>
        <?php
        $theMatch = @$theBetslip[$day['match_id']];
        ?>
        <div class="col-sm-12 top-matches hidden-overflow">

            <div class="col-sm-1 mkts-desc-head p-0 web-element">
                <?php echo $day['pos']; ?>
            </div>

            <div class="col-md-2 mkts-desc-head p-0 web-element">
            <?php echo $day["competition_name"]; ?>
            </div>

            <div class="col-sm-5 mkts-desc-head p-0 web-element">
            <span class="compt-detail">GAME ID: </span><span class="class="game-id""><?php echo $day['game_id']; ?> </span><br/>
            <?php echo $day['home_team']; ?> <span class="vs-peer"> vs. </span> <?php echo $day['away_team']; ?>
            </div>

            <div class="col-md-3 mkts-desc-head p-0 web-element">
            <?php echo date('d-m-Y H:i', strtotime($day['start_time'])); ?>
            </div>

            <div class="col-xs-11 p-0 mobi">
                <div class="compt-detail"> 
                    GAME ID: <span class="game-id"> <?php echo $day['game_id']; ?> </span> |  <?php echo $day["competition_name"]; ?> |  <?php echo date('H:i', strtotime($day['start_time'])); ?>
                </div>
                <div class="compt-teams">
                    <?php echo $day['home_team']; ?> 
                    <div class="mobile-view"><span class="compt-detail">VS.</span></div>
                    <?php echo $day['away_team']; ?>
                </div>
            </div>
            <div class="col-xs-1 auto-pick-color auto-pick<?= $count;?>">-- </div>

            <div id="<?= 'game'.$count; ?>" class="hidden" 
                hometeam="<?php echo $day['home_team']; ?>" 
                oddtype="1x2" 
                bettype='autobet'
                awayteam="<?php echo $day['away_team']; ?>" 
                odd_key="<?php echo $day['odd_key']; ?>"
                home_odd="<?php echo $day['home_odd']; ?>"
                neutral_odd="<?php echo $day['neutral_odd']; ?>"
                away_odd="<?php echo $day['away_odd']; ?>"
                parentmatchid="<?php echo $day['parent_match_id']; ?>"
                match_id="<?php echo $day['match_id']; ?>"
                pos="<?php echo $day['pos']; ?>"
                jackpot_id="<?php echo $jackpotID; ?>"
                jackpot_type="3"
                value="1" special-value-value="0"
                     >
                        </div>

            <?php $count++; ?>
     </div>

    <?php endforeach; ?>

    <header class="col-sm-12 match-header peer-header">
        <div class="col-md-2 mkts-desc-head p-0 pull-right">
            <a class="invite-peer" href="#" onclick="autobet(event)">AUTO BET</a>
        </div>
     </header>

</div>

<?php elseif(!empty($lastGames)): ?>

<div class="middle-content matches-detail">
    <?php $count =0; ?>
    <?php foreach($lastGames as $day): ?>

    <div class="col-sm-12 top-matches hidden-overflow">

        <div class="col-sm-1 mkts-desc-head p-0 web-element">
            <?php echo $day['pos']; ?>
        </div>

        <div class="col-md-2 mkts-desc-head p-0 web-element">
        <?php echo $day["competition_name"]; ?>
        </div>

        <div class="col-sm-5 mkts-desc-head p-0 web-element">
        <span class="compt-detail">GAME ID: </span><span class="class="game-id""><?php echo $day['game_id']; ?> </span><br/>
        <?php echo $day['home_team']; ?> <span class="vs-peer"> vs. </span> <?php echo $day['away_team']; ?>
        </div>

        <div class="col-md-3 mkts-desc-head p-0 web-element">
        <?php echo date('d-m-Y H:i', strtotime($day['start_time'])); ?>
        </div>

        <div class="col-xs-11 p-0 mobi">
            <div class="compt-detail"> 
                GAME ID: <span class="game-id"> <?php echo $day['game_id']; ?> </span> |  <?php echo $day["competition_name"]; ?> |  <?php echo date('H:i', strtotime($day['start_time'])); ?>
            </div>
            <div class="compt-teams">
                <?php echo $day['home_team']; ?> 
                <div class="mobile-view"><span class="compt-detail">VS.</span></div>
                <?php echo $day['away_team']; ?>
            </div>
        </div>
        <div class="col-xs-1 auto-pick-color auto-pick<?= $count;?>">
            <?php echo $day['outcome']; ?>
        </div>

        <?php $count++; ?>
     </div>

    <?php endforeach; ?>

</div>

<?php endif ?>

</div>