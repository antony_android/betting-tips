<tr>
    <td colspan="2"></td>
</tr>
<tr class="pay-method">
    <td>
        <label>
            <input type="radio" <?php if($this->session->get('auth')) echo 'checked' ?> value="1" name="payment_method">
            Wallet
        </label>
    </td>
</tr>
<tr class="pay-method">
    <td>
        <label>
            <input type="radio" <?php if(!$this->session->get('auth')) echo 'checked' ?> value="2" name="payment_method">
            Tigo Pesa
        </label>
    </td>
</tr>
<tr class="pay-method">
    <td>
        <label>
            <input type="radio" value="3" name="payment_method">
            M-pesa
        </label>
    </td>
</tr>
<tr class="pay-method">
    <td>
        <label>
            <input type="radio" value="4" name="payment_method">
            Airtel Money
        </label>
    </td>
</tr>
<tr>
    <td colspan="2"></td>
</tr>