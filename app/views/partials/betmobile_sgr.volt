<div class="betmobile">
    <div class="panel-header">
        <h2>SGR Betslip</h2>
    </div>

    {{ this.flashSession.output() }}

    <?php

        $betslip = $this->session->get('betslip_sgr');

        $betslip = is_array($betslip) ? $betslip : [];

        $totalOdds=1; 
        $matchCount = 0;
        $bonus = 0;
        $bonusOdds = 1;
    ?>

    <?php foreach((array)$betslip as $bet): ?>

    <?php

        $odd = $bet['odd_value'];

        if($bet['bet_pick']=='x'){
            $pick = 'DRAW';
        }else if($bet['bet_pick']=='2'){
            $pick = $bet['away_team'];
        }else if($bet['bet_pick']=='1'){
            $pick = $bet['home_team'];
        }
        else{
            $pick = $bet['bet_pick'];
        }

        if($bet['odd_value']>=1.2){
    $bonusOdds*=$odd;
    $matchCount++;
    }

    $totalOdds = round($totalOdds*$odd,2);

    ?>

    <li class="bet-option" <?php if($bet['odd_value'] == 1){
            echo 'style="background:#f29f7a !important;"';
        }?> >
        <?php echo $this->tag->form("betslip/removesgr"); ?>
        <div class="bet-cancel">
            <input id="<?php echo $bet['match_id']; ?>" type="submit" value="X">
            <input type="hidden" name="match_id" value="<?php echo $bet['match_id']; ?>">
            <input type="hidden" name="src" value="mobile">
        </div>
        <div class="bet-value">
        <?php if($bet['bet_type'] == "live"): ?>
            <span class="live-bslip">Live&nbsp;</span>
            <?php endif; ?>
            <?php echo $bet['home_team']." v ".$bet['away_team'] ?>
            <br><span class="sp_sport" style="">
            </span>
        </div>
        <span class="clearfix"></span>
        <div><?php echo $bet['odd_type']; ?></div>
        <div class="bet-pick">
            Pick : <span style="font-weight:500 !important;"><?php echo ucwords($pick); ?></span>
            <span class="bet-odd" rel="<?php echo $bet['parent_match_id'].$bet['sub_type_id'].$bet['special_bet_value']?>">
            <?php echo $bet['odd_value']; ?>
            <?php if($bet['odd_value'] == 1){
                echo '<span style="color:#cc0000; font-size:11px;"> Market Disabled</span>';
            }?>
            </span>
        </div>
        </form>
    </li>

    <?php endforeach; ?>

    </ul>
    {% if amount is defined %}
        <input id="bet-mobile-success" type="hidden" value="{{ amount }}">
    {% endif %}
    <?php echo $this->tag->form(["betslip/placesgrbet", "novalidate", "id" => "frmPlaceBet"]); ?>
    <table class="bet-table">
        <!--      <tr>
                  <td></td>
                  <td><button class="reset-btn" type="button" onclick="javascript: deselect_all();">REMOVE ALL</button></td>
              </tr>-->
        <tbody>

        {{ partial("partials/betslip-generic_sgr") }}

        <tr class="bet-win-tr hide-on-affix" style="background: #01034D;color: #fff;font-weight: 400;">
            <td>BOOSTED WINNINGS (30%)</td>
            <td class="pull-right">KES. <span id="net-amount"><?php echo round(($winnings - $tax)*0.3) ?></span></td>
        </tr>
        <tr class="bet-win-tr hide-on-affix" style="background: #0F0F0F;color: #fff; font-weight: 400;">
            <td>NEW BETPALACE WINNINGS </td>
            <td class="pull-right">KES. <span id="net-amount"><?php echo round(($winnings - $tax)+(($winnings - $tax)*0.3)) ?></span></td>
        </tr>

        <tr>
            <td>
            </td>
            <td id="bet-controls-bet" class="bet-controls">
                <button type="submit" id="place_bet_mobile" class="place-bet-btn primary">PLACE BET</button>
            </td>
            <td id="bet-controls-login" class="bet-controls hidden">
                <span>Please <a href="{{ url('login') }}">Login</a> to place your bet.</span>
            </td>

        </tr>
        </tbody>
    </table>

    <input type="hidden" id="max_win" name="max_win" value="200000">
    <input type="hidden" name="src" value="mobile">
    <input type="hidden" id="minimum_bet" name="minimum_bet" value="2000">
    <input type="hidden" id="user_id" name="user_id" value="{{ session.get('auth')['id'] }}">
    <input type="hidden" id="total_odd_m" name="total_odd" value="<?php echo $totalOdds; ?>">
    <input type="hidden" id="max_multi_games" value="20">

    </form>
    {#Remove bets#}
    <?php echo $this->tag->form("betslip/clearsgrslip"); ?>
    <input type="submit" class="clear-slip" value="Clear slip"/>
    <input type="hidden" name="src" value="mobile">
    </form>
</div>