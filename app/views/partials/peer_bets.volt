<?php
if (!function_exists('clean'))  
{ 
 function clean($string) {
        $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }
}
?>

<div class="odd-wrapper3">
    <div class="row inner-wr3">

        <?php echo $this->tag->form([""]); ?>

        <div class="col-md-2 col-xs-6 pad-tp">
            <input type="text" class="form-control results-filter" id="bet_amount" name="bet_amount" value="" placeholder="Bet Amount">
        </div>
        <div class="col-md-3 col-xs-6 pad-tp">
            <input type="text" class="form-control results-filter" id="msisdn" name="msisdn" value="" placeholder="Namba ya Beshte">
        </div>

        <div class="col-md-3 col-xs-6 pad-tp">
            <input type="text" class="form-control results-filter" id="keyword" name="keyword" value="" placeholder="Game ID, Match or Tournament Name">
        </div>

        <div class="col-md-2 col-xs-6 pad-tp search-button">
            <button type="submit" class="search-btn">Search</button>
        </div>

        </form>

    </div>
</div>

<div class="matches full-width match-list-games">
    <?= empty($today)? '<div class="empty-list">
    No events found for selected category. Check different category to find more sports</div>':''?>

    <?php foreach($today as $key => $group):?>
    <header class="col-sm-12 match-header peer-header">
        <div class="col-sm-12 left-text p-0 peer-innerheader">
            <?php echo $key; ?>        
        </div>
        <div class="col-sm-1 peer-subheader web-element">ID</div>
        <div class="col-sm-2 peer-subheader web-element">Phone Number</div>
        <div class="col-sm-3 peer-subheader web-element">Match</div>
        <div class="col-sm-2 peer-subheader web-element">Start Time</div>
        <div class="col-sm-2 peer-subheader web-element">Bet Amount</div>
        <div class="col-sm-1 peer-subheader web-element"></div>
        <div class="col-sm-1 peer-subheader web-element"></div>
    </header>

    <div class="middle-content">
        <?php foreach($group as $day): ?>
        <?php
        $theMatch = @$theBetslip[$day['match_id']];
        ?>
        <div class="col-sm-12 top-matches hidden-overflow">

            <div class="col-sm-1 mkts-desc-head p-2  web-element">
                <?php echo $day['game_id']; ?>
            </div>

            <div class="col-md-2 mkts-desc-head p-2  web-element">
                <?php echo $day["msisdn"]; ?>
            </div>

            <div class="col-sm-3 mkts-desc-head p-2  web-element">
                <?php echo $day['home_team']; ?> <span class="vs-peer"> vs. </span> <?php echo $day['away_team']; ?>
                <br/>
                <span class="peer-pick"> Pick: <?php echo $day["bet_pick"]; ?> </span>
            </div>

            <div class="col-md-2 mkts-desc-head p-2 web-element">
                <?php echo date('d-m-Y H:i', strtotime($day['start_time'])); ?>
            </div>

            <div class="col-sm-12 p-2 mobi">
                <div class="compt-detail"> 
                    GAME ID: <span class="game-id"> <?php echo $day['game_id']; ?> </span> |  <?php echo $day["msisdn"]; ?> |  <?php echo date('H:i', strtotime($day['start_time'])); ?>
                </div>
                <div class="compt-teams">
                    <?php echo $day['home_team']; ?> 
                    <div class="mobile-view"><span class="compt-detail">VS.</span></div>
                    <?php echo $day['away_team']; ?>
                </div>
            </div>

            <div class="col-md-2 mkts-desc-head p-2 web-element">
                 <?php echo $day["bet_amount"]; ?>
            </div>

            <div class="col-xs-12 mkts-desc-head p-2 mobi">
                Bet Amount: <span class="mobi-betamt"> <?php echo $day["bet_amount"]; ?> </span>
            </div>

            <div class="col-md-1 col-xs-5 mkts-desc-head p-2">
                <?php $sub_type_id = 1; ?>
                <button class="invite-peer home-team <?php echo $day['game_id']; ?> <?php
                        echo clean($day['game_id'].$sub_type_id.$day['home_team']);
                        if($theMatch['bet_pick']==$day['home_team'] && $theMatch['sub_type_id']=='1'){
                        echo ' picked';
                    }
                    ?>" hometeam="<?php echo $day['home_team']; ?>" bet_id="<?php echo $day['bet_id']; ?>" oddtype="1x2" bettype='peer' bet_amount="<?php echo $day['bet_amount']; ?>" 
                    awayteam="<?php echo $day['away_team']; ?>" 
                    target="javascript:;" odd-key="<?php echo $day['home_team']; ?>" parentmatchid="<?php echo $theMatch['parent_match_id']; ?>"
                    id="<?php echo $day['game_id']; ?>"
                    custom="<?php echo clean($day['game_id'].$sub_type_id.$day['home_team']); ?>" value="1" special-value-value="0"
                    onClick="addPeerBet(this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),
                    this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('bet_amount'),this.getAttribute('bet_id'))" title="Pinga beste yako"> PINGA 
                </button>
            </div>
             <div class="col-md-1 col-xs-5 mkts-desc-head p-2">
                <button  class="invite-peer new_p-bet home-team <?php echo $day['game_id']; ?> <?php
                        echo clean($day['game_id'].$sub_type_id.$day['home_team']);
                        if($theMatch['bet_pick']==$day['home_team'] && $theMatch['sub_type_id']=='1'){
                        echo ' picked';
                    }
                    ?>" hometeam="<?php echo $day['home_team']; ?>" bet_id="<?php echo $day['bet_id']; ?>" oddtype="1x2" bettype='peer' bet_amount="<?php echo $day['bet_amount']; ?>" 
                    awayteam="<?php echo $day['away_team']; ?>" 
                    target="javascript:;" odd-key="<?php echo $day['home_team']; ?>" parentmatchid="<?php echo $theMatch['parent_match_id']; ?>"
                    id="<?php echo $day['game_id']; ?>"
                    custom="<?php echo clean($day['game_id'].$sub_type_id.$day['home_team']); ?>" value="1" special-value-value="0"
                    onClick="addPeerBet(this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),
                    this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('bet_amount'),this.getAttribute('bet_id'))" title="Wekelea stake mpya">
                    WEKA
                </button>
            </div>

     </div>

    <?php endforeach; ?>

</div>

<?php endforeach; ?>

</div>