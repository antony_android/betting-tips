<?php 

  $id = $this->session->get('auth')['id'];

  $phql = "SELECT ProfileBalance.balance,ProfileBalance.bonus_balance from ProfileBalance where ProfileBalance.profile_id='$id' limit 1";
  $checkUser = $this->modelsManager->executeQuery($phql);

  $checkUser=$checkUser->toArray();

 ?>

<div id="shrink-header" class="">

<nav class="ck pc os app-navbar top-nav navbar navbar-expand-lg navbar-light bg-light">

  <div class="by hidden-overflow header-pd">

    <div class="col-sm-12 mobi paybill-mobi">
      Deposit -- MPESA Paybill: <b>290085</b>
    </div>
    <div class="col-md-3 col-xs-3 hidden-overflow">

        <div>
          <a class="e logo" href="{{url('')}}" title="Bet Palace">
          <img src="{{url('/img/logo.webp')}}" alt="Bet Palace" title="Bet Palace">
          </a>
      </div>

    </div>
    
    <div class="hidden-md col-xs-5">
    {% if session.get('auth') != null %}
      <span class="mobi balnc" href="">

        <?php echo $t->_('bal'); ?>: KSH.

          <?php
              echo floor($checkUser['0']['balance']);
              echo "<br/><br/>";
              echo "Bonus: ". floor($checkUser['0']['bonus_balance']);
          ?>
          </span>
      {% endif %}
      </div>

      <div class="col-xs-4">
        <div class="pull-right">

        <a href="{{url('betmobile')}}" class="mobi betslip-mobi url-link" title="Bet Palace Betslip">
          <?php echo $t->_('betslip'); ?> <span class="slip-counter"><?= $slipCount ?></span></a>
       </div>
      </div>
    
      <div class="col-md-8 col-xs-5 hidden-overflow pull-right pt-10" id="navbar-collapse-main">

        {% if session.get('auth') != null %} {# variable is not set #}
        <div class="og ale ss profile web-element">
          <i class="fa fa-user" aria-hidden="true"></i> {{session.get('auth')['mobile']}} <br> <?php echo $t->_('balance'); ?>: KShs.

          <?php
               if(isset($checkUser[0])){
               echo number_format(floor($checkUser[0]['balance']));
               }
              //echo " | Bonus: KShs. ".number_format(floor($checkUser['0']['bonus_balance']));

          ?>
	 <a href="{{url('deposit')}}"> Deposit |</a>
         <a href="{{url('withdraw')}}"> <?php echo $t->_('withdraw'); ?> </a> <a>|</a>
         <a href="{{url('logout')}}"> <?php echo $t->_('logout'); ?><i class="fa fa-sign-out" aria-hidden="true"></i> </a>
	 <br/>
	 <?php
              if(isset($checkUser[0])){
              echo " Bonus: KShs. ".number_format(floor($checkUser[0]['bonus_balance']));
              }

          ?>
        </div>
          {% else %} {# variable is set #}

          <?php echo $this->tag->form(["login/authenticate","class"=>"ow og i web-element"]); ?>

          <div class="col-sm-3 col-sm-offset-3 col-sm-padding">
            <input type="text" name="mobile" class="form-control phone-txt" data-action="grow" placeholder="<?php echo $t->_('phone_number'); ?>..."> 
            <div class="col-sm-12 nopadding">
              <div class="remember_me">
                <input type="checkbox" name="remember" value="1" />
              </div>
              <div class="remember_me">
                <label><?php echo $t->_('remember_me'); ?></label>
              </div>
            </div>
          </div>
          <div class="col-sm-3 col-sm-padding">
            <div class="col-sm-12 nopadding">
            </div>
            <input type="password" name="password" class="form-control phone-txt" data-action="grow" placeholder="<?php echo $t->_('password'); ?>..."><br/>
            <input type="hidden" name="ref" value="{{refURL}}">
            <a href="{{ url('reset') }}" title="Bet Palace Create or Reset password">
              <span class="forgot-pass"><?php echo $t->_('forgot_password'); ?>?</span></a>
          </div>
          <div class="col-sm-3 col-sm-padding">
            <div class="col-sm-6 col-sm-padding">
              <button class="cg fp login-butt"><?php echo $t->_('login'); ?></button>
            </div>
            </form>
            <div class="col-sm-6 col-sm-padding">
              <a href="{{ url('/signup') }}" title="Bet Palace Register">
                <button class="cg fp register-butt" type="button"><?php echo $t->_('register'); ?></button>
              </a>
            </div>
          </div>

          {% endif %}

      </div>

  </div>
</nav>



<nav class="second-nav ck pc os app-navbar ">

  <div class="by f collapse " id="navbar-collapse-main">

        <ul class="nav navbar-nav og ale ss">
        <li>
            <a class="cg fm ox anl url-link not-selectable" href="{{url('')}}" data-original-title="" title="Bet Palace Home">
              <?php echo $t->_('home'); ?>
            </a>
        </li>

        <li>
          <a class="cg fm ox anl url-link not-selectable" href="{{url('/virtual-sports')}}" data-original-title="" title="Bet Palace Home">
            Virtuals
          </a>
      </li>

        <li>
            <a class="cg fm ox anl" style="background: #0D9737;" href="{{url('/livehome')}}" data-original-title="" title="Bet Palace Live">
                Live
            </a>
        </li>
      
      	<!--
        <li>
            <a class="cg fm ox anl url-link" href="{{url('/virtual')}}" data-original-title="" title="Virtuals">
                <?php echo $t->_('virtuals'); ?>
            </a>
        </li> 
    	-->
        
         <li>
            <a class="cg fm ox anl url-link" href="{{url('/autobet')}}" data-original-title="" title="Autobet">
                Autobet
            </a>
        </li>

        <li>
            <a class="cg fm ox anl url-link" href="{{url('/sgr')}}" data-original-title="" title="SGR">
                SGR
            </a>
        </li>

        <li>
            <a class="cg fm ox anl url-link" href="{{url('/results')}}" data-original-title="" title="Results">
                <?php echo $t->_('results'); ?>
            </a>
        </li>

        <!--
        <li>
            <a class="cg fm ox anl url-link" href="{{url('jackpot')}}" data-original-title="" title="Jackpot">
                <?php echo $t->_('jackpot'); ?>
            </a>
          </li>
        -->        

          <li>
            <a class="g url-link" href="{{url('how-to-play')}}" title="How to play">
                <?php echo $t->_('how-to-play'); ?>
            </a>
          </li>

          <li>
            <a class="g url-link" href="{{url('mfalme-bonus')}}" title="Mfalme Bonus">
                Mfalme Bonus
            </a>
          </li>


          <li>
            <a class="g url-link" href="{{url('deposit')}}" title="Deposit">
                Deposit
            </a>
          </li>

          
          <!--<li>
            <a class="g url-link"  href="http://www.betpalace.co.ke/betpalace.apk" title="Download our android mobile app!">
                Download APP
            </a>
          </li> -->

          <li>
             <a class="g url-link" href="{{url('betpalace.apk')}}" title="App">
                <?php echo $t->_('APP'); ?>
            </a>
          </li>

          <!-- <li>
            <a class="cg fm ox anl" href="{{url('#')}}" data-original-title="" title="">
              Live Betting
            </a>
          </li> -->
        {% if session.get('auth') != null %} {# variable is not set #}
        <li>
         <a class="url-link" href="{{url('mybets')}}" title="Bet Palace My bets"> <?php echo $t->_('my-bets'); ?> </a>
        </li>
        <li>
         <a class="url-link" href="{{url('transactions')}}" title="Bet Palace My Transactions"> <?php echo $t->_('my-transactions'); ?> </a>
        </li>
        {% endif %}

        </ul>

        <ul class="nav navbar-nav st su sv">

        </ul>

        <ul class="nav navbar-nav hidden">

        </ul>

      </div>

      <div class="mobi">

        {% if session.get('auth') != null %} {# variable is not set #}
        <ul class="nav in">
	       	<li style="background: #0D9737;">
	        	<a class="url-link-not-selectable" href="{{url('virtual-sports')}}">Virtuals </a>
	        </li>
	        <li>
	         <a class="url-link" href="{{url('mybets')}}" title="My Bets"><?php echo $t->_('my-bets'); ?></a>
	        </li>
	        <li>
	         <a class="url-link" href="{{url('deposit')}}" title="Deposit">Deposit</a>
	        </li>
	        <li>
	         <a class="url-link" href="{{url('logout')}}" title="Bet Palace Logout"><?php echo $t->_('logout'); ?></a>
	        </li>
         </ul>
          {% else %} {# variable is set #}
          <ul class="nav men top-mobi-menu">
          	<li style="background: #0D9737;width: 25%;" >
	        	<a class="url-link-not-selectable" href="{{url('virtual-sports')}}">Virtuals</a>
	        </li>
            <li style="width: 25%;">
              <a class="active" href="{{url('signup')}}" title="Bet Palace register"><?php echo $t->_('register'); ?>!</a>
            </li>
            <li class="ml-mobi" style="width: 25%;">
              <a data-toggle="modal" href="{{url('login')}}" title="Bet Palace login"><?php echo $t->_('login'); ?></a>
            </li>
          </ul>
          {% endif %}

        {% if session.get('auth') != null %}
        <div class="col-xs-12 pad-0">
        {% else %}
        <div class="col-xs-10 pad-0">
        {% endif %}

        <ul class="main-nav">

          <li>
            <a class="url-link-not-selectable" href="{{url('')}}"><?php echo $t->_('home'); ?> |</a>
          </li>
          <li>
            <a class="url-link-not-selectable" href="{{url('livehome')}}">Live |</a>
          </li>
          <li>
          	<a class="url-link" href="{{url('withdraw')}}" title="Withdraw"> 
            	<?php echo $t->_('withdraw'); ?> |
            </a>
          </li>
          
          <li>
            <a class="url-link-not-selectable" href="{{url('/autobet')}}">Auto |</a>
          </li>

          {% if session.get('auth') != null %}
           <li>
            <a class="url-link-not-selectable" href="{{url('/sgr')}}">SGR |</a>
          </li>
          {% endif %}

          <li>
            <a class="url-link" href="{{url('mobi/football')}}" title="Bet Palace football">Soccer |</a>
          </li>

          <li>
            <a class="url-link" href="{{url('mobi/sports')}}" title="Bet Palace sports"><?php echo $t->_('sports'); ?> |</a>
          </li>
          <li>
            <a class="url-link" href="{{url('transactions')}}" title="Transactions">History | </a>
          </li>
          <li>
             <a class="url-link" href="{{url('betpalace.apk')}}" title="App">
                <?php echo $t->_('APP'); ?>
            </a>
          </li>

          <!-- <li>
            <a class="url-link" target="_blank0" href="http://www.betpalaceretail.com" title="Become our Shop Partner">Shop</a>
          </li> -->
         
        </ul>
      </div>
      {% if session.get('auth') == null %}
      <div class="col-xs-2 pad-0">
        
        <ul class="main-nav">
          
          <li>
            <a class="url-link fp-mobi" style="font-size: 9px;" href="{{url('/reset')}}" title="Forgot or No password"><?php echo $t->_('forgot_password_mobi'); ?></a>
          </li>
        </ul>
      </div>
      {% endif %}

    </div>
    

    
</nav>




<div style="padding-top:19px" class="web-element">
  <div class="slim-banner" style="background-color: black;">
        <div id="main-carousel" class="carousel slide carousel-fade" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
               <div style="height: 120px; "></div>
            </div>
          </div>

        </div>
  </div>

</div>

</div>

