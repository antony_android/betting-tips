<section class="panel jackpot"> 
<header>
   <div class="header-holder panel-header">
     BETPALACE Jackpot
   </div>
</header>

<?php 
      function clean($string) {
         $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
         $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

         return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
      }
    ?>

<img src="{{url('img/banner/betpalace-1.jpg')}}" class="full-width" alt="Jackpot"/>

<!-- <div class="events-header"><div class="events-odd text-center">The first game of the Jackpot has already started.</div></div> -->
<div id="matches" class="matches full-width">
<div class="web-element"> 

<header class="top-matches match-header events-header">
   <div class="col-sm-12 events-odd">Pick Your Choice for All Matches - JACKPOT STAKE KES 100.00 </div>
  
</header>
<?php foreach($games as $day): ?>
<?php
   $theMatch = $theBetslip[$day['match_id']];
   $odds = $day['threeway'];
   $odds = explode(',',$odds);
   $home_odd = $odds['0'];
   $neutral_odd = $odds['2'];
   $away_odd = $odds['1'];
?>

  <div class="col-sm-12 top-matches hidden-overflow match-event">
      <div class="col-sm-12 nopadding left-text">
          
          <div class="col-sm-1 game-no nopadding"><?php echo $day['pos']; ?> </div>
          
          <div class="col-sm-2 nopadding">
              <?php echo date('d/m/y H:i', strtotime($day['start_time'])); ?>
          </div>
          <div class="col-sm-2 nopadding">
              GAME ID: <?php echo $theMatch['game_id']; ?> 1234
          </div>
          <div class="compt-detail col-sm-7"> 
              <?php echo $day["category"]; ?> - <?php echo $day["competition_name"]; ?></div>
          
      </div>
  
   <div class="col-sm-12 nopadding" >
       
       <div class="col-sm-4 match-div-col" style="padding:0;">

          <button class="home-team <?php echo $day['match_id']; ?> <?php
                      echo clean($day['match_id'].$day['sub_type_id'].$day['home_team']);
                         if($theMatch['bet_pick']==$day['home_team'] && $theMatch['sub_type_id']=='1'){
                            echo ' picked';
                         }
                      ?>" pos="<?= $day['pos']; ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="1x2" bettype='jackpot' awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $home_odd; ?>" target="javascript:;" odd-key="<?php echo $day['home_team']; ?>" parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['home_team']); ?>" value="1" special-value-value="0" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('pos'))">
                      <span class="team"> <?php echo $day['home_team']; ?></span>
                      <span class="theodds "><?php echo $home_odd; ?></span>
                      </button>
       </div>
       <div class="col-sm-4 events-odd match-div-col" style="padding:0; padding-left:3px; padding-right:3px">

       <button class="draw <?php echo $day['match_id']; ?> <?php
                       echo clean($day['match_id'].$day['sub_type_id']."draw");
                         if($theMatch['bet_pick']=='draw' && $theMatch['sub_type_id']=='1'){
                            echo ' picked';
                         }
                       ?>" pos="<?= $day['pos']; ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="1x2" bettype='jackpot' awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $neutral_odd; ?>" custom="<?php echo clean($day['match_id'].$day['sub_type_id']."draw"); ?>" value="1" odd-key="draw" target="javascript:;" parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" special-value-value="0" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('pos'))">
                       <span class="team"> Draw</span>
                       <span class="theodds"> <?php echo $neutral_odd; ?></span>
                       </button>
       </div>
       <div class="col-sm-4 match-div-col" style="padding:0;">
          <button class="away-team <?php echo $day['match_id']; ?> <?php echo clean($day['match_id'].$day['sub_type_id'].$day['away_team']);
                      if($theMatch['bet_pick']==$day['away_team'] && $theMatch['sub_type_id']=='1'){
                            echo ' picked';
                         }
                       ?>" pos="<?= $day['pos']; ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="1x2" bettype='jackpot' awayteam="<?= $day['away_team']; ?>" oddvalue="<?php echo $away_odd; ?>" value="1" custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['away_team']); ?>" odd-key="<?php echo $day['away_team']; ?>" target="javascript:;" parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" special-value-value="0" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('pos'))">
                        <span class="team"> <?php echo $day['away_team']; ?></span>
                       <span class="theodds"> <?php echo $away_odd; ?></span></button>
       </div>

    </div>
</div>

<?php endforeach; ?>
</div>
</div>

<?php foreach($games as $day): ?>
<?php
   $theMatch = $theBetslip[$day['match_id']];
   $odds = $day['threeway'];
   $odds = explode(',',$odds);
   $home_odd = $odds['0'];
   $neutral_odd = $odds['2'];
   $away_odd = $odds['1'];
?>
      <div class="col-sm-12 top-matches mobi">

            <div class="teams highlited" style="">
            <span class="bold"><?php echo $day['pos'].". ".$day['home_team']; ?></span> &nbsp; v  &nbsp; <span class="bold"><?php echo $day['away_team']; ?></span>
            </div>

            <div class="meta">
<!--             <p style="display:block;"> <?php echo $day['competition_name'].", ".$day['category']; ?> </p>
 -->            <?php echo date('d/m', strtotime($day['start_time'])); ?> 
            <?php echo date('H:i', strtotime($day['start_time'])); ?>
            Game ID: <?php echo $day['game_id']; ?>
            </div>
            
            <div class="row">
            <div class="col-sm-12">
                  <button class="odds home <?php echo $day['match_id']; ?> <?php
                  echo clean($day['match_id'].$day['sub_type_id'].$day['home_team']);
                     if($theMatch['bet_pick']==$day['home_team'] && $theMatch['sub_type_id']=='1'){
                        echo ' picked';
                     }
                  ?>" pos="<?= $day['pos']; ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="1x2" bettype='jackpot' awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $home_odd; ?>" target="javascript:;" odd-key="<?php echo $day['home_team']; ?>" parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['home_team']); ?>" value="1" special-value-value="0" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('pos'))"><span class="label label-inverse"><?php echo substr($day['home_team'], 0, 10)."..."; ?><span class="mobi-odd"> <?php echo $home_odd; ?></span></span></button>

                  <button class="odds neutral <?php echo $day['match_id']; ?> <?php
                   echo clean($day['match_id'].$day['sub_type_id']."draw");
                     if($theMatch['bet_pick']=='draw' && $theMatch['sub_type_id']=='1'){
                        echo ' picked';
                     }
                   ?>" pos="<?= $day['pos']; ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="1x2" bettype='jackpot' awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $neutral_odd; ?>" custom="<?php echo clean($day['match_id'].$day['sub_type_id']."draw"); ?>" value="1" odd-key="draw" target="javascript:;" parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" special-value-value="0" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('pos'))"><span class="label label-inverse"> X <span class="mobi-odd"><?php echo $neutral_odd; ?></span></span></button>
                  <button class="odds away <?php echo $day['match_id']; ?> <?php echo clean($day['match_id'].$day['sub_type_id'].$day['away_team']);
                  if($theMatch['bet_pick']==$day['away_team'] && $theMatch['sub_type_id']=='1'){
                        echo ' picked';
                     }
                   ?>" pos="<?= $day['pos']; ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="1x2" bettype='jackpot' awayteam="<?= $day['away_team']; ?>" oddvalue="<?php echo $away_odd; ?>" value="1" custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['away_team']); ?>" odd-key="<?php echo $day['away_team']; ?>" target="javascript:;" parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" special-value-value="0" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('pos'))"><span class="label label-inverse"> <?php echo substr($day['away_team'], 0, 10)."..."; ?> <span class="mobi-odd"><?php echo $away_odd; ?></span></span></button>
                  </div>
            </div>
            </div>
<?php endforeach; ?>

<img src="{{url('img/loader.gif')}}" class="loader" />

<div class="header-holder mobi">
 <span class="col-sm-6">Total Stake: </span><span class="col-sm-4">10000 BETPALACE points</span><button type="button" id="place_bet_button" class="col-sm-6 place-bet-btn " onclick="jackpotBet()">PLACE BET</button>
</div>

<div class="betj web-element">
  <div class="col-sm-12">
    <div class="col-sm-6">
      &nbsp;
    </div>
    <div class="col-sm-6">
      <button type="button" id="place_bet_button" class="place-bet-btn" onclick="jackpotBet()">PLACE BET</button>
    </div>
  </div>
</div>


<div class='alert alert-danger alert-dismissible betslip-error' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>You have insufficient balance please top up</div>
            
</section>
