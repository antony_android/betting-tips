<?php $totalOdds=1; ?>
<div class="bet-body">
    <?php
    
    $betslip = $this->session->get('betslip-autobet');

    $betslip = is_array($betslip) ? $betslip : [];

    $matchCount = 0;
    $bonus = 0;
    $bonusOdds = 0;
    ?>
    <?php foreach($betslip as $bet): ?>
    <?php
        $odd = $bet['odd_value'];

        if($bet['bet_pick']=='x'){
            $pick = 'DRAW';
        }else if($bet['bet_pick']=='2'){
            $pick = $bet['away_team'];
        }else if($bet['bet_pick']=='1'){
            $pick = $bet['home_team'];
        }
        else{
            $pick = $bet['bet_pick'];
        }

        $totalOdds = round($totalOdds*$odd, 2);

    ?>

    <li class="bet-option hide-on-affix">
        <div class="bet-cancel">
        </div>
        <div class="bet-value">
            <?php echo $bet['home_team']." v ".$bet['away_team']; ?>
            <br><span class="sp_sport" style=""></span>
        </div>
        
        <div class="row bet-pick">

            <div class="col-md-4">
                <?= ucwords($pick); ?>
            </div>
            <div class="col-md-4">
                <span class="bet-odd"><?php echo $bet['odd_value']; ?></span>
            </div>
            <div class="col-md-4 slip-pull-right">
                <span class="bet-pick-m8">
                    <?php echo $bet['odd_type']; ?>
                </span>
            </div>

        </div>

    </li>
    <?php endforeach; ?>
</div>
<form>
    <table class="bet-table">
        <tbody>

        {{ partial("partials/betslip-generic_autobet") }}
        <tr class="possible-win-bg">
            <td>
            </td>
            <td class="pull-right">
            </td>
        </tr>
        </tbody>
    </table>
    <div class='alert alert-danger alert-dismissible ss betslip-error' role='alert'>
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
        </button>
        You have insufficient balance. Please top up
    </div>
    <input type="hidden" id="max_win" value="3000000">
    <input type="hidden" id="minimum_bet" value="2000">
    <input type="hidden" id="user_id" value="{{ session.get('auth')['id'] }}">
    <input type="hidden" id="total_odd" value="<?php echo $totalOdds; ?>">
    <input type="hidden" id="max_multi_games" value="20">
</form>