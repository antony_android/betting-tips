    <div class=" single-bet-wrapper2">
            <span class="strip">
            <img src="/svg/football.svg" alt="" height="24px"> RESULTS<span style="font-weight:bold;"></span> </span>
        </div>
    <!-- odds section -->
    <div class="odd-wrapper3">
        <div class="row inner-wr3">

        <?php echo $this->tag->form([""]); ?>

        <div class="col-md-3 pad-tp">
            Search for Games
        </div>
        <div class="col-md-3 pad-tp">
            <input type="text" class="form-control results-filter" id="date_picked" name="date_put" value="{{date('d-m-Y')}}" placeholder="Date">
        </div>

        <div class="col-md-4 pad-tp">

            <input type="text" class="form-control results-filter" id="search-pattern" name="keyword" placeholder="Search by Game ID, Match or Tournament Name">
        </div>

        <div class="col-md-2 pad-tp search-button">
            <button type="submit" class="search-btn">Search</button>
        </div>

        </form>

    </div>

    <div class="row single-livebet-wrapper">
        <div class="single-livebet-wrapper-header">
            <?php if($filter != "All") { ?>
            <span>$results[0]['category'] - $results[0]['competition_name']</span>
            <?php } else {?>
            <span>All</span>
             <?php } ?>
        </div>

        <?php foreach($results as $result): ?>

        <div class="row results-listing">

            <div class="col-md-3">

                <?php echo $result['start_time']; ?>
            </div>

            <div class="col-md-2">
                <?php echo $result['category']; ?>
            </div>

            <div class="col-md-2">
                <?php echo $result['competition_name']; ?>
            </div>

            <div class="col-md-4">
                <?php echo $result['home_team']; ?> vs <?php echo $result['away_team']; ?>
            </div>

            <div class="col-md-1">
                <?php echo $result['outcome']; ?>
            </div>

        </div>

        <?php endforeach; ?>

    </div>

</div>
