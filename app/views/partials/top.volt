{{ partial("partials/header") }}

<div class="by amt">
  <!-- Start of scroller -->
  <?php if(isset($winners)){ ?>
  <div id="" class="wrapper-mobi mobi" style="display: none;">   
    <?php $length = count($winners) * 120; ?>
    <ul class="list-mobi" style="display: flex; justify-content: left; transform: translate(-<?php echo $length; ?>px, 0px);">
      <?php foreach($winners as $winner): ?>
      <li style="padding-right: 0px; width:fit-content;">
        <div class="item-wrapper-mobi" style="padding: 5px 5px 0px 5px;">
            <div class="" style=""><?php echo substr_replace($winner['msisdn'], "*******", 7); ?> won</div>  
            <div class="cash ">KES <?php echo $winner['possible_win']; ?></div class="cash">  
            <div class="" style="">in <strong>Sports</strong>  betting</div> 
        </div>
      </li> 
      <?php endforeach; ?>  
    </ul>
  </div>
  <?php } ?>
  <!-- End of scroller -->
  <div class="gc">
    
{{ partial("partials/sidebar") }}
<div class="gz home">