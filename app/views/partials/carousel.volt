<style type="text/css">
    @media (min-width: 760px) {
        .item img {
            height: 230px !important;
        }
    }

    @media (max-width: 759px) {
        .item img {
            height: 150px !important;
        }
    }


    /*.item {*/
    /*max-height: 230px;*/
    /*!*overflow: hidden;*!*/
    /*}*/
</style>

<!-- <div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin-bottom: 0px; display: none;"> -->
    <!-- Indicators -->
    <!-- <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol> -->
  
    <!-- Wrapper for slides -->
    <!-- <div class="carousel-inner"> -->
      <!-- <div class="item active">
        <img src="img/new-banner.jpg" alt="Los Angeles">
      </div>
  
      <div class="item">
        <img src="img/new-banner.jpg" alt="Chicago">
      </div>
  
      <div class="item">
        <img src="img/new-banner.jpg" alt="New York">
      </div> -->
    <!-- </div> -->
  
    <!-- Left and right controls -->
    <!-- <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a> -->
  <!-- </div> -->



<div class="shortcut-wrapper">
        <table class="table table3 table-responsive">

                <tbody>
                  <tr>
                    <td class="shortcut-cell">
                        <a href="/competition?sp=79">
                            <div class="<?= $sportId == 79? 'shortcut-thumbnail-active':'shortcut-thumbnail'; ?>">
                                <img src="/svg/football.svg" alt="" class="shortcut-img"  width="80px" >
                            </div>
                        </a>
                    </td>
                    <td  class="shortcut-cell">
                        <a href="/competition?sp=85">
                            <div class="<?= $sportId == 85? 'shortcut-thumbnail-active':'shortcut-thumbnail'; ?>">
                                <img src="/svg/basketball.svg" alt="" class="shortcut-img" width="80px" >
                            </div>
                        </a>
                    </td>
                    <td class="shortcut-cell">
                        <a href="/competition?sp=97">
                            <div class="<?= $sportId == 97? 'shortcut-thumbnail-active':'shortcut-thumbnail'; ?>">
                                <img src="/svg/american-football.svg" alt="" class="shortcut-img" width="80px" >
                            </div>
                        </a>
                    </td>
                    <td class="shortcut-cell">
                        <a href="/twoway?sp=80">
                            <div class="<?= $sportId == 80? 'shortcut-thumbnail-active':'shortcut-thumbnail'; ?>">
                                <img src="/svg/tennis-ball.svg" alt="" class="shortcut-img" width="80px" >
                            </div>
                        </a>
                    </td>
                    <td  class="shortcut-cell">
                        <a href="/competition?sp=84">
                            <div class="<?= $sportId == 84? 'shortcut-thumbnail-active':'shortcut-thumbnail'; ?>">
                                <img src="/svg/volleyball.svg" alt="" class="shortcut-img" width="80px" >
                            </div>
                        </a>
                    </td>
                    <td class="shortcut-cell">
                            <a href="/competition?sp=82">
                                <div class="<?= $sportId == 82? 'shortcut-thumbnail-active':'shortcut-thumbnail'; ?>">
                                    <img src="/svg/hockey.svg" alt="" class="shortcut-img" width="80px" >
                                </div>
                            </a>
                    </td>

                  </tr>

                </tbody>
              </table>

</div>
