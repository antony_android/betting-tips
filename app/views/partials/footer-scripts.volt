
{{ javascript_include('js/bootstrap.min.js') }}
{{ javascript_include('js/jquery-ui.min.js') }}
{{ javascript_include('js/ajax.js') }}
{{ javascript_include('js/toolkit.js') }}
{{ javascript_include('js/moment.min.js') }}
{{ javascript_include('js/sidebar-menu.js') }}
{{ javascript_include('js/betslip.js?rand=7857') }}
<script src="https://betpalace.co.ke/js/lazysizes.min.js" type="text/javascript" async></script>

<script>
    $(document).ready(function () {

        $('.carousel').carousel({
          interval: 5000
        });

        var interval = setInterval(function () {
            var momentNow = moment();
            // momentNow.format('dddd').substring(0,3).toUpperCase() +' '+momentNow.format('MMMM DD').toUpperCase()+' - '+
            $('#date-part').html(momentNow.format('HH:mm:ss'));
        }, 1000);

        $.sidebarMenu($('.sidebar-menu'));

        // Add active classes
        $('.url-link').not('.not-selectable').removeClass('active').filter('[href="' + window.location.pathname + '"]').addClass('active');
    });
</script>
<!-- Start of betpalace Zendesk Widget script
<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=83ee9532-54c8-4765-bbcf-4f3a357aeb7f"> </script>
 End of betpalace Zendesk Widget script -->

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d49bf17e5ae967ef80ec3c8/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->


<!-- Start of global virtual sports integration footer scripts -->




<!-- End of virtual sports integration script -->