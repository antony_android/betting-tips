<div class="qv rc alu show-on-affix">
    <div class="qv rc alu paybill block-shadow">
        <div class="std-block-head bold"><?php echo $t->_('company_number'); ?></div>

        <div class="warning-txt"></div>

        <div class="sidebar company-number bold">
            <ul>
                <li class="safaricom" style="margin-bottom: 0px;">
                    <span class="paybill-images" style="display: block; background-image: url('/img/paybills/safaricom.png');"></span>
                    <span class="">290085</span>
                </li>
                <!--
                <li class="airtel">
                    <span class="paybill-images" style="display: block; background-image: url('/img/paybills/airtel.png');"></span>
                    <span>BETPALACE</span>
                </li>
                -->
            </ul>
        </div>

<?php 
function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

?>

       

        <div class="payment-selector std-side-pads no-transform">

        </div>

         <!-- Inject the scroller  -->
         <?php if(isset($winners)){ ?>
         <div class="scroller-starts-here" style="display: none;">

         <div class="std-block-head bold"> <i class="fa fa-trophy"></i>  BIG WINNERS</div>

         <div class="warning-txt"></div>
 
         <div class="sidebar company-number bold">
            <div class="wrapper">
        
      
                <ul class="list">
                    <?php foreach($winners as $winner): ?>

                  <li>
                    <div class="item-wrapper" style=""> 
                        <div>KES <?php echo $winner['possible_win']; ?></div>  
                        <div class="time-ago" style="float: right!important; display: none;"><?php echo time_elapsed_string($winner['created']); ?></div> 
                    </div>
                    <div class="item-wrapper" style=""> 
                        <div class="time-ago" style="float: left!important;">Sports</div> 
                        <div class="time-ago" style="float: right!important;"><?php echo substr_replace($winner['msisdn'], "*******", 7); ?></div> 
                    </div>
                  </li>

                  <?php endforeach; ?>
                  

                  
                
                </ul>
          
              </div>

         </div>

        </div>
        <?php } ?>
         <!--  End scroller injection  -->


        <div class="support">
            <div class="contact-us black-bg capitalize">Support</div>
            <div class="helpline">
              <!--  <i class="fa fa-phone" aria-hidden="true"></i>
                <span>0705 290 085</span><br/>-->
                <i class="fa fa-phone" aria-hidden="true"></i>
                <span>0739 290 085</span><br/>
                <i class="fa fa-phone" aria-hidden="true"></i>
                <span>0777 290 085</span>
            </div>
            <div class="whatsapp">
                <img class="lazyload" data-src="/img/whatsapp.webp" width="30px"/>
                <span>0797 290 087</span>
            </div>
            <div class="mail">
                support@betpalace.co.ke
            </div>
        </div>
    </div>
</div>


<?php 



?>
