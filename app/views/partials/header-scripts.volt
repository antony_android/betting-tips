<link rel="stylesheet" href="https://betpalace.co.ke/css/bootstrap.min.css" >


<link rel="stylesheet" href="https://betpalace.co.ke/css/tolkits.css?rand=7858" >
<!-- <link rel="stylesheet" href="https://betpalace.co.ke/css/styles.min.css?rand=7857" > -->

<link rel="preload" href="https://betpalace.co.ke/css/font-awesome.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
<noscript><link rel="stylesheet" href="https://betpalace.co.ke/css/font-awesome.css"></noscript>

<link rel="preload" href="https://betpalace.co.ke/css/jquery-ui.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
<noscript><link rel="stylesheet" href="https://betpalace.co.ke/css/jquery-ui.min.css"></noscript>

<link rel="preload" href="https://betpalace.co.ke/css/scroller.css?v=1.0.2" as="style" onload="this.onload=null;this.rel='stylesheet'">
<noscript><link rel="stylesheet" href="https://betpalace.co.ke/css/scroller.css?v=1.0.2"></noscript>

<script src="https://betpalace.co.ke/js/jquery.min.js" type="text/javascript" ></script>

<!-- Google Tag Manager --> 
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5R8FSB3');</script>
<script src="https://cdn.jsdelivr.net/npm/vue" async></script>
<!-- End Google Tag Manager -->

<script>
    $(document).ready(function(){
        $("#mModal").modal('show');
    });
</script>

<!-- virtual sports header script -->