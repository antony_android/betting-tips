<?php $totalOdds=1; ?>
<?php $readonly = "" ?>
<div class="betmobile">
    <div class="panel-header">
        <h2>Betslip</h2>
    </div>

    {{ this.flashSession.output() }}

    <?php
    
    $betslip = $this->session->get('betslip-peer');

    $betslip = is_array($betslip) ? $betslip : [];

    $matchCount = 0;
    $bonus = 0;
    $bonusOdds = 0;
    $total_odd_original = 0;
    $parentmatchid = 0;
    $matchid = 0;
    $msisdn = "";
    $bet_amount = 50;

    ?>
    <?php foreach($betslip as $bet): ?>
    <?php

        $odd = $bet['odd_value'];
        $msisdn = $bet['peer_msisdn'];
        $bet_amount = $bet['bet_amount'];

        if($bet['bet_pick']=='x'){
            $pick = 'DRAW';
        }else if($bet['bet_pick']=='2'){
            $pick = $bet['away_team'];
        }else if($bet['bet_pick']=='1'){
            $pick = $bet['home_team'];
        }
        else{
            $pick = $bet['bet_pick'];
        }

        $friend_pick = $bet['friend_pick'];

        if($bet['odd_value']>=1.6){
            $bonusOdds*=$odd;
            $matchCount++;
        }

        $parentmatchid = $bet['parent_match_id'];
        $matchid = $bet['match_id'];
        $peer_bet_id = $bet['peer_bet_id'];

        $totalOdds = 1.8;
        $total_odd_original = $odd;

    ?>

    <li class="bet-option">
        <?php echo $this->tag->form("betslip/removepeer"); ?>
        <div class="bet-cancel">
            <input id="<?php echo $bet['match_id']; ?>" type="submit" value="X">
            <input type="hidden" name="match_id" value="<?php echo $bet['match_id']; ?>">
            <input type="hidden" name="src" value="mobile">
        </div>
        <div class="bet-value"><?php echo $bet['home_team']." v ".$bet['away_team'] ?>
            <br><span class="sp_sport" style=""></span>
        </div>
        <span class="clearfix">
            </span>
        <div class="bet-pick">
            Your Pick : 
            <span class="bet-odd"><?php echo strtoupper($friend_pick); ?></span>
        </div>

        <?php  if(strpos($friend_pick, 'NOT WIN') !== false): ?>

        <?php $readonly = "readonly"; ?>
        <?php endif ?>

        <div class="bet-pick">
            <div class="col-xs-4 p-0">
                Odds
            </div>
            <div class="col-xs-8 slip-pull-right">
                2.0
            </div>
        </div>

        <div class="bet-pick">
            <div class="col-xs-4 p-0">
                Commission
            </div>
            <div class="col-xs-8 slip-pull-right">
                0.2
            </div>
        </div>

        </form>
    </li>

    <?php endforeach; ?>

    </ul>
    {% if amount is defined %}
        <input id="bet-mobile-success" type="hidden" value="{{ amount }}">
    {% endif %}
    <?php echo $this->tag->form(["betslip/placepeerbet", "novalidate", "id" => "frmPlaceBet"]); ?>
    <table class="bet-table">

        <tbody>

        <tr class="hide-on-affix odds-bg">
            <td>TOTAL ODDS</td>

            <td class="pull-right">BET AMOUNT</td>
        <tr class="odds-bg">
            <td><b><?php echo $totalOdds; ?> </b></td>
            <td class="pull-right">
                <div id="betting">
                    <input class="bet-select" type="text" id="bet_amount" name="bet_amount" onkeyup="winnings()" value="<?php echo $bet_amount; ?>" min="20" max="20000" <?php echo $readonly ?> />
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr class="bet-win-tr hide-on-affix possible-win-bg">
            <td>POSSIBLE WIN</td>
            <td class="pull-right">
            <?php $winnings = $totalOdds*$bet_amount; ?>
                KES. <span
                        id="pos_win"><?php ($winnings<1000500)?$winnings = $winnings:$winnings=1000500; $taxable = $winnings - 2000;  echo $winnings; ?></span>
            </td>
        </tr>
        <!--
        <tr class="bet-win-tr hide-on-affix">
            <td>Tax (18%)</td>
            <td>KES. <span id="tax"><?php // $tax = (18 * $taxable) / 100; // echo round($tax); ?></span></td>
        </tr>
        <tr class="bet-win-tr hide-on-affix">
            <td>Net Amount</td>
            <td>KES. <span id="net-amount"><?php // echo round($winnings - $tax) ?></span></td>
        </tr>
        -->
        <tr>
        <td colspan="3">
            <div class="bet-pick">
                <div class="col-md-12 slip-pull-right">
                    <label class="peer-label" for="peer_invite">Enter Friend Phone to Invite them to Bet Against You </label><span class="peer-mandatory"> *</span>
                    <input class="peer-invite-friends" type="text" id="peer_invite" name="peer_msisdn" value="<?php echo $msisdn; ?>" required="required" <?php echo $readonly ?> />
                </div>
            </div>
        </td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="bet-controls-bet" class="bet-controls">
                <button type="submit" id="place_bet_mobile" class="place-bet-btn primary">PLACE BET</button>
            </td>
            <td id="bet-controls-login" class="bet-controls hidden">
                <span>Please <a href="{{ url('login') }}">Login</a> to place your bet.</span>
            </td>

        </tr>
        </tbody>
    </table>

    <input type="hidden" id="user_id" name="user_id"  value="{{ session.get('auth')['id'] }}">
    <input type="hidden" id="max_win" name="max_win" value="3000000">
    <input type="hidden" id="minimum_bet" name="minimum_bet" value="2000">
    <input type="hidden" id="total_odd_original" name="total_odd_original" value="<?php echo $total_odd_original; ?>">
    <input type="hidden" id="total_odd" name="total_odd" value="1.8">
    <input type="hidden" id="parentmatchid" name="parentmatchid" value="<?php echo $parentmatchid; ?>">
    <input type="hidden" id="peer_bet_id" name="peer_bet_id" value="<?php echo $peer_bet_id; ?>">
    <input type="hidden" id="match_id" name="match_id" value="<?php echo $matchid; ?>">
    <input type="hidden" id="max_multi_games" name="max_multi_games" value="20">
    <input type="hidden" id="bet_type" name="bet_type" value="prematch">
    <input type="hidden" id="is_peer" name="is_peer" value="1">
    <input type="hidden" name="src" value="mobile">

    </form>
    {#Remove bets#}
    <?php echo $this->tag->form("betslip/clearpeerslip"); ?>
    <input type="submit" class="clear-slip" value="Clear slip"/>
    <input type="hidden" name="src" value="mobile">
    </form>
</div>