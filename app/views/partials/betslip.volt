<?php $totalOdds=1; ?>
<div class="bet-body">
    <?php
    
    $betslip = $this->session->get('betslip');

    $betslip = is_array($betslip) ? $betslip : [];

    $matchCount = 0;
    $bonus = 0;
    $bonusOdds = 0;

    ?>
    <?php //die(print_r($betslip, 1)); ?>
    <?php foreach($betslip as $bet): ?>
    <?php
        $odd = $bet['odd_value'];

        if($bet['bet_pick']=='x'){
            $pick = 'DRAW';
        }else if($bet['bet_pick']=='2'){
            $pick = $bet['away_team'];
        }else if($bet['bet_pick']=='1'){
            $pick = $bet['home_team'];
        }
        else{
            $pick = $bet['bet_pick'];
        }

        $totalOdds = round($totalOdds*$odd, 2);

    ?>

    <li class="bet-option hide-on-affix" <?php if($bet['odd_value'] == 1){
                        echo 'style="background: rgb(242, 159, 122)"';
                    }?>>
        <div class="bet-cancel">
            <input id="<?php echo $bet['match_id']; ?>" type="submit" value="X" title="Remove Match" onclick="removeMatch(this.id)">
        </div>
        <div class="bet-value">
            <?php if($bet['bet_type'] == "live"): ?>
            <span class="live-bslip">Live&nbsp;</span>
            <?php endif; ?>
            <?php echo $bet['home_team']." v ".$bet['away_team']; ?>
            <br><span class="sp_sport" style=""></span>
        </div>
        
        <div class="row bet-pick">
            <span class="clearfix"><?php echo $bet['odd_type']; ?></span>
            <div class="col-md-12 bet-pick" >
                <span class="picked-odd">Pick: <?= $pick; ?></span>
                <span class="bet-odd" rel="<?= $bet['parent_match_id'].$bet['sub_type_id'].$bet['special_bet_value']?>">
                    <?php echo $bet['odd_value']; ?>
                    <?php if($bet['odd_value'] == 1){
                        echo '<span style="color:#cc0000; font-size:11px;"> Market Disabled</span>';
                    }?>
                </span>
            </div>
        </div>

    </li>
    <?php endforeach; ?>
</div>
<form>
    <table class="bet-table">
        <tbody>

        {{ partial("partials/betslip-generic") }}
        <tr class="possible-win-bg">
            <td>
                <button class="place-bet-btn" type="button" onclick="clearSlip()">REMOVE ALL</button>
            </td>
            <td class="pull-right">
                <button type="button" id="place_bet_button" class="place-bet-btn" onclick="placeBet()">
                    PLACE BET
                </button>
            </td>
        </tr>
        </tbody>
    </table>
    <input type="hidden" id="max_win" value="3000000">
    <input type="hidden" id="minimum_bet" value="2000">
    <input type="hidden" id="user_id" value="{{ session.get('auth')['id'] }}">
    <input type="hidden" id="total_odd" value="<?php echo $totalOdds; ?>">
    <input type="hidden" id="max_multi_games" value="20">
</form>