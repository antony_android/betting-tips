<div class="inner-content whitebg">
<h2>Verify account</h2>
<p>
Thanks for your interest in BET PALACE! Once you fill in the registration form below, you will receive the verification code via SMS. To complete the registration process, please submit the verification code.<br/>
Already got the verification code? Just click HERE to finalize the validation process<br/>
An SMS will be sent to you with your new account information and SMS Shortcode for making your first deposit over the phone.</p>
        
{{ this.flashSession.output() }}
{% if this.flashSession.has('error') %}
<img src="//dsp.eskimi.com/pixel/cookie" style="display:none" />
{% endif  %}
<?php echo $this->tag->form("verify/check"); ?>

 <p>
    <label>Enter Mobile Number *</label>
    <?php echo $this->tag->numericField(["mobile","placeholder"=>"07XX XXX XXX","class"=>"form-control"]) ?>
 </p>

  <p>
    <label>Enter Verification Code *</label>
    <?php echo $this->tag->numericField(["verification_code","placeholder"=>"XXXX","class"=>"form-control"]) ?>
 </p>

  <p>
    <?php echo $this->tag->submitButton(["Verify","class"=>"cg fm"]) ?>
 </p>

</form>
</div>