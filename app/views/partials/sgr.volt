<?php
if (!function_exists('clean'))  
{ 
 function clean($string) {
        $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }
}
?>

<div class="mobi">
     <div id="search-event" class="top-matches match-header events-header ">
        <?php echo $this->tag->form([""]); ?>
            <div class="col-md-6">
                <input type="text" name="keyword" class="form-control" data-action="grow"
               placeholder="Team, Competition or Game ID">
            </div>
            <div class="col-md-2">
                <button type="submit" class="cg fp ">Search</button>
            </div>
        </form>
     </div>
</div>

<div class="matches full-width match-list-games">
    <?php if(empty($games)): ?>

    <div class="empty-list">
    No games found for the SGR. Check different category to find more sports</div>

    <?php else: ?>
    <header class="col-sm-12 top-matches match-header events-header web-element">
        <div class="col-sm-5 left-text p-0 date-header">      
        </div>
        <div class="col-sm-6 mkts-desc-head mgr-5 p-0">3 WAY
        </div>
        <!-- markets description -->
        <div class="col-sm-5 left-text">&nbsp;</div>
        <div class="col-sm-6 mkts-desc mgr-5 p-0">
            <div class="col-sm-4 p-0">HOME</div>
            <div class="col-sm-4 p-0">DRAW</div>
            <div class="col-sm-4 p-0">AWAY</div>
        </div>
        <!-- end markets description -->
    </header>
    <?php endif; ?>

    <div class="middle-content">
        <?php foreach($games as $day): ?>
        <?php
        $theMatch = @$theBetslip[$day['match_id']];
        ?>
        <div class="col-sm-12 top-matches hidden-overflow">

            <div class="col-sm-5 col-xs-11 p-0">
                <div class="compt-detail mobile-view"> 
                    GAME ID: <span class="game-id"> <?php echo $day['game_id']; ?> </span><?php echo $day["category"].' '.$day["competition_name"]; ?> |  <?php echo date('d-m-Y H:i', strtotime($day['start_time'])); ?>
                </div>
                <div class="compt-detail web-element p-3-left">
                    <div class="col-sm-12 p-0">
                        <?php echo $day["category"].' '.$day["competition_name"]; ?></div>
                    <div class="col-sm-3 p-0">ID: <span class="game-id"> <?php echo $day['game_id']; ?></div>
                    <div class="col-sm-9 web-games p-0"><?php echo $day['home_team']; ?></div>
                    <div class="col-sm-12 pt-5"></div>
                    <div class="col-sm-3 p-0"><?php echo date('H:i', strtotime($day['start_time'])); ?></div>
                    <div class="col-sm-9 web-games p-0"><?php echo $day['away_team']; ?></div>
                </div>
                <div class="compt-teams hidden-lg">
                    <?php echo $day['home_team']; ?> 
                    <span class="mobile-view">VS</span>
                    <?php echo $day['away_team']; ?>
                </div>
            </div>
             <div class="col-xs-1 events-odd pad
                <?php
                    if($theMatch && $theMatch['sub_type_id']!=1){
                        echo ' picked';
                    }
                ?>
                mobile-view">
            </div>

            <div class="col-sm-6 col-xs-12 match-div-col mgr-5  p-0" >

                    <div class="col-sm-4 col-xs-4 p-0 match-left-col">
                        <?php if($day['home_odd'] > 0): ?>
                        <button class="home-team <?php echo $day['match_id']; ?> <?php
                            echo clean($day['match_id'].$day['sub_type_id'].$day['home_team']);
                            if($theMatch['bet_pick']==$day['home_team'] && $theMatch['sub_type_id']=='1'){
                            echo ' picked';
                        }
                        ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="1x2" bettype='prematch'
                        awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['home_odd']; ?>"
                        target="javascript:;" odd-key="<?php echo $day['home_team']; ?>" parentmatchid="<?php echo $day['parent_match_id']; ?>"
                        id="<?php echo $day['match_id']; ?>"
                        custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['home_team']); ?>" value="1" special-value-value="0"
                        onClick="addSgrBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))">
                        <span class="odds-btn-label mobile-view">HOME<br/></span>
                        <span class="theodds">
                        <?php echo $day['home_odd']; ?>
                            
                        </span></button>
                        <?php endif ?>
                    </div>

                    <div class="col-sm-4 col-xs-4 events-odd match-div-col match-mid-col" 
                    style="">
                    <?php if($day['neutral_odd'] > 0): ?>
                    <button class="draw <?php echo $day['match_id']; ?> <?php
                        echo clean($day['match_id'].$day['sub_type_id']."draw
                        ");
                        if($theMatch['bet_pick']=='draw' && $theMatch['sub_type_id']=='1'){
                        echo ' picked';
                    }
                    ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="1x2" bettype='prematch'
                    awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['neutral_odd']; ?>"
                    custom="<?php echo clean($day['match_id'].$day['sub_type_id']."draw"); ?>" value="1" odd-key="draw"
                    target="javascript:;" parentmatchid="<?php echo $day['parent_match_id']; ?>"
                    id="<?php echo $day['match_id']; ?>" special-value-value="0"
                    onClick="addSgrBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))"><span
                    class="label label-inverse"> 
                    <span class="odds-btn-label mobile-view">DRAW<br/></span>
                    <?php echo $day['neutral_odd']; ?></span></button>
                    <?php endif ?>
                </div>
                <div class="col-sm-4 col-xs-4 match-div-col match-right-col" style="padding:0;">
                    <?php if($day['away_odd'] > 0): ?>
                    <button class="away-team <?php echo $day['match_id']; ?> <?php echo clean($day['match_id'].$day['sub_type_id'].$day['away_team']);
                        if($theMatch['bet_pick']==$day['away_team'] && $theMatch['sub_type_id']=='1'){
                        echo ' picked';
                    }
                    ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="1x2" bettype='prematch'
                    awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['away_odd']; ?>" value="1"
                    custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['away_team']); ?>" odd-key="<?php echo $day['away_team']; ?>"
                    target="javascript:;" parentmatchid="<?php echo $day['parent_match_id']; ?>"
                    id="<?php echo $day['match_id']; ?>" special-value-value="0"
                    onClick="addSgrBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))">
                    <span class="odds-btn-label mobile-view">AWAY<br/></span>
                    <span class="theodds"> <?php echo $day['away_odd']; ?> </span></button>
                    <?php endif ?>
                </div>
            </div> 
            <!-- end 3 way buttons -->

            <div class="col-sm-1 events-odd pad
                <?php
                    if($theMatch && $theMatch['sub_type_id']!=1){
                        echo ' picked';
                    }
                ?>
                web-element">
            </div>
    </div>

    <?php endforeach; ?>

</div>

</div>