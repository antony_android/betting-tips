
<?php
$totalOdds=1;
?>

<script type="text/javascript">
    var data = <?php echo sizeof($betslipSession) ?: 0; ?>;
    $(".slip-counter").animate({opacity:'0.8'});
    $(".slip-counter").html(data);
    $(".slip-counter").animate({opacity:'1'});
</script>

<div class="bet-body">
        <?php foreach($betslipSession as $bet): ?>

        <?php

        $odd = $bet['odd_value'];

        if($bet['bet_pick']=='x'){
            $pick = 'DRAW';
        }else if($bet['bet_pick']=='2'){
            $pick = $bet['away_team'];
        }else if($bet['bet_pick']=='1'){
            $pick = $bet['home_team'];
        }
        else{
            $pick = $bet['bet_pick'];
        }

        $totalOdds = round($totalOdds*$odd,2);

        ?>

        <li class="bet-option">
            <div class="bet-cancel">
                    <input id="<?php echo $bet['match_id']; ?>" type="submit" value="X" onclick="removeMatch(this.id)">
            </div>
             <div class="bet-value"><?php echo $bet['home_team']." v ".$bet['away_team'] ?>
                    <br><span class="sp_sport" style=""></span>
             </div>
            <div class="bet-pick">
            Pick : <?php echo ucwords($pick); ?>
            <span class="bet-odd"><?php echo $bet['odd_value']; ?></span>
            </div>

         </li>

        <?php endforeach; ?>


</div>
             <form>
             <table class="bet-table">
           <!--      <tr>
                     <td></td>
                     <td><button class="reset-btn" type="button" onclick="javascript: deselect_all();">REMOVE ALL</button></td>
                 </tr>-->
                <tbody>
                <tr>
                    <td>BET AMOUNT</td>
                    <td>
                        <div id="betting">
                            <input class="bet-select" disabled type="text" id="bet_amount" onkeyup="winnings()" minimum="3"   value="1000" min="1000" max="10000" />
                        </div>
                    </td>
                 </tr>

                <tr><td colspan="2"></td></tr>
                <tr>


  <td><button class="place-bet-btn" type="button" onclick="clearSlip()">REMOVE ALL</button></td>
 <td></td>

                                             </tr>
             </tbody></table>

             <div class='alert alert-danger alert-dismissible ss betslip-error' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>You have insufficient balance please top up</div>

             <input type="hidden" id="max_win" value="200000">
             <input type="hidden" id="minimum_bet" value="1000">
             <input type="hidden" id="user_id" value="{{session.get('auth')['id']}}">
             <input type="hidden" id="jackpot_type" value="{{ total_games }}" >
             <input type="hidden" id="jackpot_id" value="{{jackpotID}}" >
             <input type="hidden" id="total_odd" value="<?php echo $totalOdds; ?>">
             <input type="hidden" id="max_multi_games" value="20">

           </form>
