<style>
    .p-a-5 {
        padding: 5px;
    }
    .p-t-5 {
        padding-top: 5px;
    }
</style>
<?php
$totalOdds=1; 
?>

<script type="text/javascript">
    var data = <?php echo sizeof($betslip) ?: 0; ?>;
    $(".slip-counter").animate({opacity:'0.8'});
    $(".slip-counter").html(data);
    $(".slip-counter").animate({opacity:'1'});
</script>

<div class="bet-body">
        <?php foreach((array)$betslip as $bet): ?>

        <?php

        $odd = $bet['odd_value'];

        if($bet['bet_pick']=='x'){
            $pick = 'DRAW';
        }else if($bet['bet_pick']=='2'){
            $pick = $bet['away_team'];
        }else if($bet['bet_pick']=='1'){
            $pick = $bet['home_team'];
        }
        else{
            $pick = $bet['bet_pick'];
        }

        $totalOdds = round($totalOdds*$odd,2);

        ?>

        <li class="bet-option">
            <div class="bet-cancel">
                    <input id="<?php echo $bet['match_id']; ?>" type="submit" value="X" onclick="removeMatch(this.id)">
            </div>
             <div class="bet-value"><?php echo $bet['home_team']." v ".$bet['away_team'] ?>
                    <br><span class="sp_sport" style=""></span>
             </div>
            <div class="bet-pick">
            Pick : <?php echo ucwords($pick); ?>
            </div>

         </li>

        <?php endforeach; ?>


</div>
             <form>
                 <table class="bet-table">
                     <!--      <tr>
                               <td></td>
                               <td><button class="reset-btn" type="button" onclick="javascript: deselect_all();">REMOVE ALL</button></td>
                           </tr>-->
                     <tbody>
                     <tr>
                         <td>BET AMOUNT</td>
                         <td>
                             <div id="betting">
                                 <input class="bet-select" disabled type="text" id="bet_amount" onkeyup="winnings()"
                                        minimum="3" value="1000" min="1000" max="10000"/>
                             </div>
                         </td>
                     </tr>
                     <tr><td colspan="2"></td></tr>
                     <tr class="hide-on-affix">
                         <td colspan="2" style="padding-left: 5px">
                             <table>
                                 {{ partial("partials/payment-methods") }}
                             </table>
                         </td>
                     </tr>
                     <tr>
                         <td colspan="2" id="wallet-phone-number">
                             <label>Mobile Number *</label>
                             <?php echo $this->tag->numericField(["msisdn","placeholder"=>"0XXX XXX XXX","class"=>""]) ?>
                         </td>
                     </tr>
                     <tr class="p-t-5">
                         <td class="p-t-5">
                             <button class="place-bet-btn" type="button" onclick="clearSlip()">REMOVE ALL</button>
                         </td>
                         <td class="p-t-5">
                             <button type="button" id="place_bet_button" class="place-bet-btn" onclick="jackpotBet()">PLACE BET</button>
                         </td>
                     </tr>
                     </tbody>
                 </table>

             <div class='alert alert-danger alert-dismissible ss betslip-error' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>You have insufficient balance please top up</div>

             <input type="hidden" id="max_win" value="200000">
             <input type="hidden" id="minimum_bet" value="1000">
             <input type="hidden" id="user_id" value="{{session.get('auth')['id']}}">
             <input type="hidden" id="jackpot_type" value="5" >
             {#<input type="hidden" id="jackpot_id" value="{{ jackpot_id }}" >#}
             <input type="hidden" id="total_odd" value="<?php echo $totalOdds; ?>">
             <input type="hidden" id="max_multi_games" value="20">

           </form>
