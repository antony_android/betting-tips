  {{ javascript_include('js/one_livegame.js') }}

  <?php
  if(!empty($matchInfo))
  { 
    ?>
    <div class="panel-header primary-bg match-header">
      <?php 
      function clean($string) {
           $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
           $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

           return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
         }
         ?> 
         <div class="col-sm-12 nopadding left-text">
         <div class="col-md-3 col-xs-3 nopadding">
            <span class="live-header">Live</span>
            GAME ID: <?php echo $matchInfo['game_id']; ?>
          </div>
          <div class="col-md-4 col-xs-4 nopadding">
            <?php echo date('d/m/y H:i', strtotime($matchInfo['start_time'])); ?>
          </div>
          <div class="compt-detail col-md-5 col-xs-5 nopadding"> 
            <?php echo $matchInfo["category"]; ?> - 
            <?php echo $matchInfo["competition_name"]; ?></div>
          </div>

          <?php $awayteam = $matchInfo['away_team']; ?> 
        </span>
      </div>
      <?php
    }
    ?>

    <?php 
    $sub_type_id=''; 
    ?>

  <div class="col-sm-12 nopadding left-text">
    <div id="live_matches" class="col-sm-4 web-element top-matches live-match"> 
    
    <?php foreach($liveMatches as $match): ?>
    <a href="/livematch?id=<?= $match['match_id']; ?>">
    <div class="one-live">
    <span style="padding:0;">
    <img height="15" src="<?=$match['flag_url'] ?>"> 
    </span>
    <span class="live-header">Live</span>
    <span class="live-competition"><?= $match['sport_name']; ?></span> - 
    <span><?= $match['category']; ?></span> - 
    <span><?= $match['competition_name']; ?></span>
    <div style="border-bottom:1px solid #FFF;"></div>
    <span class="live-team"><?= $match['home_team']; ?></span>
    VS
    <span class="live-team"><?= $match['away_team']; ?></span>
    </div>
    </a>
    <?php endforeach; ?>
    
    </div>
    <div class="col-sm-8 col-xs-12 top-matches live-match">
      <?php $match_time= $matchInfo['match_time']; ?>
      <?php $splitTime = explode (":", $match_time); ?>
      <?php $displayTime = $splitTime[0]; ?>
      <?php if($matchInfo['match_status'] == "Halftime"){
          $displayTime = "Halftime";
      }else{

        $displayTime = $displayTime."'";
      }?>
      <div class="live-placeholder web-element">
        <div class="col-md-5 live-home"><?php echo $matchInfo['home_team']; ?> </div>
        <div class="col-md-2 live-score-wrapper"><div class="live-goals">
        <?php echo $matchInfo['score']; ?></div>
        <div class="live-score-div">
        <?php echo $displayTime; ?>
        </div> </div>
        <div class="col-md-5 live-away"><?php echo $matchInfo['away_team']; ?> </div>
        <div class="col-md-12">
        <div class="col-md-4"> </div>
        <div class="col-md-4 live-match_status"><?php echo $matchInfo['match_status']; ?></div>
        <div class="col-md-4"> </div>
        </div>
        <div class="col-md-12 live-innerplaceholder">
          <div class="col-md-12 stats-wrapper">
            <div class="col-md-2"></div>
            <div class="col-md-1 counter-live-left home_corner"><?php echo $matchInfo['home_corner']; ?></div>
            <div class="col-md-6 stats-bg">Corners</div>
             <div class="col-md-2"></div>
            <div class="col-md-1 counter-live-right away_corner"><?php echo $matchInfo['away_corner']; ?></div>
          </div>
          <div class="col-md-12 stats-wrapper">
            <div class="col-md-2"></div>
            <div class="col-md-1 counter-live-left home_yellow_card"><?php echo $matchInfo['home_yellow_card']; ?></div>
            <div class="col-md-6 stats-bg yellow-cards">Yellow Cards</div>
            <div class="col-md-2"></div>
            <div class="col-md-1 counter-live-right away_yellow_card"><?php echo $matchInfo['away_yellow_card']; ?></div>
          </div>
          <div class="col-md-12 stats-wrapper">
            <div class="col-md-2"></div>
            <div class="col-md-1 counter-live-left home_red_card"><?php echo $matchInfo['home_red_card']; ?></div>
            <div class="col-md-6 stats-bg">Red Cards</div>
            <div class="col-md-2"></div>
            <div class="col-md-1 counter-live-right away_red_card"><?php echo $matchInfo['away_red_card']; ?></div>
          </div>
        </div>

      </div>

      <div class="live-placeholder mobi">

          <div class="row">
            <div id="mobi-teams" class="col-xs-12 mobi-grey">
              <?php echo $matchInfo['home_team']; ?>
               vs 
               <?php echo $matchInfo['away_team']; ?>
            </div>
          </div>
          <div class="row">
            <div id="mobi-match-status" class="col-xs-5 match-status">
              <?php echo $matchInfo['match_status']; ?>
            </div>
            <div class="col-xs-2 match-time-pd">
              <div id="mobi-match-time" class="mobi-match-time">
                <?php echo $displayTime; ?>
              </div>
            </div>
            <div id="mobi-match-score" class="col-xs-5 match-score">
               <?php echo $matchInfo['score']; ?>
            </div>
          </div>
          <div class="row">
            <div id="mobi-home-corner" class="col-xs-2">
              <?php echo $matchInfo['home_corner']; ?>
            </div>
            <div class="col-xs-8">
               Corners
            </div>
            <div id="mobi-away-corner" class="col-xs-2">
              <?php echo $matchInfo['away_corner']; ?>
            </div>
          </div>
          <div class="row">
            <div id="mobi-home-yellow-card" class="col-xs-2">
              <?php echo $matchInfo['home_yellow_card']; ?>
            </div>
            <div class="col-xs-8 yellow-cards-mobi">
               Yellow Cards
            </div>
            <div id="mobi-away-yellow-card" class="col-xs-2">
              <?php echo $matchInfo['away_yellow_card']; ?>
            </div>
          </div>
          <div class="row">
            <div id="mobi-home-red-card" class="col-xs-2">
              <?php echo $matchInfo['home_red_card']; ?>
            </div>
            <div class="col-xs-8 red-cards-mobi">
               Red Cards
            </div>
            <div id="mobi-away-red-card" class="col-xs-2">
              <?php echo $matchInfo['away_red_card']; ?>
            </div>
          </div>
      </div>

      <div class="match-detail">
      <?php if(count($subTypes) > 0): ?>
      <?php foreach($subTypes as $bt): ?>
        <?php 
        $theMatch = @$theBetslip[$bt['match_id']];
        ?>
        <?php if($sub_type_id!=$bt['sub_type_id']): ?>
          <?php $iteration = 0; ?>

          <?php $market = ucwords($bt['name']); ?>

          <div class="col-sm-12 top-matches header yellow"><?php echo str_replace("Total", "Over/Under", $market);  ?></div>

        <?php endif; ?>

        <?php 
        if($bt['name']){
          $sub_type_id=$bt['sub_type_id']; 
          $special_bet_value=$bt['special_bet_value']; 
        }
        ?>

        <?php if($sub_type_id==$bt['sub_type_id']): ?>
          <?php $iteration += 1; ?>

          <?php 
          if(($bt['odd_key'] == 'draw' || $bt['odd_key'] == $awayteam) && $iteration == 1) {
            $temp = $bt;
            continue;
          }

          ?>
            <?php $odd_key = $bt['odd_key'].'_'.$bt['sub_type_id']; ?>
            <button class="pick market-butt an-mkt col-md-6 col-xs-6 odds <?php echo $bt['match_id']; ?> <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$special_bet_value); 
            if($theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
              echo ' picked';
            }
            ?>" rel="<?php echo $bt['sub_type_id']; ?>" oddtype="<?= $bt['name'] ?>" parentmatchid="<?php echo $matchInfo['parent_match_id']; ?>" bettype='live' hometeam="<?php echo $matchInfo['home_team']; ?>" awayteam="<?php echo $matchInfo['away_team']; ?>" oddvalue="<?php echo $bt['odd_value']; ?>" custom="<?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$special_bet_value); ?>" target="javascript:;" id="<?php echo $bt['match_id']; ?>" market_id="<?php echo $odd_key; ?>" odd-key="<?php echo $bt['odd_key']; ?>" value="<?php echo $bt['sub_type_id']; ?>" special-value-value="<?= $special_bet_value ?>" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))" title="<?php echo ucwords($bt['display']); ?>"><span class="label label-inverse blueish"> <?php echo ucwords($bt['display']); ?></span> <span class="label label-inverse blueish odd-value"> <?php echo '<span> </span>'.$bt['odd_value']; ?></span> </button>

          <?php if(isset($temp)): ?>

              <?php $odd_key = $temp['odd_key'].'_'.$temp['sub_type_id']; ?>

              <button class="pick market-butt col-md-6 col-xs-6 odds an-mkt <?php echo $temp['match_id']; ?> <?php echo clean($temp['match_id'].$temp['sub_type_id'].$temp['odd_key'].$special_bet_value); 
              if($theMatch['bet_pick']==$temp['odd_key'] && $theMatch['sub_type_id']==$temp['sub_type_id'] && $theMatch['special_bet_value']==$temp['special_bet_value']){
                echo ' picked';
              }
              ?>" rel="<?php echo $bt['sub_type_id']; ?>" oddtype="<?= $temp['name'] ?>" parentmatchid="<?php echo $matchInfo['parent_match_id']; ?>" bettype='live' hometeam="<?php echo $matchInfo['home_team']; ?>" awayteam="<?php echo $matchInfo['away_team']; ?>" oddvalue="<?php echo $temp['odd_value']; ?>" custom="<?php echo clean($temp['match_id'].$temp['sub_type_id'].$temp['odd_key'].$special_bet_value); ?>" target="javascript:;" id="<?php echo $temp['match_id']; ?>" market_id="<?php echo $odd_key; ?>" odd-key="<?php echo $temp['odd_key']; ?>" value="<?php echo $temp['sub_type_id']; ?>" special-value-value="<?= $special_bet_value ?>" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))" title="<?php echo ucwords($temp['display']); ?>"><span class="label label-inverse blueish"> <?php echo ucwords($temp['display']); ?></span> <span class="label label-inverse blueish odd-value"> <?php echo '<span> </span>'.$temp['odd_value']; ?></span> </button>
            <?php unset($temp) ?>
          <?php endif; ?>
        <?php endif; ?>

      <?php endforeach; ?>
      <?php else: ?>
      <div>
        No open markets for this live game at the moment!
      </div>
      <?php endif; ?>
      </div>
    </div>
  </div>
  <div id="active-live-match" rel="<?php echo $matchInfo['match_id']; ?>"></div>