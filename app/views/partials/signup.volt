<div class="panel-header"><h2>Register Now</h2></div>
<div class="inner-content whitebg">
<h4>Welcome to Bet Palace</h4>
<p>Please fill in the registration form below and submit. You will receive a verification code which you will use to verify your account and start betting with Bet Palace</p>
	<?php $this->flashSession->output(); ?>

<?php echo $this->tag->form("signup/join"); ?>

<div class="hidden-overflow">
<div class="col-sm-12 no-pad-left">
   <p>
    <label>Mobile Number *</label>
    <?php echo $this->tag->textField(["mobile","placeholder"=>"07XX XXX XXX","class"=>"form-control msisdn-f"]) ?>
 </p>
 </div>
	<div class="col-sm-12 no-pad-left">
	 <p>
	    <label>Password *</label>
	    <?php echo $this->tag->passwordField(["password","name"=>"password","class"=>"form-control","placeholder"=>"Password"]) ?>
	 </p>
	</div>

	 <!--
	<div class="col-sm-12 md-screen-no-pad-left mobile-pad-right no-pad-right">
	  <p>
	  <label>Confirm Password *</label>
	    <?php echo $this->tag->passwordField(["password","name"=>"repeatPassword","class"=>"form-control","placeholder"=>"Confirm password"]) ?>
	 </p>
	</div>
	-->
</div>
 <p>
 <!--
<input type="checkbox" name="terms" value="1"> I accept these <a href="{{url('terms')}}" style="padding-left:0;display:inline;">Terms & Conditions</a> * </input>
<br/>
<input type="checkbox" name="age" value="1"> I'm over 18 years of age * </input>
-->
By signing up, you agree that you are above 18 years of age and that you accept our <a class="sign-upterms" href="/terms"> terms and conditions</a>
 </p>

 <p>
    <?php echo $this->tag->submitButton(["Register","class"=>"cg fm"]) ?>
 </p>

</form>
</div>
