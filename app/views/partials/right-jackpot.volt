<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<div class="gn" id="right-generic">

<div class='alert alert-success alert-dismissible betslip-success' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>Bet successfully placed!</div>
    <ul class="bet-option-list">
<div id='slip-holder' class="bet alu">
<header><div class="betslip-header"><span class="col-sm-10 slp">Jackpot Betslip</span><span class="col-sm-2 slip-counter"><?= $slipCountJ ?></span></div></header>
<img src="{{url('img/loader.gif')}}" class="loader" />
<button id="slip-button-close" type="button" class="close mobi" aria-hidden="true">×</button>
<div id="betslipJ" class="betslipJ ">

</div>

<div id="quick-login">

<p>Please login to place a bet</p>

<?php echo $this->tag->form("login/authenticate"); ?>

 <p>
    <label>Mobile Number *</label>
    <?php echo $this->tag->numericField(["mobile","placeholder"=>"0XXX XXX XXX","class"=>"form-control"]) ?>
 </p>

 <p>
    <label>Password *</label>
    <?php echo $this->tag->passwordField(["password","name"=>"password","class"=>"form-control","placeholder"=>"Password"]) ?>
 </p>

  <div class="col-sm-12 zero-padding">
    <div class="col-sm-4 zero-padding"><?php echo $this->tag->submitButton(["Login","class"=>"cg fm"]) ?></div>
    <div class="col-sm-8 zero-padding"><a href="{{ url('signup') }}">Join now</a></div>
 </div>

</form>
</div>

</div>
<!-- <div class="qv rc alu bon">
  <a href="{{url('signup')}}"><img src="{{url('https://s3-eu-west-1.amazonaws.com/bikosport/img/join-bonus.jpg')}}" class="full-width" /></a>
</div> -->
<div class="qv rc alu">
<div class="qv rc alu paybill block-shadow bottom-std-margin-spacing">
<div class="std-block-head bold">Paybill Numbers</div>

<div class="warning-txt"></div>

        <div class="sidebar company-number bold">
            <ul>
                <li class="safaricom">
                    <span class="paybill-images" style="display: block; background-image: url('/img/paybills/safaricom.png');"></span>
                    <span class="">290085</span>
                </li>
                <li class="airtel">
                    <span class="paybill-images" style="display: block; background-image: url('/img/paybills/airtel.png');"></span>
                    <span>BETPALACE</span>
                </li>
            </ul>
        </div>

        <div class="payment-selector std-side-pads no-transform">


        </div>
        <div class="support">
            <div class="contact-us black-bg capitalize">Support</div>
            <div class="helpline">
                <span class="col-sm-12">254 705290085</span>
                
            </div>
            <div class="mail">
            support@betpalace.co.ke
            </div>
        </div>
    </div>
</div>

</div>
