<div class="panel-header">
    <h2>Registration</h2>
</div>
<div class="inner-content whitebg">
<p class="static-header"> <strong>VIA SMS </strong></p>
<p> Send the word BETPALACE to <b><span class="shortcode">29085</span></b>. You will receive a welcome message and a link for the website. Once you have registered you may bet from either web or SMS </p>

<p class="static-header"> <b>VIA WEB </b></p>
<p>Click on signup link on <b>betpalace.co.ke</b>. Enter yoour details and submit. You will receice a verification SMS which you will use to verify your account </p>
</div>

<div class="panel-header">
    <h2>TOP UP ACCOUNT</h2>
</div>
<div class="inner-content whitebg">
<p class="static-header"> <strong>VIA MPESA </strong></p>
<p> Go to your MPESA Menu and Select "Lipa na MPESA". Use Business Number <b><span class="shortcode">290085</span></b> and Account Number <span class="shortcode">BETPALACE</span>. Enter the amount you wish to topup and proceed to complete the MPESA deposit process normally.  Your BETPALACE wallet account will be credited with the topped up amount.</p>

</div>

<div class="panel-header">
    <h2>PLACE BET</h2>
</div>
<div class="inner-content whitebg">
<p class="static-header"> <strong>VIA SMS </strong></p>
<p>Look out for GAME ID of your favorite match on betpalace.co.ke or from SMS sent to your mobile.</p>
<p><b>Single Bet: </b> SMS GAMEID#PICK#STAKE to <span class="shortcode">29085</span> to place bet worth <i>STAKE</i> on <i>GAME ID</i> e.g 2345#1#100 to bet for a home win with KES 100 on GAME ID 2345 </p>
<p><b>Multi-bet: </b> SMS GAMEID1#PICK1#GAMEID2#PICK2#GAMEID3#PICK3#GAMEID4#PICK4#STAKE to <span class="shortcode">29085</span> to place multibet bet worth <i>STAKE</i> on <i>GAME ID1 ... GAMEID4</i> e.g 2345#1#5689#2#100 to bet for KES 100 on GAME ID 2345, 5689 </p>
<p>
<b>NB:</b> 1 = Home Team, X = Draw, 2 = Away Team, OV25 = Over 2.5, UN25 = Under 2.5, 1X = Home or Draw, X2 = Draw or Away Team, 12 = Home Team or Away Team
</p>
<p class="static-header"> <strong>VIA WEB </strong></p>
<p>On betpalace.co.ke select preferred matches, click on PLACE BET under BETSLIP tag. You will be required to login to complete your bet. </p>

</div>

<div class="panel-header">
    <h2>WITHDRAWAL</h2>
</div>
<div class="inner-content whitebg" style="margin-bottom: 5px;">
<p class="static-header"> <strong>VIA SMS / WEB </strong></p>
<p>SMS WITHDRAW#AMOUNT to <span class="shortcode">29085</span> or click on withdraw from betpalace.co.ke website. You will receive instructions on how to withdraw cash from your BETPALACE wallet</p>


</div>
