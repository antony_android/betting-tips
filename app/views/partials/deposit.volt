{% if session.get('auth') != null %}
<div class="">
<div class="panel-header">
        <h2>Make Instant Deposit</h2>
</div>
<div class="table-responsive inner-content whitebg ">
<p>Enter amount below:</p>

        {{ this.flashSession.output() }}

<?php echo $this->tag->form("/deposit"); ?>

 <p>
    <label>Amount (KSh.) *</label>
    <?php echo $this->tag->numericField(["amount","placeholder"=>"KSh. ","class"=>"form-control","required"=>"required"]) ?>
 </p>

<p>
    <?php echo $this->tag->submitButton(["Deposit","class"=>"cg fm"]) ?>
 </p>

</form>
</div>
</div>
{% else %}

<div class="panel-header"><h2>DEPOSIT</h2></div>
<div class="odd-wrapper">

    <div class="nav-side">

      <button class="deposit" style="background-color: #EF4323;width: 100%;">How to Top Up Your Account</button>
	  <div id="definitions">
	    <div class="bigger-list">

		<p> 1. Go to MPESA on your phone and select "Lipa Na MPESA"</p>

		<p> 2. Select "Pay Bill" </p>

		<p> 3. Select "Enter Business No."</p>

		<p> 4. Key in 290085 as the Business Number and Click "OK"</p>
		
		<p> 5. Select "Account no."</p>

		<p> 6. Key in BETPALACE as the Account no. and Click "OK"</p>

		<p> 7. Key in the amount you wish to topup e.g. 20 and Click "OK"</p>

		<p> 8. Proceed to specify your MPESA PIN and Click "OK"</p>

		<p> 9. Confirm your transaction </p>

		<p> 10. Your BET PALACE Account will be successfully credited with the specified amount </p>

		</div>
	  </div>

	  <button class="deposit" style="background-color: #EF4323;width: 100%;">Jinsi ya Kuweka Pesa kwenye Akaunti Yako</button>
	  <div id="bets">
	    <div class="bigger-list">

		<p> 1. Tumia Simu yako kuchagua Menu ya MPESA</p>

		<p> 2. Chagua "Lipa Na MPESA"</p>

		<p> 2. Chagua "Pay Bill" </p>

		<p> 3. Chagua "Enter Business No."</p>

		<p> 4. Weka 290085 kama Business Number Kisha ufinye "OK"</p>
		
		<p> 5. Chagua "Account no."</p>

		<p> 6. Weka BETPALACE kama Account no. Kisha ufinye "OK"</p>

		<p> 7.Weka kiwango cha pesa unachotaka, kwa mfano, 20 Kisha ufinye "OK"</p>

		<p> 8. Endelea kwa kuweka namba yako ya siri ya MPESA (yaani PIN) Kisha ufinye "OK"</p>

		<p> 9. Thibitisha </p>

		<p> 10. Akaunti yako ya BETPALACE itapokea kiwango cha pesa ulichokichagua </p>

		</div>
	  
	  </div>

     </div>

</div>
{%endif%}
