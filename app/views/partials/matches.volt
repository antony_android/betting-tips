
<?php
      function clean($string) {
         $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
         $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

         return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
      }
    ?>

<div class="web-element">
<?php foreach($today as $day): ?>

<?php
   $theMatch = $theBetslip[$day['match_id']];
?>

<div class="col-sm-12 top-matches">
   <div class="col-sm-2 pad"><?php echo date('H:i', strtotime($day['start_time'])); ?></div>
   <div class="col-sm-2 events-odd pad"><?php echo $day['game_id']; ?></div>
   <div class="col-sm-3" style="padding:0;">
      <button class="<?php echo $day['match_id']; ?> <?php
                  echo clean($day['match_id'].$day['sub_type_id'].$day['home_team']);
                     if($theMatch['bet_pick']==$day['home_team'] && $theMatch['sub_type_id']=='1'){
                        echo ' picked';
                     }
                  ?>" target="javascript:;" odd-key="<?php echo $day['home_team']; ?>" id="<?php echo $day['match_id']; ?>" custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['home_team']); ?>" value="1" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'))"><span class="theteam col-sm-9"><?php echo $day['home_team']; ?></span> <span class="theodds col-sm-3"><?php echo $day['home_odd']; ?></span></button>
   </div>
   <div class="col-sm-1 events-odd" style="padding:0;">
   <button class="<?php echo $day['match_id']; ?> <?php
                   echo clean($day['match_id'].$day['sub_type_id']."draw");
                     if($theMatch['bet_pick']=='draw' && $theMatch['sub_type_id']=='1'){
                        echo ' picked';
                     }
                   ?>" custom="<?php echo clean($day['match_id'].$day['sub_type_id']."draw"); ?>" value="1" odd-key="draw" target="javascript:;" id="<?php echo $day['match_id']; ?>" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'))"><span class="label label-inverse"> <?php echo $day['neutral_odd']; ?></span></button>
   </div>
   <div class="col-sm-3" style="padding:0;">
      <button class="<?php echo $day['match_id']; ?> <?php echo clean($day['match_id'].$day['sub_type_id'].$day['away_team']);
                  if($theMatch['bet_pick']==$day['away_team'] && $theMatch['sub_type_id']=='1'){
                        echo ' picked';
                     }
                   ?>" value="1" custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['away_team']); ?>" odd-key="<?php echo $day['away_team']; ?>" target="javascript:;" id="<?php echo $day['match_id']; ?>" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'))"><span class="theteam col-sm-9"> <?php echo $day['away_team']; ?> </span><span class="theodds col-sm-3"> <?php echo $day['away_odd']; ?></span></button>
   </div>
   <div class="col-sm-1 events-odd pad
   <?php
   if($theMatch['sub_type_id']!=10){
                        echo ' picked';
                     }
   ?>
   "><a class="side" href="<?php echo 'match?id='.$day['match_id']; ?>">+<?php echo $day['side_bets']; ?> </a></div>
</div>

<?php endforeach; ?>

</div>

<ul class="mobi-list">
<?php foreach($today as $day): ?>

<?php
      $theMatch = $theBetslip[$day['match_id']];
?>
      <li class="col-sm-12 top-matches mobi">

            <div class="teams highlited" style="">
            <span class="bold"><?php echo $day['home_team']; ?></span> &nbsp; v  &nbsp; <span class="bold"><?php echo $day['away_team']; ?></span>
            </div>

            <div class="col-sm-1" style="display: inline-block;font-size: 13px;float: right;">
            <a class="side" href="<?php echo 'match?id='.$day['match_id']; ?>">+<?php echo $day['side_bets']; ?> </a>
            </div>


            <div class="meta">
            <?php echo date('d/m', strtotime($day['start_time'])); ?>
            <?php echo date('H:i', strtotime($day['start_time'])); ?>
            Game ID: <?php echo $day['game_id']; ?>
            </div>

            <div class="row">
            <div class="col-sm-12">
                  <button class="odds home <?php echo $day['match_id']; ?> <?php echo clean($day['match_id'].$day['sub_type_id'].$day['home_team']);
                  if($theMatch['bet_pick']==$day['home_team'] && $theMatch['sub_type_id']=='1'){
                        echo ' picked';
                     }
                  ?>" target="javascript:;" odd-key="<?php echo $day['home_team']; ?>" id="<?php echo $day['match_id']; ?>" custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['home_team']); ?>" value="1" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'))"><span class="label label-inverse"><?php echo substr($day['home_team'], 0, 10)."..."; ?> <span class="mobi-odd"><?php echo $day['home_odd']; ?></span></span></button>

                  <button class="odds neutral <?php echo $day['match_id']; ?> <?php echo clean($day['match_id'].$day['sub_type_id']."draw");
                     if($theMatch['bet_pick']=='draw' && $theMatch['sub_type_id']=='1'){
                        echo ' picked';
                     }
                   ?>" custom="<?php echo clean($day['match_id'].$day['sub_type_id']."draw"); ?>" value="1" odd-key="draw" target="javascript:;" id="<?php echo $day['match_id']; ?>" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'))"><span class="label label-inverse"> X <span class="mobi-odd"><?php echo $day['neutral_odd']; ?></span></span></button>
                  <button class="odds away <?php echo $day['match_id']; ?> <?php echo clean($day['match_id'].$day['sub_type_id'].$day['away_team']);
                     if($theMatch['bet_pick']==$day['away_team'] && $theMatch['sub_type_id']=='1'){
                        echo ' picked';
                     }
                   ?>" value="1" custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['away_team']); ?>" odd-key="<?php echo $day['away_team']; ?>" target="javascript:;" id="<?php echo $day['match_id']; ?>" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'))"><span class="label label-inverse"> <?php echo substr($day['away_team'], 0, 10)."..."; ?> <span class="mobi-odd"><?php echo $day['away_odd']; ?></span></span></button>
                  </div>
            </div>
            </li>
<?php endforeach; ?>
</ul>