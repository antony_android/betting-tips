<?php $totalOdds=1; ?>
<div class="bet-body">
    <?php
    
    $betslip = $this->session->get('betslip-peer');

    $betslip = is_array($betslip) ? $betslip : [];

    $matchCount = 0;
    $bonus = 0;
    $bonusOdds = 0;
    ?>
    <?php foreach($betslip as $bet): ?>
    <?php
        $odd = $bet['odd_value'];

        if($bet['bet_pick']=='x'){
            $pick = 'DRAW';
        }else if($bet['bet_pick']=='2'){
            $pick = $bet['away_team'];
        }else if($bet['bet_pick']=='1'){
            $pick = $bet['home_team'];
        }
        else{
            $pick = $bet['bet_pick'];
        }

         $pick_display = $bet['pick_display'];

        if($bet['odd_value']>=1.6){
            $bonusOdds*=$odd;
            $matchCount++;
        }

        $totalOdds = round($totalOdds*$odd,2);

    ?>

    <li class="bet-option hide-on-affix">
        <div class="bet-cancel">
            <input id="<?php echo $bet['match_id']; ?>" type="submit" value="X" title="Remove Match" onclick="removePeerMatch(this.id)">
        </div>
        <div class="bet-value">
            <?php echo $bet['home_team']." v ".$bet['away_team']; ?>
            <br><span class="sp_sport" style=""></span>
        </div>
        
        <div class="bet-pick">

            <div class="col-md-4 p-0">
                Pick
            </div>
            <div class="col-md-8 slip-pull-right team-picked">
                <?= ucwords($pick_display); ?>
            </div>
        </div>
        <div class="bet-pick">
            <div class="col-md-4 p-0">
                Stake
            </div>
            <div class="col-md-8 slip-pull-right">
                <span id="peer_bet_amt"><?php echo $bet['bet_amount']; ?></span>
            </div>
        </div>

        <div class="bet-pick">
            <div class="col-md-4 p-0">
                Commission
            </div>
            <div class="col-md-8 slip-pull-right">
                <?php echo $bet['bet_amount'] * 2 * 0.1; ?>
            </div>
        </div>

        <div class="bet-pick odds-bg">
            <div class="col-md-5 p-0">
                Possible Win
            </div>
            <div class="col-md-7 slip-pull-right">
                <?php echo $bet['bet_amount'] * 2 - $bet['bet_amount'] * 2* 0.1; ?>
            </div>
        </div>

    </li>
    <?php endforeach; ?>
</div>
<form>
    <table class="bet-table">
        <tbody>

        <tr class="possible-win-bg">
            <td class="pull-right">
                <button type="button" id="place_bet_button" class="place-bet-btn" onclick="placePeerBet()">
                    PLACE BET
                </button>
            </td>
        </tr>
        </tbody>
    </table>
    <div class='alert alert-danger alert-dismissible ss betslip-error' role='alert'>
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
        </button>
        You have insufficient balance. Please top up
    </div>
    <input type="hidden" id="user_id" value="{{ session.get('auth')['id'] }}">
    <input type="hidden" id="max_win" value="3000000">
    <input type="hidden" id="minimum_bet" value="2000">
    <input type="hidden" id="peer_bet_id" value="<?php echo $bet['bet_id']; ?>">
    <input type="hidden" id="total_odd" value="<?php echo $totalOdds; ?>">
    <input type="hidden" id="max_multi_games" value="20">
</form>