<div class="panel-header">
	<h2>Login to Place a Bet</h2>
</div>
<div class="inner-content whitebg">


        {{ this.flashSession.output() }}

<?php echo $this->tag->form("login/authenticate"); ?>

 <p>
    <label>Mobile Number *</label>
    <?php echo $this->tag->textField(["mobile","placeholder"=>"07XX XXX XXX","class"=>"form-control msisdn-f"]) ?>
 </p>

 <p>
    <label>Password *</label>
    <?php echo $this->tag->passwordField(["password","name"=>"password","class"=>"form-control","placeholder"=>"Password"]) ?>
 </p>

 <?php $ref = "" ?>

 {% if session.get('ref') != null %}

 <?php  $ref = $this->session->get('ref'); ?>

 {% endif %}

<p>
  <input type="checkbox" name="remember" value="1"> Remember me</input>
  <input type="hidden" name="remember" value="{{refURL}}">
  <input type="hidden" name="ref" value="{{ref}}">
 </p>

  <p>
    <?php echo $this->tag->submitButton(["Login","class"=>"cg fm"]) ?> <a class="forgot" href="{{ url('reset') }}"><span class="sticky-hidden">Forgotten password?</span></a>
 </p>

</form>
</div>
