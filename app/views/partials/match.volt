  <?php
  if(!empty($matchInfo))
  { 
    ?>
    <div class="panel-header primary-bg match-header">
      <?php 
      function clean($string) {
           $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
           $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

           return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
         }
         ?> 
         <div class="col-sm-12 nopadding left-text">

         <div class="col-md-3 col-xs-3 nopadding">
            GAME ID: <?php echo $matchInfo['game_id']; ?>
          </div>
          <div class="col-md-4 col-xs-4 nopadding">
            <?php echo date('d/m/y H:i', strtotime($matchInfo['start_time'])); ?>
          </div>
          <div class="compt-detail col-md-5 col-xs-5 nopadding"> 
            <?php echo $matchInfo["category"]; ?> - <?php echo $matchInfo["competition_name"]; ?></div>

          </div>
          <h4 class="inline-block match-team"> <?php echo $matchInfo['home_team']." vs ".$matchInfo['away_team']; ?> </h4>

          <?php $awayteam = $matchInfo['away_team']; ?> 
        </span>
      </div>
      <?php
    }
    ?>

    <?php 
    $sub_type_id=''; 
    ?>
    <div class="col-sm-12 top-matches match"> 
      <?php foreach($subTypes as $bt): ?>
        <?php 
        $theMatch = @$theBetslip[$bt['match_id']];
        ?>
        <?php if($sub_type_id!=$bt['sub_type_id']): ?>
          <?php $iteration = 0; ?>

          <?php $market = ucwords($bt['name']); ?>

          <div class="col-sm-12 top-matches header yellow"><?php echo str_replace("Total", "Over/Under", $market);  ?></div>

        <?php endif; ?>

        <?php 
        if($bt['name']){
          $sub_type_id=$bt['sub_type_id']; 
          $special_bet_value=$bt['special_bet_value']; 
        }
        ?>

        <?php if($sub_type_id==$bt['sub_type_id']): ?>
          <?php $iteration += 1; ?>

          <?php 
          if(($bt['odd_key'] == 'draw' || $bt['odd_key'] == $awayteam) && $iteration == 1) {
            $temp = $bt;
            continue;
          }

          ?>

          <div class="match-detail">

            <button class="pick col-sm-4 odds <?php echo $bt['match_id']; ?> <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$special_bet_value); 
            if($theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
              echo ' picked';
            }
            ?>" oddtype="<?= $bt['name'] ?>" parentmatchid="<?php echo $matchInfo['parent_match_id']; ?>" bettype='prematch' hometeam="<?php echo $matchInfo['home_team']; ?>" awayteam="<?php echo $matchInfo['away_team']; ?>" oddvalue="<?php echo $bt['odd_value']; ?>" custom="<?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$special_bet_value); ?>" target="javascript:;" id="<?php echo $bt['match_id']; ?>" odd-key="<?php echo $bt['odd_key']; ?>" value="<?php echo $bt['sub_type_id']; ?>" special-value-value="<?= $special_bet_value ?>" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))" title="<?php echo ucwords($bt['display']); ?>"><span class="label label-inverse blueish"> <?php echo ucwords($bt['display']); ?></span> <span class="label label-inverse blueish odd-value"> <?php echo $bt['odd_value']; ?></span> </button>
          </div>
          <?php if(isset($temp)): ?>
            <div class="match-detail">

              <button class="pick col-sm-4 odds <?php echo $temp['match_id']; ?> <?php echo clean($temp['match_id'].$temp['sub_type_id'].$temp['odd_key'].$special_bet_value); 
              if($theMatch['bet_pick']==$temp['odd_key'] && $theMatch['sub_type_id']==$temp['sub_type_id'] && $theMatch['special_bet_value']==$temp['special_bet_value']){
                echo ' picked';
              }
              ?>" oddtype="<?= $temp['name'] ?>" parentmatchid="<?php echo $matchInfo['parent_match_id']; ?>" bettype='prematch' hometeam="<?php echo $matchInfo['home_team']; ?>" awayteam="<?php echo $matchInfo['away_team']; ?>" oddvalue="<?php echo $temp['odd_value']; ?>" custom="<?php echo clean($temp['match_id'].$temp['sub_type_id'].$temp['odd_key'].$special_bet_value); ?>" target="javascript:;" id="<?php echo $temp['match_id']; ?>" odd-key="<?php echo $temp['odd_key']; ?>" value="<?php echo $temp['sub_type_id']; ?>" special-value-value="<?= $special_bet_value ?>" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))" title="<?php echo ucwords($temp['display']); ?>"><span class="label label-inverse blueish"> <?php echo ucwords($temp['display']); ?></span> <span class="label label-inverse blueish odd-value"> <?php echo $temp['odd_value']; ?></span> </button>
            </div>
            <?php unset($temp) ?>
          <?php endif; ?>
        <?php endif; ?>

      <?php endforeach; ?>
    </div>
    <div>
    <a href="<?php echo $this->session->get('previous_address'); ?>"><button id="back_button" title="Back"><< Back</button></a>
    </div>