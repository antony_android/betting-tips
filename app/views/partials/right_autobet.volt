<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<?php
   $slipCount = $this->session->get('betslip');
   if($slipCount != null){
        $slipCount = sizeof($slipCount);
   }else{
        $slipCount = 0;
   }
    
?>
<div class="gn" id="right-generic">
    <div class='alert alert-success alert-dismissible betslip-success' role='alert'>
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
        </button>
        Bet successfully placed!
    </div>
    <div class="bet-option-list" id='slip-holder'>
        <div class="bet alu block-shadow">
            <header>
                <div class="betslip-header"><span
                            class="col-sm-7 slp">LUCKY AUTOBET</span>
                            <span
                            class="col-sm-4 slp">
                            <img src="{{ url('img/loader.gif') }}" class="loader"/></span></div>
            </header>
            <button id="slip-button-close" type="button" class="close mobi" aria-hidden="true">×</button>
            <div id="betslip-autobet" class="betslip">

                <!--
                <table class="bet-table">
                    <tbody>
                        <tr class="bet-win-tr hide-on-affix odds-bg">
                            <td>YOUR STAKE</td>
                            <td class="pull-right">
                                KES. <span id="pos_win">20</span>
                            </td>
                        </tr>
                        <tr class="bet-win-tr hide-on-affix possible-win-bg">
                            <td>POSSIBLE WIN</td>
                            <td class="pull-right">
                                KES. <span id="pos_win">10,000.00</span>
                            </td>
                        </tr>

                    </tbody>
                </table>
                -->
            </div>

            <div id="quick-login">

                <?php echo $this->tag->form("login/authenticate"); ?>

                <p>
                    <label>Mobile Number *</label>
                    <?php echo $this->tag->textField(["mobile",
                     "placeholder"=>"0XXX XXX XXX","class"=>"form-control msisdn-f"]) ?>
                </p>

                <p>
                    <label>Password *</label>
                    <input type="hidden" name="ref" value="{{ refURL }}">
                    <?php echo $this->
                    tag->passwordField(["password","name"=>"password","class"=>"form-control","placeholder"=>"Password"])
                    ?>
                </p>

                <div class="col-sm-12 zero-padding">
                    <div class="col-sm-4 zero-padding"><?php echo $this->tag->submitButton(["Login","class"=>"cg fm"])
                        ?>
                    </div>
                    <div class="col-sm-4 zero-padding"><a href="{{ url('signup') }}">Join Now</a></div>
                </div>

                </form>
            </div>

        </div>

        <!-- <div class="qv rc alu bon">
  <a href="{{ url('signup') }}"><img src="{{ url('https://s3-eu-west-1.amazonaws.com/bikosport/img/join-bonus.jpg') }}" class="full-width" /></a>
</div> -->

        <div id="payScroller"></div>


        {{ partial("partials/company-info") }}

    </div>
   
</div>
