<div class="">
<div class="panel-header">
  <h2>Top Leagues</h2>
</div>

<?php if(@$rugby): ?>
<ul class="f-list">
  <?php foreach($rugby as $league): ?>
      <li>
        <a class="b fa" href="competition?country=<?= $league['category'] ?>"><span class="f-league">
        <?= $league['category'] ?></span></a>
      </li>
  <?php endforeach; ?>
</ul>
<?php else: ?>

	 <ul class="f-list">
            <li>
                <a class="b fa fa-chevron-right" href="{{url('upcoming')}}">
                    <span class="f-league"> Upcoming Events </span>
                </a>
            </li>
         <?php foreach($this->topCompetitions as $competition): ?>
            <li>
                <a class="b" href="{{ url('competition?id=') }}<?php echo $competition['competition_id']?>&sp=<?= $competition['sport_id']?>">
                    <span class="col-sm-1" style="padding:0;"><img class="side-icon" src="<?php echo $this->categoryLogo->getFlag($competition['country_code']) ?>"/> </span>
                    <span class="col-sm-9 topl" style=""><?php echo $competition['competition_name']?></span>
                </a>
            </li>
         <?php endforeach; ?>
  </ul>

<div class="panel-header">
  <h2>Football (A-Z)</h2>
</div>
<?php 
$categoryID='';
?>
<ul class="f-list">
<?php foreach($sports as $sp): ?>

<?php if($sp['sport_id']=='79'): ?>

<?php if($categoryID!==$sp['category_id']): ?>
<li>
<a class="b fa" href="{{url('mobi/competition?country')}}={{sp['category']}}">
<span class="f-league"> {{sp['category']}} </span>
<img class="down-arrow pull-right" src="{{url('img/down-arrow.svg')}}">
</a>
</li>

<?php endif; ?>

<?php endif; ?>
<?php $categoryID=$sp['category_id']; ?>
<?php endforeach; ?>
</ul>

<?php endif; ?>
</div>