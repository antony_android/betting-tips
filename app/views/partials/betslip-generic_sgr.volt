<tr class="hide-on-affix odds-bg">

    <td>BET AMOUNT</td>
    <td class="pull-right">
        <div id="betting">
            <input class="bet-select" type="text" id="bet_amount" name="bet_amount" onkeyup="winnings()" value="50"
                   min="20" max="20000"/>
        </div>
    </td>
</tr>
<!--
<tr class="hide-on-affix">
    <td colspan="2">
        <table>
            {{ partial("partials/payment-methods") }}
        </table>
    </td>
</tr>
<tr>
    <td colspan="2" class="{% if session.get('auth').id is defined %}hidden{% endif %}" id="wallet-phone-number">
        <label>Mobile Number *</label>
        <?php echo $this->tag->numericField(["msisdn","placeholder"=>"0XXX XXX XXX","class"=>""]) ?>
    </td>
</tr>
-->

<?php 
    $winnings = round(($totalOdds*50)/1.2,2);
    ($winnings<70000)?$winnings = $winnings:$winnings=70000; 
    $taxable = $winnings-(50/1.2);
    $stakeaftertax = 50/1.2; 
?>
<tr class="bet-win-tr hide-on-affix">
    <td>Stake after Tax </td>
    <td class="pull-right">KES. <span id="stake-after-tax">
        <?php echo round($stakeaftertax,2); ?></span>
    </td>
</tr>

<tr class="bet-win-tr hide-on-affix">
    <td>Total Odds</td>
    <td class="pull-right"><b><?php echo $totalOdds; ?> </b></td>
</tr>

<tr class="bet-win-tr hide-on-affix ">
    <td>Winnings</td>
    <td class="pull-right">
        KES. <span id="pos_win">
        <?php echo $winnings; ?>
        </span>
    </td>
</tr>

<tr class="bet-win-tr hide-on-affix">
    <td>Withholding Tax</td>
    <td class="pull-right">
    KES. <span id="tax"><?php  $tax = (20 * $taxable) / 100;  
    echo round($tax); ?></span>
    </td>
</tr>

<tr><td colspan=2></td></tr>