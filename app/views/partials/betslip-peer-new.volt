<?php $totalOdds=1; ?>
<?php $readonly = "" ?>
<div class="bet-body">
    <?php
    
    $betslip = $this->session->get('betslip-peer');

    $betslip = is_array($betslip) ? $betslip : [];

    // die(print_r($betslip, 1));

    $matchCount = 0;
    $bonus = 0;
    $bonusOdds = 0;
    $total_odd_original = 0;
    $parentmatchid = 0;
    $matchid = 0;
    $msisdn = "";
    $bet_amount = 50;

    ?>
    <?php foreach($betslip as $bet): ?>
    <?php

        $odd = $bet['odd_value'];
        $msisdn = $bet['peer_msisdn'];
        $bet_amount = $bet['bet_amount'];

        if($bet['bet_pick']=='x'){
            $pick = 'DRAW';
        }else if($bet['bet_pick']=='2'){
            $pick = $bet['away_team'];
        }else if($bet['bet_pick']=='1'){
            $pick = $bet['home_team'];
        }
        else{
            $pick = $bet['bet_pick'];
        }

        $friend_pick = $bet['friend_pick'];

        if($bet['odd_value']>=1.6){
            $bonusOdds*=$odd;
            $matchCount++;
        }

        $parentmatchid = $bet['parent_match_id'];
        $matchid = $bet['match_id'];
        $peer_bet_id = $bet['peer_bet_id'];

        $totalOdds = 1.8;
        $total_odd_original = $odd;

    ?>

    <li class="bet-option hide-on-affix">
        <div class="bet-cancel">
            <input id="<?php echo $bet['match_id']; ?>" type="submit" value="X" title="Remove Match" onclick="removePeerMatch(this.id)">
        </div>
        <div class="bet-value">
            <?php echo $bet['home_team']." v ".$bet['away_team']; ?>
            <br><span class="sp_sport" style=""></span>
        </div>
        
        <div class="bet-pick">

            <div class="col-md-4 p-0">
                Your Pick
            </div>
            <div class="col-md-8 normal-fw slip-pull-right team-picked">
                <?= strtoupper($friend_pick); ?>
            </div>
        </div>

        <?php  if(strpos($friend_pick, 'NOT WIN') !== false): ?>

            <?php $readonly = "readonly"; ?>
        <?php endif ?>

        <div class="bet-pick">
            <div class="col-md-4 p-0">
                Odds
            </div>
            <div class="col-md-8 slip-pull-right">
                2.0
            </div>
        </div>

        <div class="bet-pick">
            <div class="col-md-4 p-0">
                Commission
            </div>
            <div class="col-md-8 slip-pull-right">
                0.2
            </div>
        </div>

        <div class="bet-pick">
            <div class="col-md-12 slip-pull-right">
                <label class="peer-label" for="peer_invite">Enter Friend Phone to Invite them to Bet Against You </label><span class="peer-mandatory"> *</span>
                <input class="peer-invite-friends" type="text" id="peer_invite" name="peer_invite" value="<?php echo $msisdn; ?>" required="required"/>
            </div>
        </div>

    </li>
    <?php endforeach; ?>
</div>
<form>
    <table class="bet-table">
        <tbody>
        <tr class="hide-on-affix odds-bg">
            <td>TOTAL ODDS</td>

            <td class="pull-right">BET AMOUNT</td>
        </tr>
        <tr class="odds-bg">
            <td><b><?php echo $totalOdds; ?> </b></td>
            <td class="pull-right">
                <div id="betting">
                    <input class="bet-select" type="text" id="bet_amount" name="bet_amount" onkeyup="winnings()" value="<?php echo $bet_amount; ?>"
                           min="20" max="20000" <?php echo $readonly ?>/>
                </div>
            </td>
        </tr>
        <tr class="bet-win-tr hide-on-affix possible-win-bg">
            <td>POSSIBLE WIN</td>
            <td class="pull-right">
            <?php $winnings = $totalOdds*$bet_amount; ?>
                KES. <span id="pos_win">
                    <?php ($winnings<1000500)?$winnings = $winnings:$winnings=1000500; echo $winnings; ?>
                    </span>
            </td>
        </tr>
        <tr class="possible-win-bg">
            <td></td>
            <td class="pull-right">
                <button type="button" id="place_bet_button" class="place-bet-btn" onclick="placePeerBet()">
                    PLACE BET
                </button>
            </td>
        </tr>
        </tbody>
    </table>
    <div class='alert alert-danger alert-dismissible ss betslip-error' role='alert'>
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
        </button>
        You have insufficient balance. Please top up
    </div>
    <input type="hidden" id="user_id" value="{{ session.get('auth')['id'] }}">
    <input type="hidden" id="max_win" value="1000000">
    <input type="hidden" id="minimum_bet" value="20">
    <input type="hidden" id="total_odd_original" value="<?php echo $total_odd_original; ?>">
    <input type="hidden" id="total_odd" value="1.8">
    <input type="hidden" id="parentmatchid" value="<?php echo $parentmatchid; ?>">
    <input type="hidden" id="peer_bet_id" value="<?php echo $peer_bet_id; ?>">
    <input type="hidden" id="match_id" value="<?php echo $matchid; ?>">
    <input type="hidden" id="max_multi_games" value="20">
    <input type="hidden" id="bet_type" value="prematch">
</form>