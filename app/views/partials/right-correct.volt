<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<div class="gn" id="right-generic">
    <div class='alert alert-success alert-dismissible betslip-success' role='alert'>
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
        </button>
        Bet successfully placed!
    </div>
    <div class="bet-option-list" id='slip-holder'>
        <div  class="bet alu jackpot-betslip-holder">
            <header>
                <div class="betslip-header"><span class="col-sm-2 bkmrk"><i class="fa fa-bookmark-o"
                                                                            aria-hidden="true"></i></span><span
                            class="col-sm-8 slp">Jackpot Betslip</span><span
                            class="col-sm-2 slip-counter"><?= $slipCountJ ?></span></div>
            </header>
            <img src="{{ url('img/loader.gif') }}" class="loader"/>
            <button id="slip-button-close" type="button" class="close mobi" aria-hidden="true">×</button>
            <div id="betslipB" class="betslipB ">

            </div>

            <div id="quick-login">

                <p>Please login to place a bet</p>

                <?php echo $this->tag->form("login/authenticate?ref=correct"); ?>

                <p>
                    <label>Mobile Number *</label>
                    <?php echo $this->tag->numericField(["mobile","placeholder"=>"0XXX XXX XXX","class"=>"form-control"]) ?>
                </p>

                <p>
                    <label>Password *</label>
                    <?php echo $this->tag->passwordField(["password","name"=>"password","class"=>"form-control","placeholder"=>"Password"]) ?>
                </p>

                <div class="col-sm-12 zero-padding">
                    <div class="col-sm-4 zero-padding"><?php echo $this->tag->submitButton(["Login","class"=>"cg fm"])
                        ?>
                    </div>
                    <div class="col-sm-8 zero-padding"><a href="{{ url('signup') }}">Join now</a></div>
                </div>

                </form>
            </div>
        </div>

        <!-- <div class="qv rc alu bon">
  <a href="{{ url('signup') }}"><img src="{{ url('https://s3-eu-west-1.amazonaws.com/bikosport/img/join-bonus.jpg') }}" class="full-width" /></a>
</div> -->


        {{ partial("partials/company-info") }}


        {#<div class="qv rc alu">#}
            {#<div class="qv rc alu paybill block-shadow bottom-std-margin-spacing">#}
                {#<div class="std-block-head bold">Namba ya Kampuni</div>#}

                {#<div class="warning-txt"></div>#}

                {#<div class="sidebar company-number bold">101010</div>#}

                {#<div class="payment-selector std-side-pads no-transform">#}

                    {#<form class="bs-example " action="">#}

                        {#<div class="panel-group" id="accordion">#}

                            {#<div id="howToPay" class="panel panel-default basic-list pay-instruction">#}
                                {#<div class="panel-heading">#}
                                    {#<h4 class="panel-title">#}
                                        {#<label for='r11' style='width: 350px;'>#}
                                            {#<input type='radio' id='r11' name='occupation' value='Working' required/>#}
                                            {#Wallet#}
                                            {#<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"></a>#}
                                        {#</label>#}
                                    {#</h4>#}
                                {#</div>#}
                                {#<div id="collapseOne" class="panel-collapse collapse">#}
                                    {#<div class="panel-body">#}
                                        {#Proceed#}
                                    {#</div>#}
                                {#</div>#}
                            {#</div>#}

                            {#<div class="panel panel-default">#}
                                {#<div class="panel-heading">#}
                                    {#<h4 class="panel-title">#}
                                        {#<label for='r12' style='width: 350px;'>#}
                                            {#<input type='radio' id='r12' name='occupation' value='Working' required/>#}
                                            {#Tigo Pesa#}
                                            {#<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><img#}
                                                        {#class="payment-preview-img"#}
                                                        {#src="/img/payment_logos/tigo_pesa.png"></a>#}
                                        {#</label>#}
                                    {#</h4>#}
                                {#</div>#}
                                {#<div id="collapseTwo" class="panel-collapse collapse basic-list pay-instruction">#}
                                    {#<div class="panel-body">#}
                                        {#<ol class="no-pad-left">#}
                                            {#<li>Dial <span class="pay-highlight">*150*01#</span></li>#}
                                            {#<li>Select Paybill#}
                                                {#<ul>#}
                                                    {#<li>Business Number <span class="pay-highlight">101010</span></li>#}
                                                {#</ul>#}
                                            {#</li>#}
                                            {#<li>Reference Number <span class="pay-highlight"><span id="payRef">nambari jamvi lako</span></span>#}
                                            {#</li>#}
                                        {#</ol>#}
                                    {#</div>#}
                                {#</div>#}
                            {#</div>#}

                            {#<div class="panel panel-default basic-list pay-instruction">#}
                                {#<div class="panel-heading">#}
                                    {#<h4 class="panel-title">#}
                                        {#<label for='r13' style='width: 350px;'>#}
                                            {#<input type='radio' id='r13' name='occupation' value='Working' required/>#}
                                            {#M-pesa#}
                                            {#<a data-toggle="collapse" data-parent="#accordion"#}
                                               {#href="#collapseThree"><img class="payment-preview-img"#}
                                                                          {#src="/img/payment_logos/mpesa-logo.png"></a>#}
                                        {#</label>#}
                                    {#</h4>#}
                                {#</div>#}
                                {#<div id="collapseThree" class="panel-collapse collapse">#}
                                    {#<div class="panel-body">#}
                                        {#<ol class="no-pad-left">#}
                                            {#<li>Dial <span class="pay-highlight">*150*00#</span></li>#}
                                            {#<li>Select Pay by#}
                                                {#<ul>#}
                                                    {#<li>Business Number <span class="pay-highlight">101010</span></li>#}
                                                {#</ul>#}
                                            {#</li>#}
                                            {#<li>Reference Number <span class="pay-highlight"><span id="payRef">nambari jamvi lako</span></span>#}
                                            {#</li>#}
                                        {#</ol>#}
                                    {#</div>#}
                                {#</div>#}
                            {#</div>#}


                            {#<div class="panel panel-default basic-list pay-instruction">#}
                                {#<div class="panel-heading">#}
                                    {#<h4 class=panel-title>#}
                                        {#<label for='r14' style='width: 350px;'>#}
                                            {#<input type='radio' id='r14' name='occupation' value='Not-Working'#}
                                                   {#required/>Airtel Money#}
                                            {#<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><img#}
                                                        {#class="payment-preview-img" src="/img/payment_logos/index.png"></a>#}
                                        {#</label>#}
                                    {#</h4>#}
                                {#</div>#}
                                {#<div id="collapseFour" class="panel-collapse collapse">#}
                                    {#<div class="panel-body">#}
                                        {#<ol class="no-pad-left">#}
                                            {#<li>Dial <span class="pay-highlight">*150*60#</span></li>#}
                                            {#<li>Select Paybill#}
                                                {#<ul>#}
                                                    {#<li>Business Number- <span class="pay-highlight">101010</span></li>#}
                                                {#</ul>#}
                                            {#</li>#}
                                            {#<li>Reference Number <span class="pay-highlight"><span id="payRef">nambari jamvi lako</span></span>#}
                                            {#</li>#}
                                        {#</ol>#}
                                    {#</div>#}
                                {#</div>#}
                            {#</div>#}
                        {#</div>#}
                    {#</form>#}

                {#</div>#}
                {#<div class="support">#}
                    {#<div class="helpline">#}
                        {#<span class="col-sm-4"><img src="{{ url('img/contact-24.png') }}"/></span>#}
                        {#<span class="col-sm-8">0746811190</span>#}
                    {#</div>#}
                    {#<div class="mail">#}
                        {#<i class="fa fa-envelope-o" aria-hidden="true"></i> &nbsp; info@betpalalce.co.ke#}
                    {#</div>#}
                {#</div>#}
            {#</div>#}
        {#</div>#}
        
</div>

<script type="text/javascript">
    $('#r11, #r12, #r13, #r14').on('click', function () {
        $(this).parent().find('a').trigger('click');
    });
</script>
