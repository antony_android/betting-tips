<div class="panel-header">
<h2>Reset password</h2>
</div>
<div class="inner-content whitebg">
<p>
Please enter your mobile number to receive a reset code</p>
        {{ this.flashSession.output() }}

{% if profile_id is not defined %}
<?php echo $this->tag->form("reset/code"); ?>

 <p>
    <label>Mobile Number *</label>
    <?php echo $this->tag->numericField(["mobile","placeholder"=>"0XXX XXX XXX","class"=>"form-control"]) ?>
 </p>

 <p>
    <?php echo $this->tag->submitButton(["Get Reset Code","class"=>"cg fm"]) ?>
 </p>

</form>
{% else %}
<?php echo $this->tag->form("reset/password"); ?>

 <p>
    <label>Mobile Number *</label>
    <?php echo $this->tag->numericField(["mobile","disabled"=>"disabled","placeholder"=>"0XXX XXX XXX","class"=>"form-control","value"=>$msisdn]) ?>
 </p>


  <p>
    <label>Reset code *</label>
    <?php echo $this->tag->numericField(["reset_code","placeholder"=>"XXXX","class"=>"form-control"]) ?>
 </p>

<p>
    <label>Password *</label>
    <?php echo $this->tag->passwordField(["password","name"=>"password","class"=>"form-control","placeholder"=>"Password"]) ?>
 </p>

  <p>
  <label>Confirm Password *</label>
    <?php echo $this->tag->passwordField(["password","name"=>"repeatPassword","class"=>"form-control","placeholder"=>"Confirm password"]) ?>
 </p>

 <input type="hidden" value="{{profile_id}}" name="profile_id" />

  <p>
    <?php echo $this->tag->submitButton(["Reset password","class"=>"cg fm"]) ?>
 </p>

</form>

{% endif %}
</div>