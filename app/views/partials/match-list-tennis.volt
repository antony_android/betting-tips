<?php
if (!function_exists('clean'))  
{ 
 function clean($string) {
        $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }
}
?>

<div class="mobi">
     <div id="search-event" class="top-matches match-header events-header ">
        <?php echo $this->tag->form([""]); ?>
            <div class="col-md-6">
                <input type="text" name="keyword" class="form-control" data-action="grow"
               placeholder="Team, Competition or Game ID">
            </div>
            <div class="col-md-2">
                <button type="submit" class="cg fp ">Search</button>
            </div>
        </form>
     </div>
</div>


<div class="matches full-width match-list-games">
    <?= empty($today)? '<div class="empty-list">
    No events found for selected category. Check different category to find more sports</div>':''?>

    <header class="col-sm-12 top-matches match-header events-header web-element">
        <div class="col-sm-8 left-text p-0 date-header"> 
        </div>
        <div class="col-sm-3 mkts-desc-head mgr-5 p-0">WINNER
        </div>
        <div class="col-sm-0 p-0">&nbsp;</div>
        <!-- markets description -->
        <div class="col-sm-8 left-text">&nbsp;</div>
        <div class="col-sm-3 mkts-desc mgr-5 p-0">
            <div class="col-sm-6 p-0">1</div>
            <div class="col-sm-6 p-0">2</div>
        </div>
        <div class="col-sm-1 p-0 bets-zaidi">More</div>
        <!-- end markets description -->
    </header>
    <?php foreach($today as $key => $group):?>

    <div class="middle-content">
        <?php foreach($group as $day): ?>
        <?php
        $theMatch = @$theBetslip[$day['match_id']];
        ?>
        <div class="col-sm-12 top-matches hidden-overflow">

            <div class="col-sm-8 col-xs-11 p-0">
                <div class="compt-detail"> 
                    GAME ID: <span class="game-id"> <?php echo $day['game_id']; ?> </span> |  <?php echo $day["competition_name"]; ?> |  <?php echo date('d-m-Y H:i', strtotime($day['start_time'])); ?></div>
                <div class="compt-teams"><?php echo $day['home_team']; ?>  
                    <div class="web-element"><br/></div>
                    <div class="mobile-view">
                        <span class=""> VS. </span>
                    </div>
                    <?php echo $day['away_team']; ?>
                </div>
            </div>
            <div class="col-xs-1 events-odd pad
                <?php
                    if($theMatch && $theMatch['sub_type_id']!=1){
                        echo ' picked';
                    }
                ?>
                mobile-view">

                <a class="side" href="<?php echo 'match?id='.$day['match_id']; ?>">+<?php echo $day['side_bets']; ?> </a>
            </div>

                <div class="col-sm-3 col-xs-12 match-div-col mgr-5  p-0" >

                    <div class="col-sm-6 col-xs-6 p-0 match-left-col">
                        <button class="home-team <?php echo $day['match_id']; ?> <?php
                            echo clean($day['match_id'].$day['sub_type_id'].$day['home_team']);
                            if($theMatch['bet_pick']==$day['home_team'] && $theMatch['sub_type_id']==$day['sub_type_id']){
                            echo ' picked';
                        }
                        ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="Winner" bettype='prematch'
                        awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['home_odd']; ?>"
                        target="javascript:;" odd-key="<?php echo $day['home_team']; ?>" parentmatchid="<?php echo $day['parent_match_id']; ?>"
                        id="<?php echo $day['match_id']; ?>"
                        custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['home_team']); ?>" value="<?php echo $day['sub_type_id']; ?>" special-bet-value=""
                        onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))">
                        <span class="odds-btn-label mobile-view">HOME WIN<br/></span>
                        <span
                        class="theodds"><?php echo $day['home_odd']; ?></span></button>
                    </div>

                    
                <div class="col-sm-6 col-xs-6 match-div-col match-right-col" style="padding:0;">
                    <button class="away-team <?php echo $day['match_id']; ?> <?php echo clean($day['match_id'].$day['sub_type_id'].$day['away_team']);
                        if($theMatch['bet_pick']==$day['away_team'] && $theMatch['sub_type_id']==$day['sub_type_id']){
                        echo ' picked';
                    }
                    ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="Winner" bettype='prematch'
                    awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['away_odd']; ?>" value="<?php echo $day['sub_type_id']; ?>"
                    custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['away_team']); ?>" odd-key="<?php echo $day['away_team']; ?>"
                    target="javascript:;" parentmatchid="<?php echo $day['parent_match_id']; ?>"
                    id="<?php echo $day['match_id']; ?>" special-bet-value=""
                    onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))">
                    <span class="odds-btn-label mobile-view">AWAY WIN<br/></span>
                    <span
                    class="theodds"> <?php echo $day['away_odd']; ?> </span></button>
                </div>
            </div> 
            <!-- end 2 way buttons -->
             <!-- start 1st set winner -->

            <!--
            <div class="col-sm-2 col-xs-12 match-div-col mgr-5  p-0" >
                <div class="col-sm-6 col-xs-6 p-0 match-left-col">
                    <button class="home-team <?php echo $day['match_id']; ?> <?php
                        echo clean($day['match_id'].$day['sub_type_id'].$day['home_team'].'1');
                        if($theMatch['bet_pick']==$day['home_team'] && 
                         $theMatch['sub_type_id']==$day['sub_type_id'] && $theMatch['special_bet_value']=='1'){
                        echo ' picked';
                    }
                    ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="first set - winner" bettype='prematch'
                    awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['1st_set_winner1']; ?>"
                    target="javascript:;" odd-key="<?php echo $day['home_team']; ?>" parentmatchid="<?php echo $day['parent_match_id']; ?>"
                    id="<?php echo $day['match_id']; ?>"
                    custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['home_team'].'1'); ?>" value="<?php echo $day['sub_type_id']; ?>" special-bet-value="1"
                    onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))">
                    <span class="odds-btn-label mobile-view">HOME 1st SET WINNER<br/></span>
                    <span
                    class="theodds"><?php echo $day['1st_set_winner1']; ?></span></button>
                </div>

                <div class="col-sm-6 col-xs-6 events-odd match-div-col match-mid-col" style="">

                    <button class="draw <?php echo $day['match_id']; ?> <?php
                        echo clean($day['match_id'].$day['sub_type_id'].$day['away_team'].'1');
                        if($theMatch['bet_pick']==$day['away_team'] && $theMatch['sub_type_id']==$day['sub_type_id'] &&
                        $theMatch['special_bet_value'] == '1'){
                        echo ' picked';
                    }
                    ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="first set - winner" bettype='prematch'
                    awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['1st_set_winner2']; ?>"
                    custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['away_team'].'1'); ?>" value="<?php echo $day['sub_type_id']; ?>" odd-key="<?= $day['away_team']; ?>"
                    target="javascript:;" parentmatchid="<?php echo $day['parent_match_id']; ?>"
                    id="<?php echo $day['match_id']; ?>" special-bet-value="1"
                    onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))">
                    <span class="odds-btn-label mobile-view">AWAY 1ST SET WINNER<br/></span>
                    <span
                    class="label label-inverse"> <?php echo $day['1st_set_winner2']; ?></span></button>
                </div>
               
            </div> <!-- start 1st set winner -->

            <!-- start 2st set winner 

            <div class="col-sm-2 col-xs-12 match-div-col over-under mgr-5  p-0" >
                <div class="col-sm-6 col-xs-6 p-0 match-left-col">
                    <button class="home-team <?php echo $day['match_id']; ?> <?php
                        echo clean($day['match_id'].$day['sub_type_id'].'12');
                        if($theMatch['bet_pick']==$day['home_team'] && $theMatch['sub_type_id']==$day['sub_type_id']
                        && $theMatch['special_bet_value'] =='2' ){
                        echo ' picked';
                    }
                    ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="second set - winner" bettype='prematch'
                    awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['2st_set_winner1']; ?>"
                    target="javascript:;" odd-key="<?php echo $day['home_team']; ?>" parentmatchid="<?php echo $day['parent_match_id']; ?>"
                    id="<?php echo $day['match_id']; ?>"
                    custom="<?php echo clean($day['match_id'].$day['sub_type_id'].'12'); ?>" value="<?php echo $day['sub_type_id']; ?>" special-bet-value="2"
                    onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))">
                    <span class="odds-btn-label mobile-view">HOME 2ND SET WINNER<br/></span><span

                    class="theodds"><?php echo $day['2st_set_winner1']; ?></span></button>
                </div>

                <div class="col-sm-6 col-xs-6 events-odd match-div-col match-right-col" style="padding:0;">

                    <button class="draw <?php echo $day['match_id']; ?> <?php
                        echo clean($day['match_id'].$day['sub_type_id']."22");
                        if($theMatch['bet_pick']==$day['away_team'] && $theMatch['sub_type_id']==$day['sub_type_id'] &&
                        $theMatch['special_bet_value'] == '2'){
                        echo ' picked';
                    }
                    ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="second set - winner" bettype='prematch'
                    awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['2st_set_winner2']; ?>"
                    custom="<?php echo clean($day['match_id'].$day['sub_type_id'].'22'); ?>" value="<?php echo $day['sub_type_id']; ?>" odd-key="<?php echo $day['away_team']; ?>"
                    target="javascript:;" parentmatchid="<?php echo $day['parent_match_id']; ?>"
                    id="<?php echo $day['match_id']; ?>" special-bet-value="2"
                    onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))">
                    <span class="odds-btn-label mobile-view">AWAY 2ND SET WINNER<br/></span>
                    <span
                    class="label label-inverse"> <?php echo $day['2st_set_winner2']; ?></span></button>
                </div>

            </div> -->


            <div class="col-sm-1 events-odd pad
                <?php
                    if($theMatch && $theMatch['sub_type_id']!= 186){
                    echo ' picked';
                }
                ?> web-element">

                <a class="side" href="<?php echo 'match?id='.$day['match_id']; ?>">
                    +<?php echo $day['side_bets']; ?> 
                </a>
            </div>
    </div>

    <?php endforeach; ?>

</div>

<?php endforeach; ?>

<!-- <?php// if($pages):?>
<div class="paginator">
    <a href="?page=<?= $page-1; ?>">Prev </a>
    <?php// for ($x = 0; $x <= $pages; $x++): ?>
    <a href="?page=<?= $x; ?>" class="
        <?php
       // if($x==$page){
       // echo 'current-page';
   // }
    ?>
    "><?= $x+1; ?> </a>
    <?php // endfor; ?>
    <a href="?page=<?= $page+1; ?>">Next </a>
</div>
<?php //endif; ?> -->
</div>