<?php
if (!function_exists('clean'))  
{ 
 function clean($string) {
        $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }
}
?>

<div class="odd-wrapper3">

        <div class="row inner-wr3">

        <?php echo $this->tag->form([""]); ?>

       <div class="col-md-12 pad-tp web-element" style="text-align:center;color:#FFF;font-size:12px;">
            Pick you Selection, Invite your Friend at the Betslip to bet Against You
        </div>
       <!--
        <div class="col-md-3 pad-tp web-element">
            <input type="text" class="form-control results-filter" id="date_put" name="date_put" value="" placeholder="Date">
        </div>

        <div class="col-md-4 col-xs-9 pad-tp">

            <input type="text" class="form-control results-filter" id="keyword" name="keyword" placeholder="Search by Game ID, Match or Tournament Name">
        </div>

        <div class="col-md-2 col-xs-3 pad-tp search-button">
            <button type="submit" class="search-btn">Search</button>
        </div>

        </form> -->	

    </div>
</div>

<div class="matches full-width match-list-games">

    <?= empty($invited_peerGames)? '<div class="empty-list invited-header">
    You have not yet been invited by a friend to a challenge on Beste Bet. You can invite a friend on a selected game below for a challenge</div>':'<header class="col-sm-12 match-header peer-header invited-header">
        <div class="col-sm-12 left-text p-0 peer-innerheader">
            INVITED BESTE BETS       
        </div>
        <div class="col-sm-1 peer-subheader web-element">ID</div>
        <div class="col-sm-2 peer-subheader web-element">Competition</div>
        <div class="col-sm-3 peer-subheader web-element">Game</div>
        <div class="col-sm-2 peer-subheader web-element">Start Time</div>
        <div class="col-sm-2 peer-subheader web-element">Invited By</div>
        <div class="col-sm-2 peer-subheader web-element"></div>
    </header>'?>

    <?php foreach($invited_peerGames as $day): ?>
        <?php
        $theMatch = @$theBetslip[$day['match_id']];
        ?>
        <div class="col-sm-12 top-matches hidden-overflow">

            <div class="col-sm-1 mkts-desc-head p-0 web-element">
                <?php echo $day['game_id']; ?>
            </div>

            <div class="col-md-2 mkts-desc-head p-0 web-element">
            <?php echo $day["competition_name"]; ?>
            </div>

            <div class="col-sm-3 mkts-desc-head p-0 web-element">
            <?php echo $day['home_team']; ?> <span class="vs-peer"> vs. </span> <?php echo $day['away_team']; ?>
            </div>

            <div class="col-md-2 mkts-desc-head p-0 web-element">
            <?php echo date('d-m-Y H:i', strtotime($day['start_time'])); ?>
            </div>

            <div class="col-sm-12 p-0 mobi">
                <div class="compt-detail"> 
                    GAME ID: <span class="game-id"> <?php echo $day['game_id']; ?> </span> |  <?php echo $day["competition_name"]; ?> |  <?php echo date('H:i', strtotime($day['start_time'])); ?>
                </div>
                <div class="compt-teams">
                    <?php echo $day['home_team']; ?> 
                    <div class="mobile-view"><span class="compt-detail">VS.</span></div>
                    <?php echo $day['away_team']; ?>
                </div>
            </div>

            <div class="col-md-2 col-xs-6 mkts-desc-head p-0">
                <?php echo $day["msisdn"]; ?><br/>
                <span class="rafiki-pick"><?php echo ucwords($day["pick"])." Will Win"; ?></span>
            </div>

            <?php $sub_type_id = 10 ?>
            <?php $peer_odds = 2 ?>

            <div class="col-md-2 col-xs-6 mkts-desc-head p-0">
                <button class="rafiki-font against-butt home-team <?php echo $day['game_id']; ?> <?php
                    echo clean($day['game_id'].$sub_type_id.$day['home_team']);
                ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="1x2" bettype='peer_accept'
                    awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $peer_odds; ?>" bet_amount="<?php echo $day['bet_amount']; ?>" 
                    custom="<?php echo clean($day['match_id'].$sub_type_id.$day['away_team']); ?>" odd-key="<?php echo $day['peer_pick']; ?>"  
                friend_pick="<?php echo $day['pick']; ?>" target="javascript:;" odd-key="<?php echo $day['away_team']; ?>" parentmatchid="<?php echo $day['parent_match_id']; ?>"
                peer_bet_id="<?php echo $day['peer_bet_id']; ?>" peer_msisdn="<?php echo $day['msisdn']; ?>" id="<?php echo $day['game_id']; ?>"
                custom="<?php echo clean($day['game_id'].$sub_type_id.$day['home_team']); ?>" value="10" special-value-value="0"
                onClick="pingaPeerBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'), this.getAttribute('peer_bet_id'), this.getAttribute('peer_msisdn'), this.getAttribute('friend_pick'), this.getAttribute('bet_amount'))" title="Bet Against {{ day['pick'] }}">
                PINGA
                </button>
            </div>

        </div>

    <?php endforeach; ?>

    </div>

<div class="matches full-width match-list-games">

    <?= empty($today)? '<div class="empty-list">
    No events found for selected category. Check different category to find more sports</div>':''?>

    <?php foreach($today as $key => $group):?>
    <div class="beste-invite-header">
        Invite Friends to Bet Against You for the Games Below
    </div>
    <header class="col-sm-12 match-header peer-header">
        <div class="col-sm-12 left-text p-0 peer-innerheader">
            <?php echo $key; ?>        
        </div>
        <div class="col-sm-1 peer-subheader web-element">ID</div>
        <div class="col-sm-2 peer-subheader web-element">Competition</div>
        <div class="col-sm-3 peer-subheader web-element">Game</div>
        <div class="col-sm-2 peer-subheader web-element">Start Time</div>
        <div class="col-sm-2 peer-subheader web-element">Your Pick</div>
        <div class="col-sm-2 peer-subheader web-element"></div>
    </header>

    <div class="middle-content matches-detail">
        <?php foreach($group as $day): ?>
        <?php
        $theMatch = @$theBetslip[$day['match_id']];
        ?>
        <div class="col-sm-12 top-matches hidden-overflow">

            <div class="col-sm-1 mkts-desc-head p-0 web-element">
                <?php echo $day['game_id']; ?>
            </div>

            <div class="col-md-2 mkts-desc-head p-0 web-element">
            <?php echo $day["competition_name"]; ?>
            </div>

            <div class="col-sm-3 mkts-desc-head p-0 web-element">
            <?php echo $day['home_team']; ?> <span class="vs-peer"> vs. </span> <?php echo $day['away_team']; ?>
            </div>

            <div class="col-md-2 mkts-desc-head p-0 web-element">
            <?php echo date('d-m-Y H:i', strtotime($day['start_time'])); ?>
            </div>

            <div class="col-sm-12 p-0 mobi">
                <div class="compt-detail"> 
                    GAME ID: <span class="game-id"> <?php echo $day['game_id']; ?> </span> |  <?php echo $day["competition_name"]; ?> |  <?php echo date('H:i', strtotime($day['start_time'])); ?>
                </div>
                <div class="compt-teams">
                    <?php echo $day['home_team']; ?> 
                    <div class="mobile-view"><span class="compt-detail">VS.</span></div>
                    <?php echo $day['away_team']; ?>
                </div>
            </div>

            <div class="col-md-2 col-xs-6 mkts-desc-head p-0">
                
                <button class="rafiki-font home-team <?php echo $day['game_id']; ?> <?php
                    echo clean($day['game_id'].$theMatch['sub_type_id'].$day['home_team']);
                    if($theMatch['bet_pick']==$day['home_team'] && $theMatch['sub_type_id']=='1'){
                    echo ' picked';
                }
                ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="1x2" bettype='prematch' 
                    awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['home_odd']; ?>" value="1"
                    custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['home_team']); ?>" odd-key="<?php echo $day['home_team']; ?>"
                target="javascript:;" parentmatchid="<?php echo $day['parent_match_id']; ?>"
                id="<?php echo $day['game_id']; ?>"
                custom="<?php echo clean($day['game_id'].$theMatch['sub_type_id'].$day['home_team']); ?>" value="1" special-value-value="0"
                onClick="addPeerBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))" title="Wekelea {{ day['home_team'] }}">
                <?php echo $day['home_team'] ?>
                </button>
            </div>

            <div class="col-md-2 col-xs-6 mkts-desc-head p-0">
                <button class="rafiki-font home-team <?php echo $day['game_id']; ?> <?php
                    echo clean($day['game_id'].$theMatch['sub_type_id'].$day['home_team']);
                    if($theMatch['bet_pick']==$day['away_team'] && $theMatch['sub_type_id']=='1'){
                    echo ' picked';
                }
                ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="1x2" bettype='newpeer' 
                    awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['away_odd']; ?>" value="1"
                    custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['away_team']); ?>" odd-key="<?php echo $day['away_team']; ?>"
                target="javascript:;" odd-key="<?php echo $day['away_team']; ?>" parentmatchid="<?php echo $day['parent_match_id']; ?>"
                id="<?php echo $day['game_id']; ?>"
                custom="<?php echo clean($day['game_id'].$day['sub_type_id'].$day['home_team']); ?>" value="1" special-value-value="0"
                onClick="addPeerBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))" title="Wekelea {{ day['away_team'] }}">
                <?php echo $day['away_team'] ?>
                </button>
            </div>

        </div>

    <?php endforeach; ?>

</div>

<?php endforeach; ?>

</div>
