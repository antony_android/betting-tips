<div class="">
<div class="panel-header">
  <h2>Sports (A-Z)</h2>
</div>
<?php 
$id=''; 
$categoryID='';
$competitionID='';
?>
<ul class="sidebar-menu aoi nav base-bg">
<?php foreach($sports as $sp): ?>
<?php if($id!=$sp['sport_id'] && $sp['sport_id'] != '79'): ?>
<li class="treeview">
<a href="#" class="b-sports fa">
<span style="padding:0;"><img class="side-icon" src="<?= '/svg/'. $sp['sport_name'].'.svg' ?>"> </span>
<span class="topl"> {{sp['sport_name']}} </span>
<img class="down-arrow pull-right" src="{{ url('img/down-arrow.svg') }}">
</a>
<ul class="treeview-menu">
<?php $id=$sp['sport_id']; ?>

<?php foreach($sports as $s): ?>

  <?php $url=$sportType[$s['sport_id']].'?id='.$s['competition_id'].'&sp=
        '.$id; ?>

  <?php if($id == $s['sport_id'] && $categoryID != $s['category_id']): ?>
      <li class="treeview"><a class="b-sports" href="{{url(url)}}">
     <span class="gothb"> {{s['category']}} </span>
      </a>

      <ul class="treeview-menu second-child">
      <?php $categoryID=$s['category_id']; ?>
      <?php foreach($sports as $st): ?>
        <?php 
        $url=$sportType[$s['sport_id']].'?id='.$st['competition_id'].'&sp=
        '.$st['sport_id']; 
        $competitionID=$st['competition_id'];
        ?>
        <?php if($categoryID == $st['category_id']): ?>
            <li><a class="b" href="{{url(url)}}">
            <span> {{st['competition_name']}} </span>
            </a></li>
        <?php endif; ?>
        <?php $categoryID=$s['category_id']; ?>
      <?php endforeach; ?>
      </ul>

      </li>
  <?php endif; ?>
<?php endforeach; ?>
</ul>
</li>
<?php endif; ?>

<?php endforeach; ?>
</ul>

</div>