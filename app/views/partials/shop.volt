<div class="panel-header"><h2>Retailshop Partnership</h2></div>
	<div class="odd-wrapper">

	    <div class="nav-side">

	      <button class="accordion acc-large" data-toggle="collapse" data-target="#definitions">1. Bet Palace offers you the chance to run your own business by becoming one of our partners</button>
		  <div id="definitions" class="collapse shop">
		    <div class="bigger-list">

			<p> Our products, training and support team will provide you with 
			everything a self-motivated person needs to start and grow his/her own shop.<br>
			By offering customers the chance to play on over 80,000+ Pre-Match  events per month, 20,000+ In-Play Events per month (Live), 80+ Betting Markets, 20+ Different Sports and the best odds Guaranteed, we make your job an easy sell.
			For further information please call <div class="shop-contacts"><b>0705290085/0700290086</b> <u> <b> (7 days a week, 8:00 am to 21:00 pm)</b></u></div> or fill in and submit the registration form.
			Our sales representative will get in touch with you shortly.</p>

			</div>
		  </div>

		  <button class="accordion acc-large" data-toggle="collapse" data-target="#bets">2. Your Benefits</button>
		  <div id="bets" class="collapse shop">
		    <div class="bigger-list">

			<p> Are you entrepreneurial and interested in operating an independent sports betting business? Do not miss this opportunity to run your own business supported by a premium product and a big brand. Our agent program is supported by professionals with many years of experience.<br> Our professionalism, comprehensive and trustworthy service and our reliable and fast processing of commissions and payments have already convinced many partners.</p>

			<p>As a partner, you will derive the following benefits from our unique competitive advantages:
			<ol>
				<li>Big profit sharing of up to 60%</li>
				<li>Commissions for all online and mobile customers registered by in your shop.</li>
				<li>Over 80,000+ pre-match events per month</li>
				<li>20,000+ In-Play Events per month (Live)</li>
				<li>80+ Betting Markets</li>
				<li>20+ different sports and the best odds guarantee</li>
				<li>Constant development of new and exciting products.</li>
			</ol>

			</p>

			</div>
		  </div>

		  <button class="accordion acc-large" data-toggle="collapse" data-target="#identity-customers">3. Technology and Equipment</button>
		  <div id="identity-customers" class="collapse shop">
		    <div class="bigger-list">

			<p>
				<ol>
					<li>Set-up support for the hardware equipment at your shop.</li>
					<li>Technical support on-site and via telephone.</li>
				</ol>
			  </p>

			</div>
		  </div>

		  <button class="accordion acc-large" data-toggle="collapse" data-target="#age-restriction">4. Support</button>
		  <div id="age-restriction" class="collapse shop">
		    <div class="bigger-list">

			<p> 4.1 On-site support from competent Bet Palace field staff. </p>

			<p> 4.2 Free training course to prepare you and your staff for the opening of your betting shop</p>

			</div>
		  </div>

		  <button class="accordion acc-large" data-toggle="collapse" data-target="#account-security">5. Easy Steps to Start</button>
		  <div id="account-security" class="collapse shop">
		    <div class="bigger-list">

			<p> 
				<ol>
					<li>Submission of documentation to Bet Palace.</li>
					<li>Check of documents and location.</li>
					<li>Introductory conversation in person with our field staff.</li>
					<li>Verification of premises.</li>
					<li>Signing of the partnership contract.</li>
					<li>Get your shop ready to go with help from our professional support.</li>
				</ol>
			</p>

			</div>

		  </div>

     </div>

</div>