<div class="container">
{{ partial("partials/header") }}

<style>
  @media (min-width: 768px){
        .os{
            position: absolute!important;
        }
        body{
          overflow-x: hidden!important;
        }
  }
  @media (max-width: 768px){
    .betslip-mobi{
      color: black!important;
    }
    .slip-counter {
      color: black!important;
      background-color: black!important;
    }


    .bottom.rectangle #tawkchat-minified-wrapper{
      display: none!important;
    }
  }
</style>
<!-- <iframe style="width: 100%; height: 100vh;" src="https://betpalace-test.virtual-horizon.net/responsive/ext/betpalace/vspro-headless.jsp"></iframe>
 -->

 <span id="verification_code" style="display: none;"><?php echo $verification_code; ?></span>
 <div id="globalbet" style="overflow-x: hidden; max-width: 100%;"></div>
</div>

<script src="https://betpalace.virtual-horizon.com/responsive/js/widgetIntegration.nocache.js" type="text/javascript" ></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
    var container = document.getElementById("globalbet");
    if(document.getElementById("verification_code") != null){
        var code = document.getElementById("verification_code").innerHTML;
    }else{
        var code = "";
    }

    if(code == ""){
        if(screen.width > 768){
            widgetAdapter.registerVirtualSports(container, "https://betpalace.virtual-horizon.com/engine/web/autologin/account?webRedirectTo=/responsive/ext/betpalace/vspro-headless.jsp%3Flocale=en_US%26agent=betpalace", { sticky: true });
        }else{
            widgetAdapter.registerVirtualSports(container, "https://betpalace.virtual-horizon.com/engine/web/autologin/account?webRedirectTo=/responsive/ext/betpalace/vspro-headless.jsp%3Flocale=en_US%26agent=betpalace");
        }
    }else{
        if(screen.width > 768){
            widgetAdapter.registerVirtualSports(container, "https://betpalace.virtual-horizon.com/engine/web/autologin/account?code="+code+"&webRedirectTo=/responsive/ext/betpalace/vspro-headless.jsp%3Flocale=en_US%26agent=betpalace", { sticky: true });
        }else{
            widgetAdapter.registerVirtualSports(container, "https://betpalace.virtual-horizon.com/engine/web/autologin/account?code="+code+"&webRedirectTo=/responsive/ext/betpalace/vspro-headless.jsp%3Flocale=en_US%26agent=betpalace");
        }
    }
    
    
    });
</script>

{{ partial("partials/footer") }}