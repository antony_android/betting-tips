<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
// Register some namespaces

$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->servicesDir,
        $config->application->modelsDir,
    ]
);


$loader->registerFiles([APP_PATH . '/helpers.php']);

$loader->register();


