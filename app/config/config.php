<?php
/**
 * Copyright (c) Anto 2018.
 *
 * All rights reserved.
 */

defined('APP_PATH') || define('APP_PATH', realpath('.'));

if (!function_exists('env')) {
    function env($key, $default = null)
    {
        $env = new \Dotenv\Dotenv(APP_PATH);
        $env->load();

        return getenv($key) ?: $default;
    }
}

return new \Phalcon\Config([
    'database'    => [
        'adapter'  => env('DATABASE_ADAPTER'),
        'host'     => env('DATABASE_HOST'),
        'username' => env('DATABASE_USER'),
        'password' => env('DATABASE_PASSWORD'),
        'dbname'   => env('DATABASE_NAME'),
        'charset'  => 'utf8',
    ],
    'application' => [
        'controllersDir' => APP_PATH . '/app/controllers/',
        'servicesDir'    => APP_PATH . '/app/services/',
        'modelsDir'      => APP_PATH . '/app/models/',
        'migrationsDir'  => APP_PATH . '/app/migrations/',
        'viewsDir'       => APP_PATH . '/app/views/',
        'pluginsDir'     => APP_PATH . '/app/plugins/',
        'libraryDir'     => APP_PATH . '/app/library/',
        'vendorDir'      => APP_PATH . '/vendor/',
        'cacheDir'       => APP_PATH . '/app/cache/',
        'baseUri'        => env('BASE_URL', '/'),
        'debug'          => env('APP_DEBUG', true),
    ],
]
);
