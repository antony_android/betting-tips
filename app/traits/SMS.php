<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

/**
 * Created by PhpStorm.
 * User: mxgel
 * Date: 08/03/2018
 * Time: 14:45
 */
trait SMS
{
    /**
     * Send SMS
     * @param $message
     * @param $recipient
     *
     * @return mixed
     */
    protected function sendShortMessage($message, $recipient)
    {
        $URL = "35.233.126.93:8008/sendsms";

        $httpRequest = curl_init($URL);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, $this->constructSMS($message, $recipient));
        curl_setopt($httpRequest, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = json_decode($results);

        return $status_code;
    }

    /**
     * @param $message
     * @param $recipient
     *
     * @return string
     */
    protected function constructSMS($message, $recipient)
    {
        $sms = "msisdn=%s&message=%s&short_code=12312&correlator=&message_type=BULK&link_id=&exchange=BETPALACE_SENDSMS_EX";

        return sprintf($sms, $recipient, str_replace(' ', '+', $message));
    }
}
