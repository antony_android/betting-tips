<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

class RunningBalance extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $profile_id;

    /**
     *
     * @var string
     */
    public $account;

    /**
     *
     * @var double
     */
    public $amount;

    /**
     *
     * @var double
     */
    public $running_balance;

    /**
     *
     * @var string
     */
    public $created_by;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var string
     */
    public $modified;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'running_balance';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RunningBalance[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RunningBalance
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
