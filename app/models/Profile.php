<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

class Profile extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $profile_id;

    /**
     *
     * @var string
     */
    public $msisdn;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var integer
     */
    public $status;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $modified;

    /**
     *
     * @var string
     */
    public $created_by;

    /**
     *
     * @var string
     */
    public $network;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('profile_id', 'Bet', 'profile_id', array('alias' => 'Bet'));
        $this->hasMany('profile_id', 'Outbox', 'profile_id', array('alias' => 'Outbox'));
        $this->hasMany('profile_id', 'Transaction', 'profile_id', array('alias' => 'Transaction'));
        $this->hasMany('profile_id', 'Winner', 'profile_id', array('alias' => 'Winner'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'profile';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Profile[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Profile
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
