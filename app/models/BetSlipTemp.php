<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

class BetSlipTemp extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $bet_slip_id;

    /**
     *
     * @var integer
     */
    public $match_id;

    /**
     *
     * @var string
     */
    public $bet_pick;

    /**
     *
     * @var string
     */
    public $reference_id;

    /**
     *
     * @var integer
     */
    public $status;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var string
     */
    public $modified;

    /**
     *
     * @var integer
     */
    public $sub_type_id;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'bet_slip_temp';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BetSlipTemp[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BetSlipTemp
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
