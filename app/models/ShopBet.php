<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

class ShopBet extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $shop_bet_id;

    /**
     *
     * @var string
     */
    public $bet_id;

    /**
     *
     * @var double
     */
    public $status;

    /**
     *
     * @var string
     */
    public $created;


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'shop_bet';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ShopBet[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ShopBet
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}