<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

class ProfileBalance extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $profile_balance_id;

    /**
     *
     * @var string
     */
    public $profile_id;

    /**
     *
     * @var double
     */
    public $balance;

    /**
     *
     * @var string
     */
    public $transaction_id;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var string
     */
    public $modified;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'profile_balance';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProfileBalance[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProfileBalance
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
