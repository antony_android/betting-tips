<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

class ProfileSettings extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $profile_setting_id;

    /**
     *
     * @var string
     */
    public $profile_id;

    /**
     *
     * @var string
     */
    public $balance;

    /**
     *
     * @var integer
     */
    public $status;

    /**
     *
     * @var integer
     */
    public $verification_code;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $reference_id;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     *
     * @var string
     */
    public $password;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'profile_settings';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProfileSettings[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProfileSettings
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
