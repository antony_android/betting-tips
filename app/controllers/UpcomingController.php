<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

/**
 * Class UpcomingController
 */
class UpcomingController extends ControllerBase
{
    /**
     * Init
     */
    public function initialize()
    {
        $this->tag->setTitle('Upcoming Matches');
    }

    /**
     * Index
     */
    public function indexAction()
    {

        $sport_id = $this->request->get('sp', 'int');

        if(empty($sport_id)) {$sport_id = 79;}

        list($page, $limit, $skip) = $this->getPaginationParams();

        $this->session->set("previous_address", $_SERVER['REQUEST_URI']);

        $keyword = $this->request->getPost('keyword');

        $date = new DateTime(); 

        $where = " and date(start_time) =  '" . $date->format('Y-m-d') . "' ";

        list($today, $total, $sCompetitions) = $this->getGames($keyword, $skip, $limit, $where, 'start_time asc');

        $total = $total['0']['total'];

        $pages = ceil($total / $limit);

        $theBetslip = $this->session->get("betslip");

       $this->view->setVars([
            'today'         => $today,
            'theBetslip'    => $theBetslip,
            'sCompetitions' => $sCompetitions,
            'total'         => $total,
            'pages'         => $this->getResultPages($total, $limit),
            'page'          => $page,
            'topSports'     => $this->topSports(),
            'sportId' =>$sport_id,
            'keyword' =>$keyword
        ]);

        $this->tag->setTitle($this->view->t->_('title'));
    }

}

