<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

class TomorrowController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Tomorrow Games');
    }

    public function indexAction()
    {
        $sport_id = $this->request->get('sp', 'int');

        if(empty($sport_id)) {$sport_id = 79;}

        list($page, $limit, $skip) = $this->getPaginationParams();

        $this->session->set("previous_address", $_SERVER['REQUEST_URI']);

        $keyword = $this->request->getPost('keyword');

        $where = " and date(start_time) = curdate() + INTERVAL 1 DAY ";

        list($today, $total, $sCompetitions) = $this->getGames($keyword, $skip, $limit, $where, 'm_priority desc, priority desc, start_time asc');

        $total = $total['0']['total'];

        $pages = ceil($total / $limit);

        $theBetslip = $this->session->get("betslip");

       $this->view->setVars([
            'today'         => $today,
            'theBetslip'    => $theBetslip,
            'sCompetitions' => $sCompetitions,
            'total'         => $total,
            'pages'         => $this->getResultPages($total, $limit),
            'page'          => $page,
            'topSports'     => $this->topSports(),
            'sportId' =>$sport_id,
            'keyword' =>$keyword
        ]);

        $this->tag->setTitle($this->view->t->_('title'));
    }

}