<?php

/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */
class PeerbetsController extends ControllerBase
{
    /**
     *
     */
    public function indexAction()
    {
        $id = $this->request->get('id', 'int');
        $keyword = $this->request->getPost('keyword', 'string');
        $bindings = [];
               
        $where = "";
    
        if(!empty($keyword)){
            $bindings['keyword'] = "%$keyword%";
            $where .= " and (p.msisdn like :keyword or b.bet_amount like :keyword or bs.bet_pick like :keyword) ";
        }
        
        $sCompetitions = $this->getCompetitions();
        list($page, $limit, $skip) = $this->getPaginationParams();

        $matches = $this->rawQueries("SELECT m.game_id, pb.bet_id, p.msisdn, m.home_team, m.away_team, b.bet_amount, m.start_time, bs.bet_pick FROM peer_bet pb
     INNER JOIN bet b ON pb.bet_id = b.bet_id INNER JOIN `profile` p ON p.profile_id = b.profile_id 
     INNER JOIN bet_slip bs ON b.bet_id = bs.bet_id INNER JOIN `match` m 
     ON bs.parent_match_id = m.parent_match_id $where GROUP BY pb.bet_id LIMIT $skip, $limit", $bindings);

        $items = $this->rawQueries("select count(*) FROM peer_bet pb
     INNER JOIN bet b ON pb.bet_id = b.bet_id INNER JOIN `profile` p ON p.profile_id = b.profile_id 
     INNER JOIN bet_slip bs ON b.bet_id = bs.bet_id INNER JOIN `match` m 
     ON bs.parent_match_id = m.parent_match_id $where LIMIT $skip, $limit", $bindings);

        $total = $items['0']['total'];

        $results = [];
        foreach ($matches as $match) {
            $results[(new DateTime($match['start_time']))->format($this->getDefaultDateFormat())][] = $match;
        }
        $theCompetition = $this->rawQueries("SELECT competition_name, competition_id,category, sport_name FROM competition inner join sport using(sport_id) WHERE competition_id=? LIMIT 1", [$id]);

        $theBetslip = $this->session->get("betslip-peer");

        $title = $theCompetition['0']['competition_name'] . ", " . $theCompetition['0']['category'];

        $this->tag->setTitle($title);

        $this->view->setVars([
            'today'          => $results,
            'sCompetitions'  => $this->getCompetitions(),
            'theCompetition' => $theCompetition,
            'theBetslip'     => $theBetslip,
            'pages'          => $this->getResultPages(0, $limit),
            'total'          => $total,
            'topSports'      => $this->topSports(),
            'sportId' =>$sport_id,
            'keyword' =>$keyword,
            'title' => $title,
            'competitionId'=>$id,
        ]);

        $this->view->pick("rafiki/bets");
    }

}