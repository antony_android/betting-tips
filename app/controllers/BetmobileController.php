<?php

/**
 * Copyright (c) Tony 2018.
 *
 * All rights reserved.
 */

use Phalcon\Http\Response;

class BetmobileController extends ControllerBase
{
    /**
     *
     */
    public function indexAction()
    {
        $reference_id = $this->reference();

        $referrer = isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"";

        if(strpos($referrer, 'rafiki') !== false){

            $betslip = $this->session->get('betslip-peer');

            $referenceID = $this->reference();

            $slipCount = sizeof($betslip);

            $this->tag->setTitle('Betslip - betpalace');

            $this->view->setVars([
                "betslip"     => $betslip,
                "referenceID" => $referenceID,
                "slipCount"   => $slipCount, 
                "amount"      => $this->session->get('bet-amount', null, true),
            ]);

            $this->view->pick("betmobile/peer");

        }else if(strpos($referrer, 'sgr') !== false){

            $betslip = $this->session->get('betslip_sgr');

            $referenceID = $this->reference();

            $slipCount = sizeof($betslip);

            $this->tag->setTitle('SGR Betslip - betpalace');

            $this->view->setVars([
                "betslip"     => $betslip,
                "referenceID" => $referenceID,
                "slipCount"   => $slipCount, 
                "amount"      => $this->session->get('bet-amount', null, true),
            ]);

            $this->view->pick("betmobile/sgr");

        }else{

            $betslip = $this->session->get('betslip');
            $slipCount = sizeof($betslip);

            $referenceID = $this->reference();

            $this->tag->setTitle('Betslip - betpalace');

            $this->session->set('betslip', $betslip);

            $this->view->setVars([
                "betslip"     => $betslip,
                "referenceID" => $referenceID,
                "slipCount"   => $slipCount, 
                "amount"      => $this->session->get('bet-amount', null, true),
            ]);
        }
        
    }


    public function fetchslipAction()
    {
        $reference_id = $this->reference();

        $referrer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER']:"";
        $this->tag->setTitle('Betslip - betpalace');

        $referenceID = $this->reference();
        if(strpos($referrer, 'rafiki') !== false){

            $betslip = $this->session->get('betslip-peer');
            $slipCount = sizeof($betslip);

            $this->tag->setTitle('Betslip - betpalace');

            $this->view->setVars([
                "betslip"     => $betslip,
                "referenceID" => $referenceID,
                "slipCount"   => $slipCount, 
                "amount"      => $this->session->get('bet-amount', null, true),
            ]);

            $this->view->pick("betmobile/peer");

        }else{

            $max_delay_time = strtotime("+10 seconds");

            $last_upadated =  $this->session->get("last_update_datetime");

            $slip_matches = "(";

            $betslip = $this->session->get('betslip');
            $slipCount = sizeof($betslip);
            $counter = 0;
            $hasLive = false;
            $totalOdd = 1;
            $betslip = empty($betslip) ? [] : $betslip;

            foreach($betslip as $match_id => $slip){
                if($slip['bet_type'] == 'live'){
                    $hasLive = true;
                }
                $totalOdd *= $slip['odd_value'];
                 $slip_matches = $slip_matches.$slip['parent_match_id'].",";
                 $counter++;
            }

            if(!$hasLive){

                sleep(15);
                $results = [
                    "betslip"     => $betslip,
                    "has_live"    => 0,
                    "referenceID" => $referenceID,
                    "slipCount"   => $slipCount, 
                    'totalOdd'    => $totalOdd,
                    "amount"      => $this->session->get('bet-amount', null, true),
                ];

                $response = new Response();
                $response->setStatusCode(201, "OK");
                $response->setHeader("Content-Type", "application/json");
                $results = json_encode($results);
                $response->setContent($results);

                return $response;
            }

            if($counter > 0){

                $slip_matches = substr($slip_matches, 0, strlen($slip_matches)-1);
            }else{

                $slip_matches = $slip_matches."0";
            }

            $slip_matches = $slip_matches.")";
            
            while(!empty($last_upadated)){

                 $updateQuery = "select c.modified from live_odds_change c inner join live_match using(parent_match_id) 
                    where match_id IN ".$slip_matches." order by 1 desc limit 1";

                $lastUpdateDateResult = $this->rawQueries($updateQuery);

                if(!empty($lastUpdateDateResult)){
                    
                    $lastUpdateDateResult = $lastUpdateDateResult[0]['modified'];
                    if($lastUpdateDateResult != $last_upadated){
                        $this->session->set('last_update_datetime', $lastUpdateDateResult);
                        break;
                    }
                }
                if(time() > $max_delay_time){
                    //Alow update on UI after max delay time
                    break;
                }
                sleep(1);
                
            }
            
            if(empty($last_upadated)){
                $updateQuery = "select now() as modified ";
               
                $lastUpdateDateResult = $this->rawQueries($updateQuery);
                if(!empty($lastUpdateDateResult)){
                    $lastUpdateDateResult = $lastUpdateDateResult[0]['modified'];
                    $this->session->set('last_update_datetime', $lastUpdateDateResult); 
                }

            }

            $newslip = [];
            $totalOdd = 1;
            foreach($betslip as $match_id => $slip){
                $sub_type_id = $slip['sub_type_id'];
                $parent_match_id = $slip['parent_match_id'];
                $special_bet_value = $slip['special_bet_value'];
                $bet_pick = $slip['bet_pick'];
                $bet_type =  $slip['bet_type'];

                $query = "select m.parent_match_id, e.sub_type_id, e.special_bet_value, 
                        e.odd_value, e.odd_active, e.market_active, "
                    . " e.active as eactive from live_odds_change e inner join `live_match` "
                    . " m using(parent_match_id) where m.active=1 "
                    . " and e.market_active = 'Active' and  "
                    . " e.parent_match_id='$parent_match_id' and e.sub_type_id='$sub_type_id' "
                    . " and e.special_bet_value='$special_bet_value' and e.odd_key='$bet_pick' "
                    . " order by e.betradar_timestamp desc limit 1 ";
            
                $data = $this->rawQueries($query);

                if(!empty($data) && 
                    $data[0]['odd_active'] == 1 && $data[0]['market_active'] == 'Active' 
                    &&  $data[0]['eactive'] == 1){
                    
                        $new_odd = $data[0]['odd_value'];
                        $parent_match_id = $data[0]['parent_match_id'];
                        $sub_type_id = $data[0]['sub_type_id'];
                        $special_bet_value = $data[0]['special_bet_value'];

                        $slip['parent_match_id'] = $parent_match_id;
                        $slip['sub_type_id'] = $sub_type_id;
                        $slip['special_bet_value'] = $special_bet_value;
                        $slip['odd_value'] = $new_odd;

                        $newslip[$match_id] = $slip;
                }else{
                    
                    $slip['odd_value'] = 1;
                    $slip['parent_match_id'] = $parent_match_id;
                    $slip['sub_type_id'] = $sub_type_id;
                    $slip['special_bet_value'] = $special_bet_value;
                    $newslip[$match_id] = $slip; 
                    
                }
                $totalOdd *=  $slip['odd_value'];
            }

            $totalOdd = round($totalOdd,2);

            $betslip = $newslip;
            $this->session->set('betslip', $betslip);

            $results = [
                "betslip"     => $betslip,
                "has_live"    => 1,
                "referenceID" => $referenceID,
                "slipCount"   => $slipCount, 
                'totalOdd'    => $totalOdd,
                "amount"      => $this->session->get('bet-amount', null, true),
            ];

            $response = new Response();
            $response->setStatusCode(201, "OK");
            $response->setHeader("Content-Type", "application/json");

            $results = json_encode($results);

            $response->setContent($results);

            return $response;
        }
        
    }
}
