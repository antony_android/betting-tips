<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

class LogoutController extends ControllerBase
{

    public function indexAction()
    {

        $user = $this->session->get('auth');
        $profile_id = $user['id'];

        $prefix = "CTPLEBAAE";
        $uniqid1 = uniqid();
        $uniqid2 = uniqid();
        $uniqid = strtoupper($prefix.'-'.$uniqid1.'-'.$uniqid2);

        $findCode = $this->rawQueries("select * from virtualsports_verification_codes where player_id='$profile_id' limit 1");
        
        if(empty($findCode)){
            // $insert = $this->rawInsert("insert into virtualsports_verification_codes (player_code, player_id) values('$uniqid', '$profile_id')");
        }else{
            $statement = "delete from virtualsports_verification_codes where player_id='$profile_id'";
            $connection = $this->di->getShared("db");
            $success = $connection->query($statement);
        }
     
    	$this->session->destroy();
    	$this->cookies->get('auth')->delete();
        $this->response->redirect($_SERVER['HTTP_REFERER']);
        $this->view->disable();
    }

   

}

