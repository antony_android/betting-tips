<?php

/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */
class VerifyController extends ControllerBase
{

    /**
     *
     */
    public function indexAction()
    {
        $this->view->setVars(["topLeagues" => $this->topLeagues()]);
        $title = "Verify account";

        $this->tag->setTitle($title);
    }

    /**
     *
     */
    public function checkAction()
    {
        $mobile = $this->request->get('mobile', 'int');
        $verification_code = $this->request->get('verification_code', 'int');

        if (!$mobile || !$verification_code) {
            $this->flashSession->error($this->flashMessages('All fields are required'));
            $this->response->redirect('verify');

            // Disable the view to avoid rendering
            $this->view->disable();
        }

        $mobile = $this->formatMobileNumber($mobile);

        $phql = "SELECT * from Profile where msisdn='$mobile' limit 1";
        $user = $this->modelsManager->executeQuery($phql);

        $user = $user->toArray();

        if ($user == false) {
            $this->flashSession->error($this->flashMessages('User does not exist'));
            $this->response->redirect('verify');
            // Disable the view to avoid rendering
            $this->view->disable();
        }

        $profile_id = $user['0']['profile_id'];

        $phql = "SELECT * from ProfileSettings where profile_id='$profile_id' limit 1";
        $checkUser = $this->modelsManager->executeQuery($phql);

        $checkUser = $checkUser->toArray();

        if (!$user) {
            $this->flashSession->error($this->flashMessages('User is not registered'));
            $this->response->redirect('verify');
            // Disable the view to avoid rendering
            $this->view->disable();
        } else {
            $data = $this->rawInsert("update profile_settings set status='1' where profile_id='$profile_id'");

            // $insert_bonus = $this->rawInsert("insert into profile_bonus(profile_id, bonus_amount, status, bet_on_status, referred_msisdn,created_by) values('$profile_id','1000','CLAIMED','1','$mobile','registration_bonus')");

            // $select_balance = $this->rawSelect("select profile_id from profile_bonus where profile_id='$profile_id' limit 1");

            // if (!$select_balance) {
            //     $insert_bonus = $this->rawInsert("insert into profile_balance(profile_id, balance, transaction_id, bonus_balance) values('$profile_id','0','-1','0')");
            // }

            // $this->rawInsert("update profile_balance set bonus_balance = bonus_balance+1000 where profile_id = '$profile_id' limit 1");

            $dat = ['profile_id' => $profile_id];

            $exp = time() + 3600;

            $token = $this->generateToken($dat, $exp);

            $transaction = "token=$token";

            $bonus = $this->bonus($transaction);

            $this->flashSession->error($this->flashMessages('Login to access your account'));
            $this->response->redirect('login');
            // Disable the view to avoid rendering
            $this->view->disable();

            // Send sms
            $this->sendShortMessage(
                "Karibu betpalace.co.ke SMS GAMES to 29085 Paybill 290085 "
                . " Singlebet: Send GAMEID#PICK#AMOUNT. Multibet: "
                ." GAMEID#PICK#GAMEID#PICK#AMOUNT to 29085", $mobile);
        }
    }

}

