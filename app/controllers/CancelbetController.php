<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\View;

class CancelbetController extends ControllerBase
{

    public function indexAction()
    {
        $id = $this->request->get('id', 'int');
        $profile_id = $this->session->get('auth')['id'];

        $myBet = $this->rawQueries("SELECT b.bet_id,b.created, total_games AS total_matches, b.bet_message,b.bet_amount,b.possible_win,b.status, b.profile_id, if(FIND_IN_SET(b.status,(SELECT group_concat(status) FROM bet_slip WHERE bet_id = b.bet_id)), b.status, 1) xstatus FROM bet b INNER JOIN bet_slip s ON b.bet_id=s.bet_id WHERE b.bet_id=? GROUP BY s.bet_id ORDER BY b.created DESC LIMIT 40", [$id]);
        $myBet = $myBet['0'];

        if ($myBet['profile_id'] != $profile_id) {
            $this->response->redirect('betmobile');
            $this->view->disable();
        } else {
            $this->view->setVars(["myBet" => $myBet, 'winners'=>$this->topWinners()]);
            $this->view->pick("mybets/cancel");
        }
    }

    public function cancelAction()
    {
        $bet_id = $this->request->getPost('bet_id', 'int');
        $redr = 'mybets?id=' . $bet_id;


        if (!$bet_id) {
            $this->flashSession->error($this->flashMessages('Bet ID not found'));

            return $this->response->redirect($redr);
            $this->view->disable();
        } else {
            $mobile = $this->session->get('auth')['mobile'];
            $profile_id = $this->session->get('auth')['id'];

            $data = ['bet_id'      => $bet_id,
                     'profile_id'  => $profile_id,
                     'cancel_code' => '101',
                     'msisdn'      => $mobile,
            ];

            $exp = time() + 3600;

            $token = $this->generateToken($data, $exp);

            $transaction = "token=$token";

            $cancelbet = $this->cancelBet($transaction);

            $this->flashSession->error($this->flashSuccess($cancelbet['message']));

            return $this->response->redirect($redr);
            $this->view->disable();
        }
    }

}
