<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

class SignupController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Sign up');
    }

    public function indexAction()
    {
        $this->view->setVars(["topLeagues" => $this->topLeagues(), 'winners' =>$this->topWinners()]);
    }

    public function joinAction()
    {

        if ($this->request->isPost()) {

            $mobile = $this->request->getPost('mobile', 'int');
            // $age = $this->request->getPost('age', 'int');
            // $terms = $this->request->getPost('terms', 'int');
            $password = $this->request->getPost('password');
            // $repeatPassword = $this->request->getPost('repeatPassword');
            $src = 'DESKTOP';

            if (!$mobile || !$password) {
                $this->flashSession->error($this->flashMessages('Please specify your mobile number and the chosen password'));
                $this->response->redirect('signup');

                // Disable the view to avoid rendering
                $this->view->disable();
            } else {

                // if ($password != $repeatPassword) {
                //     $this->flashSession->error($this->flashMessages('Passwords do not match'));
                //     $this->response->redirect('signup');

                //     // Disable the view to avoid rendering
                //     $this->view->disable();
                // }

                $mobile = $this->formatMobileNumber($mobile);
                $password = $this->security->hash($password);
                $verification_code = substr(number_format(time() * mt_rand(), 0, '', ''), 0, 4);

                if ($mobile == false) {
                    $this->flashSession->error($this->flashMessages('Invalid mobile number'));
                    $this->response->redirect('signup');

                    // Disable the view to avoid rendering
                    $this->view->disable();
                } else {
                    $message = "Karibu betpalace.co.ke SMS GAMES to 29085 Paybill 290085 Singlebet: Send GAMEID#PICK#AMOUNT. Multibet: GAMEID#PICK#GAMEID#PICK#AMOUNT to 29085";

                    $checkProfile = $this->rawQueries("select msisdn,profile_id from profile where msisdn='$mobile' limit 1");

                    if (empty($checkProfile)) {

                        $profile_id = $this->rawInsert("insert into profile (msisdn,status,created_by,created) values('$mobile','1','mabet.bet',now())");

                        $insert = $this->rawInsert("insert into profile_settings (profile_id,password,verification_code,reference_id, status, created_at) values('$profile_id','$password','$verification_code','$src', 1, now())");

                        $dat = ['profile_id' => $profile_id];
                        $exp = time() + 3600;
                        $token = $this->generateToken($dat, $exp);
                        $transaction = "token=$token";
                        $bonus = $this->bonus($transaction);

                        // FIXME: Update to swahili
                        // $msg = 'Enter the verification code sent to your phone to complete the registration process.';
                        $msg = "Karibu betpalace.co.ke SMS GAMES to 29085 Paybill 290085 Singlebet: Send GAMEID#PICK#AMOUNT. Multibet: GAMEID#PICK#GAMEID#PICK#AMOUNT to 29085";
                        $send = $this->sendShortMessage($message, $mobile);
                        $this->flashSession->error($this->flashMessages($msg));
                        $this->response->redirect('/');

                        //Disable the view to avoid rendering

                        $this->view->disable();

                    } else {

                        $profile_id = '';

                        foreach ($checkProfile as $check) {
                            $profile_id = $check['profile_id'];
                        }

                        $checkUser = $this->rawQueries("select profile_id from profile_settings where profile_id='$profile_id' limit 1");

                        if (empty($checkUser)) {
                            $insert = $this->rawInsert("insert into profile_settings (profile_id,password,verification_code,reference_id, status, created_at) values('$profile_id','$password','$verification_code','$src', 1, now())");

                            $send = $this->sendShortMessage($message, $mobile);

                            $this->flashSession->error($this->flashMessages('Welcome to BETPALACE! SMS GAMES to 29085.  To place a bet, top up your account: 290085. Single bet:Send GAMEID#PICK#AMOUNT to 29085. Multibet:Send GAMEID#PICK#GAMEID#PICK#AMOUNT to 29085. 0705 290085. www.betpalace.co.ke'));
                            $this->response->redirect('/');

                            // Disable the view to avoid rendering
                            $this->view->disable();

                        } else {
                            $this->flashSession->error($this->flashMessages('Mobile number already in use'));
                            $this->response->redirect('signup');

                            // Disable the view to avoid rendering
                            $this->view->disable();
                        }
                    }
                }
            }
        }
    }

}

