<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

class FootballController extends ControllerBase
{

    public function indexAction()
    {
    	$id = $this->request->get('id', 'int');

	if(!is_numeric($id))
		$id = 1;

    	$matches = $this->rawQueries("select c.priority, (select count(distinct sub_type_id) from event_odd where parent_match_id = m.parent_match_id) as side_bets, o.sub_type_id, MAX(CASE WHEN o.odd_key = '1' THEN odd_value END) AS home_odd, MAX(CASE WHEN o.odd_key = 'x' THEN odd_value END) AS neutral_odd, MAX(CASE WHEN o.odd_key = '2' THEN odd_value END) AS away_odd, m.game_id, m.match_id, m.start_time, m.away_team, m.home_team, m.parent_match_id from `match` m inner join event_odd o on m.parent_match_id = o.parent_match_id inner join competition c on c.competition_id = m.competition_id inner join sport s on s.sport_id = c.sport_id where c.competition_id='$id' and m.start_time > now() and m.status <> 3 and o.sub_type_id = 1  group by m.parent_match_id order by m.priority desc, c.priority desc , m.start_time limit 60");

    	$theCompetition = $this->rawQueries("select competition_name,competition_id,category from competition where competition_id='$id' limit 1");

        $theBetslip = $this->session->get("betslip");

        $title = $theCompetition['0']['competition_name'].", ".$theCompetition['0']['category'];

        $this->tag->setTitle($title);

    	$this->view->setVars(['matches'=>$matches,'theCompetition'=>$theCompetition,'theBetslip'=>$theBetslip]);

    	$this->view->pick("football/index");
    }
 
    public function competitionAction($id)
    {
    	$id = $this->request->getPost('id','int');
    	$matches = $this->rawQueries("select o.sub_type_id, MAX(CASE WHEN o.odd_key = '1' THEN odd_value END) AS one, MAX(CASE WHEN o.odd_key = 'x' THEN odd_value END) AS x, MAX(CASE WHEN o.odd_key = '2' THEN odd_value END) AS two, count(distinct o.sub_type_id) as side_bets, m.game_id, m.match_id, m.start_time, m.away_team, m.home_team, m.parent_match_id from `match` m inner join event_odd o on m.parent_match_id = o.parent_match_id  where m.competition_id='$id' and o.sub_type_id = 1 and m.start_time > now() group by m.parent_match_id order by start_time limit 60");

    	$this->view->pick("football/index");
    }

}