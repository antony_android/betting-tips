<?php

/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */
class SgrController extends ControllerBase
{

    /**
     * Results controller to get the results, outcome hardcoded to be changed later 
     */
    public function indexAction()
    {

        $jackpotID = $this->rawQueries("SELECT jackpot_event_id, jackpot_type FROM jackpot_event 
            WHERE status = 'ACTIVE' AND jackpot_type = 4 AND jp_key='SGR' ORDER BY 1 DESC LIMIT 1");

        $jackpotID = $jackpotID['0']['jackpot_event_id'];

        $jackpot_type = $jackpotID['0']['jackpot_type'];
        $games = $this->rawQueries("select e.odd_key, j.game_order as pos, jackpot_match_id,
            e.sub_type_id, MAX(CASE WHEN e.odd_key = m.home_team THEN odd_value END) AS home_odd,
             MAX(CASE WHEN e.odd_key = 'draw' THEN odd_value END) AS neutral_odd, MAX(CASE WHEN 
             e.odd_key = m.away_team THEN odd_value END) AS away_odd, m.game_id, m.match_id, 
             m.start_time, m.parent_match_id, m.away_team, m.home_team, c.competition_name, 
             c.category from jackpot_match j inner join `match` m on m.parent_match_id = 
             j.parent_match_id INNER JOIN competition c ON m.competition_id = c.competition_id 
             inner join event_odd e on e.parent_match_id = m.parent_match_id where 
             j.jackpot_event_id='$jackpotID' and j.status='ACTIVE'  and e.sub_type_id=1 group by 
             j.parent_match_id order by game_order");

        $lastGames = null;

        if(empty($games)){

            $lastAutoID = $this->rawQueries("SELECT jackpot_event_id, jackpot_type FROM 
                jackpot_event WHERE jackpot_type = 4 ORDER BY 1 DESC LIMIT 1");

            $lastID = $lastAutoID['0']['jackpot_event_id'];
            $jackpot_type = $jackpotID['0']['jackpot_type'];
            $lastAutoType = $lastAutoID['0']['jackpot_type'];

            $lastGames = $this->rawQueries("SELECT e.odd_key, o.winning_outcome AS outcome, 
                j.game_order AS pos, jackpot_match_id,e.sub_type_id, MAX(CASE WHEN e.odd_key 
                = m.home_team THEN  odd_value END) AS home_odd, MAX(CASE WHEN e.odd_key = 'draw' 
                THEN odd_value END)  AS neutral_odd, MAX(CASE WHEN e.odd_key = m.away_team THEN 
                odd_value END) AS away_odd,   m.game_id, m.match_id, m.start_time, 
                m.parent_match_id, m.away_team, m.home_team, c.competition_name, c.category FROM 
                jackpot_match j INNER JOIN `match` m ON m.parent_match_id = j.parent_match_id 
                INNER JOIN competition c ON m.competition_id = c.competition_id INNER JOIN 
                event_odd e ON e.parent_match_id      = m.parent_match_id INNER JOIN outcome o 
                ON m.parent_match_id = o.parent_match_id WHERE j.jackpot_event_id='$lastID' AND 
                e.sub_type_id=1 AND o.is_winning_outcome=1 AND o.sub_type_id=45 GROUP BY 
                j.parent_match_id ORDER BY game_order");
        }

        $theBetslip = [];

        $betslip = $this->session->get("betslip_sgr");

        unset($theBetslip);

        foreach ($betslip as $slip) {
            if ($slip['bet_type'] == 'sgr') {
                $theBetslip[$slip['match_id']] = $slip;
            }
        }

        $slipCount = sizeof($theBetslip);

        $this->tag->setTitle('SGR BET - betpalace.co.ke');

        $this->view->setVars([
            "games"      => $games,
            'slipCount' => $slipCount,
            'theBetslip' => $theBetslip,
            'jackpotID' => $jackpotID,
            'jackpot_type' => $jackpot_type,
            "lastGames"    => $lastGames,
            'lastAutoID' => $lastID,
            'lastAutoType' => $lastAutoType,
        ]);

    }
}