<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\View;

class MatchesController extends ControllerBase
{

    public function indexAction()
    {
        $today = $this->rawQueries("SELECT c.priority, 
            (SELECT count(DISTINCT sub_type_id) FROM event_odd WHERE 
            parent_match_id = m.parent_match_id) AS side_bets, o.sub_type_id, 
            MAX(CASE WHEN o.odd_key = '1' THEN odd_value END) AS home_odd, 
            MAX(CASE WHEN o.odd_key = 'x' THEN odd_value END) AS neutral_odd, 
            MAX(CASE WHEN o.odd_key = '2' THEN odd_value END) AS away_odd, 
            m.game_id, m.match_id, m.start_time, m.away_team, m.home_team, 
            m.parent_match_id FROM `match` m INNER JOIN event_odd o ON 
            m.parent_match_id = o.parent_match_id INNER JOIN competition c ON 
            c.competition_id = m.competition_id INNER JOIN sport s ON s.sport_id = 
            c.sport_id WHERE date(m.start_time) = date(now()) AND m.start_time > 
            now() AND s.sport_id = 79 AND o.sub_type_id = 1 GROUP BY 
            m.parent_match_id ORDER BY m.priority DESC, c.priority DESC , m.start_time
             LIMIT 30");

        $theBetslip = $this->session->get("betslip");

        $this->view->setVars([
            "topLeagues" => $this->topLeagues(),
            'today'      => $today,
            'theBetslip' => $theBetslip,
        ]);

        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);

        $this->view->pick("partials/matches");

    }
    

    public function betslipAction()
    {
        $reference_id = $this->reference();

        $jackpot = $this->request->get('jackpot');

        $betslip = $this->session->get('betslip');
        $newslip = [];
        $totalOdd=1;
        $betslip = empty($betslip) ? [] : $betslip;

        foreach($betslip as $match_id => $slip) {
            $sub_type_id = $slip['sub_type_id'];
            $parent_match_id = $slip['parent_match_id'];
            $special_bet_value = $slip['special_bet_value'];
            $bet_pick = str_replace("'", "&apos;",$slip['bet_pick']);
            $bet_type =  $slip['bet_type'];
            if($bet_type == 'live'){
                $query = "select e.odd_value, e.odd_active, e.market_active, e.active as eactive, lo.active as m_active from "
                . " live_odds_change e inner join live_match lo using(parent_match_id) where "
                . " e.parent_match_id='$parent_match_id' and e.sub_type_id='$sub_type_id' "
                . " and e.market_active = 'Active' and lo.event_status = 'Live' "
                . " and e.special_bet_value='$special_bet_value' and e.odd_key='$bet_pick' "
                . " and lo.active = 1 order by e.betradar_timestamp desc limit 1 ";

            }else{
                $query = "select odd_value from event_odd where "
                . "parent_match_id='$parent_match_id' and sub_type_id='$sub_type_id' "
                . " and special_bet_value='$special_bet_value' and odd_key='$bet_pick'";
            }
    
            $data = $this->rawQueries($query);
            if(!empty($data) && $bet_type != 'live'){
                $new_odd = $data[0]['odd_value'];
                $slip['odd_value'] = $new_odd;
                $newslip[$match_id] = $slip;

            }else if(!empty($data) && 
                $data[0]['odd_active'] == 1 && $data[0]['market_active'] == 'Active' 
                && $data[0]['eactive'] == 1 && $data[0]['m_active'] == 1){
                $new_odd = $data[0]['odd_value'];
                $slip['odd_value'] = $new_odd;
                $newslip[$match_id] = $slip;
            }else{
                $slip['odd_value'] = 1;
                $newslip[$match_id] = $slip; 
            }
            $totalOdd *=  $slip['odd_value'];
        }

        $totalOdd = round($totalOdd,2);
        $betslip = $newslip;
        $this->session->set('betslip', $betslip);

        $referenceID = $this->reference();

        $betslipSession = $this->session->get("betslip");

        $this->view->setVars([
            "betslip"        => $betslip,
            'betslipSession' => $betslipSession,
            "referenceID"    => $referenceID,
            'totalOdd'       => $totalOdd,
        ]);

        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);

        $this->view->pick("partials/betslip");
    }


    public function sgrAction()
    {
        $reference_id = $this->reference();

        $jackpot = $this->request->get('jackpot');

        $betslip = $this->session->get('betslip_sgr');
        $newslip = [];
        $totalOdd=1;
        $betslip = empty($betslip) ? [] : $betslip;

        foreach($betslip as $match_id => $slip){
            $sub_type_id = $slip['sub_type_id'];
            $parent_match_id = $slip['parent_match_id'];
            $special_bet_value = $slip['special_bet_value'];
            $bet_pick = str_replace("'", "&apos;",$slip['bet_pick']);
            $bet_type =  $slip['bet_type'];
            if($bet_type == 'live'){
                $query = "select e.odd_value, e.odd_active, e.market_active, e.active as eactive, lo.active as m_active from "
                . " live_odds_change e inner join live_match lo using(parent_match_id) where "
                . " e.parent_match_id='$parent_match_id' and e.sub_type_id='$sub_type_id' "
                . " and e.market_active = 'Active' and lo.event_status = 'Live' "
                . " and e.special_bet_value='$special_bet_value' and e.odd_key='$bet_pick' "
                . " and lo.active = 1 order by e.betradar_timestamp desc limit 1 ";

            }else{
                $query = "select odd_value from event_odd where "
                . "parent_match_id='$parent_match_id' and sub_type_id='$sub_type_id' "
                . " and special_bet_value='$special_bet_value' and odd_key='$bet_pick'";
            }
    
            $data = $this->rawQueries($query);
            if(!empty($data) && $bet_type != 'live'){
                $new_odd = $data[0]['odd_value'];
                $slip['odd_value'] = $new_odd;
                $newslip[$match_id] = $slip;

            }else if(!empty($data) && 
                $data[0]['odd_active'] == 1 && $data[0]['market_active'] == 'Active' 
                && $data[0]['eactive'] == 1 && $data[0]['m_active'] == 1){
                $new_odd = $data[0]['odd_value'];
                $slip['odd_value'] = $new_odd;
                $newslip[$match_id] = $slip;
            }else{
                $slip['odd_value'] = 1;
                $newslip[$match_id] = $slip; 
            }
            $totalOdd *=  $slip['odd_value'];
        }

        $totalOdd = round($totalOdd,2);
        $betslip = $newslip;
        $this->session->set('betslip_sgr', $betslip);

        $referenceID = $this->reference();

        $betslipSession = $this->session->get("betslip_sgr");

        $this->view->setVars([
            "betslip"        => $betslip,
            'betslipSession' => $betslipSession,
            "referenceID"    => $referenceID,
            'totalOdd'       => $totalOdd,
        ]);

        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);

        $this->view->pick("partials/betslip_sgr");
    }


    public function peerAction()
    {
        $reference_id = $this->reference();

        $jackpot = $this->request->get('jackpot');

        $betslip = $this->session->get('betslip-peer');
        $newslip = [];
        foreach($betslip as $match_id => $slip){
  
            $parent_match_id = $slip['parent_match_id'];
            // $special_bet_value = $slip['special_bet_value'];
            $bet_pick = $slip['bet_pick'];
            $bet_amount = $slip['bet_amount'];
    
            $data = $this->rawQueries("select odd_value from event_odd where "
                . "parent_match_id='$parent_match_id' and sub_type_id='$sub_type_id' "
                . " and special_bet_value='$special_bet_value' and odd_key='$bet_pick'");
            $new_odd = $data[0]['odd_value'];;
            $slip['odd_value'] = $new_odd;
            $slip['bet_pick'] = $bet_pick;
            $slip['bet_amount'] = $bet_amount;
            $newslip[$match_id] = $slip;
        }

        $betslip = $newslip;

        $this->session->set('betslip-peer', $betslip);

        $referenceID = $this->reference();

        $betslipSession = $this->session->get("betslip-peer");

        $this->view->setVars([
            "betslip"        => $betslip,
            'betslipSession' => $betslipSession,
            "referenceID"    => $referenceID,
        ]);

        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);

        $this->view->pick("partials/betslip-peer");
    }


    public function peernewAction()
    {

        $reference_id = $this->reference();

        $betslip = $this->session->get('betslip-peer');

        $newslip = [];
        foreach($betslip as $match_id => $slip){
  
            $parent_match_id = $slip['parent_match_id'];
            $special_bet_value = $slip['special_bet_value'];
            $bet_pick = $slip['bet_pick'];
            $bet_amount = $slip['bet_amount'];
            $peer_msisdn = $slip['peer_msisdn'];

            $new_odd = $slip['odd_value'];

            $slip['odd_value'] = $new_odd;
            $slip['bet_pick'] = $bet_pick;
            $slip['bet_amount'] = $bet_amount;
            $slip['peer_msisdn'] = $peer_msisdn;
            $newslip[$match_id] = $slip;
        }

        $betslip = $newslip;

        $this->session->set('betslip-peer', $betslip);

        $referenceID = $this->reference();

        $betslipSession = $this->session->get("betslip-peer");

        $this->view->setVars([
            "betslip" => $betslip,
            'betslipSession' => $betslipSession,
            "referenceID"    => $referenceID,
        ]);

        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);

        $this->view->pick("partials/betslip-peer-new");
    }


    public function autobetAction()
    {
        $reference_id = $this->reference();

        $betslip = $this->session->get('betslip-autobet');
        $newslip = [];
        
        foreach($betslip as $match_id => $slip){

            $sub_type_id = $slip['sub_type_id'];
            $parent_match_id = $slip['parent_match_id'];
            $special_bet_value = $slip['special_bet_value'];
            $bet_pick = $slip['bet_pick'];
    
            $data = $this->rawQueries("select odd_value from event_odd where "
                . "parent_match_id='$parent_match_id' and sub_type_id='$sub_type_id' "
                . " and special_bet_value='$special_bet_value' and odd_key='$bet_pick'");

            $new_odd = $data[0]['odd_value'];
            $slip['odd_value'] = $new_odd;
            $newslip[$match_id] = $slip;
        }

        $betslip = $newslip;
        $this->session->set('betslip-autobet', $betslip);

        $referenceID = $this->reference();

        $betslipSession = $this->session->get("betslip-autobet");

        $this->view->setVars([
            "betslip"        => $betslip,
            'betslipSession' => $betslipSession,
            "referenceID"    => $referenceID,
        ]);

        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);

        $this->view->pick("partials/betslip_autobet");
    } 

    public function jackpotAction()
    {
        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
        $jackpotEvent = $this->rawQueries("SELECT jackpot_event_id, total_games FROM jackpot_event WHERE status = 'ACTIVE' ORDER BY 1 DESC LIMIT 1");
        $totalGames = 12;
        $jackpotID = null;
        if (count($jackpotEvent)) {
            $jackpotID = $jackpotEvent['0']['jackpot_event_id'];
            $totalGames = $jackpotEvent['0']['total_games'];
        }
        $jackpot = $this->request->get('jackpot');

        $betslip = $this->session->get("betslip");

        $betslipSession = [];

        foreach ((array)$betslip as $slip) {
            if ($slip['bet_type'] == 'jackpot') {
                $betslipSession[$slip['pos']] = $slip;
            }
        }
//
        $betslipSession = $this->array_msort($betslipSession, ['pos' => SORT_ASC]);

        $this->view->setVars([
            "betslipSession" => $betslipSession,
            'jackpotID'      => $jackpotID,
            'total_games'    => $totalGames,
        ]);
        $this->view->pick('partials/betslip-jackpot');
    }

    /**
     *
     */
    public function correctAction()
    {
        $jackpot = $this->request->get('jackpot');

        $betslip = $this->session->get("betslip");

        $betslipSession = [];

        foreach ($betslip as $slip) {
            if ($slip['bet_type'] == 'bingwafour') {
                $betslipSession[$slip['pos']] = $slip;
            }
        }

        $betslipSession = $this->array_msort($betslipSession, ['pos' => SORT_ASC]);

        $this->view->setVars([
            "betslip"        => $betslip,
            "betslipSession" => $betslipSession,
        ]);

        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);

        $this->view->pick("partials/betslip-correct");
    }

    /**
     * @param $array
     * @param $cols
     *
     * @return array
     */
    private function array_msort($array, $cols)
    {
        $colarr = [];
        foreach ($cols as $col => $order) {
            $colarr[$col] = [];
            foreach ($array as $k => $row) {
                $colarr[$col]['_' . $k] = strtolower($row[$col]);
            }
        }
        $eval = 'array_multisort(';
        foreach ($cols as $col => $order) {
            $eval .= '$colarr[\'' . $col . '\'],' . $order . ',';
        }
        $eval = substr($eval, 0, -1) . ');';
        eval($eval);
        $ret = [];
        foreach ($colarr as $col => $arr) {
            foreach ($arr as $k => $v) {
                $k = substr($k, 1);
                if (!isset($ret[$k])) $ret[$k] = $array[$k];
                $ret[$k][$col] = $array[$k][$col];
            }
        }

        return $ret;
    }

}