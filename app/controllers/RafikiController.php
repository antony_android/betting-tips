<?php

/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */
class RafikiController extends ControllerBase
{

    /**
     * Results controller to get the results, outcome hardcoded to be changed later 
     */
    public function indexAction()
    {

        $id = $this->session->get('auth')['id'];

        if ($id) {

            list($page, $limit, $skip) = $this->getPaginationParams();

            $keyword = $this->request->getPost('keyword');

            $date_put = $this->request->getPost('date_put');

            $msisdn = $this->session->get('auth')['mobile'];

            $invited_peerGames = $this->rawQueries("SELECT p.peer_bet_id, p.profile_id, p.parent_match_id, p.sub_type_id, p.pick, CASE WHEN p.pick = m.home_team THEN CONCAT_WS(' ', 'draw ' 'or',  m.away_team) WHEN p.pick = m.away_team THEN CONCAT_WS(' ', m.home_team, 'or',  'draw') END AS peer_pick, p.peer_msisdn, p.bet_amount, p.status,  m.match_id, m.home_team, m.away_team, m.start_time, m.game_id, m.competition_id, c.competition_name, c.category, c.sport_id, pf.msisdn, MAX(CASE WHEN o.odd_key = m.home_team THEN odd_value END) AS home_odd, MAX(CASE WHEN o.odd_key = 'draw' THEN odd_value END) AS neutral_odd, MAX(CASE WHEN o.odd_key = m.away_team THEN odd_value END) AS away_odd, MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', m.home_team, 'or',  'draw') THEN odd_value END) AS double_chance_1x_odd, MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', 'draw ' 'or',  m.away_team) THEN odd_value END) AS double_chance_x2_odd, MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', m.home_team, 'or',  m.away_team) THEN odd_value END) AS double_chance_12_odd, MAX(CASE WHEN o.odd_key = 'under 2.5' THEN odd_value END) AS under_25_odd, MAX(CASE WHEN o.odd_key = 'over 2.5' THEN odd_value END) AS over_25_odd FROM peer_bet p INNER JOIN profile pf ON p.profile_id = pf.profile_id INNER JOIN `match` m ON p.parent_match_id = m.parent_match_id INNER JOIN  competition c ON m.competition_id = c.competition_id INNER JOIN event_odd o ON m.parent_match_id = o.parent_match_id WHERE m.start_time > NOW() AND p.peer_msisdn = '$msisdn' AND p.status = 'PENDING' GROUP BY m.parent_match_id ORDER BY m.peer_priority, m.priority, c.priority, m.start_time;");

            // die(print_r("SELECT p.peer_bet_id, p.profile_id, p.parent_match_id, p.sub_type_id, p.pick, CASE WHEN p.pick = m.home_team THEN CONCAT_WS(' ', 'draw ' 'or',  m.away_team) WHEN p.pick = m.away_team THEN CONCAT_WS(' ', m.home_team, 'or',  'draw') END AS peer_pick, p.peer_msisdn, p.status, m.home_team, m.away_team, m.start_time, m.game_id, m.competition_id, c.competition_name, c.category, c.sport_id, pf.msisdn, MAX(CASE WHEN o.odd_key = m.home_team THEN odd_value END) AS home_odd, MAX(CASE WHEN o.odd_key = 'draw' THEN odd_value END) AS neutral_odd, MAX(CASE WHEN o.odd_key = m.away_team THEN odd_value END) AS away_odd, MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', m.home_team, 'or',  'draw') THEN odd_value END) AS double_chance_1x_odd, MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', 'draw ' 'or',  m.away_team) THEN odd_value END) AS double_chance_x2_odd, MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', m.home_team, 'or',  m.away_team) THEN odd_value END) AS double_chance_12_odd, MAX(CASE WHEN o.odd_key = 'under 2.5' THEN odd_value END) AS under_25_odd, MAX(CASE WHEN o.odd_key = 'over 2.5' THEN odd_value END) AS over_25_odd FROM peer_bet p INNER JOIN profile pf ON p.profile_id = pf.profile_id INNER JOIN `match` m ON p.parent_match_id = m.parent_match_id INNER JOIN  competition c ON m.competition_id = c.competition_id INNER JOIN event_odd o ON m.parent_match_id = o.parent_match_id WHERE m.start_time > NOW() AND p.peer_msisdn = '$msisdn' ORDER BY m.priority, c.priority, m.start_time;"));
            list($today, $total, $sCompetitions) = $this->getPeerGames($keyword, $date_put, $skip, $limit);

            $theBetslip = $this->session->get("betslip-peer");
            $slipCount = sizeof($theBetslip);

            $this->view->setVars([
                'today'         => $today,
                'theBetslip'    => $theBetslip,
                "slipCount"   => $slipCount, 
                'sCompetitions' => $sCompetitions,
                'winners' =>$this->topWinners(),
                'invited_peerGames' => $invited_peerGames,
            ]);

            $this->tag->setTitle($this->view->t->_('title'));

        }else{

            $this->session->set("ref", "rafiki");
            $this->flashSession->error($this->flashMessages('Login to access this page'));
            $this->response->redirect('login');
        }

    }

}
