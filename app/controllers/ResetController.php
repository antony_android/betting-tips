<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

class ResetController extends ControllerBase
{

    public function indexAction()
    {
    	$id = $this->request->get('id','int');

    	if ($id) {
    		$phql = "SELECT * from Profile where profile_id='$id' limit 1";
    		$user = $this->modelsManager->executeQuery($phql);
    
    		$user=$user->toArray();

    		$this->view->setVars(["msisdn"=>$user['0']['msisdn'],"profile_id"=>$user['0']['profile_id'], 
                'winners'=>$this->topWinners(),]);
    	}else{
              $this->view->setVars(['winners'=>$this->topWinners()]);
        }
        
    }

    public function codeAction()
    {
    	$verification_code = substr(number_format(time() * mt_rand(),0,'',''),0,4);
    	$mobile = $this->request->getPost('mobile', 'int');

    	if (!$mobile) {
        	$this->flashSession->error($this->flashMessages('Please enter your mobile number'));
            $this->response->redirect('reset');

	        $this->view->disable();
        }else{
        		$mobile = $this->formatMobileNumber($mobile);

                if ($mobile==false) {
                $this->flashSession->error($this->flashMessages('Invalid mobile number'));
                $this->response->redirect('reset');

                // Disable the view to avoid rendering
                $this->view->disable();
                }else{

                $phql = "SELECT * from Profile where msisdn='$mobile' limit 1";
        		$user = $this->modelsManager->executeQuery($phql);
        
        		$user=$user->toArray();
        
        		if (!$user) {
        			$this->flashSession->error($this->flashMessages('User does not exist'));
            		$this->response->redirect('reset');
        		    // Disable the view to avoid rendering
        		    $this->view->disable();
        		}else{
        		    // FIXME: Change to swahili
        	    	$message = "Your Bet Palace password reset code is $verification_code. Use this to reset your password.";
        	        $profile_id = $user['0']['profile_id'];
        
        	    	$phql = "SELECT * from ProfileSettings where profile_id='$profile_id' limit 1";
        			$checkUser = $this->modelsManager->executeQuery($phql);
        
        			$checkUser=$checkUser->toArray();
        
        			if ($checkUser) {
        				$insert = $this->rawInsert("update profile_settings set verification_code='$verification_code' where profile_id='$profile_id'");
        				$send = $this->sendShortMessage($message, $mobile);
                        $this->flashSession->error($this->flashSuccess('Use the code sent to your phone to reset your password'));
        				$this->response->redirect("reset?id=$profile_id");
        		    // Disable the view to avoid rendering
        		    $this->view->disable();
        			}else {
                        $insert = $this->rawInsert("insert into profile_settings (profile_id,password,verification_code,created_at) values('$profile_id','','$verification_code',now())");

                        $send = $this->sendShortMessage($message, $mobile);

                        $this->flashSession->error($this->flashSuccess('Use the code sent to your phone to reset your password'));
                        $this->response->redirect("reset?id=$profile_id");
                    }
        		}
            }
        	}
    }

    public function passwordAction()
    {
    	$password = $this->request->getPost('password');
        $repeatPassword = $this->request->getPost('repeatPassword');
        $reset_code = $this->request->getPost('reset_code','int');
        $profile_id = $this->request->getPost('profile_id','int');

        if (!$password || !$reset_code || !$repeatPassword || !$profile_id) {
            $this->flashSession->error($this->flashMessages('All fields are required'));
            $this->response->redirect("reset?id=$profile_id");

            // Disable the view to avoid rendering
            $this->view->disable();
        }else{
                if ($password != $repeatPassword) {
                    $this->flashSession->error($this->flashMessages('Passwords do not match'));
                    $this->response->redirect("reset?id=$profile_id");
        
                    // Disable the view to avoid rendering
                    $this->view->disable();
                }else{
                    $valid = $this->rawQueries("select profile_id from profile_settings where profile_id='$profile_id' and verification_code='$reset_code'");
                    if ($valid) {
                       $password = $this->security->hash($password);
                       $updatePassword = $this->rawInsert("update profile_settings set password='$password', status='1' where profile_id='$profile_id'");
                       $this->flashSession->error($this->flashSuccess('Password successfully reset'));
                        $this->response->redirect('login');
        
                        // Disable the view to avoid rendering
                        $this->view->disable();
                    }else{
                        $this->flashSession->error($this->flashMessages('Invalid reset code'));
                        $this->response->redirect("reset?id=$profile_id");
        
                        // Disable the view to avoid rendering
                        $this->view->disable();
                    }
                }
            }
    }

}
