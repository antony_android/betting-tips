<?php

/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */
class ResultsController extends ControllerBase
{

    /**
     * Results controller to get the results, outcome hardcoded to be changed later 
     */
    public function indexAction()
    {

        $keyword = $this->request->getPost('keyword', 'string');
        $date_put = $this->request->getPost('date_put', 'string');

        $bindings = [];

        $this->tag->setTitle('Results');

        $where = "";
        if(!empty($keyword)){
            $bindings['keyword'] = "%$keyword%";
            $where .= " and (game_id like :keyword or home_team like :keyword or away_team like :keyword or competition_name like :keyword) ";
        }else if(!empty($date_put)){

            $bindings['date_put'] = $date_put;
            $where .= " and date(o.created) >= :date_put ";

        }else{

            $bindings['date_put'] = date('Y-m-d');
            $where .= " and date(o.created) >= :date_put ";
        }

        $results = $this->rawQueries("SELECT competition.category, competition.competition_name, 
         m.start_time, m.home_team, m.away_team, o.winning_outcome AS outcome  FROM outcome o
          INNER JOIN `match` m ON o.parent_match_id = m.parent_match_id INNER JOIN 
          competition ON m.competition_id = competition.competition_id WHERE
           o.created > NOW()-INTERVAL 7 DAY AND o.is_winning_outcome=1 AND o.sub_type_id=45
            $where LIMIT 50;", $bindings);

        $theBetslip = $this->session->get("betslip");

        $this->view->setVars([
            "topLeagues" => $this->topLeagues(),
            'results'      => $results,
            'theBetslip' => $theBetslip,
            'filter' => "All",
            'winners'=> $this->topWinners(),
        ]);

    }

}

