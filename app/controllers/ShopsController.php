<?php

/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */
class ShopsController extends ControllerBase
{

    /**
     *
     */
    public function indexAction()
    {
        $this->view->setVars(["topLeagues" => $this->topLeagues()]);

        $this->tag->setTitle('Open your Shop Today!');

    }

}

