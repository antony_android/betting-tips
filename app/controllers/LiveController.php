<?php

/**
 * Copyright (c) Anto 2018.
 *
 * All rights reserved.
 */

use Phalcon\Http\Response;

class LiveController extends ControllerBase
{

    /**
     * Results controller to get the results, outcome hardcoded to be changed later 
     */
    public function indexAction()
    {

        $hour = date('H');
        $sport_id = $this->request->get('sp', 'int');

        list($page, $limit, $skip) = $this->getPaginationParams();

        $keyword = $this->request->getPost('keyword', 'string');

        $theBetslip = $this->session->get("betslip");
        $where = "";
        if ($sport_id){
            $where = " and s.sport_id = '$sport_id'";
        }

        $orderBy = "m.priority desc, c.priority desc, m.start_time asc";

        list($today, $total, $sCompetitions) = $this->getLiveGames($keyword, $skip, $limit,
         $where, $orderBy);

        $total = $total['0']['total'];

        $this->view->setVars([
            'today'         => $today,
            'theBetslip'    => $theBetslip,
            'sCompetitions' => $sCompetitions,
            'total'         => $total,
            'pages'         => $this->getResultPages($total, $limit),
            'page'          => $page,
            'topSports'     => $this->topSports(),
            'sportId' =>$sport_id,
            'keyword' =>$keyword
        ]);

        $this->tag->setTitle($this->view->t->_('title'));
        $this->view->pick("live/live-home");
    }


    public function fetchgamesAction()
    {

        $theBetslip = $this->session->get("betslip_live");

        $keyword = $this->request->getPost('keyword', 'string');

        $where = "";

        $bindings = null;

        if(!empty($keyword)){
            $bindings['keyword'] = "%$keyword%";
            $where .= " and (game_id like :keyword or home_team like :keyword or away_team 
            like :keyword or competition_name like :keyword) ";
        }

        $query = "SELECT c.priority, (SELECT count(DISTINCT e.sub_type_id) FROM live_odds e
                  INNER JOIN odd_type o ON (o.sub_type_id = e.sub_type_id AND 
                  o.parent_match_id = e.parent_match_id) WHERE e.parent_match_id = 
                  m.parent_match_id AND e.active = 1) AS side_bets, o.sub_type_id, 
                  MAX(CASE WHEN o.odd_key = m.home_team THEN odd_value END) AS home_odd,
                  MAX(CASE WHEN o.odd_key = 'draw' THEN odd_value END) AS neutral_odd,
                  MAX(CASE WHEN o.odd_key = m.away_team THEN odd_value END) AS away_odd,
                  MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', m.home_team, 'or',  'draw') 
                  THEN odd_value END) AS double_chance_1x_odd, 
                  MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', 'draw ' 'or',  m.away_team) THEN odd_value END)
                  AS double_chance_x2_odd, MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', m.home_team, 'or',  
                  m.away_team) THEN odd_value END) AS double_chance_12_odd, MAX(CASE WHEN o.odd_key =
                  'under 2.5' THEN odd_value END) AS under_25_odd, MAX(CASE WHEN o.odd_key = 'over 2.5'
                  THEN odd_value END) AS over_25_odd,
                  m.game_id, MAX(CASE WHEN o.odd_key=m.home_team THEN o.active END) as
                  home_odd_status, MAX(CASE WHEN o.odd_key = 'draw' THEN o.active END) as
                  draw_odd_status, MAX(CASE WHEN o.odd_key = m.away_team THEN o.active END)
                  as away_odd_status, MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', m.home_team, 'or',  'draw') 
                  THEN o.active END) AS onex_odd_status, 
                  MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', 'draw ' 'or',  m.away_team) THEN o.active END)
                  AS xtwo_odd_status, MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', m.home_team, 'or',  
                  m.away_team) THEN o.active END) AS onetwo_odd_status, MAX(CASE WHEN o.odd_key =
                  'under 2.5' THEN o.active END) AS under_25_odd_status, MAX(CASE WHEN o.odd_key = 'over 2.5'
                  THEN o.active END) AS over_25_odd_status, s.sport_name, o.market_active, 
                  m.match_id, m.start_time, m.away_team,
                  m.home_team, m.parent_match_id, c.competition_name,c.category, lo.match_time,
                  lo.score, lo.bet_status, lo.match_status, lo.event_status, lo.home_red_card,
                  lo.away_red_card, lo.home_yellow_card, lo.away_yellow_card, lo.home_corner,
                  lo.away_corner, lo.active as event_live FROM `live_match` m INNER JOIN 
                  live_odds_meta lo on lo.parent_match_id = m.parent_match_id INNER JOIN 
                  live_odds o ON m.parent_match_id = o.parent_match_id  INNER JOIN competition c
                  ON c.competition_id = m.competition_id INNER JOIN sport s ON s.sport_id = 
                  c.sport_id where m.status <> 3 AND m.start_time > now()-interval 4  hour
                  AND o.sub_type_id IN (1,10,18) AND lo.event_status = 'Live'  
                  GROUP BY m.parent_match_id ORDER BY s.betradar_sport_id, m.priority DESC,
                   c.priority DESC, m.start_time";

        $games = $this->rawQueries($query, $bindings);

        $results = [
            'games'   => $games,
            'betslip_live' => $theBetslip
        ];

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        $response->setContent(json_encode($results));

        return $response;
    }

}