<?php
/**
 * Copyright (c) Anto 2018.
 *
 * All rights reserved.
 */

use Phalcon\Http\Response;

/**
 * Class LivehomeController
 */
class LivehomeController_old extends ControllerBase
{

    /**
     * Index
     */
    public function indexAction()
    {


        $theBetslip = $this->session->get("betslip");
        $title = "All Live Games";

        $live_matches_query = "SELECT m.match_id, m.parent_match_id, m.home_team, m.away_team,
                c.competition_name, c.category, s.sport_name,lo.home_yellow_card,lo.away_yellow_card
                ,lo.home_red_card,lo.away_red_card,lo.home_corner,lo.away_corner ,lo.score
                ,lo.match_time FROM `live_match` m INNER JOIN 
            live_odds_meta lo using(parent_match_id) INNER JOIN competition c using(competition_id)
            INNER JOIN sport s using(sport_id) WHERE  m.start_time > now() - INTERVAL 4 HOUR AND
            lo.active = 1 and lo.event_status = 'Live' GROUP BY m.parent_match_id ORDER BY m.priority DESC,
            c.priority DESC, m.start_time LIMIT 50";
        
        // die($live_matches_query);

        $liveMatches = $this->rawQueries($live_matches_query);

        $this->tag->setTitle($title);

        $this->view->setVars([
            "topLeagues" => $this->topLeagues(),
            'liveMatches' => $liveMatches,
            'theBetslip' => $theBetslip,
        ]);

        $this->view->pick("live/live-home");

    }

    public function fetchgameAction()
    {

        $query = "SELECT m.match_id, m.parent_match_id, m.home_team, m.away_team, 
            c.competition_name, c.category, s.sport_name FROM `live_match` m INNER JOIN 
            live_odds_meta lo using(parent_match_id) INNER JOIN competition c 
            using(competition_id) INNER JOIN sport s using(sport_id) WHERE  m.start_time > 
            now() - INTERVAL 4 HOUR AND lo.active = 1 and lo.event_status = 'Live' GROUP BY 
            m.parent_match_id ORDER BY m.priority DESC, c.priority DESC, m.start_time LIMIT 
            50";

        $liveMatches = $this->rawQueries($query);

        $results = [
            'liveMatches' => $liveMatches,
        ];

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        $response->setContent(json_encode($results));

        return $response;
    }
}