<?php

use Phalcon\Mvc\Model\Query;
use Phalcon\Http\Response;

class TabController extends ControllerBase
{
	public function initialize()
    {
        $this->tag->setTitle('About us');
    }

    public function indexAction()
    {
        $sCompetitions = $this->getCompetitions();
        list($page, $limit, $skip) = $this->getPaginationParams();
        $sport_id = $tab = $this->request->get('sp', 'int');

        $tab = $this->request->get('t', 'int');
        $tab_original = $tab;
        $tab = $tab == null ? -1 : $tab;
        $select_date = null;

        if($tab_original == 0){

            $this->session->set("previous_address", "/");
        }elseif($tab_original == 1){
            $this->session->set("previous_address", "/upcoming");
        }elseif($tab_original == 2){
            $this->session->set("previous_address", "/tomorrow");
        }
        
        $where = "";
        if($tab > 0){
            $tab -= 1;
            $date = new DateTime(); 
            $interval = new DateInterval('P'.$tab.'D');
            $date->add($interval);
            $where .= " and date(start_time) =  '" . $date->format('Y-m-d') . "' ";
        }

        $keyword = $this->request->getPost('keyword', 'string');

        if ($sport_id){
                $where .= " and sport_id = '$sport_id'";
        }
       
        if($tab_original == 1){
	
	    $limit = 300;
            list($today, $total, $sCompetitions) = $this->getGames($keyword, $skip, $limit, $where , 'start_time asc');     
        }else{

            list($today, $total, $sCompetitions) = $this->getGames($keyword, $skip, $limit, $where , 'm_priority desc, priority desc, start_time asc');     
        }

        $theBetslip = $this->session->get("betslip");

        $total = isset($total['total'])?$total['total']:0;

        $this->view->setVars(['today'=>$today,
            'theBetslip'=>$theBetslip,
            'sCompetitions'=>$sCompetitions,
            'keyword'=>$keyword,
            'total'=>$total,
            'pages' => $this->getResultPages($total, $limit),
            'page' => $page
        ]);

    	$this->tag->setTitle('BetPalace - Where Kings are made');

        if($tab != -1){

            //select template to load tab only
            //$this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
            $this->view->pick("partials/match-list");
        }
    }

}

