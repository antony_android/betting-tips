<?php

/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */
class MfalmeBonusController extends ControllerBase
{

    /**
     * Bonus
     */
    public function indexAction()
    {
        $this->tag->setTitle('How to use your betpalace bonus');
        $this->view->pick("howtoplay/bonus");
        $this->view->setVars([
            'data' => json_decode(file_get_contents(__DIR__ . "/../storage/how-it-works-swahili.json")),
            'winners'=>$this->topWinners(),
        ]);

    }

}

