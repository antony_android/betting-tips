<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

use Phalcon\Http\Response;
use Phalcon\Mvc\View;

/**
 * Class BetslipController
 */
class BetslipController extends ControllerBase
{

    /**
     * @return array
     */
    public function indexAction()
    {
        $reference_id = $this->reference();

        $jackpot = $this->request->get('jackpot');

        $betslip = $this->session->get('betslip');

        $referenceID = $this->reference();

        $betslipSession = $this->session->get("betslip");
        $data = [
            "betslip"        => $betslip,
            'betslipSession' => $betslipSession,
            "referenceID"    => $referenceID,
        ];

        $this->view->setVars([
            "betslip"        => $betslip,
            'betslipSession' => $betslipSession,
            "referenceID"    => $referenceID,
        ]);

        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);

        $this->view->pick("partials/betslip");
    }

    /**
     * @return \Phalcon\Http\Response
     */
    public function jsonAction()
    {
        $reference_id = $this->reference();

        $betslip = $this->rawQueries("select b.bet_pick, m.parent_match_id, b.sub_type_id, 
            m.home_team, m.away_team, m.match_id, d.odd_value, b.sub_type_id, 
            concat(t.name,' ',d.special_bet_value) as odd_name from bet_slip_temp b inner 
            join `match` m on b.match_id=m.match_id inner join event_odd d on 
            m.parent_match_id=d.parent_match_id inner join odd_type t on 
            t.sub_type_id=d.sub_type_id where d.sub_type_id=b.sub_type_id and 
            d.odd_key=b.bet_pick and d.special_bet_value=b.special_bet_value and 
            b.reference_id='$reference_id' and bet_type in ('prematch','live')");

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        $response->setContent(json_encode($betslip));

        return $response;

    }

    /**
     *
     */
    public function jackpotAction()
    {
//        $jackpotID = $this->rawQueries("SELECT jackpot_event_id FROM jackpot_event WHERE status = 'ACTIVE' ORDER BY 1 DESC LIMIT 1");
//        $jackpotID = count($jackpotID) ? $jackpotID['0']['jackpot_event_id'] : null;
//        $jackpot = $this->request->get('jackpot');
//
//        $betslip = $this->session->get("betslip");
//
//        $betslipSession = [];
//
//        foreach ((array)$betslip as $slip) {
//            if ($slip['bet_type'] == 'jackpot') {
//                $betslipSession[$slip['pos']] = $slip;
//            }
//        }
//
//        $betslipSession = $this->array_msort($betslipSession, ['pos' => SORT_ASC]);
//
//        $this->view->setVars([
//            "betslipSession" => $betslipSession,
//            'jackpotID'      => $jackpotID,
//        ]);
//        $this->view->disable();
        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);

        $this->view->pick("partials/betslip-jackpot");
    }

    /**
     * @return \Phalcon\Http\Response
     */
    public function addAction()
    {

        $reference_id = $this->reference();
        $match_id = $this->request->getPost('match_id', 'int');
        $bet_pick = $this->request->getPost('odd_key');
        $sub_type_id = $this->request->getPost('sub_type_id', 'int');
        $special_bet_value = $this->request->getPost('special_bet_value');
        $bet_type = $this->request->getPost('bet_type') ?: 'prematch';
        $home_team = $this->request->getPost('home');
        $away_team = $this->request->getPost('away');
        $odd_value = $this->request->getPost('odd','float');
        $odd_type = $this->request->getPost('oddtype');
        $parent_match_id = $this->request->getPost('parentmatchid');
        $jackpot_id = $this->request->getPost('jackpot_event_id', 'int');
        $pos = $this->request->getPost('pos');

        if ($special_bet_value == '0') {
            $special_bet_value = '';
        }

        // if ($bet_type == 'live') {
        //     $this->session->set("betslip", '');
        // }

        $data = 'selection updated';

        $betslip[] = '';

        unset($betslip);

        if ($this->session->has("betslip")) {
            $betslip = $this->session->get("betslip");
        }

        $betslip["$match_id"] = [
            'match_id'          => $match_id,
            'bet_pick'          => $bet_pick,
            'sub_type_id'       => $sub_type_id,
            'special_bet_value' => $special_bet_value,
            'bet_type'          => $bet_type,
            'home_team'         => $home_team,
            'away_team'         => $away_team,
            'odd_value'         => $odd_value,
            'odd_type'          => $odd_type,
            'parent_match_id'   => $parent_match_id,
            'jackpot_id'        => $jackpot_id,
            'pos'               => $pos,
            'peer_bet_id'       => "-1",
            'peer_msisdn'       => "",
        ];

        $this->session->set("betslip", $betslip);

        if($bet_type == 'live' || $bet_type == 1){
            if ($this->session->has("orig_betslip")) {
                $live_slip = $this->session->get("orig_betslip");
            }

             $live_slip["$match_id"] = [
                'match_id'          => $match_id,
                'bet_pick'          => $bet_pick,
                'sub_type_id'       => $sub_type_id,
                'special_bet_value' => $special_bet_value,
                'bet_type'          => $bet_type,
                'home_team'         => $home_team,
                'away_team'         => $away_team,
                'odd_value'         => $odd_value,
                'odd_type'          => $odd_type,
                'parent_match_id'   => $parent_match_id,
                'pos'               => $pos,
            ];

            $this->session->set("orig_betslip", $live_slip);

        }

        $bets = $this->session->get('betslip');

        $count = sizeof($bets);

        $data = [
            'total'   => $count,
            'message' => 'Selection updated',
        ];

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        $response->setContent(json_encode($data));

        return $response;
    }


    /**
     * @return \Phalcon\Http\Response
     */
    public function addsgrAction()
    {

        $reference_id = $this->reference();
        $match_id = $this->request->getPost('match_id', 'int');
        $bet_pick = $this->request->getPost('odd_key');
        $sub_type_id = $this->request->getPost('sub_type_id', 'int');
        $special_bet_value = $this->request->getPost('special_bet_value');
        $bet_type = $this->request->getPost('bet_type') ?: 'prematch';
        $home_team = $this->request->getPost('home');
        $away_team = $this->request->getPost('away');
        $odd_value = $this->request->getPost('odd','float');
        $odd_type = $this->request->getPost('oddtype');
        $parent_match_id = $this->request->getPost('parentmatchid');
        $jackpot_id = $this->request->getPost('jackpot_event_id', 'int');
        $pos = $this->request->getPost('pos');

        if ($special_bet_value == '0') {
            $special_bet_value = '';
        }

        $data = 'selection updated';

        $betslip[] = '';

        unset($betslip);

        if ($this->session->has("betslip_sgr")) {
            $betslip = $this->session->get("betslip_sgr");
        }

        $betslip["$match_id"] = [
            'match_id'          => $match_id,
            'bet_pick'          => $bet_pick,
            'sub_type_id'       => $sub_type_id,
            'special_bet_value' => $special_bet_value,
            'bet_type'          => $bet_type,
            'home_team'         => $home_team,
            'away_team'         => $away_team,
            'odd_value'         => $odd_value,
            'odd_type'          => $odd_type,
            'product'           => "SGR",
            'parent_match_id'   => $parent_match_id,
            'jackpot_id'        => $jackpot_id,
            'pos'               => $pos,
            'peer_bet_id'       => "-1",
            'peer_msisdn'       => "",
        ];

        $this->session->set("betslip_sgr", $betslip);

        $bets = $this->session->get('betslip_sgr');

        $count = sizeof($bets);

        $data = [
            'total'   => $count,
            'message' => 'Selection updated',
        ];

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        $response->setContent(json_encode($data));

        return $response;
    }

    public function betagainstAction()
    {

        $reference_id = $this->reference();
        $match_id = $this->request->getPost('match_id', 'int');
        $bet_pick = $this->request->getPost('odd_key');
        $sub_type_id = $this->request->getPost('sub_type_id', 'int');
        $special_bet_value = $this->request->getPost('special_bet_value');
        $bet_type = $this->request->getPost('bet_type') ?: 'prematch';
        $home_team = $this->request->getPost('home');
        $away_team = $this->request->getPost('away');
        $odd_value = $this->request->getPost('odd');
        $odd_type = $this->request->getPost('oddtype');
        $parent_match_id = $this->request->getPost('parentmatchid');
        $is_peer = $this->request->getPost('is_peer');
        $bet_amount = $this->request->getPost('bet_amount');
        $peer_bet_id = $this->request->getPost('peer_bet_id');
        $peer_msisdn = $this->request->getPost('peer_msisdn');
        $friend_pick = $this->request->getPost('friend_pick');

        if ($special_bet_value == '0') {
            $special_bet_value = '';
        }

        if ($bet_type == 'prematch') {
            $this->session->set("betslip-peer", '');
        }

        $data = 'selection updated';

        $betslip[] = '';

        unset($betslip);

        if ($this->session->has("betslip-peer")) {
            $betslip = $this->session->remove("betslip-peer");
        }

        $betslip["$match_id"] = [
            'match_id'          => $match_id,
            'bet_pick'          => $bet_pick,
            'friend_pick'       => $friend_pick.' WILL NOT WIN',
            'sub_type_id'       => $sub_type_id,
            'special_bet_value' => $special_bet_value,
            'bet_type'          => $bet_type,
            'home_team'         => $home_team,
            'away_team'         => $away_team,
            'odd_value'         => $odd_value,
            'odd_type'          => $odd_type,
            'parent_match_id'   => $parent_match_id,
            'is_peer'           => $is_peer,
            'bet_amount'        => $bet_amount,
            'peer_bet_id'       => $peer_bet_id,
            'peer_msisdn'       => $peer_msisdn
        ];

        $this->session->set("betslip-peer", $betslip);

        $bets = $this->session->get('betslip-peer');

        // die(print_r($bets, 1));

        $count = sizeof($bets);

        $data = [
            'total'   => $count,
            'message' => 'Selection updated',
        ];

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        $response->setContent(json_encode($data));

        return $response;
    }


    public function addnewpeerAction()
    {

        $reference_id = $this->reference();
        $match_id = $this->request->getPost('match_id', 'int');
        $bet_pick = $this->request->getPost('odd_key');
        $sub_type_id = $this->request->getPost('sub_type_id', 'int');
        $special_bet_value = $this->request->getPost('special_bet_value');
        $bet_type = $this->request->getPost('bet_type') ?: 'prematch';
        $home_team = $this->request->getPost('home');
        $away_team = $this->request->getPost('away');
        $odd_value = $this->request->getPost('odd');
        $odd_type = $this->request->getPost('oddtype');
        $parent_match_id = $this->request->getPost('parentmatchid');
        $is_peer = $this->request->getPost('is_peer');
        $bet_amount = $this->request->getPost('bet_amount');
        $bet_id = -1;

        if ($special_bet_value == '0') {
            $special_bet_value = '';
        }

        if ($bet_type == 'prematch') {
            $this->session->set("betslip-peer", '');
        }

        $data = 'selection updated';

        $betslip[] = '';

        unset($betslip);

        if ($this->session->has("betslip-peer")) {
            $betslip = $this->session->remove("betslip-peer");
        }

        $betslip["$match_id"] = [
            'match_id'          => $match_id,
            'bet_pick'          => $bet_pick,
            'friend_pick'      => $bet_pick." WILL WIN",
            'sub_type_id'       => $sub_type_id,
            'special_bet_value' => $special_bet_value,
            'bet_type'          => $bet_type,
            'home_team'         => $home_team,
            'away_team'         => $away_team,
            'odd_value'         => $odd_value,
            'odd_type'          => $odd_type,
            'parent_match_id'   => $parent_match_id,
            'is_peer'           => $is_peer,
            'bet_amount'        => $bet_amount,
            'peer_bet_id'        => "-1",
            'bet_id'            => $bet_id
        ];

        $this->session->set("betslip-peer", $betslip);

        $bets = $this->session->get('betslip-peer');

        $count = sizeof($bets);

        $data = [
            'total'   => $count,
            'message' => 'Selection updated',
        ];

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        $response->setContent(json_encode($data));

        return $response;
    }

    /**
     * @return \Phalcon\Http\Response
     */
    public function removeAction()
    {
        $reference_id = $this->reference();
        $match_id = $this->request->getPost('match_id', 'int');
        $src = $this->request->getPost('src', 'string');
        $betslip = $this->session->get("betslip");

        unset($betslip["$match_id"]);

        $this->session->set("betslip", $betslip);

        if ($src == 'mobile') {
            $this->flashSession->error($this->flashSuccess('Match successfully removed'));
            $this->response->redirect('betmobile');
            // Disable the view to avoid rendering
            $this->view->disable();
        } else {
            $response = new Response();
            $response->setStatusCode(201, "OK");
            $response->setHeader("Content-Type", "application/json");

            $response->setContent(json_encode([
                'message' => 'Match successfully removed',
                'count'   => count($betslip),
            ]));

            return $response;
        }
    }


    /**
     * @return \Phalcon\Http\Response
     */
    public function removesgrAction()
    {
        $reference_id = $this->reference();
        $match_id = $this->request->getPost('match_id', 'int');
        $src = $this->request->getPost('src', 'string');
        $betslip = $this->session->get("betslip_sgr");

        unset($betslip["$match_id"]);

        $this->session->set("betslip_sgr", $betslip);

        if ($src == 'mobile') {
            $this->flashSession->error($this->flashSuccess('SGR Match successfully removed'));
            $this->response->redirect('betmobile');
            // Disable the view to avoid rendering
            $this->view->disable();
        } else {
            $response = new Response();
            $response->setStatusCode(201, "OK");
            $response->setHeader("Content-Type", "application/json");

            $response->setContent(json_encode([
                'message' => 'SGR Match successfully removed',
                'count'   => count($betslip),
            ]));

            return $response;
        }
    }


    /**
     * @return \Phalcon\Http\Response
     */
    public function removepeerAction()
    {

        $reference_id = $this->reference();
        $match_id = $this->request->getPost('match_id', 'int');
        $src = $this->request->getPost('src', 'string');
        $betslip = $this->session->get("betslip-peer");

        unset($betslip["$match_id"]);

        $this->session->set("betslip-peer", $betslip);

        if ($src == 'mobile') {
            $this->flashSession->error($this->flashSuccess('Match successfully removed'));
            $this->response->redirect('betmobile');
            // Disable the view to avoid rendering
            $this->view->disable();
        } else {
            $response = new Response();
            $response->setStatusCode(201, "OK");
            $response->setHeader("Content-Type", "application/json");

            $response->setContent(json_encode([
                'message' => 'Match successfully removed',
                'count'   => count($betslip),
            ]));

            return $response;
        }
    }

    /**
     * @return \Phalcon\Http\Response
     */
    public function clearslipAction()
    {

        $reference_id = $this->reference();

        $this->session->remove("betslip");

        $data = '1';

        $src = $this->request->getPost('src', 'string');

        if ($src == 'mobile') {
            $this->flashSession->error($this->flashSuccess('Betslip cleared'));
            $this->response->redirect('betmobile');
            // Disable the view to avoid rendering
            $this->view->disable();
        } else {
            $response = new Response();
            $response->setStatusCode(201, "OK");
            $response->setHeader("Content-Type", "application/json");

            $response->setContent(json_encode($data));

            return $response;
        }

    }


    /**
     * @return \Phalcon\Http\Response
     */
    public function clearsgrslipAction()
    {

        $reference_id = $this->reference();

        $this->session->remove("betslip_sgr");

        $data = '1';

        $src = $this->request->getPost('src', 'string');

        if ($src == 'mobile') {
            $this->flashSession->error($this->flashSuccess('SGR Betslip cleared'));
            $this->response->redirect('betmobile');
            // Disable the view to avoid rendering
            $this->view->disable();
        } else {
            $response = new Response();
            $response->setStatusCode(201, "OK");
            $response->setHeader("Content-Type", "application/json");

            $response->setContent(json_encode($data));

            return $response;
        }

    }

    public function clearpeerslipAction()
    {

        $reference_id = $this->reference();

        $this->session->remove("betslip-peer");

        $data = '1';

        $src = $this->request->getPost('src', 'string');

        if ($src == 'mobile') {
            $this->flashSession->error($this->flashSuccess('Betslip cleared'));
            $this->response->redirect('betmobile');
            // Disable the view to avoid rendering
            $this->view->disable();
        } else {
            $response = new Response();
            $response->setStatusCode(201, "OK");
            $response->setHeader("Content-Type", "application/json");

            $response->setContent(json_encode($data));

            return $response;
        }
    }

    /**
     * @return \Phalcon\Http\Response
     */
    public function placebetAction()
    {
        $reference_id = $this->reference();
        $user_id = $this->request->getPost('user_id', 'int');
        $bet_amount = $this->request->getPost('bet_amount', 'float');
        $total_odd = $this->request->getPost('total_odd', 'float');
        $possible_win = $bet_amount * $total_odd;
        $src = $this->request->getPost('src', 'string') ?: 'internet';
        $betslip = empty($this->session->get('betslip')) ? [] : $this->session->get('betslip');
        $endCustomerIP = $this->getClientIP();
        $account = $this->getPaymentAccount();
        $phoneNumber = $this->request->get('msisdn', 'int');

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        if(($account && !$user_id) || (!$user_id && !$phoneNumber)){

            if ($src == 'mobile') {

                $this->flashSession->error($this->flashMessages('Please login in to place your bet'));
                $this->response->redirect('login');
                $this->view->disable();
                return;
            }
        }

        if ($bet_amount == 0 || strlen($bet_amount) == 0 || $bet_amount == null) {
            if ($src == 'mobile') {
                $this->flashSession->error($this->flashMessages('Please specify your bet amount.'));

                $this->response->redirect('betmobile');
                $this->view->disable();
            } else {
                $data = [
                    "status_code" => 421,
                    "message"     => "Please specify your bet amount.",
                ];
                $response->setContent(json_encode($data));

                return $response;
                $this->view->disable();
            }

        } else {
            if ($bet_amount < 19) {
                if ($src == 'mobile') {
                    $this->flashSession->error($this->flashMessages('The bet amount should be at least KShs. 19'));
                    $this->response->redirect('betmobile');
                    // Disable the view to avoid rendering
                    $this->view->disable();
                } else {
                    $data = [
                        "status_code" => 421,
                        "message"     => "Bet amount should be at least KShs. 19",
                    ];
                    $response->setContent(json_encode($data));

                    return $response;
                    $this->view->disable();
                }
            } else {
                $phql = "SELECT * from Profile where profile_id='$user_id' limit 1";
                $checkUser = $this->modelsManager->executeQuery($phql);

                $checkUser = $checkUser->toArray();

                $mobile = $checkUser['0']['msisdn'];

                $totalMatch = sizeof($betslip);

                $slip = [];

                foreach ($betslip as $match) {
                    
                    $parent_match_id = $match['parent_match_id'];
                    $bet_pick = $match['bet_pick'];
                    $odd_value = $match['odd_value'];
                    $sub_type_id = $match['sub_type_id'];
                    $home_team = $match['home_team'];
                    $away_team = $match['away_team'];
                    $special_bet_value = $match['special_bet_value'];

                    $bet_type = $match['bet_type'] == 'live'?1:0;

                    $thisMatch = [
                        "sub_type_id"       => $sub_type_id,
                        "special_bet_value" => $special_bet_value,
                        "pick_key"          => $bet_pick,
                        "odd_value"         => $odd_value,
                        "parent_match_id"   => $parent_match_id,
                        "bet_type"          => $bet_type,
                    ];

                    $slip[] = $thisMatch;
                }

                $bet = [
                    "bet_string"     => 'sms',
                    "app_name"       => $this->getDevice() == '1' ? 'MOBILE_WEB' : 'API_WEB',
                    "possible_win"   => $possible_win,
                    "profile_id"     => $user_id,
                    "stake_amount"   => $bet_amount,
                    "bet_total_odds" => $total_odd,
                    "deviceID"       => "771046PY",
                    "endCustomerIP"  => $endCustomerIP,
                    "channelID"      => $src,
                    "slip"           => $slip,
                    "account"        => $account,
                    'msisdn'         => $phoneNumber,
                ];

                $placeB = $this->bet($bet);

                if ($src == 'mobile') {
                    $feedback = $placeB['message'];
                    if ($placeB['status_code'] == 201) {
                        $this->session->remove("betslip");
                        $this->flashSession->error($this->flashSuccess($feedback));

                        // Flash amount back to trigger ga
                        $this->session->set('bet-amount', $bet_amount);
                    } else {
                        $mesg = $placeB['status_code'] == 500
                            ? "Could not place your bet. Kindly try again later."
                            : $feedback;
                        $this->flashSession->error($this->flashMessages($mesg));
                    }

                    $this->response->redirect('betmobile');
                    // Disable the view to avoid rendering
                    $this->view->disable();

                } else {
                    $placeB['message'] = $this->formatMessage($placeB['message']);
                    if ($placeB['status_code'] == 201) {
                        $this->deleteReference();
                        $this->session->remove("betslip");
                        $this->reference();
                    } else if ($placeB['status_code'] === 500) {
                        $placeB['message'] = "Could not place your bet. Kindly try again later.";
                    }

                    $response->setContent(json_encode($placeB));

                    return $response;
                    $this->view->disable();
                }
            }
        }

    }



    /**
     * @return \Phalcon\Http\Response
     */
    public function placesgrbetAction()
    {
        $reference_id = $this->reference();
        $user_id = $this->request->getPost('user_id', 'int');
        $bet_amount = $this->request->getPost('bet_amount', 'float');
        $total_odd = $this->request->getPost('total_odd', 'float');
        $possible_win = $bet_amount * $total_odd;
        $src = $this->request->getPost('src', 'string') ?: 'internet';
        $betslip = empty($this->session->get('betslip_sgr')) ? [] : $this->session->get('betslip_sgr');
        $endCustomerIP = $this->getClientIP();
        $account = $this->getPaymentAccount();
        $phoneNumber = $this->request->get('msisdn', 'int');

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        if(($account && !$user_id) || (!$user_id && !$phoneNumber)){

            if ($src == 'mobile') {

                $this->flashSession->error($this->flashMessages('Please login in to place your bet'));
                $this->response->redirect('login');
                $this->view->disable();
                return;
            }
        }

        if ($bet_amount == 0 || strlen($bet_amount) == 0 || $bet_amount == null) {
            if ($src == 'mobile') {
                $this->flashSession->error($this->flashMessages('Please specify your bet amount.'));

                $this->response->redirect('betmobile');
                $this->view->disable();
            } else {
                $data = [
                    "status_code" => 421,
                    "message"     => "Please specify your bet amount.",
                ];
                $response->setContent(json_encode($data));

                return $response;
                $this->view->disable();
            }

        } else {
            if ($bet_amount < 19) {
                if ($src == 'mobile') {
                    $this->flashSession->error($this->flashMessages('The bet amount should be at least KShs. 19'));
                    $this->response->redirect('betmobile');
                    // Disable the view to avoid rendering
                    $this->view->disable();
                } else {
                    $data = [
                        "status_code" => 421,
                        "message"     => "Bet amount should be at least KShs. 19",
                    ];
                    $response->setContent(json_encode($data));

                    return $response;
                    $this->view->disable();
                }
            } else {
                $phql = "SELECT * from Profile where profile_id='$user_id' limit 1";
                $checkUser = $this->modelsManager->executeQuery($phql);

                $checkUser = $checkUser->toArray();

                $mobile = $checkUser['0']['msisdn'];

                $totalMatch = sizeof($betslip);

                if($totalMatch < 7){

                    if ($src == 'mobile') {

                        $this->flashSession->error($this->flashMessages('You must predict for all the seven matches in SGR'));
                        $this->response->redirect('betmobile');
                        $this->view->disable();
                        return;
                    }else{

                        $data = [
                            "status_code" => 421,
                            "message"     => "You must predict for all the seven matches in SGR",
                        ];
                        $response->setContent(json_encode($data));

                        return $response;
                        $this->view->disable();
                    }
                }

                $slip = [];

                foreach ($betslip as $match) {
                    
                    $parent_match_id = $match['parent_match_id'];
                    $bet_pick = $match['bet_pick'];
                    $odd_value = $match['odd_value'];
                    $sub_type_id = $match['sub_type_id'];
                    $home_team = $match['home_team'];
                    $away_team = $match['away_team'];
                    $special_bet_value = $match['special_bet_value'];

                    $bet_type = $match['bet_type'] == 'live'?1:0;

                    $thisMatch = [
                        "sub_type_id"       => $sub_type_id,
                        "special_bet_value" => $special_bet_value,
                        "pick_key"          => $bet_pick,
                        "odd_value"         => $odd_value,
                        "parent_match_id"   => $parent_match_id,
                        "bet_type"          => $bet_type,
                    ];

                    $slip[] = $thisMatch;
                }

                $bet = [
                    "bet_string"     => 'sms',
                    "app_name"       => $this->getDevice() == '1' ? 'MOBILE_WEB' : 'API_WEB',
                    "possible_win"   => $possible_win,
                    "profile_id"     => $user_id,
                    "stake_amount"   => $bet_amount,
                    "bet_total_odds" => $total_odd,
                    "deviceID"       => "771046PY",
                    "endCustomerIP"  => $endCustomerIP,
                    "channelID"      => $src,
                    "slip"           => $slip,
                    "product"        => "SGR",
                    "account"        => $account,
                    'msisdn'         => $phoneNumber,
                ];

                $placeB = $this->bet($bet);

                if ($src == 'mobile') {
                    $feedback = $placeB['message'];
                    if ($placeB['status_code'] == 201) {
                        $this->session->remove("betslip_sgr");
                        $this->flashSession->error($this->flashSuccess($feedback));

                        // Flash amount back to trigger ga
                        $this->session->set('bet-amount', $bet_amount);
                    } else {
                        $mesg = $placeB['status_code'] == 500
                            ? "Could not place your bet. Kindly try again later."
                            : $feedback;
                        $this->flashSession->error($this->flashMessages($mesg));
                    }

                    $this->response->redirect('betmobile');
                    // Disable the view to avoid rendering
                    $this->view->disable();

                } else {
                    $placeB['message'] = $this->formatMessage($placeB['message']);
                    if ($placeB['status_code'] == 201) {
                        $this->deleteReference();
                        $this->session->remove("betslip_sgr");
                        $this->reference();
                    } else if ($placeB['status_code'] === 500) {
                        $placeB['message'] = "Could not place your bet. Kindly try again later.";
                    }

                    $response->setContent(json_encode($placeB));

                    return $response;
                    $this->view->disable();
                }
            }
        }

    }


    /**
     * @return \Phalcon\Http\Response
     */
    public function placepeerbetAction()
    {

        $reference_id = $this->reference();
        $user_id = $this->request->getPost('user_id', 'int');
        $bet_amount = $this->request->getPost('bet_amount', 'float');
        $total_odd = $this->request->getPost('total_odd', 'float');
        $possible_win = $bet_amount * $total_odd;
        $src = $this->request->getPost('src', 'string') ?: 'internet';
        $betslip = $this->session->get('betslip-peer');
        $endCustomerIP = $this->getClientIP();
        $account = $this->getPaymentAccount();
        $phoneNumber = $this->request->get('msisdn', 'int');
        $is_peer = $this->request->getPost('is_peer', 'int');
        $peer_bet_id = $this->request->getPost('peer_bet_id', 'int');
        $bet_type = $this->request->getPost('bet_type', 'string');
        $peer_msisdn = $this->request->getPost('peer_msisdn', 'string');

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        if(($account && !$user_id) || (!$user_id && !$phoneNumber)){

            if ($src == 'mobile') {
                $this->flashSession->error($this->flashMessages('Please login in to place your bet'));
                $this->response->redirect('login');
                $this->view->disable();
                return;
            }
        }

        if (!$bet_amount || !$total_odd || !$possible_win) {
            if ($src == 'mobile') {
                $this->flashSession->error($this->flashMessages('Please specify your bet amount.'));

                $this->response->redirect('betmobile');
                // Disable the view to avoid rendering
                $this->view->disable();
            } else {
                $data = [
                    "status_code" => 421,
                    "message"     => "All fields are required including the bet amount and the invited friend's no.",
                ];
                $response->setContent(json_encode($data));

                return $response;
                $this->view->disable();
            }

        } else {
            if ($bet_amount < 19) {
                if ($src == 'mobile') {
                    $this->flashSession->error($this->flashMessages('The bet amount should be at least KShs. 19'));
                    $this->response->redirect('betmobile');
                    // Disable the view to avoid rendering
                    $this->view->disable();
                } else {
                    $data = [
                        "status_code" => 421,
                        "message"     => "Bet amount should be at least KShs. 19",
                    ];
                    $response->setContent(json_encode($data));

                    return $response;
                    $this->view->disable();
                }
            } else {
                $phql = "SELECT * from Profile where profile_id='$user_id' limit 1";
                $checkUser = $this->modelsManager->executeQuery($phql);

                $checkUser = $checkUser->toArray();

                $mobile = $checkUser['0']['msisdn'];

                if(substr($mobile, -9) == substr($peer_msisdn, -9)){

                     $data = [
                        "status_code" => 321,
                        "message"     => "You cannot invite yourself to oppose yourself. Please invite a friend to oppose your selection.",
                    ];
                    $response->setContent(json_encode($data));

                    return $response;
                    $this->view->disable();
                }

                $totalMatch = sizeof($betslip);

                $slip = [];

                foreach ($betslip as $match) {

                    $parent_match_id = $match['parent_match_id'];
                    $bet_pick = $match['bet_pick'];
                    $odd_value = $match['odd_value'];
                    $sub_type_id = $match['sub_type_id'];
                    $home_team = $match['home_team'];
                    $away_team = $match['away_team'];
                    $special_bet_value = $match['special_bet_value'];
                    $is_peer = $is_peer;
                    $bet_type = $bet_type;
                    $peer_bet_id = $peer_bet_id;

                    $thisMatch = [
                        "sub_type_id"       => $sub_type_id,
                        "special_bet_value" => $special_bet_value,
                        "pick_key"          => $bet_pick,
                        "odd_value"         => $odd_value,
                        "parent_match_id"   => $parent_match_id,
                        "bet_amount"   => $bet_amount,
                        "is_peer"        => $is_peer,
                        'peer_msisdn'    => $peer_msisdn,
                        'peer_bet_id'    => $peer_bet_id,
                    ];

                    $slip[] = $thisMatch;
                }

                $bet = [
                    "bet_string"     => 'sms',
                    "app_name"       => $this->getDevice() == '1' ? 'MOBILE_WEB' : 'API_WEB',
                    "possible_win"   => $possible_win,
                    "profile_id"     => $user_id,
                    "stake_amount"   => $bet_amount,
                    "bet_total_odds" => $total_odd,
                    "deviceID"       => "771046PY",
                    "endCustomerIP"  => $endCustomerIP,
                    "channelID"      => $src,
                    "slip"           => $slip,
                    "account"        => $account,
                    "is_peer"        => $is_peer,
                    "bet_type"       => $bet_type,
                    "peer_bet_id"    => $peer_bet_id,
                    'msisdn'         => $phoneNumber,
                    'peer_msisdn'    => $peer_msisdn,
                    "bet_amount"   => $bet_amount,
                ];

                $placeB = $this->peerBet($bet);

                if ($src == 'mobile') {

                    $feedback = $placeB['message'];
                    if ($placeB['status_code'] == 201) {
                        $this->session->remove("betslip-peer");
                        $this->flashSession->error($this->flashSuccess($feedback));

                        // Flash amount back to trigger ga
                        $this->session->set('bet-amount', $bet_amount);
                    } else {
                        $mesg = $placeB['status_code'] == 500
                            ? "Could not place your bet. Kindly try again later."
                            : $feedback;
                        $this->flashSession->error($this->flashMessages($mesg));
                    }

                    $this->response->redirect('betmobile');
                    // Disable the view to avoid rendering
                    $this->view->disable();

                } else {
                    $placeB['message'] = $this->formatMessage($placeB['message']);
                    if ($placeB['status_code'] == 201) {
                        $this->deleteReference();
                        $this->session->remove("betslip");
                        $this->reference();
                    } else if ($placeB['status_code'] === 500) {
                        $placeB['message'] = "Could not place your bet. Kindly try again later.";
                    }

                    $response->setContent(json_encode($placeB));

                    return $response;
                    $this->view->disable();
                }
            }
        }

    }


    /**
     * @return \Phalcon\Http\Response
     */
    public function betJackpotAction()
    {

        $reference_id = $this->reference();
        $user_id = $this->request->getPost('user_id', 'int');
        $src = $this->request->getPost('src', 'string');
        $jackpot_type = $this->request->getPost('jackpot_type', 'int');
        
        $jackpot_id = $this->request->getPost('jackpot_id', 'int');
        $account = $this->getPaymentAccount();
        $phoneNumber = $this->formatMobileNumber($this->request->get('msisdn', 'int'));

        $bet_type = 'jackpot';
        $url = "http://35.233.126.93:8008/jp/bet";

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");
        
        if (!$reference_id || ($account!=0 && !$user_id) || ($account==0 && !$phoneNumber)) {
            if ($src == 'mobile') {
                $this->flashSession->error($this->flashMessages('Please make sure you are logged in order to play.'));
                $this->response->redirect('betmobile');
                // Disable the view to avoid rendering
                $this->view->disable();
            } else {
                $data = [
                    "status_code" => "421",
                    "message"     => "Please make sure you are logged in order to play. ",
                ];
                $response->setContent(json_encode($data));

                return $response;
                $this->view->disable();
            }

        } else {

            $matches[] = '';

            $betslip = $this->session->get("betslip");

            foreach ($betslip as $slip) {
                if ($slip['bet_type'] == $bet_type) {
                    $matches[$slip['match_id']] = $slip;
                }
            }

            $matches = $this->array_msort($matches, ['pos' => SORT_ASC]);

            $totalMatch = sizeof($matches);

            if ($totalMatch < $jackpot_type) {
                if ($src == 'mobile') {
                    $this->flashSession->error($this->flashMessages('You must select an outcome for all Jackpot Matches'));
                    $this->response->redirect('betmobile');
                    // Disable the view to avoid rendering
                    $this->view->disable();
                } else {
                    $data = [
                        "status_code"  => "421",
                        "message"      => "You must select an outcome for all Jackpot Matches",
                        "jackpot_type" => $jackpot_type,
                    ];
                    $response->setContent(json_encode($data));

                    return $response;
                    $this->view->disable();
                }
            } else {
                $mobile = null;
                if (!$phoneNumber) {
                    $phql = "SELECT * from Profile where profile_id='$user_id' limit 1";
                    $checkUser = $this->modelsManager->executeQuery($phql);

                    $checkUser = $checkUser->toArray();

                    $mobile = $checkUser['0']['msisdn'];
                }

                $message = '';

                foreach ($matches as $match) {
                    if ($jackpot_type == 5) {
                        $message = $message . "#" . $match['bet_pick'];
                        $message = str_replace(":", "-", $message);
                    } else {
                        $message = $message . "|" . $match['bet_pick'];
                    }
                }

                if ($jackpot_type == 5) {
                    $message = substr($message, 1);
                    $url = "http://35.233.126.93:8008/jp/bet";
                }

                $bet = [
                    "profile_id" => $user_id,
                    'jackpot_id' => $jackpot_id,
                    'msisdn'     => $mobile ?: $phoneNumber,
                    'message'    => $message,
                    'account'    => $account,
                    'jackpot_type'    => $jackpot_type,
                ];

                $placeB = $this->betJackpot($bet, $url);

                if ($src == 'mobile') {
                    $feedback = $placeB['message'];
                    if ($placeB['status_code'] == 201) {
                        $feedback = $placeB['message'];
                        $this->flashSession->error($this->flashSuccess($feedback));
                    } else {
                        $this->flashSession->error($this->flashMessages($feedback));
                    }

                    $this->response->redirect('betmobile');
                    // Disable the view to avoid rendering
                    $this->view->disable();

                } else {

                    if ($placeB['status_code'] == 201) {
                        $this->deleteReference();
                        $this->reference();
                        $this->session->remove("betslip");
                    }

                    $response->setContent(json_encode($placeB));

                    return $response;
                    $this->view->disable();
                }
            }
        }

    }

    /**
     * @param $array
     * @param $cols
     *
     * @return array
     */
    private function array_msort($array, $cols)
    {
        $colarr = [];
        foreach ($cols as $col => $order) {
            $colarr[$col] = [];
            foreach ($array as $k => $row) {
                $colarr[$col]['_' . $k] = strtolower($row[$col]);
            }
        }
        $eval = 'array_multisort(';
        foreach ($cols as $col => $order) {
            $eval .= '$colarr[\'' . $col . '\'],' . $order . ',';
        }
        $eval = substr($eval, 0, -1) . ');';
        eval($eval);
        $ret = [];
        foreach ($colarr as $col => $arr) {
            foreach ($arr as $k => $v) {
                $k = substr($k, 1);
                if (!isset($ret[$k])) $ret[$k] = $array[$k];
                $ret[$k][$col] = $array[$k][$col];
            }
        }

        return $ret;
    }

    /**
     * @return int
     */
    protected function getPaymentAccount()
    {
        $account = $this->request->get('account', 'int') ?: $this->request->get('payment_method', 'int');

        // Make account 0 if not 1;
        $account = $account == 1 ? 1 : 0;

        return $account;
    }


}
    
