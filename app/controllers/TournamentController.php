<?php

/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */
class TournamentController extends ControllerBase
{

    /**
     *
     */
    public function indexAction()
    {

        $groups = [
            8805,
            8807,
            8809,
            8810,
            8811,
            8812,
            8813,
            8815,
        ];
        $title = "UEFA Champions League";

        $id = $this->request->get('id');

        if ($id == 'europa') {
            $groups = [
                8817,
                8818,
                8821,
                8823,
                8825,
                8827,
                8829,
                8831,
                8833,
                8835,
                8836,
                8838,
            ];
            $title = "UEFA Europa League";
        }

        $groups = implode(',', $groups);

        if (!is_numeric($id))
            $id = 1;

        $matches = $this->rawQueries("select c.priority, (select count(distinct sub_type_id) from event_odd where parent_match_id = m.parent_match_id) as side_bets, o.sub_type_id, MAX(CASE WHEN o.odd_key = '1' THEN odd_value END) AS home_odd, MAX(CASE WHEN o.odd_key = 'x' THEN odd_value END) AS neutral_odd, MAX(CASE WHEN o.odd_key = '2' THEN odd_value END) AS away_odd, m.game_id, m.match_id, m.start_time, m.away_team, m.home_team, m.parent_match_id from `match` m inner join event_odd o on m.parent_match_id = o.parent_match_id inner join competition c on c.competition_id = m.competition_id inner join sport s on s.sport_id = c.sport_id where c.competition_id in($groups) and m.start_time > now() and o.sub_type_id = 1  group by m.parent_match_id order by m.priority desc, c.priority desc , m.start_time limit 60");

        $theCompetition = $this->rawQueries("select competition_name,competition_id,category from competition where competition_id='$id' limit 1");

        $theBetslip = $this->session->get("betslip");

        $this->tag->setTitle($title);

        $this->view->setVars([
            'matches'    => $matches,
            'title'      => $title,
            'theBetslip' => $theBetslip,
        ]);

        $this->view->pick("tournament/index");
    }

    /**
     * @param $id
     */
    public function competitionAction($id)
    {
        $id = $this->request->getPost('id', 'int');
        $matches = $this->rawQueries("select o.sub_type_id, MAX(CASE WHEN o.odd_key = '1' THEN odd_value END) AS one, MAX(CASE WHEN o.odd_key = 'x' THEN odd_value END) AS x, MAX(CASE WHEN o.odd_key = '2' THEN odd_value END) AS two, count(distinct o.sub_type_id) as side_bets, m.game_id, m.match_id, m.start_time, m.away_team, m.home_team, m.parent_match_id from `match` m inner join event_odd o on m.parent_match_id = o.parent_match_id  where m.competition_id='$id' and o.sub_type_id = 1 and m.start_time > now() group by m.parent_match_id order by start_time limit 60");

        $this->view->pick("football/index");
    }

}

