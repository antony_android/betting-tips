<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

class LoginController extends ControllerBase
{

    public function indexAction()
    {
    	$refURL = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $this->view->setVars(['refURL'=>$refURL, 'winners'=>$this->topWinners(),]);
    }

    public function authenticateAction()
    {
    	if ($this->request->isPost()) {

        	$mobile = $this->request->getPost('mobile', 'int');

            $remember = $this->request->getPost('remember', 'int') ?: 0;

            $refURL = $this->request->getPost('ref') ? : '';

        	$password = $this->request->getPost('password');

        	if (!$mobile || !$password ) {
            	$this->flashSession->error($this->flashMessages('All fields are required'));
                return $this->response->redirect('login');
    	        $this->view->disable();
            }

            $mobile = $this->formatMobileNumber($mobile);

        	$phql = "SELECT * from Profile where msisdn='$mobile' limit 1";
    		$user = $this->modelsManager->executeQuery($phql);

    		$user=$user->toArray();

        	if ($user == false) {
                $refU="login?ref=".$refURL;
        		$this->flashSession->error($this->flashMessages('Invalid username/password'));
        		$this->response->redirect($refU);
    		    // Disable the view to avoid rendering
    		    $this->view->disable();
        	}

        	$profile_id = $user['0']['profile_id'];

        	$phql = "SELECT * from ProfileSettings where profile_id='$profile_id' limit 1";
    		$checkUser = $this->modelsManager->executeQuery($phql);

    		$checkUser=$checkUser->toArray();

            // if ($checkUser['0']['status'] == '0') {
            //     $this->flashSession->error($this->flashMessages('Your account is not verified, please enter the code sent to your phone to verify'));
            //     $this->response->redirect('verify');
            //     // Disable the view to avoid rendering
            //     $this->view->disable();
            // }else{
            
            if ($checkUser) {
                $thePassword = $checkUser['0']['password'];

                if (!$this->security->checkHash($password, $thePassword)) {
                    $this->flashSession->error($this->flashMessages('Invalid username/password'));
                    $this->response->redirect('login');
                    // Disable the view to avoid rendering
                    $this->view->disable();
                }else{
                    
                    $device=$this->getDevice();
                    $sessionData=['id' => $checkUser['0']['profile_id'],'remember' => $remember,'mobile'=>$mobile,'device'=>$device];
                    $exp=time()+(3600*24*5);
                    $this->registerAuth($sessionData,$exp);
                    $this->response->redirect($refURL);

                }
                
            } else{
                $this->flashSession->error($this->flashMessages('Invalid username/password'));
                $this->response->redirect('login');
                // Disable the view to avoid rendering
                $this->view->disable();
            }
            // }
        }
    }

}
