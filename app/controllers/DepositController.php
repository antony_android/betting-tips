<?php

/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */
class DepositController extends ControllerBase
{

    /**
     *
     */
    public function indexAction()
    {
        
        if ($this->request->isPost() && $this->session->get('auth')) {

            $amount = $this->request->getPost('amount', 'int');
            $data = 'amount='.$amount.'&msisdn='.$this->session->get('auth')['mobile'];
            $success = $this->topup($data);
            if($success == 200){
               $message = 'Deposit prompt sent to your mobile number. Kindly follow the prompt to complete your request';
            }else{
               $message = 'Failed to sent deposit request. Kindly try again';
            }
              
           $this->flashSession->error($this->flashMessages($message));
        }

        $this->view->setVars([
           "topLeagues" => $this->topLeagues(),
           'winners'=>$this->topWinners(),]);
        $this->tag->setTitle('How to Deposit');

    }

}
