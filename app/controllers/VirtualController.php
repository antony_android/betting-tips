<?php

/**
 * Copyright (c) Anto 2018.
 *
 * All rights reserved.
 */
class VirtualController extends ControllerBase
{

    /**
     * Results controller to get the results, outcome hardcoded to be changed later 
     */
    public function indexAction()
    {

        list($page, $limit, $skip) = $this->getPaginationParams();

        $keyword = $this->request->getPost('keyword');

        $date_put = $this->request->getPost('date_put');

        list($today, $total, $sCompetitions) = $this->getGames($keyword, $skip, $limit);

        $theBetslip = $this->session->get("betslip");

        $this->view->setVars([
            'today'         => $today,
            'theBetslip'    => $theBetslip,
            'sCompetitions' => $sCompetitions,
        ]);

        $this->tag->setTitle($this->view->t->_('title'));

    }

}