<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

use Phalcon\Mvc\Model\Query;
use Phalcon\Http\Response;

class IndexController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('BetPalace');
        $connection = $this->di->getShared("db");
    }

    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function indexAction()
    {
        $hour = date('H');
        $sport_id = $this->request->get('sp', 'int');

        if(empty($sport_id)) {$sport_id = 79;}

        $this->session->set("previous_address", $_SERVER['REQUEST_URI']);

        //if ($hour == 23) {
            //return $this->response->redirect('upcoming');
            //$this->view->disable();
        //} else {

            list($page, $limit, $skip) = $this->getPaginationParams();

            $keyword = $this->request->getPost('keyword', 'string');

            $theBetslip = $this->session->get("betslip");
            $where = "";
            if ($sport_id){
                $where = " and s.sport_id = '$sport_id'";
            }

            $orderBy = "m_priority desc, priority desc, start_time asc";

            list($today, $total, $sCompetitions) = $this->getGames($keyword, $skip, $limit, $where, $orderBy);

            $total = $total['0']['total'];
            $showModal = 1;

            $this->view->setVars([
                'today'         => $today,
                'theBetslip'    => $theBetslip,
                'sCompetitions' => $sCompetitions,
                'total'         => $total,
                'pages'         => $this->getResultPages($total, $limit),
                'page'          => $page,
                'topSports'     => $this->topSports(),
                'sportId'       =>$sport_id,
                'winners'	=> $this->topWinners(),
                'keyword'       =>$keyword,
                'showModal' => $showModal
            ]);

            // die(print_r($today));

            $this->tag->setTitle($this->view->t->_('title'));
            $this->view->pick("layout/index");
        //}
    }


    public function matchjsonAction()
    {
        $today = $this->rawQueries("SELECT * FROM ux_todays_highlights WHERE start_time > now() LIMIT 30");

        $this->cookies->set('thematch', "this is a new cookie", time() + 15 * 86400);

        $cookieMatches = $this->cookies->get('thematch');

        $cookieMatches = $cookieMatches->getValue();

    }

    public function aboutAction($statement)
    {
        if ($this->view->getCache()->exists("sidebar")) {
            return $this->forward('/');
        }
    }

    public function signupAction()
    {
        # code...
    }

}
