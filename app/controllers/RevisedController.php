<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

use Phalcon\Mvc\Model\Query;
use Phalcon\Http\Response;

class RevisedController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('BetPalace');
        $connection = $this->di->getShared("db");
    }

    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function indexAction()
    {
        $hour = date('H');
        $sport_id = $this->request->get('sp', 'int');

        if(empty($sport_id)) {$sport_id = 79;}

        $this->session->set("previous_address", $_SERVER['REQUEST_URI']);

        list($page, $limit, $skip) = $this->getPaginationParams();

        $keyword = $this->request->getPost('keyword', 'string');

        $theBetslip = $this->session->get("betslip");
        $where = "";
        if ($sport_id){
            $where = " and sport_id = '$sport_id'";
        }

        $orderBy = "m_priority desc, priority desc, start_time asc";

        list($today, $total, $sCompetitions) = $this->getRevisedGames($keyword, $skip, $limit, $where, $orderBy);

        $total = $total['0']['total'];


        // print_r($today);
        // exit();

        $this->view->setVars([
            'today'         => $today,
            'theBetslip'    => $theBetslip,
            'sCompetitions' => $sCompetitions,
            'total'         => $total,
            'pages'         => $this->getResultPages($total, $limit),
            'page'          => $page,
            'topSports'     => $this->topSports(),
            'sportId'       =>$sport_id,
            'keyword'       =>$keyword
        ]);

        $this->tag->setTitle($this->view->t->_('title'));
        $this->view->pick("revised/revised");
    }

}