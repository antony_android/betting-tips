<?php
/**
 * Copyright (c) Anto 2018.
 *
 * All rights reserved.
 */

use Phalcon\Http\Response;

/**
 * Class MatchController
 */
class LivehomeController extends ControllerBase
{

    /**
     * Index
     */
    public function indexAction()
    {
        
        $theBetslip = $this->session->get("betslip");
        

        $liveMatches = $this->rawQueries("SELECT m.match_id, m.parent_match_id, m.home_team,
            m.away_team, c.competition_name, c.category, s.sport_name, ct.country_code,
            m.match_time, m.match_status, m.score, m.event_status, m.home_yellow_card, 
            m.away_yellow_card,m.home_red_card,m.away_red_card,m.home_corner,m.away_corner
            FROM `live_match` m INNER JOIN competition c using(competition_id) 
            inner join category ct on ct.category_id =c.category_id INNER JOIN sport s
            on s.sport_id=c.sport_id WHERE m.active=1 and m.match_status
            not in ('Ended', 'Suspended') and m.event_status not in 
            ('Ended', 'Finished', 'Deactivated', 'Fixture Change') 
            GROUP BY m.parent_match_id ORDER BY m.priority DESC, c.priority DESC, 
            m.start_time");

        $this->tag->setTitle("Live Games");

        $updatedLive = [];
        foreach($liveMatches as $row=>$data){
            $updatedLive[ $row] = $data;
            $updatedLive[ $row]['flag_url'] = $this->categoryLogo->getFlag($data['country_code']) ;
        }
        $liveMatches = $updatedLive;

        $theBetslip = $this->session->get("betslip");
        $newslip = [];
        $totalOdd = 1;
        foreach($theBetslip as $match_id => $slip){
            $sub_type_id = $slip['sub_type_id'];
            $parent_match_id = $slip['parent_match_id'];
            $special_bet_value = $slip['special_bet_value'];
            $bet_pick = $slip['bet_pick'];
            $bet_type =  $slip['bet_type'];
            $query = "select e.odd_value, e.odd_active, e.market_active, e.active as eactive, 
                    lo.active as m_active from "
                . " live_odds_change e inner join live_match lo using(parent_match_id) where "
                . " e.parent_match_id='$parent_match_id' and e.sub_type_id='$sub_type_id' "
                . " and e.special_bet_value='$special_bet_value' and e.odd_key='$bet_pick' "
                . " and lo.active = 1 and lo.event_status = 'Live' order by e.betradar_timestamp desc limit 1 ";
        
            $data = $this->rawQueries($query);
            if(!empty($data) && 
                $data[0]['odd_active'] == 1 && $data[0]['market_active'] == 'Active' 
                && $data[0]['eactive'] == 1 && $data[0]['m_active'] == 1){
                $new_odd = $data[0]['odd_value'];
                $slip['odd_value'] = $new_odd;
                $newslip[$match_id] = $slip;
            }else{
                $slip['odd_value'] = 1;
                $newslip[$match_id] = $slip; 
            }
            $totalOdd *=  $slip['odd_value'];
        }

        $totalOdd = round($totalOdd,2);
        //die(print_r($newslip, 1));
        $theBetslip = $newslip;
        $this->session->set('betslip', $theBetslip);



        $this->view->setVars([
            "topLeagues" => $this->topLeagues(),
            'winners'=>$this->topWinners(),
            'liveMatches' => $liveMatches,
            'theBetslip' => $theBetslip,
            'totalOdd'=>$totalOdd,
        ]);

        $this->view->pick("live/live-home");

    }

    public function fetchgameAction()
    {
       
        $max_delay_time = strtotime("+20 seconds");

        $last_upadated =  $this->session->get("m_last_update_datetime");
        
        while(!empty($last_upadated)){
            $updateQuery = "select m.modified from live_match m where m.modified > 
            '$last_upadated' order by 1 desc limit 1";
            $lastUpdateDateResult = $this->rawQueries($updateQuery);

            if(!empty($lastUpdateDateResult)){
                $lastUpdateDateResult = $lastUpdateDateResult[0]['modified'];
                if($lastUpdateDateResult != $last_upadated){
                    $this->session->set('m_last_update_datetime', $lastUpdateDateResult);
                    break;
                }
            }
            if(time() > $max_delay_time){
                //Alow update on UI after max delay time
                break;
            }
            sleep(2);
            
        }
        if(empty($last_upadated)){
            $updateQuery = "select now() as modified ";
            $lastUpdateDateResult = $this->rawQueries($updateQuery);
            if(!empty($lastUpdateDateResult)){
                $lastUpdateDateResult = $lastUpdateDateResult[0]['modified'];
                $this->session->set('m_last_update_datetime', $lastUpdateDateResult);
            }

        }


        $liveMatches = $this->rawQueries("SELECT m.match_id, m.parent_match_id, m.home_team,
            m.away_team, c.competition_name, c.category, s.sport_name, ct.country_code,
            m.match_time, m.match_status, m.score, m.event_status, m.home_yellow_card, 
            m.away_yellow_card,m.home_red_card,m.away_red_card,m.home_corner,m.away_corner
            FROM `live_match` m INNER JOIN competition c using(competition_id) 
            inner join category ct on ct.category_id =c.category_id INNER JOIN sport s
            on s.sport_id=c.sport_id WHERE s.sport_id=79 and m.active=1 and m.match_status
            not in ('Ended', 'Suspended') and m.event_status not in 
            ('Ended', 'Finished', 'Deactivated', 'Fixture Change') 
            GROUP BY m.parent_match_id ORDER BY m.priority DESC, c.priority DESC, 
            m.start_time");

        $updatedLive = [];
        foreach($liveMatches as $row=>$data){
            $updatedLive[ $row] = $data;
            $updatedLive[ $row]['flag_url'] = $this->categoryLogo->getFlag($data['country_code']) ;
        }
        $liveMatches = $updatedLive;

        

        $theBetslip = $this->session->get("betslip");
        $newslip = [];
        $totalOdd = 1;
        foreach($theBetslip as $match_id => $slip){
            $sub_type_id = $slip['sub_type_id'];
            $parent_match_id = $slip['parent_match_id'];
            $special_bet_value = $slip['special_bet_value'];
            $bet_pick = $slip['bet_pick'];
            $bet_type =  $slip['bet_type'];
            $query = "select e.odd_value, e.odd_active, e.market_active from "
                . " live_odds_change e inner join live_match lo using(parent_match_id) where "
                . " e.parent_match_id='$parent_match_id' and e.sub_type_id='$sub_type_id' "
                . " and e.special_bet_value='$special_bet_value' and e.odd_key='$bet_pick' "
                . " and lo.active = 1 and lo.event_status = 'Live' order by e.betradar_timestamp desc limit 1 ";

            //die($query);
            $data = $this->rawQueries($query);
            if(!empty($data) && 
                $data[0]['odd_active'] == 1 && $data[0]['market_active'] == 'Active'  ){
                $new_odd = $data[0]['odd_value'];
                $slip['odd_value'] = $new_odd;
                $newslip[$match_id] = $slip;
            }else{
                $slip['odd_value'] = 1;
                $newslip[$match_id] = $slip; 
            }
            $totalOdd *=  $slip['odd_value'];
        }

        $totalOdd = round($totalOdd,2);
        //die(print_r($newslip, 1));
        $theBetslip = $newslip;
        $this->session->set('betslip', $theBetslip);

        $results = [
            'liveMatches' => $liveMatches,
            'totalOdd'=>$totalOdd,
            'theBetslip'=>$theBetslip,
            
        ];
        //die(print_r($results,1));

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        $results = json_encode($results);
        //die(print_r($results));

        $response->setContent($results);

        return $response;
    }
}
