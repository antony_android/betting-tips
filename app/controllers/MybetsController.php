<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

class MybetsController extends ControllerBase
{

    public function indexAction()
    {
    	$betID = $this->request->get('id','int');
  
	if(!is_numeric($betID))
		$betID = 0;

  	$id = $this->session->get('auth')['id'];

    if ($id) {

    	$myBets = $this->rawQueries("select b.bet_id,b.created, jb.jackpot_bet_id, total_games
         as total_matches, b.bet_message,b.bet_amount,b.possible_win,b.status, 
         if(FIND_IN_SET(b.status,(select group_concat(status) from bet_slip where bet_id = 
         b.bet_id)), b.status, 1) xstatus from bet b inner join bet_slip s on b.bet_id=s.bet_id
          left join jackpot_bet jb on jb.bet_id=b.bet_id where b.profile_id=$id group by
           s.bet_id order by b.created desc limit 30;");
  
    	$title = "My Bets";

        $this->tag->setTitle($title);

    	$this->view->setVars(["myBets"=>$myBets, 'winners'=>$this->topWinners()]);

    	if ($betID) {

    	   $myBet = $this->rawQueries("select b.bet_id,b.created, total_games as total_matches,
            b.bet_message,b.bet_amount,b.possible_win,b.status, b.profile_id, 
            b.revised_possible_win, b.is_void, if(FIND_IN_SET(b.status,(select 
            group_concat(status) from bet_slip where bet_id = b.bet_id)), b.status, 1) 
            xstatus from bet b inner join bet_slip s on b.bet_id=s.bet_id where 
            b.bet_id='$betID' group by s.bet_id order by b.created desc limit 40");

    		$myBet = $myBet['0'];

            if ($myBet['profile_id']==$id) {

                $betQ  = "select count(*) as betcount, count(if(l.parent_match_id is null, null, 
                1))livetable from bet_slip s left join live_odds_change l on 
                l.parent_match_id=s.parent_match_id and l.sub_type_id=s.sub_type_id and 
                s.special_bet_value = l.special_bet_value where bet_id = '$betID' and live_bet=1";

                $liveDetails = $this->rawQueries($betQ);

                if(!empty($liveDetails) && $liveDetails[0]['betcount'] > 0){

                    $query = "select b.bet_id, b.created, s.live_bet, m.start_time, 
                    b.bet_amount, possible_win, b.win, b.status, m.game_id, m.away_team, 
                    (select winning_outcome from outcome where sub_type_id=45 and 
                    parent_match_id=s.parent_match_id and is_winning_outcome =1 and 
                    s.live_bet=outcome.live_bet) as ft_score, ( select market_name from 
                    live_odds_change tt where tt.parent_match_id = s.parent_match_id and 
                    tt.sub_type_id=s.sub_type_id and tt.special_bet_value = 
                    s.special_bet_value limit 1) as bet_type, m.home_team, s.odd_value, 
                    s.sub_type_id, s.bet_pick, s.void_factor, group_concat(o.winning_outcome) 
                    as winning_outcome from bet b inner join bet_slip s on s.bet_id = b.bet_id 
                    inner join `match` m on m.parent_match_id = s.parent_match_id left join 
                    outcome o on o.parent_match_id = s.parent_match_id and s.sub_type_id = 
                    o.sub_type_id and s.special_bet_value = o.special_bet_value and s.live_bet 
                    = o.live_bet and o.is_winning_outcome = 1 where s.bet_id = '$betID' 
                    group by s.bet_slip_id, s.special_bet_value order by b.bet_id desc;";

                    $betDetails = $this->rawQueries($query);

                }else{
                    $betDetails = $this->rawQueries("select b.bet_id, b.created, s.live_bet, 
                        m.start_time, b.bet_amount, possible_win, b.win, b.status, m.game_id, 
                        m.away_team, (select winning_outcome from outcome where sub_type_id=45 
                        and parent_match_id=s.parent_match_id and is_winning_outcome =1 and 
                        s.live_bet=outcome.live_bet) as ft_score, m.home_team, s.odd_value, 
                        s.sub_type_id, s.bet_pick, s.void_factor, group_concat(o.winning_outcome) 
                        as winning_outcome, concat(t.name,' ',s.special_bet_value) as bet_type 
                        from bet b inner join bet_slip s on s.bet_id = b.bet_id inner join `match` 
                        m on m.parent_match_id = s.parent_match_id inner join odd_type t on 
                        (t.parent_match_id = s.parent_match_id and s.sub_type_id=t.sub_type_id) 
                        left join outcome o on o.parent_match_id = s.parent_match_id and 
                        s.sub_type_id = o.sub_type_id and s.special_bet_value = o.special_bet_value 
                        and s.live_bet = o.live_bet and o.is_winning_outcome = 1 where s.bet_id = 
                        '$betID' group by s.bet_slip_id, s.special_bet_value order by b.bet_id desc");
                
                }

                //$betDetails = $this->rawQueries("select b.bet_id, b.created, s.live_bet, m.start_time, b.bet_amount, possible_win, b.win, b.status, m.game_id, m.away_team, (select winning_outcome from outcome where sub_type_id=45 and parent_match_id=s.parent_match_id and is_winning_outcome =1 and s.live_bet = outcome.live_bet) as ft_score, m.home_team, s.odd_value, s.sub_type_id, s.bet_pick, s.void_factor, group_concat(o.winning_outcome) as winning_outcome, concat(t.name,' ',s.special_bet_value) as bet_type from bet b inner join bet_slip s on s.bet_id = b.bet_id inner join `match` m on m.parent_match_id = s.parent_match_id inner join odd_type t on (t.parent_match_id = s.parent_match_id and s.sub_type_id=t.sub_type_id) and s.live_bet = t.live_bet left join outcome o on o.parent_match_id = s.parent_match_id and s.sub_type_id = o.sub_type_id and s.special_bet_value = o.special_bet_value and s.live_bet = o.live_bet and o.is_winning_outcome = 1 where s.bet_id = '$betID' group by s.bet_slip_id, s.special_bet_value order by b.bet_id desc");
                
                $this->view->setVars(["betDetails"=>$betDetails,'myBet'=>$myBet]);
                $this->view->pick("mybets/details");
            }else{
                $this->flashSession->error($this->flashMessages('You do not have permission to view 
                    this page'));
                $this->response->redirect('mybets');
                // Disable the view to avoid rendering
                $this->view->disable();
            }
    		
    	}

        }else{
            $this->flashSession->error($this->flashMessages('Login to access this page'));
            $this->response->redirect('login');
        }
    }

}

