<?php

/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */
class MobiController extends ControllerBase
{
    /**
     *
     */
    public function indexAction()
    {
        $this->view->pick("mobile/index");
    }

    /**
     *
     */
    public function footballAction()
    {
        $this->view->setVars([
            "allLeagues" => $this->allLeagues(),
        ]);

        $this->view->pick("mobile/football");
    }

    /**
     *
     */
    public function rugbyAction()
    {
        $this->view->setVars(['rugby' => $this->rugby()]);
        $this->view->pick("mobile/football");
    }

    /**
     *
     */
    public function tennisAction()
    {
        $this->view->pick("mobile/tennis");
    }

    /**
     *
     */
    public function sportsAction()
    {
        $this->view->pick("mobile/sport");
    }

    /**
     *
     */
    public function competitionAction()
    {
        $id = $this->request->get('country');
        $competitions = $this->rawQueries("select c.competition_id, c.competition_name, c.category,c.sport_id from competition c inner join `match` m on m.competition_id = c.competition_id where c.sport_id = 79 and c.category=? and m.start_time BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 7 DAY) group by c.competition_name order by c.competition_name", [$id]);
        $theCompetition = $this->rawQueries("select competition_name,competition_id,category,sport.sport_name,sport.sport_id  from competition left join sport on competition.sport_id=sport.sport_id where category=? limit 1", [$id]);

        $title = $theCompetition['0']['competition_name'] . ", " . $theCompetition['0']['category'];

        $this->tag->setTitle($title);

        $this->view->setVars([
            "competitions"   => $competitions,
            "theCompetition" => $theCompetition,
        ]);
        $this->view->pick("mobile/competition");
    }

}

