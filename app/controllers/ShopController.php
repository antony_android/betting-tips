<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

use Phalcon\Http\Response;
use Phalcon\Mvc\View;

class ShopController extends ControllerBase
{

    public function indexAction()
    {
    	// $refURL = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        // $this->view->setVars(['refURL'=>$refURL]);

        $this->tag->setTitle("Shop Login");

        $shopLoggedIn =  $this->session->get('auth')['id'];
        $theBetslip = $this->session->get("shopbetslip");

        if($shopLoggedIn != null){

            $this->response->redirect('shop/matches');
            $this->view->disable();
        }

        $this->view->setVars([
            'theBetslip'    => $theBetslip,
            'winners' => $this->topWinners(),
        ]);


        $this->view->pick("shop/login");
    }


    public function loginAction()
    {
        // $refURL = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        // $this->view->setVars(['refURL'=>$refURL]);

        $this->tag->setTitle("Shop Login");

        $shopLoggedIn = $this->session->get('auth')['id'];
        $theBetslip = $this->session->get("shopbetslip");

        if($shopLoggedIn != null){

            $this->response->redirect('shop/matches');
            $this->view->disable();
        }

        $this->view->setVars([
            'theBetslip'    => $theBetslip,
        ]);


        $this->view->pick("shop/login");
    }


    public function authenticateAction()
    {
        if ($this->request->isPost()) {

            $mobile = $this->request->getPost('mobile', 'int');

            $remember = $this->request->getPost('remember', 'int') ?: 0;

            $refURL = $this->request->getPost('ref') ?: '';

            $password = $this->request->getPost('password');

            if (!$mobile || !$password ) {
                $this->flashSession->error($this->flashMessages('All fields are required'));
                return $this->response->redirect('/shop/login');
                $this->view->disable();
            }

            $mobile = $this->formatMobileNumber($mobile);

            $phql = "SELECT * from Profile where msisdn='$mobile' limit 1";
            $user = $this->modelsManager->executeQuery($phql);

            $user=$user->toArray();

            if ($user == false) {
                $refU="/shop/login?ref=".$refURL;
                $this->flashSession->error($this->flashMessages('Invalid username/password'));
                $this->response->redirect($refU);
                // Disable the view to avoid rendering
                $this->view->disable();
            }

            $profile_id = $user['0']['profile_id'];

            $phql = "SELECT * from ProfileSettings where profile_id='$profile_id' limit 1";
            $checkUser = $this->modelsManager->executeQuery($phql);

            $checkUser=$checkUser->toArray();
            
            if ($checkUser) {
                $thePassword = $checkUser['0']['password'];

                if (!$this->security->checkHash($password, $thePassword)) {
                    $this->flashSession->error($this->flashMessages('Invalid username/password'));
                    $this->response->redirect('/shop/login');
                    // Disable the view to avoid rendering
                    $this->view->disable();
                }else{
                    
                    $device=$this->getDevice();
                    $sessionData=['id' => $checkUser['0']['profile_id'],'remember' => $remember,'mobile'=>$mobile,'device'=>$device];
                    $exp=time()+(3600*24*5);
                    $this->registerAuth($sessionData,$exp);
                    $this->response->redirect('/shop/matches');

                }
                
            } else{
                $this->flashSession->error($this->flashMessages('Invalid username/password'));
                $this->response->redirect('/shop/login');
                // Disable the view to avoid rendering
                $this->view->disable();
            }
        }
    }


    public function logoutAction()
    {
        $this->session->destroy();
        $this->cookies->get('auth')->delete();
        $this->response->redirect('shop');
        $this->view->disable();
    }


    public function matchesAction() {

        $hour = date('H');

        list($page, $limit, $skip) = $this->getPaginationParams();
        $sport_id = $this->request->get('sp', 'int');
        if(empty($sport_id)) {$sport_id = 79;}

        $keyword = $this->request->getPost('keyword', 'string');

        list($today, $total, $sCompetitions) = $this->getGames($keyword, $skip, $limit,
         '','m_priority desc, priority desc');

        $total = $total['0']['total'];

        $theBetslip = $this->session->get("shopbetslip");

        $this->view->setVars([
            'today'         => $today,
            'theBetslip'    => $theBetslip,
            'sCompetitions' => $sCompetitions,
            'total'         => $total,
            'topSports'     => $this->topSports(),
            'pages'         => $this->getResultPages($total, $limit),
            'sportId'       => $sport_id,
            'page'          => $page,
            'keyword'       => $keyword
        ]);

        $this->tag->setTitle($this->view->t->_('title'));
        $this->view->pick("shop/matches");
    }


    public function matchAction(){

        $id = $this->request->get('id', 'int');

        $matchInfo = $this->rawQueries("select m.home_team, m.game_id, m.away_team,m.start_time,
            c.competition_name,c.category,m.parent_match_id from `match` m left join competition 
            c on m.competition_id=c.competition_id where match_id=$id  and m.start_time > now() 
            limit 1");
        
        $matchInfo = array_shift($matchInfo);

        $subTypes = $this->rawQueries("select o.priority, m.match_id, e.odd_key as display, 
            o.name, e.odd_key, e.odd_value, e.sub_type_id, e.special_bet_value from event_odd e 
            inner join odd_type o on (o.sub_type_id = e.sub_type_id AND e.parent_match_id = 
            o.parent_match_id) inner join `match` m on m.parent_match_id = e.parent_match_id 
            left join market_priority mp on o.sub_type_id = mp.sub_type_id where m.start_time > 
            now() and match_id = '$id' and o.active = 1 and e.odd_key <> '-1' and e.max_bet IN 
            (20000,1) and e.sub_type_id not in (select sub_type_id from prematch_disabled_market 
            where status=1) and CASE WHEN e.sub_type_id = 18 THEN e.odd_key REGEXP '[.]5$' WHEN 
            e.sub_type_id = 68 THEN e.odd_key REGEXP '[.]5$' WHEN e.sub_type_id = 90 THEN 
            e.odd_key REGEXP '[.]5$' ELSE 1=1 END and case when e.sub_type_id =1 then e.odd_key 
            in (m.home_team, 'draw', m.away_team) else 1=1 end group by e.sub_type_id, e.odd_key, 
            e.special_bet_value order by mp.priority desc, sub_type_id, FIELD(e.odd_key,
            m.home_team,'draw',m.away_team, concat(m.home_team, ' or ', m.away_team), 
            concat(m.home_team, ' or draw'), concat('draw or ', m.away_team)), special_bet_value 
            asc");

        $theBetslip = $this->session->get("shopbetslip");
        $title = $matchInfo['home_team'] . " vs " . $matchInfo['away_team'];

        $this->tag->setTitle($title);

        $this->view->setVars([
            "topLeagues" => $this->topLeagues(),
            'subTypes'   => $subTypes,
            'matchInfo'  => $matchInfo,
            'theBetslip' => $theBetslip,
        ]);

        $this->view->pick("shop/match");
    }


    // public function betslipAction()
    // {

    //     $betslip = $this->session->get('shopbetslip');
    //     $slipCount = sizeof($betslip);
    //     $stake = $this->session->get('stake') ?: 50;
    //     $this->view->setVars([
    //         "stake" => $stake,
    //         "shopSlipCount" => $slipCount,
    //         "betslip"=> $betslip
    //     ]);
    // }

    public function addAction()
    {
        $match_id = $this->request->getPost('match_id', 'int');
        $bet_pick = $this->request->getPost('odd_key');
        $sub_type_id = $this->request->getPost('sub_type_id', 'int');
        $special_bet_value = $this->request->getPost('special_bet_value');
        $bet_type = $this->request->getPost('bet_type') ?: 'prematch';
        $home_team = $this->request->getPost('home');
        $away_team = $this->request->getPost('away');
        $odd_value = $this->request->getPost('odd');
        $odd_type = $this->request->getPost('oddtype');
        $parent_match_id = $this->request->getPost('parentmatchid', 'int');
        $pos = $this->request->getPost('pos');

        if ($special_bet_value == '0') {
            $special_bet_value = '';
        }

        $status = 1;
        $betslip = [];

        if ($this->session->has("shopbetslip")) {
            $betslip = $this->session->get("shopbetslip");
        }

        if ($betslip["$match_id"] && ($betslip["$match_id"]["bet_pick"] == $bet_pick)) {
            unset($betslip["$match_id"]);
            $status = 0;
        } else {
            $betslip["$match_id"] = [
                'match_id'          => $match_id,
                'bet_pick'          => $bet_pick,
                'sub_type_id'       => $sub_type_id,
                'special_bet_value' => $special_bet_value,
                'bet_type'          => $bet_type,
                'home_team'         => $home_team,
                'away_team'         => $away_team,
                'odd_value'         => $odd_value,
                'odd_type'          => $odd_type,
                'parent_match_id'   => $parent_match_id,
                'pos'               => $pos,
            ];
        }

        $this->session->set("shopbetslip", $betslip);

        $count = sizeof($betslip);

        $data = [
            'status'  => $status,
            'total'   => $count,
            'betslip' => $betslip,
        ];

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        $response->setContent(json_encode($data));

        return $response;
    }


    public function betslipAction()
    {
        $reference_id = $this->reference();

	if(!$this->session->has("shopbetslip")) {
           $this->session->set('shopbetslip', []);
        }

        $betslip = $this->session->get('shopbetslip');
        $newslip = [];
        foreach($betslip as $match_id => $slip){
            $sub_type_id = $slip['sub_type_id'];
            $parent_match_id = $slip['parent_match_id'];
            $special_bet_value = $slip['special_bet_value'];
            $bet_pick = $slip['bet_pick'];
    
            $data = $this->rawQueries("select odd_value from event_odd where "
                . "parent_match_id='$parent_match_id' and sub_type_id='$sub_type_id' "
                . " and special_bet_value='$special_bet_value' and odd_key='$bet_pick'");
            $new_odd = $data[0]['odd_value'];
            $slip['odd_value'] = $new_odd;
            $newslip[$match_id] = $slip;
        }
        //die(print_r($newslip, 1));
        $betslip = $newslip;
        $this->session->set('shopbetslip', $betslip);

        $referenceID = $this->reference();

        $betslipSession = $this->session->get("shopbetslip");

        $this->view->setVars([
            "betslip"        => $betslip,
            'betslipSession' => $betslipSession,
            "referenceID"    => $referenceID,
        ]);

        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);

        $this->view->pick("partials/shop/betslip");
    }


    public function removeAction()
    {
        $match_id = $this->request->getPost('match_id', 'int');
        $betslip = $this->session->get("shopbetslip");

        unset($betslip["$match_id"]);

        $this->session->set("shopbetslip", $betslip);

        $this->flashSession->error($this->flashSuccess('Match successfully removed'));
        $this->response->redirect('/shop/betslip');
        
        // Disable the view to avoid rendering
        $this->view->disable();
    }


    public function clearslipAction()
    {

        $this->session->remove("shopbetslip");

        $data = '1';

        $src = $this->request->getPost('src', 'string');
        $this->flashSession->error($this->flashSuccess('Betslip cleared'));
        $this->response->redirect('shop/betslip');
        // Disable the view to avoid rendering
        $this->view->disable();

    }


    public function competitionAction()
    {
        $id = $this->request->get('id', 'int');
        $sport_id = $this->request->get('sp', 'int');

        $keyword = $this->request->getPost('keyword', 'string');
        $bindings = [];
        if(!empty($id)) $bindings['competition_id'] = $id;
        if(!empty($sport_id)) $bindings['sport_id'] = $sport_id;
               
        $where = "";
        if(!empty($id)) $where .= " and c.competition_id=:competition_id ";
        if(!empty($sport_id)) $where .= " and c.sport_id = :sport_id ";
    
        if(!empty($keyword)){
            $bindings['keyword'] = "%$keyword%";
            $where .= " and (game_id like :keyword or home_team like :keyword or away_team like :keyword or competition_name like :keyword) ";
        }

        $this->session->set("previous_address", $_SERVER['REQUEST_URI']);
        
        $sCompetitions = $this->getCompetitions();
        list($page, $limit, $skip) = $this->getPaginationParams();

        $matches = $this->rawQueries("SELECT c.priority, (SELECT count(DISTINCT e.sub_type_id)
         FROM event_odd e INNER JOIN odd_type o ON (o.sub_type_id = e.sub_type_id AND 
         o.parent_match_id = e.parent_match_id) WHERE e.sub_type_id not in (select sub_type_id from prematch_disabled_market where status = 1) and e.parent_match_id = m.parent_match_id
          AND o.active = 1) AS side_bets, o.sub_type_id, 
          MAX(CASE WHEN o.odd_key = m.home_team THEN odd_value END) AS home_odd,
           MAX(CASE WHEN o.odd_key = 'draw' THEN odd_value END) AS neutral_odd,
            MAX(CASE WHEN o.odd_key = m.away_team THEN odd_value END) AS away_odd,
             MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', m.home_team, 'or',  'draw') 
             THEN odd_value END) AS double_chance_1x_odd, 
             MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', 'draw ' 'or',  m.away_team) THEN odd_value END)
              AS double_chance_x2_odd, MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', m.home_team, 'or',  
              m.away_team) THEN odd_value END) AS double_chance_12_odd, MAX(CASE WHEN o.odd_key =
               'under 2.5' THEN odd_value END) AS under_25_odd, MAX(CASE WHEN o.odd_key = 'over 2.5'
                THEN odd_value END) AS over_25_odd, m.game_id, m.match_id, m.start_time,
                 m.away_team, m.home_team, m.parent_match_id,c.competition_name,c.category 
                 FROM `match` m INNER JOIN event_odd o ON m.parent_match_id = o.parent_match_id
                  INNER JOIN competition c ON c.competition_id = m.competition_id INNER JOIN sport s
                   ON s.sport_id = c.sport_id WHERE m.start_time > now() AND m.start_time 
                   < now() + interval 1 month AND o.sub_type_id IN (1,10,18) AND m.status <> 3  
                   $where GROUP BY m.parent_match_id ORDER BY m.priority DESC, c.priority DESC , 
                   m.start_time LIMIT $skip,$limit", $bindings);

        $items = $this->rawQueries("select count(*) as total FROM `match` m INNER JOIN event_odd o ON m.parent_match_id = o.parent_match_id INNER JOIN competition c ON c.competition_id = m.competition_id INNER JOIN sport s ON s.sport_id = c.sport_id WHERE m.start_time > now() AND o.sub_type_id = 1 AND m.status <> 3  $where ", $bindings);

        $total = $items['0']['total'];

        $results = [];
        foreach ($matches as $match) {
            $results[(new DateTime($match['start_time']))->format($this->getDefaultDateFormat())][] = $match;
        }

        $theBetslip = $this->session->get("shopbetslip");

        if(!empty($id)) {

            $theCompetition = $this->rawQueries("SELECT competition_name,competition_id,category, sport_name FROM competition inner join sport using(sport_id) WHERE competition_id=? LIMIT 1", [$id]);
            $title = $theCompetition['0']['sport_name'].' - '.$theCompetition['0']['competition_name'] . ", " . $theCompetition['0']['category'];
        }else{

            $theCompetition = $this->rawQueries("SELECT sport_name FROM sport WHERE sport_id=?", [$sport_id]);
            $title = $theCompetition['0']['sport_name'].' - '.'All Competitions';
        }
        $this->tag->setTitle($title);

        $this->view->setVars([
            'today'          => $results,
            'sCompetitions'  => $this->getCompetitions(),
            'theCompetition' => $theCompetition,
            'theBetslip'     => $theBetslip,
            'pages'          => $this->getResultPages(0, $limit),
            'total'          => $total,
            'topSports'      => $this->topSports(),
            'sportId' =>$sport_id,
            'keyword' =>$keyword,
            'title' => $title,
            'competitionId'=>$id,
        ]);

        $this->view->pick("shop/threeway");
    }


    public function twowayAction()
    {
        $id = $this->request->get('id', 'int');

        $this->session->set("previous_address", $_SERVER['REQUEST_URI']);

        $sport_id = $this->request->get('sp', 'int');
        if(empty($sport_id)) $sport_id = 80;

        $keyword = $this->request->getPost('keyword', 'string');
        list($page, $limit, $skip) = $this->getPaginationParams();

        $bindings = [];
        if(!empty($id)) $bindings['competition_id'] = $id;
        if(!empty($sport_id)) $bindings['sport_id'] = $sport_id;

        $where = "";
        if(!empty($id)) $where .= " and c.competition_id=:competition_id ";
        if(!empty($sport_id)) $where .= " and c.sport_id = :sport_id ";

    
        if(!empty($keyword)){
            $bindings['keyword'] = "%$keyword%";
            $where .= " and (game_id like :keyword or home_team like :keyword or away_team like :keyword or competition_name like :keyword) ";
        }

        $allowed_types = ['186' =>[
            'market'=>'Winner',
            'display' => '1, 2' ]
        ];

        if(array_key_exists($sport_id,
            $this->view->defaultDisplayIds)){
            $allowed_types = $this->view->defaultDisplayIds[$sport_id] ;
        }
        $types = implode(",", array_keys($allowed_types));
       
        $matches = $this->rawQueries("select c.priority, (select count(distinct e.sub_type_id) from event_odd e inner join odd_type o on o.sub_type_id = e.sub_type_id where o.parent_match_id = m.parent_match_id and o.active = 1) as side_bets, o.sub_type_id, 
            MAX(CASE WHEN o.odd_key=m.home_team  and special_bet_value='' THEN odd_value END) AS home_odd,  
            MAX(CASE WHEN o.odd_key = m.away_team and special_bet_value='' THEN odd_value END) AS away_odd, 
            MAX(CASE WHEN o.odd_key = m.home_team and special_bet_value=1 THEN odd_value END) AS 1st_set_winner1, 
            MAX(CASE WHEN o.odd_key = m.away_team and special_bet_value=1 THEN odd_value END) AS 1st_set_winner2, 
             MAX(CASE WHEN o.odd_key = m.home_team and special_bet_value=2 THEN odd_value END) AS 2st_set_winner1, 
            MAX(CASE WHEN o.odd_key = m.away_team and special_bet_value=2 THEN odd_value END) AS 2st_set_winner2, 
            m.game_id, m.match_id, m.start_time, m.away_team, m.home_team, m.parent_match_id,c.competition_name,
            c.category from `match` m inner join event_odd o on m.parent_match_id = o.parent_match_id 
            inner join competition c on c.competition_id = m.competition_id inner join sport s on 
            s.sport_id = c.sport_id where m.start_time > now() and o.sub_type_id in ($types) and 
            m.status <> 3 $where group by m.parent_match_id order by m.priority desc, c.priority desc, 
            m.start_time limit 0, 20", $bindings);

        $items = $this->rawQueries("select count(*) as total from `match` m inner join event_odd o on
         m.parent_match_id = o.parent_match_id inner join competition c on c.competition_id = 
         m.competition_id inner join sport s on s.sport_id = c.sport_id where m.start_time > 
         now() and o.sub_type_id = 186 and m.status <> 3 $where ", $bindings);

        $total = $items['0']['total'];

        $theBetslip = $this->session->get("shopbetslip");

        if(!empty($id)) {

            $theCompetition = $this->rawQueries("SELECT competition_name,competition_id,category, sport_name FROM competition inner join sport using(sport_id) WHERE competition_id=? LIMIT 1", [$id]);
            $title = $theCompetition['0']['sport_name'].' - '.$theCompetition['0']['competition_name'] . ", " . $theCompetition['0']['category'];
        }else{

            $theCompetition = $this->rawQueries("SELECT sport_name FROM sport WHERE sport_id=?", [$sport_id]);
            $title = $theCompetition['0']['sport_name'].' - '.'All Competitions';
        }

        $this->tag->setTitle($title);

        $results = [];


        foreach ($matches as $day) {
            $results[(new DateTime($day['start_time']))->format($this->getDefaultDateFormat())][] = $day;
        }

        $vars = [
                'today'         => $results,
                'theBetslip'    => $theBetslip,
                'sCompetitions' =>$this->getCompetitions(),
                'sCompetition' => $theCompetition,
                'total'         => $total,
                'pages'         => $this->getResultPages($total, $limit),
                'page'          => $page,
                'topSports'      => $this->topSports(),
                'sportId' =>$sport_id,
                'keyword' =>$keyword,
                'title' => $title,
                'competitionId'=>$id,
                
            ];

        $this->view->setVars($vars);
        
       
        $this->view->pick("shop/twoway");
    }  

    public function printbetAction()
    {
        $betid = $this->request->get('id', 'int');
        $betDetails = $this->rawQueries("SELECT total_odd,bet_amount,tax, raw_possible_win,
            possible_win FROM bet WHERE bet_id=? LIMIT 1", [$betid]);

        $betslipDetails = $this->rawQueries("SELECT m.home_team, m.away_team, bs.bet_pick,
         bs.odd_value from bet_slip bs inner join `match` m using(parent_match_id) WHERE 
         bet_id=?", [$betid]);

        $vars = [
            'bet_id'=>$betid,
            'betDetails'=>$betDetails,
            'betslipDetails'=>$betslipDetails
        ];

        $this->view->setVars($vars);
        $this->view->pick("shop/printbet");
    }


    public function fixturesAction()
    {
        $fixturesDetails = $this->rawQueries("SELECT  m.game_id,m.home_team as HOME_TEAM, 
            m.away_team as AWAY_TEAM, CONCAT(m.home_team,'  vs ',m.away_team) as 
            GAME,REPLACE(c.competition_name,',',' ') as COMPETITION,REPLACE(c.category,',',' ') 
            as COUNTRY, m.start_time as KICK_OFF, MAX(CASE WHEN o.odd_key = m.home_team THEN 
            odd_value END) AS HOME,MAX(CASE WHEN o.odd_key = 'draw' THEN odd_value END)  AS 
            DRAW,MAX(CASE WHEN o.odd_key = m.away_team THEN odd_value END) AS AWAY_ODDS,
            MAX(CASE WHEN o.odd_key = 'yes' THEN odd_value END) AS GG, MAX(CASE WHEN 
            o.odd_key = 'no' THEN odd_value END) AS NG, MAX(CASE WHEN o.odd_key = 'under 0.5' 
            THEN odd_value END) AS UNDER_05, MAX(CASE WHEN o.odd_key = 'over 0.5' THEN odd_value 
            END) AS OVER_05, MAX(CASE WHEN o.odd_key = 'under 1.5' THEN odd_value END) AS 
            UNDER_15, MAX(CASE WHEN  o.odd_key = 'over 1.5' THEN odd_value END) AS OVER_15, 
            MAX(CASE WHEN o.odd_key = 'under 2.5' THEN odd_value END) AS UNDER_25, 
            MAX(CASE WHEN o.odd_key = 'over 2.5' THEN odd_value END) AS OVER_25,MAX(CASE WHEN 
            o.odd_key='over 3.5' THEN odd_value END) as OVER_35,MAX(CASE WHEN o.odd_key='under 3.5' 
            THEN odd_value END) as UNDER_35,MAX(CASE WHEN o.odd_key=concat(home_team, ' or draw') 
            THEN odd_value END) as DC1X,MAX(CASE WHEN o.odd_key=concat('draw or ', away_team) THEN 
            odd_value END) as DCX2, MAX(CASE WHEN o.odd_key=concat(home_team, ' or ', away_team) 
            THEN odd_value END) as DC12 FROM `match` m INNER JOIN event_odd o ON m.parent_match_id 
            = o.parent_match_id INNER  JOIN competition c ON c.competition_id = m.competition_id 
            INNER JOIN sport s ON s.sport_id = c.sport_id WHERE s.sport_id = 79 AND o.sub_type_id 
            IN (1,29,18,10) AND m.status <> 3 AND date(m.bet_closure)=curdate() GROUP BY 
            m.parent_match_id ORDER BY m.start_time ASC, m.priority DESC, c.priority DESC");

        $vars = [
            'fixturesDetails'=>$fixturesDetails
        ];

        $this->view->setVars($vars);
        $this->view->pick("shop/fixtures");
    }


    public function placebetAction()
    {
        $user_id = $this->request->getPost('user_id', 'int');
        $bet_amount = $this->request->getPost('bet_amount', 'float');
        $total_odd = $this->request->getPost('total_odd', 'float');
        $possible_win = $bet_amount * $total_odd;
        $src = $this->request->getPost('src', 'string') ?: 'internet';
        $betslip = $this->session->get('shopbetslip');
        $endCustomerIP = $this->getClientIP();
        $account = $this->request->getPost('account', 'int', 1);

        if ($account !== 1) {
            $account = 0;
        }

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        if(!$user_id){

            if ($src == 'mobile') {
                $this->flashSession->error($this->flashMessages('Please login in to place your bet'));
                $this->response->redirect('/shop/login');
                $this->view->disable();
                return;
            }else{

                $data = [
                        "status_code" => 421,
                        "message"     => "Please login to place your bet",
                    ];

                $response->setContent(json_encode($data));
                return $response;
                $this->view->disable();

            }
        }

        if ($bet_amount == 0 || strlen($bet_amount) == 0 || $bet_amount == null) {
            $this->flashSession->error($this->flashSession->error('Please specify your bet amount.'));
            if($src == 'mobile'){

                $this->response->redirect('/shop/betslip');
            }
            $this->view->disable();
        } else {

            if ($bet_amount < 40) {
                if ($src == 'mobile') {
                    $this->flashSession->error($this->flashMessages('Bet amount should be atleast Ksh. 40'));
                    $this->response->redirect('/shop/betslip');
                    // Disable the view to avoid rendering
                    $this->view->disable();
                } else {
                    $data = [
                        "status_code" => 421,
                        "message"     => "Bet amount should be atleast Ksh. 40",
                    ];
                    $response->setContent(json_encode($data));

                    return $response;
                    $this->view->disable();
                }
            } else {
                $phql = "SELECT * from Profile where profile_id='$user_id' limit 1";
                $checkUser = $this->modelsManager->executeQuery($phql);

                $checkUser = $checkUser->toArray();

                $mobile = $checkUser['0']['msisdn'];

                $totalMatch = sizeof($betslip);

                $slip = [];

                foreach ($betslip as $match) {
                    
                    $parent_match_id = $match['parent_match_id'];

                    if(!$parent_match_id){

                        continue;
                    }
                    $bet_pick = $match['bet_pick'];
                    $odd_value = $match['odd_value'];
                    $sub_type_id = $match['sub_type_id'];
                    $home_team = $match['home_team'];
                    $away_team = $match['away_team'];
                    $special_bet_value = $match['special_bet_value'];

                    $bet_type = $match['bet_type'] == 'live'?1:0;

                    $thisMatch = [
                        "sub_type_id"       => $sub_type_id,
                        "special_bet_value" => $special_bet_value,
                        "pick_key"          => $bet_pick,
                        "bet_type"          => $bet_type,
                        "odd_value"         => $odd_value,
                        "parent_match_id"   => $parent_match_id,
                    ];

                    array_push($slip, $thisMatch);
                }

                $bet = [
                    "bet_string"     => 'sms',
                    "app_name"       => "LITE",
                    "possible_win"   => $possible_win,
                    "profile_id"     => $user_id,
                    "stake_amount"   => $bet_amount,
                    "bet_total_odds" => $total_odd,
                    "deviceID"       => "6489000GX",
                    "endCustomerIP"  => $endCustomerIP,
                    "channelID"      => $src,
                    "slip"           => $slip,
                    "account"        => $account,
                    'msisdn'         => $mobile,
                ];

                // die(print_r($bet, 1));

                $placeB = $this->bet($bet);

                if ($src == 'mobile') {
                    $feedback = $placeB['message'];
                    if ($placeB['status_code'] == 201) {
                        $feedback = $placeB['message'];
                        $this->session->remove("shopbetslip");
                        $this->flashSession->success($this->flashSuccess($feedback));
                    } else {
                        $this->flashSession->error($this->flashError($feedback));
                    }

                    $this->response->redirect('/shop/betslip');
                    // Disable the view to avoid rendering
                    $this->view->disable();

                } else {
                    if ($placeB['status_code'] == 201) {
                        $this->deleteReference();
                        $feedback = $placeB['message'];
                        $matches = explode(" ", $feedback);
                        preg_match("/\d*/", $matches[4], $matchedID, PREG_OFFSET_CAPTURE);
                        $placeB["bet_id"] = $matchedID[0][0];
                        
                        $this->session->remove("shopbetslip");
                        $this->reference();
                    }

                    $response->setContent(json_encode($placeB));

                    return $response;
                    $this->view->disable();
                }
            }
        }

    }


    public function mybetsAction()
    {
        
        $betID = $this->request->get('id','int');
       
        if(!is_numeric($betID)){
            $betID = 0;
        }
          

        $id = $this->session->get('auth')['id'];

        if($id == null){

            $this->response->redirect('shop/login');
            $this->view->disable();
            return;
        }

        $myBets = $this->rawQueries("select b.bet_id,b.created, jb.jackpot_bet_id, total_games
         as total_matches, b.bet_message,b.bet_amount,b.possible_win,b.status, sp.status as sp_status, 
         if(FIND_IN_SET(b.status,(select group_concat(status) from bet_slip where bet_id = 
         b.bet_id)), b.status, 1) xstatus from bet b inner join bet_slip s on b.bet_id=s.bet_id
          left join jackpot_bet jb on jb.bet_id=b.bet_id left join shop_bet sp on 
          b.bet_id = sp.bet_id where b.profile_id=$id group by s.bet_id order by b.created desc
           limit 50;");
          
        $title = "My Bets";

        $this->tag->setTitle($title);

        $this->view->setVars(["myBets"=>$myBets]);

        if ($betID) {

           $myBet = $this->rawQueries("select b.bet_id,b.created, total_games as total_matches,
            b.bet_message,b.bet_amount,b.possible_win,b.status, b.profile_id, sp.status as sp_status,
            b.revised_possible_win, b.is_void, if(FIND_IN_SET(b.status,(select 
            group_concat(status) from bet_slip where bet_id = b.bet_id)), b.status, 1) 
            xstatus from bet b inner join bet_slip s on b.bet_id=s.bet_id left join shop_bet sp 
            on b.bet_id = sp.bet_id where b.bet_id='$betID' group by s.bet_id order by 
            b.created desc limit 40");

           $myBet = $myBet['0'];

           if ($myBet['profile_id']==$id) {

            $betDetails = $this->rawQueries("select b.bet_id, b.created, s.live_bet, 
                        m.start_time, b.bet_amount, possible_win, b.win, b.status, m.game_id, 
                        m.away_team, (select winning_outcome from outcome where sub_type_id=45 
                        and parent_match_id=s.parent_match_id and is_winning_outcome =1 and 
                        s.live_bet=outcome.live_bet) as ft_score, m.home_team, s.odd_value, 
                        s.sub_type_id, s.bet_pick, s.void_factor, group_concat(o.winning_outcome) 
                        as winning_outcome, concat(t.name,' ',s.special_bet_value) as bet_type 
                        from bet b inner join bet_slip s on s.bet_id = b.bet_id inner join `match` 
                        m on m.parent_match_id = s.parent_match_id inner join odd_type t on 
                        (t.parent_match_id = s.parent_match_id and s.sub_type_id=t.sub_type_id) 
                        left join outcome o on o.parent_match_id = s.parent_match_id and 
                        s.sub_type_id = o.sub_type_id and s.special_bet_value = o.special_bet_value 
                        and s.live_bet = o.live_bet and o.is_winning_outcome = 1 where s.bet_id = 
                        '$betID' group by s.bet_slip_id, s.special_bet_value order by b.bet_id desc");

            $this->view->setVars(["betDetails"=>$betDetails,'myBet'=>$myBet]);
            $this->view->pick("shop/mybets/details");
          }else{
            $this->flashSession->error($this->flashMessages('You do not have permission to view this page'));
            $this->response->redirect('/shop/mybets');
                        // Disable the view to avoid rendering
            $this->view->disable();
          }
          
        }else{

            $this->view->pick("shop/mybets/mybets");
        }
    }


    public function searchbetAction(){

        $bet_id = $this->request->getPost('bet_id', 'int');
        $id = $this->session->get('auth')['id'];

        if($id == null){

            $this->response->redirect('shop/login');
            $this->view->disable();
            return;
        }

        $myBets = $this->rawQueries("select b.bet_id,b.created, jb.jackpot_bet_id, total_games
         as total_matches, b.bet_message,b.bet_amount,b.possible_win,b.status,sp.status as sp_status,
         if(FIND_IN_SET(b.status,(select group_concat(status) from bet_slip where bet_id = 
         b.bet_id)), b.status, 1) xstatus from bet b inner join bet_slip s on b.bet_id=s.bet_id
         left join jackpot_bet jb on jb.bet_id=b.bet_id left join shop_bet sp 
         on b.bet_id = sp.bet_id where b.bet_id=$bet_id and b.profile_id=$id 
         group by s.bet_id order by b.created desc limit 50;");
          
        $title = "My Bets";

        $this->tag->setTitle($title);

        $this->view->setVars(["myBets"=>$myBets]);

        $this->view->pick("shop/mybets/mybets");
    }


    public function paybetAction(){

        $bet_id = $this->request->getPost('bet_id', 'int');
        $id = $this->session->get('auth')['id'];

        if($id == null){

            $this->response->redirect('shop/login');
            $this->view->disable();
            return;
        }

        $phql = "INSERT INTO ShopBet VALUES(null,$bet_id,50,now())";
        $affected = $this->modelsManager->executeQuery($phql);

        //$affected = $this->rawQueries("insert into shop_bet values(null,$bet_id,50,now())");

        if($affected){

            $data = [
                "status_code" => 201,
                "message"     => "Bet successfully indicated as Paid. Thank you.",
            ];
        }else{

            $data = [
                "status_code" => 421,
                "message"     => "Bet Payment for this Bet Failed! Please try again.
                 If problem persists, please contact Bet Palace Support.",
            ];
        }

        $response = new Response();
        $response->setContent(json_encode($data));
        return $response;
        $this->view->disable();

    }


    public function cancelbetAction()
    {
        $id = $this->request->get('id', 'int');
        $profile_id = $this->session->get('auth')['id'];

        $myBet = $this->rawQueries("SELECT b.bet_id,b.created, total_games AS total_matches,
         b.bet_message,b.bet_amount,b.possible_win,b.status, b.profile_id, 
         if(FIND_IN_SET(b.status,(SELECT group_concat(status) FROM bet_slip WHERE 
         bet_id = b.bet_id)), b.status, 1) xstatus FROM bet b INNER JOIN bet_slip s
          ON b.bet_id=s.bet_id WHERE b.bet_id=? GROUP BY s.bet_id ORDER BY b.created
           DESC LIMIT 40", [$id]);

        $myBet = $myBet['0'];

        if ($myBet['profile_id'] != $profile_id) {
            $this->response->redirect('/shop/mybets/mybets');
            $this->view->disable();
        } else {
            $this->view->setVars(["myBet" => $myBet]);
            $this->view->pick("shop/mybets/cancel");
        }
    }

    public function cancelAction()
    {
        $bet_id = $this->request->getPost('bet_id', 'int');
        $redr = '/shop/mybets?id=' . $bet_id;

        if (!$bet_id) {
            $this->flashSession->error($this->flashMessages('Bet ID not found'));

            return $this->response->redirect($redr);
            $this->view->disable();
        } else {
            $mobile = $this->session->get('auth')['mobile'];
            $profile_id = $this->session->get('auth')['id'];

            $data = ['bet_id'      => $bet_id,
                     'profile_id'  => $profile_id,
                     'cancel_code' => '101',
                     'msisdn'      => $mobile,
            ];

            $exp = time() + 3600;

            $token = $this->generateToken($data, $exp);

            $transaction = "token=$token";

            $cancelbet = $this->cancelBet($transaction);

            $this->flashSession->error($this->flashSuccess($cancelbet['message']));

            return $this->response->redirect($redr);
            $this->view->disable();
        }
    }


    public function withdrawAction()
    {

        $this->tag->setTitle("Withdraw - BetPalace");

        $shopLoggedIn = $this->session->get('auth')['id'];
        $theBetslip = $this->session->get("shopbetslip");

        if($shopLoggedIn == null){

            $this->response->redirect('shop/login');
            $this->view->disable();
        }

        $this->view->setVars([
            'theBetslip'    => $theBetslip,
        ]);

        $this->view->pick("shop/withdraw");
    }

    public function withdrawalAction()
    {
        $amount = $this->request->getPost('amount', 'int');

        if ($amount<50) {
           $this->flashSession->error($this->flashError('Sorry, minimum withdraw amount is Ksh. 50.'));
            return $this->response->redirect('/shop/withdraw');
            $this->view->disable();
        }elseif ($amount>70000) {
            $this->flashSession->error($this->flashError('Sorry, maximum withdraw amount is Ksh. 70,000.'));
            return $this->response->redirect('/shop/withdraw');
            $this->view->disable();
        }else{
                $mobile = $this->session->get('auth')['mobile'];

                if($mobile == null){

                    $this->flashSession->error($this->flashError('You are not logged in. Please login to withdraw.'));
                    return $this->response->redirect('/shop/withdraw');
                    $this->view->disable();
                }
        
                $data = ['amount'=>$amount,'msisdn'=>$mobile];
                $exp=time()+3600;
        
                $token = $this->generateToken($data, $exp);
                $transaction = "token=$token";
                $withdraw = $this->withdraw($transaction);
        
                $this->flashSession->error($this->flashSuccess('Your withdrawal of '.$amount.' to '.$mobile.'.is being processed, you will receive a confirmation on SMS shortly'));
                $this->response->redirect('/shop/withdraw');
        
                $this->view->disable();
           }
    }


    public function pnlAction()
    {
        
        $dateFrom = $this->request->get('date_from','string');
        $dateTo = $this->request->get('date_to','string');

        $id = $this->session->get('auth')['id'];

        if($id == null){

            $this->response->redirect('shop/login');
            $this->view->disable();
            return;
        }

        if(strlen(trim($dateFrom)) == 0 && strlen(trim($dateTo)) == 0){

            $this->view->setVars(["pnl"=>[]]);
            $this->view->pick("shop/pnl");
            return;
        }

        $pnl = $this->rawQueries("SELECT date(b.created) as dc, count(*) AS num,SUM(b.bet_amount)
         as bet_amount, SUM(IF(b.status=5,b.raw_possible_win,0))   AS won_possible_win,
         ((SUM(b.bet_amount))-(SUM(IF(b.status=5,b.raw_possible_win,0)))) as pnl, 
         ((SUM(IF(b.status=3,b.bet_amount,0))  + 
         SUM(IF(b.status=5,b.bet_amount,0)))/SUM(b.bet_amount)) *100 as centage FROM bet b 
         LEFT JOIN bonus_bet bb ON b.bet_id=bb.bet_id WHERE 1=1 AND b.profile_id = $id AND 
         b.created >= '$dateFrom' and  b.created < '$dateTo' GROUP BY dc DESC;");
          
        $title = "P&L Report";

        $this->tag->setTitle($title);

        $this->view->setVars(["pnl"=>$pnl]);

        $this->view->pick("shop/pnl");
    }

}
