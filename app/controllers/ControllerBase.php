<?php
/**
 * Copyright (c) Antony 2018.
 *
 * All rights reserved.
 */

use Phalcon\Mvc\Controller;
use Phalcon\Crypt;
use \Firebase\JWT\JWT;

/**
 * Class ControllerBase
 */
class ControllerBase extends Controller
{
    use SMS;
    /**
     *
     */
    const JWT_KEY = "m1XR6ajgepqyh^7&21012G$%_q90)hte====";

    /**
     * @param $dispatcher
     */
    public function beforeExecuteRoute($dispatcher)
    {
        if ($this->cookies->has('auth')) {
            $token = $this->cookies->get('auth');
            if (!$this->session->has("auth")) {
                try {
                    $user = JWT::decode($token, self::JWT_KEY, ['HS256']);
                    $user = $user->user;
                    if ($user->remember == '1' || $user->device == '1') {
                        $user = [
                            'id'       => $user->id,
                            'name'     => property_exists($user, 'name') ? $user->name: "",
                            'mobile'   => $user->mobile,
                            'device'   => $user->device,
                            'remember' => $user->remember,
                        ];
                        $this->_registerSession($user);
                    }

                } catch (Exception $e) {
                    $decoded = $e->getMessage();
                }

            }
        }

        // FIXME: Please add a sport type for any new game.
        // FIXME: Failure to which, u'll get a beautiful undefined offset error (at some point)

        $sportType = [
            '79' => 'competition',
            '85' => 'twoway',
            '97' => 'competition',
            '84' => 'twoway',
            '82' => 'competition',
            '83' => 'competition',
            '86' => 'competition',
            '96' => 'competition',
            '98' => 'twoway',
            '99' => 'competition',
            '100' => 'competition',
            '102' => 'competition',
            '104' => 'competition',
            '105' => 'competition',
            '106' => 'competition',
            '107' => 'competition',
            '108' => 'competition',
            '109' => 'competition',
            '162' => 'competition',
            '163' => 'competition',
            '164' => 'competition',
            '165' => 'twoway',
            '166' => 'competition',
            '167' => 'competition',
            '168' => 'competition',
            '169' => 'competition',
            '80' => 'twoway',
            '239'=> 'competition',
        ];

        $default_display_ids = [
            '79' => ['1' =>  ['market'=>'3 Way','display'=>'HOME, DRAW, AWAY'],
                '10' => ['market' => 'Double Chance','display'=>'1 OR X, X OR 2, 1 OR 2'],
                '18' => ['market'=>'Over/Under 2.5', 'display'=>'OVER, UNDER'],
                ],
            '85' => ['219' =>['market'=>'Winner (incl. overtime)',
                        'display' => '1, 2' ],],
            '162' => ['219' =>['market'=>'Winner (incl. overtime)','display' => '1, 2' ],],
            '84' => ['186' =>['market'=>'Winner', 'display' => '1, 2' ], ],
            '99' => ['186' =>['market'=>'Winner','display' => '1, 2' ], ],
            '165' => ['186' =>['market'=>'Winner','display' => '1, 2' ], ],
            '80' => ['186' =>['market'=>'Winner','display' => '1, 2' ],],
            '98' => ['340' =>['market'=>'Winner (incl. super over)','display' => '1, 2' ],],

            ];

        $betslip = $this->session->get('betslip');
        $slipCount = sizeof($betslip);

        $sports = $this->sports();

        $refURL = 'http://'. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

        // $winners = $this->topWinners();

        $this->view->setVars([
            'sports'    => $sports,
            'slipCount' => $slipCount,
            'sportType' => $sportType,
            'refURL'    => $refURL,
            'betslip'   => $betslip,
            'defaultDisplayIds'=>$default_display_ids
        ]);

    }

    protected function topSports(){
        return array(
            '79'=>'Soccer', 
            '85'=>'Basketball', 
            '97'=>'Rugby',
            '98'=>'Cricket',
            '80' =>'Tennis', 
            '84' =>'Volleyball', 
            '82' =>'Ice Hockey',
            '107' =>'Baseball',
            '163' =>'Field Hockey',
            '165' =>'Football'
        );

    }

    /**
     * @return mixed
     */
    protected function sports() {
        #$sports = $this->rawQueries("select ux.ux_id, ux.sport_name, if(LENGTH(ux.category) <17, ux.category, concat(SUBSTRING(ux.category, 1,15), '...')) as category, if(LENGTH(ux.competition_name) <17, ux.competition_name, concat(SUBSTRING(ux.competition_name, 1,15), '...')) as competition_name, ux.sport_id, ux.category_id, ux.competition_id, country_code, (select count(*) from `match` m where competition_id = ux.competition_id and m.bet_closure > now() and m.status=1)as games, (select count(*) from `match` mm where competition_id in (select competition_id from competition cc where category_id=ux.category_id and mm.bet_closure > now() and mm.status=1 ))as cat_games from ux_categories ux inner join category c on c.category_id = ux.category_id having games > 0 order by ux.sport_id asc, ux.category asc");
        
        $sports = $this->rawQueries("select ux.ux_id, ux.sport_name, if(LENGTH(ux.category) <17, ux.category, concat(SUBSTRING(ux.category, 1,15), '...')) as category, if(LENGTH(ux.competition_name) <17, ux.competition_name, concat(SUBSTRING(ux.competition_name, 1,15), '...')) as competition_name, ux.sport_id, ux.category_id, ux.competition_id, country_code from ux_categories ux inner join category c on c.category_id = ux.category_id order by ux.sport_id asc, ux.category asc ");
        return $sports;
    }

    /**
     * @return float
     */
    protected function getMicrotime()
    {
        list ($msec, $sec) = explode(" ", microtime());

        return ((float)$msec + (float)$sec);
    }

    /**
     * @return mixed
     */
    protected function topLeagues()
    {
        $leagues = $this->rawQueries('SELECT c.competition_id, c.competition_name, c.category FROM competition c INNER JOIN `match` m ON m.competition_id = c.competition_id WHERE c.sport_id = 79 AND m.start_time BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 DAY) GROUP BY m.competition_id ORDER BY c.priority DESC , c.competition_name LIMIT 10');

        return $leagues;
    }


    protected function topWinners(){
        $winners = $this->rawQueries("select bet.possible_win, bet.created, profile.msisdn from bet INNER JOIN profile ON bet.profile_id = profile.profile_id where date(bet.created) >= now()-interval 7 day and bet.status=5 and possible_win > 1000 order by possible_win DESC limit 50");
       return $winners;
    }

    /**
     * @return mixed
     */
    protected function allLeagues()
    {
        $leagues = $this->rawQueries('SELECT * FROM ux_categories WHERE sport_id = 79');

        return $leagues;
    }

    /**
     * @return mixed
     */
    protected function rugby()
    {
        $rugby = $this->rawQueries('SELECT category,competition_id, competition_name FROM competition WHERE sport_id=41 GROUP BY category ORDER BY category ASC');

        return $rugby;
    }

    /**
     * @param $statement
     *
     * @param array $bindings
     *
     * @return mixed
     */
    protected function rawQueries($statement, $bindings = [])
    {
        $connection = $this->di->getShared("db");
        $success = $connection->query($statement, $bindings);
        $success->setFetchMode(Phalcon\Db::FETCH_ASSOC);
        $success = $success->fetchAll($success);

        return $success;
    }

    /**
     * @param $statement
     *
     * @return mixed
     */
    protected function rawInsert($statement)
    {
        $connection = $this->di->getShared("db");
        $success = $connection->query($statement);

        $id = $connection->lastInsertId();

        return $id;
    }

    /**
     * @param $message
     *
     * @return string
     */
    protected function flashMessages($message)
    {
//        "<div class='alert pv alert-dismissible' role='alert'>
//                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>";
        return "$message";
    }

    /**
     * @param $message
     *
     * @return mixed
     */
    protected function flashSuccess($message)
    {
//        return "<div class='alert alert-success alert-dismissible' role='alert'>
//                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>$message</div>";
        return $message;
    }

    /**
     * @return mixed|\Phalcon\Http\Cookie|\Phalcon\Http\CookieInterface
     */
    protected function reference()
    {

        if (!$this->cookies->has('referenceID')) {

            $crypt = new Crypt();

            $key = 'GcYk9L5SPooUTE7p';
            $text = $_SERVER['HTTP_USER_AGENT'] . time() . uniqid();

            $key = $crypt->encryptBase64($text, $key);

            $this->cookies->set('referenceID', $key, time() + 15 * 86400);
        }

        $referenceID = $this->cookies->get('referenceID');

        $referenceID = $referenceID->getValue();

        return $referenceID;
    }

    /**
     * @param $user
     */
    protected function registerAuth($user)
    {
        $exp = time() + (3600 * 24 * 5);
        $token = $this->generateToken($user, $exp);
        $this->cookies->set('auth', $token, $exp);
        $this->_registerSession($user);
    }

    /**
     * @param $user
     */
    private function _registerSession($user)
    {
        $this->session->set('auth', $user);
    }

    /**
     * @param $data
     * @param $exp
     *
     * @return string
     */
    protected function generateToken($data, $exp)
    {
//        $key = "m1XR6ajgepqyh^7&21012G$%_q90)hte====";
        $token = [
            "iss"  => "http://betpalace.co.ke",
            "iat"  => 1356999524,
            "nbf"  => 1357000000,
            "exp"  => $exp,
            "user" => $data,
        ];

        return JWT::encode($token, self::JWT_KEY);
    }

    /**
     * @param $token
     *
     * @return object
     */
    protected function decodeToken($token)
    {
//        $key = "m1XR6ajgepqyh^7&21012G$%_q90)hte====";
        $decoded = JWT::decode($token, self::JWT_KEY, ['HS256']);

        return $decoded;
    }


    /**
     *
     */
    protected function deleteReference()
    {

        $this->cookies->set('referenceID');

    }


    /**
     * @return string
     */
    protected function getDevice()
    {
        $device = '2';
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|1000[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(1000|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|1000|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m1000\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(1000|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n1000(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|1000)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v71000|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
            $device = '1';
        }

        return $device;
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    protected function topup($data)
    {

        $URL = "35.233.126.93:9000/kibeti";

        $httpRequest = curl_init($URL);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, "$data");
        curl_setopt($httpRequest, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = json_decode($results);

        return $status_code;
    }

    /**
     * @param $transaction
     *
     * @return mixed
     */
    protected function withdraw($transaction)
    {
        $URL = "35.233.126.93:8008/macatm";

        $httpRequest = curl_init($URL);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, "$transaction");
        curl_setopt($httpRequest, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = json_decode($results);

        return $response;
    }

    /**
     * @param $transaction
     *
     * @return mixed
     */
    protected function bonus($transaction)
    {

        $URL = "35.233.126.93:8008/profilemgt";

        $httpRequest = curl_init($URL);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, "$transaction");
        curl_setopt($httpRequest, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = json_decode($results);

        return $status_code;
    }

    /**
     * @param $transaction
     *
     * @return array
     */
    protected function betTransaction($transaction)
    {
        $URL = "35.233.126.93:8008/bet";

        $httpRequest = curl_init($URL);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, "$transaction");
        curl_setopt($httpRequest, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = [
            "status_code" => $status_code,
            "message"     => $this->formatMessage($results),
        ];

        return $response;
    }

    /**
     * @param $data
     *
     * @return array
     */
    protected function bet($data)
    {
        $URL = "http://35.233.126.93:8008/bet";

        $bet = json_encode($data);

        $httpRequest = curl_init($URL);

        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, $bet);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($bet),
        ]);
        curl_setopt($httpRequest, CURLOPT_HTTPAUTH, CURLAUTH_ANY);

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = [
            "status_code" => $status_code,
            "message"     => $results,
        ];

        return $response;
    }


    /**
     * @param $data
     *
     * @return array
     */
    protected function peerBet($data)
    {
        $URL = "http://35.233.126.93:8008/bet";

        $bet = json_encode($data);

        $httpRequest = curl_init($URL);

        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, $bet);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($bet),
        ]);
        curl_setopt($httpRequest, CURLOPT_HTTPAUTH, CURLAUTH_ANY);

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = [
            "status_code" => $status_code,
            "message"     => $results,
        ];

        return $response;
    }

    /**
     * @param $data
     * @param $url
     *
     * @return array
     */
    protected function betJackpot($data, $url)
    {
        $bet = json_encode($data);

        $httpRequest = curl_init($url);

        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, $bet);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($bet),
        ]);
        curl_setopt($httpRequest, CURLOPT_HTTPAUTH, CURLAUTH_ANY);

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = [
            "status_code" => $status_code,
            "message"     => $this->formatMessage($results),
        ];

        return $response;
    }

    /**
     * @param $transaction
     *
     * @return array
     */
    protected function cancelBet($transaction)
    {

        $URL = "35.233.126.93:8008/bet_cancel";

        $httpRequest = curl_init($URL);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, "$transaction");
        curl_setopt($httpRequest, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = [
            'status'  => $status_code,
            'message' => $this->formatMessage($results),
        ];

        return $response;
    }


    /**
     * @param $number
     *
     * @return bool|string
     */
    protected function formatMobileNumber($number)
    {
        $regex = '/^(?:\+?(?:[1-9]{3})|0)?([6,7]([0-9]{8}))$/';
        if (preg_match_all($regex, $number, $capture)) {
            return '254' . $capture[1][0];
        }

        return false;
    }

    // Function to get the client IP address

    /**
     * @return array|false|string
     */
    function getClientIP()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

    /**
     * @param $keyword
     * @param $skip
     * @param $limit
     *
     * @param string $filter
     * @param string $orderBy
     *
     * @return array
     */
    /*
    protected function getGames($keyword, $skip, $limit, $filter = "", $orderBy = "priority desc, m_priority desc, start_time asc")
    {
        $where = "where start_time > now() " . $filter;

        if ($keyword) {
            
            $bindings = [
                'keyword' => "%$keyword%",
            ];
            $whereClause = "$where and (game_id like :keyword or home_team like :keyword or away_team like :keyword or competition_name like :keyword)";
            $query = "SELECT * FROM ux_todays_highlights $whereClause ORDER BY $orderBy LIMIT $skip,$limit";
            $today = $this->rawQueries($query, $bindings);
            $items = $this->rawQueries("select count(*) as total from ux_todays_highlights $whereClause limit 1", $bindings);
        } else {
            $whereClause = "$where ";
        
            $today = $this->rawQueries("SELECT * FROM ux_todays_highlights $whereClause ORDER BY $orderBy LIMIT $skip,$limit");
            $items = $this->rawQueries("select count(*) as total from ux_todays_highlights $whereClause limit 1");
        }

        $results = [];


        foreach ($today as $day) {
            $results[(new DateTime($day['start_time']))->format($this->getDefaultDateFormat())][] = $day;
        }

        return [
            $results,
            $items,
            $this->getCompetitions(),
        ];
    }*/


    protected function getGames($keyword, $skip, $limit, $filter = "", $orderBy = "start_time asc")
    {
        $search_where = "where start_time >= now() and date(start_time) <= date(now() + INTERVAL 1 MONTH) " . $filter;
        $where = "where start_time >= now() and date(start_time) <= date(now() + INTERVAL 1 MONTH) and home_odd is not null and under_25_odd is not null " . $filter;

        if ($keyword) {

            $bindings = [
                'keyword' => "%$keyword%",
            ];

            $whereClause = "$search_where and (game_id like :keyword or home_team like 
            :keyword or away_team like :keyword or competition_name like :keyword)";
            
            $query = "SELECT c.priority, (SELECT count(DISTINCT e.sub_type_id)
                        FROM event_odd e INNER JOIN odd_type o ON (o.sub_type_id = e.sub_type_id AND 
                        o.parent_match_id = e.parent_match_id) WHERE e.sub_type_id not in 
                        (select sub_type_id from prematch_disabled_market where status = 1) 
                        and e.parent_match_id = m.parent_match_id
                        AND o.active = 1) AS side_bets, o.sub_type_id, 
                        MAX(CASE WHEN o.odd_key = m.home_team THEN odd_value END) AS home_odd,
                        MAX(CASE WHEN o.odd_key = 'draw' THEN odd_value END) AS neutral_odd,
                        MAX(CASE WHEN o.odd_key = m.away_team THEN odd_value END) AS away_odd,
                        MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', m.home_team, 'or',  'draw') 
                        THEN odd_value END) AS double_chance_1x_odd, 
                        MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', 'draw ' 'or',  m.away_team) THEN odd_value END)
                        AS double_chance_x2_odd, MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', m.home_team, 'or',  
                        m.away_team) THEN odd_value END) AS double_chance_12_odd, MAX(CASE WHEN o.odd_key =
                        'under 2.5' THEN odd_value END) AS under_25_odd, MAX(CASE WHEN o.odd_key = 'over 2.5'
                        THEN odd_value END) AS over_25_odd, m.game_id, m.match_id, m.start_time,
                        m.away_team, m.home_team, m.parent_match_id,c.competition_name,c.category 
                        FROM `match` m INNER JOIN event_odd o ON m.parent_match_id = o.parent_match_id
                        INNER JOIN competition c ON c.competition_id = m.competition_id INNER JOIN sport s
                        ON s.sport_id = c.sport_id $whereClause  AND o.sub_type_id IN (1,10,18) AND m.status <> 3
                         GROUP BY m.parent_match_id ORDER BY m.priority DESC, c.priority DESC, m.start_time
                          LIMIT $skip,$limit";

            $items = $this->rawQueries("select count(*) as total FROM `match` m INNER JOIN
             event_odd o ON m.parent_match_id = o.parent_match_id INNER JOIN competition c
              ON c.competition_id = m.competition_id INNER JOIN sport s ON s.sport_id
               = c.sport_id $whereClause LIMIT 1", $bindings);

            $today = $this->rawQueries($query, $bindings);

        } else {
            
           $whereClause = "$where ";
        
            $today = $this->rawQueries("SELECT * FROM ux_todays_highlights s $whereClause ORDER BY $orderBy LIMIT $skip,$limit");
            $items = $this->rawQueries("select count(*) as total from ux_todays_highlights s $whereClause limit 1");
        }

        $results = [];

        foreach ($today as $day) {
            $results[(new DateTime($day['start_time']))->format($this->getDefaultDateFormat())][] = $day;
        }

        return [
            $results,
            $items,
            $this->getCompetitions(),
        ];
    }


    /**
     * @param $keyword
     * @param $skip
     * @param $limit
     *
     * @param string $filter
     * @param string $orderBy
     *
     * @return array
     */
    protected function getLiveGames($keyword, $skip, $limit, $filter = "",
        $orderBy = "m.priority desc, c.priority desc, m.start_time asc")
    {
        $where = "where m.status <> 3 AND m.start_time > now()-interval 4 hour AND o.sub_type_id =1 and m.active=1 " . $filter;

        //$where = "where m.status <> 3  AND o.sub_type_id =1 " . $filter;

       
        $whereClause = "$where ";


        $query ="SELECT c.priority, '' AS side_bets, o.sub_type_id, 
            group_concat(if(o.odd_key = m.home_team, o.odd_value, null)) as home_odd, 
            group_concat(if(o.odd_key = 'draw', o.odd_value, null)) as neutral_odd, 
            group_concat(if(o.odd_key = m.away_team, o.odd_value, null)) as away_odd,  
            group_concat(if(o.odd_key = m.home_team, o.active, null)) as home_odd_status, 
            group_concat(if(o.odd_key = 'draw', o.active, null)) as draw_odd_status, 
            group_concat(if(o.odd_key = m.away_team, o.active, null)) as away_odd_status, 
            o.market_active, m.match_id, m.start_time, m.away_team, m.home_team,
            m.parent_match_id, c.competition_name,c.category, m.match_time, 
            m.score, m.bet_status, m.match_status, m.event_status, m.home_red_card, 
            m.away_red_card, m.home_yellow_card, m.away_yellow_card, m.home_corner, 
            m.away_corner, m.active as event_live FROM `live_odds_change` o inner join
            live_match m on o.parent_match_id=m.parent_match_id INNER JOIN competition c 
            ON c.competition_id = m.competition_id INNER JOIN sport s 
            ON s.sport_id = c.sport_id where s.sport_id=79 AND m.event_status='Live' 
            AND m.active=1 GROUP BY m.parent_match_id,
            o.sub_type_id,o.special_bet_value ORDER BY $orderBy LIMIT 0,200";

        //die($query);

        $today = $this->rawQueries($query);

        $results = [];


        foreach ($today as $day) {
            $results[(new DateTime($day['start_time']))->format($this->getDefaultDateFormat())][] = $day;
        }

        return [
            $results,
            // $items,
            $this->getCompetitions(),
        ];
    }


    protected function getPeerGames($keyword, $date_put, $skip, $limit, $filter = "", $orderBy = "start_time asc")
    {
        $where = "where 1=1 " . $filter;
        $and = " and m.peer_priority IS NOT NULL and m.start_time > now() ";

        if ($date_put) {
            $bindings = [
                'date_put' => "$date_put",
            ];
            $whereClause = "$where and m.start_time >= :date_put";
            $query = "SELECT ux.sport_id, ux.competition_name, ux.m_priority, ux.priority, ux.side_bets, ux.sub_type_id, ux.home_odd, ux.neutral_odd, ux.away_odd, ux.double_chance_12_odd, ux.double_chance_x2_odd, ux.double_chance_1x_odd, ux.over_25_odd, ux.under_25_odd, ux.game_id, m.match_id,  m.parent_match_id, m.home_team, m.away_team, m.start_time, m.competition_id, m.status, m.peer_priority, b.bet_id, b.bet_pick FROM ux_todays_highlights ux INNER JOIN `match` m ON m.parent_match_id = ux.parent_match_id LEFT JOIN bet_slip b ON m.parent_match_id = b.parent_match_id $whereClause.$and.GROUP BY m.match_id ORDER BY $orderBy LIMIT 10";
            $today = $this->rawQueries($query, $bindings);
            $items = $this->rawQueries("select count(*) as total from ux_todays_highlights $whereClause limit 1", $bindings);
        } elseif ($keyword) {
            $bindings = [
                'keyword' => "%$keyword%",
            ];
            $whereClause = "$where and (game_id like :keyword or home_team like :keyword or away_team like :keyword or competition_name like :keyword)";
            $query = "SELECT ux.sport_id, ux.competition_name, ux.m_priority, ux.priority, ux.side_bets, ux.sub_type_id, ux.home_odd, ux.neutral_odd, ux.away_odd    , ux.double_chance_12_odd, ux.double_chance_x2_odd, ux.double_chance_1x_odd, ux.over_25_odd, ux.under_25_odd, ux.game_id, m.match_id,  m.parent_match_id, m.home_team, m.away_team, m.start_time, m.competition_id, m.status, m.peer_priority, b.bet_id, b.bet_pick FROM ux_todays_highlights ux INNER JOIN `match` m ON m.parent_match_id = ux.parent_match_id LEFT JOIN bet_slip b ON m.parent_match_id = b.parent_match_id $whereClause $and GROUP BY m.match_id ORDER BY $orderBy LIMIT 10";
            $today = $this->rawQueries($query, $bindings);
            $items = $this->rawQueries("select count(*) as total from ux_todays_highlights $whereClause limit 1", $bindings);
        }
        else {
            $whereClause = "$where ";

            $today = $this->rawQueries("SELECT ux.sport_id, ux.competition_name, ux.m_priority, ux.priority, ux.side_bets, ux.sub_type_id, ux.home_odd, ux.neutral_odd, ux.away_odd , ux.double_chance_12_odd, ux.double_chance_x2_odd, ux.double_chance_1x_odd, ux.over_25_odd, ux.under_25_odd, ux.game_id, m.match_id,  m.parent_match_id, m.home_team, m.away_team, m.start_time, m.competition_id, m.status, m.peer_priority, b.bet_id, b.bet_pick FROM ux_todays_highlights ux INNER JOIN `match` m ON m.parent_match_id = ux.parent_match_id LEFT JOIN bet_slip b ON m.parent_match_id = b.parent_match_id $whereClause $and GROUP BY m.match_id ORDER BY $orderBy LIMIT 10");
            $items = $this->rawQueries("select count(*) as total from ux_todays_highlights $whereClause limit 1");
        }

        $results = [];


        foreach ($today as $day) {
            $results[(new DateTime($day['start_time']))->format($this->getDefaultDateFormat())][] = $day;
        }

        return [
            $results,
            $items,
            $this->getCompetitions(),
        ];
    }

    /**
     * @return array
     */
    protected function getCompetitions()
    {
        return [
            'UEFA Youth League, Group A'                     => 'UEFA Youth',
            'UEFA Youth League, Group B'                     => 'UEFA Youth',
            'UEFA Youth League, Group C'                     => 'UEFA Youth',
            'UEFA Youth League, Group D'                     => 'UEFA Youth',
            'UEFA Youth League, Group E'                     => 'UEFA Youth',
            'UEFA Youth League, Group F'                     => 'UEFA Youth',
            'UEFA Youth League, Group H'                     => 'UEFA Youth',
            'UEFA Youth League, Group G'                     => 'UEFA Youth',
            'Int. Friendly Games, Women'                     => 'Women',
            'U17 European Championship, Group B'             => 'U17',
            'U17 European Championship, Qualification Gr. 9' => 'U17',
            'U21'                                            => 'U21',
            'Premier League 2, Division 1'                   => 'Amateur',
            'Premier League 2, Division 2'                   => 'Amateur',
            'Youth League'                                   => 'Youth',
            'Development League'                             => 'Youth',
            'U19 European Championship Qualif. - Gr. 1'      => 'U19',
            'U19 European Championship Qualif. - Gr. 2'      => 'U19',
            'U19 European Championship Qualif. - Gr. 3'      => 'U19',
            'U19 European Championship Qualif. - Gr. 4'      => 'U19',
            'U19 European Championship Qualif. - Gr. 5'      => 'U19',
            'U19 European Championship Qualif. - Gr. 6'      => 'U19',
            'U19 European Championship Qualif. - Gr. 7'      => 'U19',
            'U19 European Championship Qualif. - Gr. 8'      => 'U19',
            'U19 European Championship Qualif. - Gr. 9'      => 'U19',
            'U19 European Championship Qualif. - Gr. 10'     => 'U19',
            'U19 European Championship Qualif. - Gr. 11'     => 'U19',
            'U19 European Championship Qualif. - Gr. 12'     => 'U19',
            'U19 European Championship Qualif. - Gr. 13'     => 'U19',
            'U19 Int. Friendly Games'                        => 'U19',
            'U20 Friendly Games'                             => 'U20',
            'U21 Friendly Games'                             => 'U21',
            'U21 EURO, Qualification, Group 1'               => 'U21',
            'U21 EURO, Qualification, Group 2'               => 'U21',
            'U21 EURO, Qualification, Group 3'               => 'U21',
            'U21 EURO, Qualification, Group 4'               => 'U21',
            'U21 EURO, Qualification, Group 5'               => 'U21',
            'U21 EURO, Qualification, Group 6'               => 'U21',
            'U21 EURO, Qualification, Group 7'               => 'U21',
            'U21 EURO, Qualification, Group 8'               => 'U21',
            'UEFA Youth League, Knockout stage'              => 'UEFA Youth',
            'U21 EURO, Qualification, Group 9'               => 'U21',
            'U21 EURO, Qualification, Playoffs'              => 'U21',
            'UEFA Champions League, Women'                   => 'W',
            'Africa Cup Of Nations, Women, Group A'          => 'W',
            'Primera Division Femenina'                      => 'W',
            'W-League'                                       => 'W',
            'A-Jun-BL West'                                  => 'U19',
            'Bundesliga, Women'                              => 'W',
            'Campionato Primavera, Girone A'                 => 'Youth',
            'Campionato Primavera, Girone B'                 => 'Youth',
            'Campionato Primavera, Girone C'                 => 'Youth',
        ];
    }

    /**
     * @return array
     */
    protected function getPaginationParams()
    {
        $page = $this->request->get('page', 'int') ?: 0;
        if ($page < 0) {
            $page = 0;
        }
        $limit = $this->request->get('limit', 'int') ?: 50;
        $skip = $page * $limit;

        return [
            $page,
            $limit,
            $skip,
        ];
    }

    /**
     * @param $total
     * @param $limit
     *
     * @return float|int
     */
    protected function getResultPages($total, $limit)
    {
        $pages = ceil($total / $limit);

        if ($pages > 14) {
            $pages = 14;
        }

        return $pages;
    }

    /**
     * @return string
     */
    protected function getDefaultDateFormat()
    {
        return 'l jS, F Y';
    }

    /**
     * @param $message
     *
     * @return mixed
     */
    protected function formatMessage($message)
    {
        return preg_replace('/(KES\s+)?(\d{1,3}[,]?)+([.]?\d{2})?/', '$0', $message);
    }

}
