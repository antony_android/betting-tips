<?php

/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */



//  SystemId = betpalace_ke
// SystemPw = CTPLEBAAE5E0A0899005F3
class VirtualSportsController extends ControllerBase
{

    public function indexAction()
    {
        if($this->session->has('auth')){
            $prefix = "CTPLEBAAE";
            $uniqid1 = uniqid();
            $uniqid2 = uniqid();
            $uniqid = strtoupper($prefix.'-'.$uniqid1.'-'.$uniqid2);

            $user = $this->session->get('auth');
            $profile_id = $user['id'];

            $findCode = $this->rawQueries("select * from virtualsports_verification_codes where player_id='$profile_id' limit 1");
            
            if(empty($findCode)){
                $insert = $this->rawInsert("insert into virtualsports_verification_codes (player_code, player_id) values('$uniqid', '$profile_id')");
            }else{
                $insert = $this->rawInsert("update virtualsports_verification_codes set player_code='$uniqid' where player_id='$profile_id'");
            }

            $this->view->setVars(['verification_code' => $uniqid]);
        }else{
            $this->view->setVars(['verification_code' => ""]);
        }
        $this->tag->setTitle('Betpalace Virtual Sports');
    }

    public function findShopsAction()
    {


        header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
        exit;
    }

    public function authenticatePlayerByCodeAction()
    {
        // Set DOc headers
        header('Content-Type: application/xml');

        if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
            //If it isn't, send back a 405 Method Not Allowed header.
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }
        
        
        //Get the raw POST data from PHP's input stream.
        //This raw data should contain XML.
        $postData = trim(file_get_contents('php://input'));
        
        
        //Use internal errors for better error handling.
        libxml_use_internal_errors(true);
        
        
        //Parse the POST data as XML.
        $xml = simplexml_load_string($postData);
        
        
        //If the XML could not be parsed properly.
        if($xml === false) {
            //Send a 400 Bad Request error.
            header($_SERVER["SERVER_PROTOCOL"]." 400 Bad Request", true, 400);
            //Print out details about the error and kill the script.
            foreach(libxml_get_errors() as $xmlError) {
                echo $xmlError->message . "\n";
            }
            exit;
        }

        // Get their request params and set them to variables

        $systemId = $xml->Authorization->systemId;
        $systemPassword = $xml->Authorization->systemPassword;
        //params
        $Login = $xml->Login;
        $Code = $xml->Code;

        // Process the values to authenticate the user
        if($systemId == env('SYSTEM_ID') && $systemPassword == env('SYSTEM_PASSWORD')){

            $checkProfile = $this->rawQueries("select * from virtualsports_verification_codes where player_code='$Code' limit 1");

            if(empty($checkProfile)){
                header("HTTP/1.0 400 Bad Request");
                $error = 'INVALID_CREDENTIALS';
                $error_details = 'Invalid Player credentials';

                $output = "<Error>
                <Code>".$error."</Code>
                <Details>".$error_details."</Details>
                </Error>";
                echo $output;
                die();
            }else{
                foreach($checkProfile as $profile){
                    $player_id = $profile['player_id'];

                }

                $checkPlayer =  $this->rawQueries("select * from profile where profile_id='$player_id' limit 1");
                foreach($checkPlayer as $player){
                    $player_id = $player['profile_id'];
                    $checkBalance = $this->rawQueries("select * from profile_balance where profile_id='$player_id' limit 1");

                    if(empty($checkBalance)){
                        $Balance = '0.00';
                        $CurrencyCode = 'KES';
                    }else{
                        foreach ($checkBalance as $check){
                            $Balance = $check['balance'];
                            $CurrencyCode = 'KES';
                        }
                    }
    
                    $dat = ['player_id' => $Login];
                    $exp = time() + 3600;
                    // $token = $this->generateToken($dat, $exp);
                    $token = bin2hex(openssl_random_pseudo_bytes(32));
                    $token = 'b'.$token;
                    $sessToken = $this->session->set("SessionToken", $token);
    
    
    
                    $PlayerId = $player_id;
                    $PlayerLogin = $player_id;
                    $SessionToken = $token;
                    $AgentId = 'betpalace_ke_web';
                    $Balance = $Balance;
                    $CurrencyCode = 'KES';
    
    
                    $output = "<AuthenticateByCodeResponse>
                    <PlayerId>".$PlayerId."</PlayerId>
                    <PlayerLogin>".$PlayerLogin."</PlayerLogin>
                    <SessionToken>".$SessionToken."</SessionToken>
                    <AgentId>".$AgentId."</AgentId>
                    <Balance>".$Balance."</Balance>
                    <CurrencyCode>".$CurrencyCode."</CurrencyCode>
                    </AuthenticateByCodeResponse>";
    
    
                    $this->view->disable();
                    
                    //var_dump the structure, which will be a SimpleXMLElement object.
                    print($output);   
                }

            }

        }else{
            header("HTTP/1.0 403 Forbidden");
            $error = '';
            $error_details = 'Invalid System credentials';

            $output = "<Error>
            <Code>".$error."</Code>
            <Details>".$error_details."</Details>
            </Error>";
            echo $output;
            die();
        }
        // if ($this->session->has("virtualSportsCode")) {

        //     //Retrieve its value
        //     $sessionCode = $this->session->get("virtualSportsCode");
        // }
        // //$checkCode = $this->session("select * from virtualsports_verification_codes where player_id='$Login' and player_code='$Code' limit 1");

        if(isset($sessionCode) && $Code != $sessionCode){
            header("HTTP/1.0 400 Bad Request");
            $error = '';
            $error_details = 'Verification code mismatch';

            $output = "<Error>
            <Code>".$error."</Code>
            <Details>".$error_details."</Details>
            </Error>";
            echo $output;
            die();
        }else{

            $findProfile = $this->rawQueries("select * from virtualsports_verification_codes where player_code='$Code' limit 1");

            if(empty($findProfile)){
                header("HTTP/1.0 400 Bad Request");
                $error = '';
                $error_details = 'Verification code mismatch';

                $output = "<Error>
                <Code>".$error."</Code>
                <Details>".$error_details."</Details>
                </Error>";
                echo $output;
                die();
            }

            foreach($findProfile as $profile){
                $Login = $profile['player_id'];
            }

               
            
        }


        // End value processing and set values in variables

        
    }

    public function generateCodeAction()
    {
        // Set DOc headers
        header('Content-Type: application/xml');

        if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
            //If it isn't, send back a 405 Method Not Allowed header.
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }
        
        
        //Get the raw POST data from PHP's input stream.
        //This raw data should contain XML.
        $postData = trim(file_get_contents('php://input'));
        
        
        //Use internal errors for better error handling.
        libxml_use_internal_errors(true);
        
        
        //Parse the POST data as XML.
        $xml = simplexml_load_string($postData);
        
        
        //If the XML could not be parsed properly.
        if($xml === false) {
            //Send a 400 Bad Request error.
            header($_SERVER["SERVER_PROTOCOL"]." 400 Bad Request", true, 400);
            //Print out details about the error and kill the script.
            foreach(libxml_get_errors() as $xmlError) {
                echo $xmlError->message . "\n";
            }
            exit;
        }

        // Get their request params and set them to variables


        // authorization params
        $systemId = $xml->Authorization->systemId;
        $systemPassword = $xml->Authorization->systemPassword;

        if($systemId == env('SYSTEM_ID') && $systemPassword == env('SYSTEM_PASSWORD')){
            // request params

            $PlayerId = $xml->PlayerId;
            $SessionToken = $xml->SessionToken;

            $ST = $this->session->get("SessionToken");

            if($ST != $SessionToken){
                header("HTTP/1.0 400 Bad Request");
                $error = 'INVALID_SESSION';
                $error_details = '';

                $output = "<Error>
                <Code>".$error."</Code>
                <Details>".$error_details."</Details>
                </Error>";
                
                // print($output);
                echo $output;
                
                die();
            }

            $checkProfile = $this->rawQueries("select * from profile where profile_id='$PlayerId' limit 1");

            if (count($checkProfile)<1) {
                header("HTTP/1.0 400 Bad Request");
                $error = 'INVALID_PLAYER_ID';
                $error_details = 'Player not found';

                $output = "<Error>
                <Code>".$error."</Code>
                <Details>".$error_details."</Details>
                </Error>";
                print($output);
                die();

            }

            // Process the values to get the users balance in the wallet
            $prefix = "CTPLEBAAE";
            $uniqid1 = uniqid();
            $uniqid2 = uniqid();
            $uniqid = strtoupper($prefix.'-'.$uniqid1.'-'.$uniqid2);

            $sess = $this->session->set("virtualSportsCode", $uniqid);
            $findCode = $this->rawQueries("select * from virtualsports_verification_codes where player_id='$PlayerId' limit 1");
            if(empty($findCode)){
                $insert = $this->rawInsert("insert into virtualsports_verification_codes (player_code, player_id) values('$uniqid', '$PlayerId')");
            }else{
                $insert = $this->rawInsert("update virtualsports_verification_codes set player_code='$uniqid' where player_id='$PlayerId'");
            }
            

            // var_dump($this->session->get("virtualSportsCode"));
                
            // End value processing and set values in variables

            $output = "<GenerateCodeResponse>
            <Code>".$uniqid."</Code>
            </GenerateCodeResponse>";


            $this->view->disable();
            
            //var_dump the structure, which will be a SimpleXMLElement object.
            print($output);
        }else{
            header("HTTP/1.0 403 Forbidden");
            $error = '';
            $error_details = 'Invalid System credentials';

            $output = "<Error>
            <Code>".$error."</Code>
            <Details>".$error_details."</Details>
            </Error>";
            echo $output;
            die();
        }

        
    }

    public function getAgentBalanceAction()
    {
        // Set DOc headers
        header('Content-Type: application/xml');

        if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
            //If it isn't, send back a 405 Method Not Allowed header.
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }
        
        
        //Get the raw POST data from PHP's input stream.
        //This raw data should contain XML.
        $postData = trim(file_get_contents('php://input'));
        
        
        //Use internal errors for better error handling.
        libxml_use_internal_errors(true);
        
        
        //Parse the POST data as XML.
        $xml = simplexml_load_string($postData);
        
        
        //If the XML could not be parsed properly.
        if($xml === false) {
            //Send a 400 Bad Request error.
            header($_SERVER["SERVER_PROTOCOL"]." 400 Bad Request", true, 400);
            //Print out details about the error and kill the script.
            foreach(libxml_get_errors() as $xmlError) {
                echo $xmlError->message . "\n";
            }
            exit;
        }

        // Get their request params and set them to variables
        
        $systemId = $xml->Authorization->systemId;
        $systemPassword = $xml->Authorization->systemPassword;

        if($systemId == env('SYSTEM_ID') && $systemPassword == env('SYSTEM_PASSWORD')){
            // Agent Credentials

            // request params

            $SubjectSessionId = $xml->SubjectSessionId;
            $Subject = $xml->Subject;
            // $ssid = $this->session->get("SubjectSessionId");
            if($this->session->has("SubjectSessionId")){
                $ssid = $this->session->get("SubjectSessionId");
                $ssln = $this->session->get("Agent");
    
                if($ssid != $SubjectSessionId){
                    header("HTTP/1.0 400 Bad Request");
                    $error = 'INVALID_SESSION';
                    $error_details = 'Invalid long token';
        
                    $output = "<Error>
                    <Code>".$error."</Code>
                    <Details>".$error_details."</Details>
                    </Error>";
                    echo $output;
                    die();
                }
                if($ssln != $Subject){
                    header("HTTP/1.0 400 Bad Request");
                    $error = 'INVALID_AGENT';
                    $error_details = 'Invalid Agent';
        
                    $output = "<Error>
                    <Code>".$error."</Code>
                    <Details>".$error_details."</Details>
                    </Error>";
                    echo $output;
                    die();
                }

                
            }else{
                header("HTTP/1.0 400 Bad Request");
                $error = 'INVALID_SESSION';
                $error_details = 'Invalid long token';
    
                $output = "<Error>
                <Code>".$error."</Code>
                <Details>".$error_details."</Details>
                </Error>";
                echo $output;
                die();
            }

            $checkAgent = $this->rawQueries("select * from virtualsports_agents where agent_id='$Subject' limit 1");

            if(empty($checkAgent)){
                header("HTTP/1.0 400 Bad Request");
                $error = 'INVALID_AGENT';
                $error_details = 'Agent not found';

                $output = "<Error>
                <Code>".$error."</Code>
                <Details>".$error_details."</Details>
                </Error>";
                print($output);
                die();
            }else{
                foreach ($checkAgent as $check) {
                    // var_dump($check['password_hash']);
                    // die();

                    $Balance = $check['agent_balance'];
                    $CurrencyCode = 'KES';

                    // End value processing and set values in variables

                    $output = "<GetAgentBalanceResponse>
                    <Balance>".$Balance."</Balance>
                    <CurrencyCode>".$CurrencyCode."</CurrencyCode>
                    </GetAgentBalanceResponse>";


                    $this->view->disable();
                    
                    //var_dump the structure, which will be a SimpleXMLElement object.
                    print($output);
                    
                }

            }

            
        }else{
            header("HTTP/1.0 403 Forbidden");
            $error = '';
            $error_details = 'Invalid system credentials';

            $output = "<Error>
            <Code>".$error."</Code>
            <Details>".$error_details."</Details>
            </Error>";
            echo $output;
            die();
        }

        
    }

    public function getBalanceAction()
    {
        // Set DOc headers
        header('Content-Type: application/xml');

        if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
            //If it isn't, send back a 405 Method Not Allowed header.
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }
        
        
        //Get the raw POST data from PHP's input stream.
        //This raw data should contain XML.
        $postData = trim(file_get_contents('php://input'));
        
        
        //Use internal errors for better error handling.
        libxml_use_internal_errors(true);
        
        
        //Parse the POST data as XML.
        $xml = simplexml_load_string($postData);
        
        
        //If the XML could not be parsed properly.
        if($xml === false) {
            //Send a 400 Bad Request error.
            header($_SERVER["SERVER_PROTOCOL"]." 400 Bad Request", true, 400);
            //Print out details about the error and kill the script.
            foreach(libxml_get_errors() as $xmlError) {
                echo $xmlError->message . "\n";
            }
            exit;
        }

        // Get their request params and set them to variables


        // authorization params
        $systemId = $xml->Authorization->systemId;
        $systemPassword = $xml->Authorization->systemPassword;

        if($systemId == env('SYSTEM_ID') && $systemPassword == env('SYSTEM_PASSWORD')){

        }else{
            header("HTTP/1.0 403 Forbidden");
            $error = '';
            $error_details = 'Invalid System credentials';

            $output = "<Error>
            <Code>".$error."</Code>
            <Details>".$error_details."</Details>
            </Error>";
            echo $output;
            die();
        }

        // request params

        $PlayerId = $xml->PlayerId;
        $SessionToken = $xml->SessionToken;

        if($this->session->has("SessionToken")){
            if($SessionToken != $this->session->get("SessionToken")){
                header("HTTP/1.0 400 Bad Request");
                $error = 'INVALID_SESSION';
                $error_details = 'Session token is invalid';

                $output = "<Error>
                <Code>".$error."</Code>
                <Details>".$error_details."</Details>
                </Error>";
                print($output);
                die();
            }
        }else{
            header("HTTP/1.0 400 Bad Request");
                $error = 'INVALID_SESSION';
                $error_details = 'Session token is invalid';

                $output = "<Error>
                <Code>".$error."</Code>
                <Details>".$error_details."</Details>
                </Error>";
                print($output);
                die();
        }

        // Process the values to get the users balance in the wallet

        $checkProfile = $this->rawQueries("select * from profile where profile_id='$PlayerId' limit 1");

        if (empty($checkProfile)) {
            header("HTTP/1.0 400 Bad Request");
            $error = 'INVALID_PLAYER_ID';
            $error_details = 'Player not found';

            $output = "<Error>
            <Code>".$error."</Code>
            <Details>".$error_details."</Details>
            </Error>";
            print($output);
            die();

        }

        foreach ($checkProfile as $check) {
            $checkBalance = $this->rawQueries("select * from profile_balance where profile_id='$PlayerId' limit 1");

            if(empty($checkBalance)){
                $Balance = '0.00';
                $CurrencyCode = 'KES';

                // End value processing and set values in variables

                $output = "<GetBalanceResponse>
                <Balance>".$Balance."</Balance>
                <CurrencyCode>".$CurrencyCode."</CurrencyCode>
                </GetBalanceResponse>";


                $this->view->disable();
                
                //var_dump the structure, which will be a SimpleXMLElement object.
                print($output);
            }else{
                foreach ($checkBalance as $check){
                    $Balance = $check['balance'];
                    $CurrencyCode = 'KES';

                    // End value processing and set values in variables

                    $output = "<GetBalanceResponse>
                    <Balance>".$Balance."</Balance>
                    <CurrencyCode>".$CurrencyCode."</CurrencyCode>
                    </GetBalanceResponse>";


                    $this->view->disable();
                    
                    //var_dump the structure, which will be a SimpleXMLElement object.
                    print($output);
                }
            }
        }



        
        
    }

    public function loadAgentTreeAction()
    {
        // Set DOc headers
        header('Content-Type: application/xml');

        if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
            //If it isn't, send back a 405 Method Not Allowed header.
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }
        
        
        //Get the raw POST data from PHP's input stream.
        //This raw data should contain XML.
        $postData = trim(file_get_contents('php://input'));
        
        
        //Use internal errors for better error handling.
        libxml_use_internal_errors(true);
        
        
        //Parse the POST data as XML.
        $xml = simplexml_load_string($postData);
        
        
        //If the XML could not be parsed properly.
        if($xml === false) {
            //Send a 400 Bad Request error.
            header($_SERVER["SERVER_PROTOCOL"]." 400 Bad Request", true, 400);
            //Print out details about the error and kill the script.
            foreach(libxml_get_errors() as $xmlError) {
                echo $xmlError->message . "\n";
            }
            exit;
        }

        // Get their request params and set them to variables

        $systemId = $xml->Authorization->systemId;
        $systemPassword = $xml->Authorization->systemPassword;

        if($systemId == env('SYSTEM_ID') && $systemPassword == env('SYSTEM_PASSWORD')){
            // Agent Credentials

            // request params

            $SubjectSessionId = $xml->SubjectSessionId;
            $Subject = $xml->Subject;
            $ssid = $this->session->get("SubjectSessionId");

            if($ssid != $SubjectSessionId){
                header("HTTP/1.0 400 Bad Request");
                $error = 'INVALID_SESSION';
                $error_details = 'Invalid session Id';

                $output = "<Error>
                <Code>".$error."</Code>
                <Details>".$error_details."</Details>
                </Error>";
                print($output);
                die();
            }

            $checkAgent = $this->rawQueries("select * from virtualsports_agents where agent_id='$Subject' limit 1");
            $subagents = $this->rawQueries("select * from virtualsports_agents where parent_agent='9' limit 1");

            if(empty($checkAgent)){
                header("HTTP/1.0 400 Bad Request");
                $error = 'INVALID_AGENT';
                $error_details = 'Agent not found';

                $output = "<Error>
                <Code>".$error."</Code>
                <Details>".$error_details."</Details>
                </Error>";
                print($output);
                die();
            }else{
                foreach ($checkAgent as $check) {
                    // var_dump($check['password_hash']);
                    // die();

                   // End value processing and set values in variables

                    $AgentId =  $check['agent_id'];
                    $Login =  $check['agent_id'];
                    $FullName =  $check['full_name'];

                    // Create output

                    foreach ($subagents as $agent){
                    $output = "<LoadAgentTreeResponse>
                    <Agent>
                    <AgentId>".$AgentId."</AgentId>
                    <Login>".$Login."</Login>
                    <FullName>".$FullName."</FullName>
                    <SubAgents>

                        <Agent>
                        <AgentId>".$agent['agent_id']."</AgentId>
                        <Login>".$agent['agent_id']."</Login>
                        <FullName>".$agent['full_name']."</FullName>
                        </Agent>
                    </SubAgents>
                    </Agent>
                    </LoadAgentTreeResponse>";
                    }

                    


                    $this->view->disable();
                    
                    //var_dump the structure, which will be a SimpleXMLElement object.
                    print($output);
                    
                }

            }

            
        }else{
            header("HTTP/1.0 403 Forbidden");
            $error = '';
            $error_details = 'Invalid system credentials';

            $output = "<Error>
            <Code>".$error."</Code>
            <Details>".$error_details."</Details>
            </Error>";
            echo $output;
            die();
        }

    }

    public function loginAgentByPasswordAction()
    {
        
        // Set DOc headers
        header('Content-Type: application/xml');

        if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
            //If it isn't, send back a 405 Method Not Allowed header.
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }
        
        
        //Get the raw POST data from PHP's input stream.
        //This raw data should contain XML.
        $postData = trim(file_get_contents('php://input'));
        
        
        //Use internal errors for better error handling.
        libxml_use_internal_errors(true);
        
        
        //Parse the POST data as XML.
        $xml = simplexml_load_string($postData);
        
        
        //If the XML could not be parsed properly.
        if($xml === false) {
            //Send a 400 Bad Request error.
            header($_SERVER["SERVER_PROTOCOL"]." 400 Bad Request", true, 400);
            //Print out details about the error and kill the script.
            foreach(libxml_get_errors() as $xmlError) {
                echo $xmlError->message . "\n";
            }
            exit;
        }

        // Get their request params and set them to variables

        $systemId = $xml->Authorization->systemId;
        $systemPassword = $xml->Authorization->systemPassword;

        if($systemId == env('SYSTEM_ID') && $systemPassword == env('SYSTEM_PASSWORD')){
            // Agent Credentials

            $Login = $xml->Login;
            $Password = $xml->Password;

            $checkAgent = $this->rawQueries("select * from virtualsports_agents where agent_id='$Login' limit 1");

            if(empty($checkAgent)){
                header("HTTP/1.0 400 Bad Request");
                $error = 'INVALID_CREDENTIALS';
                $error_details = 'Agent not found';

                $output = "<Error>
                <Code>".$error."</Code>
                <Details>".$error_details."</Details>
                </Error>";
                print($output);
                die();
            }else{
                foreach ($checkAgent as $check) {
                    // var_dump($check['password_hash']);
                    // die();

                   

                    if (!$this->security->checkHash($Password, $check['password_hash'])) {
                        header("HTTP/1.0 400 Bad Request");
                        $error = 'INVALID_CREDENTIALS';
                        $error_details = 'Invalid agent credentials';

                        $output = "<Error>
                        <Code>".$error."</Code>
                        <Details>".$error_details."</Details>
                        </Error>";
                        print($output);
                        die();
                    }else{
                        
                        $dat = ['agent_id' => $check['agent_id']];
                        $exp = time() + 3600;
                        $token = bin2hex(openssl_random_pseudo_bytes(32));

                        // End value processing and set values in variables
                        $ssid = $this->session->set("SubjectSessionId", $token);

                        $SubjectSessionId = $token;
                        $Subject = $check['agent_id'];
                        $Login = $check['agent_id'];

                        
                        $ssln = $this->session->set("Agent", $Login);

                        // Create output


                        $output = "<LoginAgentByPasswordResponse>
                        <SubjectSessionId>".$SubjectSessionId."</SubjectSessionId>
                        <Subject>".$Subject."</Subject>
                        <Login>".$Login."</Login>
                        </LoginAgentByPasswordResponse>";

                        print($output);


                        $this->view->disable();

                    }
                }

            }

            
        }else{
            header("HTTP/1.0 403 Forbidden");
            $error = '';
            $error_details = 'Invalid system credentials';

            $output = "<Error>
            <Code>".$error."</Code>
            <Details>".$error_details."</Details>
            </Error>";
            
            // print($output);
            echo $output;
            
            die();
        }

       
    }

    public function registerAgentAction()
    {
        // Set DOc headers
        header('Content-Type: application/xml');

        if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
            //If it isn't, send back a 405 Method Not Allowed header.
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }
        
        
        //Get the raw POST data from PHP's input stream.
        //This raw data should contain XML.
        $postData = trim(file_get_contents('php://input'));
        
        
        //Use internal errors for better error handling.
        libxml_use_internal_errors(true);
        
        
        //Parse the POST data as XML.
        $xml = simplexml_load_string($postData);
        
        
        //If the XML could not be parsed properly.
        if($xml === false) {
            //Send a 400 Bad Request error.
            header($_SERVER["SERVER_PROTOCOL"]." 400 Bad Request", true, 400);
            //Print out details about the error and kill the script.
            foreach(libxml_get_errors() as $xmlError) {
                echo $xmlError->message . "\n";
            }
            exit;
        }

        // Agent Credentials

        $agent_id = $xml->agent_id;
        $full_name = $xml->full_name;
        $type = $xml->type;
        $parent_agent = $xml->parent_agent;
        $agent_balance = $xml->agent_balance;
        $password = $xml->password;

        $password_hash = $this->security->hash($password);

        $dat = ['agent_id' => $agent_id];
        $exp = time() + 3600;
        $token = $this->generateToken($dat, $exp);

        // Process the values to authenticate the user

        $insert = $this->rawInsert("insert into virtualsports_agents (agent_id, full_name, type, parent_agent, agent_balance, password_hash, session_id) values('$agent_id', '$full_name', '$type', '$parent_agent', '$agent_balance', '$password_hash', '$token')");


        // End processing 

       

        // End value processing and set values in variables

        $SubjectSessionId = '12345';
        $Subject = 'MyAgent';
        $Login = 'MyAgent';


        // Create output


        $output = "<RegisterAgentResponse>
        <Token>".$token."</Token>
        </RegisterAgentResponse>";



        
        //var_dump the structure, which will be a SimpleXMLElement object.
        print($output);


        $this->view->disable();
    }

    public function openTicketAction()
    {
        // Set DOc headers
        header('Content-Type: application/xml');

        if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
            //If it isn't, send back a 405 Method Not Allowed header.
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }
        
        
        //Get the raw POST data from PHP's input stream.
        //This raw data should contain XML.
        $postData = trim(file_get_contents('php://input'));
        
        
        //Use internal errors for better error handling.
        libxml_use_internal_errors(true);
        
        
        //Parse the POST data as XML.
        $xml = simplexml_load_string($postData);
        
        
        //If the XML could not be parsed properly.
        if($xml === false) {
            //Send a 400 Bad Request error.
            header($_SERVER["SERVER_PROTOCOL"]." 400 Bad Request", true, 400);
            //Print out details about the error and kill the script.
            foreach(libxml_get_errors() as $xmlError) {
                echo $xmlError->message . "\n";
            }
            exit;
        }

        $systemId = $xml->Authorization->systemId;
        $systemPassword = $xml->Authorization->systemPassword;

        if($systemId == env('SYSTEM_ID') && $systemPassword == env('SYSTEM_PASSWORD')){
            // Agent Credentials

            $PlayerId = $xml->PlayerId;
            $SessionToken = $xml->SessionToken;
            $TicketNumber = $xml->TicketNumber;
            $Stake = $xml->Stake;
            $Stake = number_format((float)$Stake, 4, '.', '');
            $CurrencyCode = $xml->CurrencyCode;


            if($CurrencyCode != 'KES'){
                header("HTTP/1.0 400 Bad Request");
                $error = 'ILLEGAL_CURRENCY';
                $error_details = '';

                $output = "<Error>
                <Code>".$error."</Code>
                <Details>".$error_details."</Details>
                </Error>";
                
                // print($output);
                echo $output;
                
                die();
            }

            $ST = $this->session->get("SessionToken");

            $checkPlayer = $this->rawQueries("select * from profile where profile_id='$PlayerId' limit 1");
            if(empty($checkPlayer)){
                header("HTTP/1.0 400 Bad Request");
                $error = 'INVALID_PLAYER_ID';
                $error_details = '';

                $output = "<Error>
                <Code>".$error."</Code>
                <Details>".$error_details."</Details>
                </Error>";
                
                // print($output);
                echo $output;
                
                die();
            }

            $checkVerificationCode = $this->rawQueries("select * from virtualsports_verification_codes where player_id='$PlayerId' limit 1");
            if(empty($checkVerificationCode)){
                header("HTTP/1.0 400 Bad Request");
                $error = 'INVALID_SESSION';
                $error_details = '';

                $output = "<Error>
                <Code>".$error."</Code>
                <Details>".$error_details."</Details>
                </Error>";
                
                // print($output);
                echo $output;
                
                die();
            }

            //var_dump($this->rawQueries("select * from profile_balance where profile_id='$PlayerId' limit 1"));
            // var_dump($PlayerId);

            $checkPlayerBalance = $this->rawQueries("select * from profile_balance where profile_id='$PlayerId' limit 1");

            if(empty($checkPlayerBalance)){
                
                // var_dump($checkPlayerBalance);
                header("HTTP/1.0 400 Bad Request");
                $error = 'INSUFFICIENT_BALANCE';
                $error_details = 'Player balance is insuficient';

                $output = "<Error>
                <Code>".$error."</Code>
                <Details>".$error_details."</Details>
                </Error>";
                
                // print($output);
                echo $output;
                
                die();
            }else{
                
                foreach($checkPlayerBalance as $Check){
                    $Balance = $Check['balance'];
                }



                if($Stake > $Balance)
                {
                    header("HTTP/1.0 400 Bad Request");
                    $error = 'INSUFFICIENT_BALANCE';
                    $error_details = 'Player balance is insuficient';

                    $output = "<Error>
                    <Code>".$error."</Code>
                    <Details>".$error_details."</Details>
                    </Error>";
                    
                    // print($output);
                    echo $output;
                    
                    die();
                }else{
                    
                    // var_dump($Stake);
                    // var_dump($Balance);
                    $ticket_id = $TicketNumber;
                    $date_time = date('Y-m-d H:i:s');
                    $currency_code = 'KES';

                    // var_dump($Balance);

                    $checkTicketNumber = $this->rawQueries("select * from virtualsports_tickets where ticket_number='$TicketNumber'");
                    
                    if(empty($checkTicketNumber)){
                    //     var_dump("insert into virtualsports_tickets (ticket_number, ticket_stake, profile_id, status, currency_code, date_time) values('$TicketNumber', '$Stake', '$PlayerId', 'open', 'KES', '$date_time'");
                    //    exit();
                        $insert = $this->rawInsert("insert into virtualsports_tickets (ticket_number, ticket_stake, profile_id, status, currency_code, date_time) values('$TicketNumber', '$Stake', '$PlayerId', 'open', 'KES', '$date_time')");
                        $new_balance = $Balance - $Stake;

                        // var_dump("update profile_balance set balance='$new_balance' where profile_id='$PlayerId'");
    
                    
                       // With placeholders

                       
                       $update = $this->rawInsert("update profile_balance set balance='$new_balance' where profile_id='$PlayerId'");
                    }else{
                        header("HTTP/1.0 400 Bad Request");
                        $error = 'DUPLICATED_TICKET_NUMBER';
                        $error_details = 'This ticket number already exists';

                        $output = "<Error>
                        <Code>".$error."</Code>
                        <Details>".$error_details."</Details>
                        </Error>";
                        
                        // print($output);
                        echo $output;
                        
                        die();
                    }
                    $new_balance = number_format((float)$new_balance, 2, '.', '');

                    //header("HTTP/1.0 200 OK");
                    
                    $output = "<OpenTicketResponse>
                    <Balance>".$new_balance."</Balance>
                    <CurrencyCode>".$currency_code."</CurrencyCode>
                    </OpenTicketResponse>";
                    
                    // print($output);
                    echo $output;
                    
                    die();

                }   
            }
        }else{
            header("HTTP/1.0 403 Forbidden");
            $error = '';
            $error_details = 'Invalid system credentials';

            $output = "<Error>
            <Code>".$error."</Code>
            <Details>".$error_details."</Details>
            </Error>";
            
            // print($output);
            echo $output;
            
            die();
        }
    }


    public function payoutTicketAction()
    {
        // Set DOc headers
        header('Content-Type: application/xml');

        if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
            //If it isn't, send back a 405 Method Not Allowed header.
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }
        
        
        //Get the raw POST data from PHP's input stream.
        //This raw data should contain XML.
        $postData = trim(file_get_contents('php://input'));
        
        
        //Use internal errors for better error handling.
        libxml_use_internal_errors(true);
        
        
        //Parse the POST data as XML.
        $xml = simplexml_load_string($postData);
        
        
        //If the XML could not be parsed properly.
        if($xml === false) {
            //Send a 400 Bad Request error.
            header($_SERVER["SERVER_PROTOCOL"]." 400 Bad Request", true, 400);
            //Print out details about the error and kill the script.
            foreach(libxml_get_errors() as $xmlError) {
                echo $xmlError->message . "\n";
            }
            exit;
        }

        $systemId = $xml->Authorization->systemId;
        $systemPassword = $xml->Authorization->systemPassword;

        if($systemId == env('SYSTEM_ID') && $systemPassword == env('SYSTEM_PASSWORD')){
            // Agent Credentials

            $PlayerId = $xml->PlayerId;
            $SessionToken = $xml->SessionToken;
            $TicketNumber = $xml->TicketNumber;
            $Payout = $xml->Payout;
            $CurrencyCode = $xml->CurrencyCode;
            $Jackpot = $xml->Jackpot;
            $Stake = $xml->Stake;

            $Stake = number_format((float)$Stake, 4, '.', '');
            $Jackpot = number_format((float)$Jackpot, 4, '.', '');
            $Payout = number_format((float)$Payout, 4, '.', '');

            //var_dump($this->rawQueries("select * from profile_balance where profile_id='$PlayerId' limit 1"));
            // var_dump($PlayerId);

            if($CurrencyCode != 'KES'){
                header("HTTP/1.0 400 Bad Request");
                $error = 'ILLEGAL_CURRENCY';
                $error_details = '';

                $output = "<Error>
                <Code>".$error."</Code>
                <Details>".$error_details."</Details>
                </Error>";
                
                // print($output);
                echo $output;
                
                die();
            }
            

            $checkPlayer = $this->rawQueries("select * from profile where profile_id='$PlayerId' limit 1");
            if(empty($checkPlayer)){
                header("HTTP/1.0 400 Bad Request");
                $error = 'INVALID_PLAYER_ID';
                $error_details = '';

                $output = "<Error>
                <Code>".$error."</Code>
                <Details>".$error_details."</Details>
                </Error>";
                
                // print($output);
                echo $output;
                
                die();
            }

            $checkPlayerBalance = $this->rawQueries("select * from profile_balance where profile_id='$PlayerId' limit 1");

            if(empty($checkPlayerBalance)){
                
                $Balance = 0;
            }else{
                
                foreach($checkPlayerBalance as $Check){
                    $Balance = $Check['balance'];
                }   

            }
                // var_dump($Stake);
                // var_dump($Balance);
                $ticket_id = $TicketNumber;
                $date_time = date('Y-m-d H:i:s');
                $currency_code = $CurrencyCode;

                // var_dump($Balance);

                $checkTicketNumber = $this->rawQueries("select * from virtualsports_tickets where ticket_number='$TicketNumber' and profile_id = '$PlayerId'");
                if(empty($checkTicketNumber)){
                    header("HTTP/1.0 400 Bad Request");
                    $error = 'ILLEGAL_TICKET_NUMBER';
                    $error_details = '';

                    $output = "<Error>
                    <Code>".$error."</Code>
                    <Details>".$error_details."</Details>
                    </Error>";
                    
                    // print($output);
                    echo $output;
                    
                    die();
                }else{

                    foreach($checkTicketNumber as $ticket){
                        $status = $ticket['status'];

                        if($status == 'cancelled'){
                            header("HTTP/1.0 400 Bad Request");
                            $error = 'ALREADY_CANCELLED';
                            $error_details = '';

                            $output = "<Error>
                            <Code>".$error."</Code>
                            <Details>".$error_details."</Details>
                            </Error>";
                            
                            // print($output);
                            echo $output;
                            
                            die();
                        }else if($status == 'paid'){
                            header("HTTP/1.0 400 Bad Request");
                            $error = 'ALREADY_PROCESSED';
                            $error_details = '';

                            $output = "<Error>
                            <Code>".$error."</Code>
                            <Details>".$error_details."</Details>
                            </Error>";
                            
                            // print($output);
                            echo $output;
                            
                            die();
                        }else if($status == 'revert'){
                            header("HTTP/1.0 400 Bad Request");
                            $error = 'TICKET_ALREADY_REVERTED';
                            $error_details = '';

                            $output = "<Error>
                            <Code>".$error."</Code>
                            <Details>".$error_details."</Details>
                            </Error>";
                            
                            // print($output);
                            echo $output;
                            
                            die();
                        }

                        if($Stake != $ticket['ticket_stake']){
                            header("HTTP/1.0 400 Bad Request");
                            $error = 'ILLEGAL_STAKE';
                            $error_details = '';

                            $output = "<Error>
                            <Code>".$error."</Code>
                            <Details>".$error_details."</Details>
                            </Error>";
                            
                            // print($output);
                            echo $output;
                            
                            die();
                        }


                    }

                    $insert = $this->rawInsert("update virtualsports_tickets set ticket_payout = '$Payout', jackpot = '$Jackpot', updated_at = '$date_time', status = 'paid' where ticket_number = '$TicketNumber'");
                    $new_balance = $Balance + $Payout + $Jackpot;
                    $new_balance = number_format((float)$new_balance, 2, '.', '');

                    $checkPlayerBalance = $this->rawQueries("select * from profile_balance where profile_id='$PlayerId' limit 1");
                    if(empty($checkPlayerBalance)){
                        $amnt = $Payout + $Jackpot;
                        $account = $PlayerId.'_virtual'; 
                        $insert_txn = $this->rawInsert("insert into transaction ( profile_id, account, iscredit, reference, amount, running_balance, created_by, created) values('$PlayerId', '$account', '1', '$TicketNumber', '$amnt', '$new_balance', 'virtuals', '$date_time')");
                        $insert = $this->rawInsert("insert into profile_balance ( profile_id, balance, transaction_id, created) values('$PlayerId', '$new_balance', '$insert_txn', '$date_time')");
                    }else{
                        $amnt = $Payout + $Jackpot;
                        $account = $PlayerId.'_virtual'; 
                        $insert_txn = $this->rawInsert("insert into transaction ( profile_id, account, iscredit, reference, amount, running_balance, created_by, created) values('$PlayerId', '$account', '1', '$TicketNumber', '$amnt', '$new_balance', 'virtuals', '$date_time')");
                        $update = $this->rawInsert("update profile_balance set balance='$new_balance' where profile_id='$PlayerId'");
        
                    }
                
                    

                    header("HTTP/1.0 200 OK");
            
                    $output = "<PayoutTicketResponse>
                    <Balance>".$new_balance."</Balance>
                    <CurrencyCode>".$currency_code."</CurrencyCode>
                    </PayoutTicketResponse>";
                    
                    // print($output);
                    echo $output;
                    
                    die();                  
                }
            
        }else{
            header("HTTP/1.0 403 Forbidden");
            $error = '';
            $error_details = 'Invalid system credentials';

            $output = "<Error>
            <Code>".$error."</Code>
            <Details>".$error_details."</Details>
            </Error>";
            
            // print($output);
            echo $output;
            
            die();
        }


    }

    public function cancelTicketAction()
    {
        // Set DOc headers
        header('Content-Type: application/xml');

        if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
            //If it isn't, send back a 405 Method Not Allowed header.
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }
        
        
        //Get the raw POST data from PHP's input stream.
        //This raw data should contain XML.
        $postData = trim(file_get_contents('php://input'));
        
        
        //Use internal errors for better error handling.
        libxml_use_internal_errors(true);
        
        
        //Parse the POST data as XML.
        $xml = simplexml_load_string($postData);
        
        
        //If the XML could not be parsed properly.
        if($xml === false) {
            //Send a 400 Bad Request error.
            header($_SERVER["SERVER_PROTOCOL"]." 400 Bad Request", true, 400);
            //Print out details about the error and kill the script.
            foreach(libxml_get_errors() as $xmlError) {
                echo $xmlError->message . "\n";
            }
            exit;
        }

        $systemId = $xml->Authorization->systemId;
        $systemPassword = $xml->Authorization->systemPassword;

        if($systemId == env('SYSTEM_ID') && $systemPassword == env('SYSTEM_PASSWORD')){
            // Agent Credentials

            $PlayerId = $xml->PlayerId;
            $SessionToken = $xml->SessionToken;
            $TicketNumber = $xml->TicketNumber;
            $Payout = $xml->Payout;
            $CurrencyCode = $xml->CurrencyCode;
            $Jackpot = $xml->Jackpot;
            $Stake = $xml->Stake;

            if($CurrencyCode != 'KES'){
                header("HTTP/1.0 400 Bad Request");
                $error = 'ILLEGAL_CURRENCY';
                $error_details = '';

                $output = "<Error>
                <Code>".$error."</Code>
                <Details>".$error_details."</Details>
                </Error>";
                
                // print($output);
                echo $output;
                
                die();
            }
            

            //var_dump($this->rawQueries("select * from profile_balance where profile_id='$PlayerId' limit 1"));
            // var_dump($PlayerId);

            $checkPlayer = $this->rawQueries("select * from profile where profile_id='$PlayerId' limit 1");
            if(empty($checkPlayer)){
                header("HTTP/1.0 400 Bad Request");
                $error = 'INVALID_PLAYER_ID';
                $error_details = '';

                $output = "<Error>
                <Code>".$error."</Code>
                <Details>".$error_details."</Details>
                </Error>";
                
                // print($output);
                echo $output;
                
                die();
            }

            $checkPlayerBalance = $this->rawQueries("select * from profile_balance where profile_id='$PlayerId' limit 1");

            if(empty($checkPlayerBalance)){
                
                $Balance = 0;
            }else{
                
                foreach($checkPlayerBalance as $Check){
                    $Balance = $Check['balance'];
                }   
                // var_dump($Stake);
                // var_dump($Balance);
                $ticket_id = $TicketNumber;
                $date_time = date('Y-m-d H:i:s');
                $currency_code = $CurrencyCode;

                // var_dump($Balance);

                $checkTicketNumber = $this->rawQueries("select * from virtualsports_tickets where ticket_number='$TicketNumber' and profile_id = '$PlayerId'");
                if(empty($checkTicketNumber)){
                    header("HTTP/1.0 400 Bad Request");
                    $error = 'ILLEGAL_TICKET_NUMBER';
                    $error_details = '';

                    $output = "<Error>
                    <Code>".$error."</Code>
                    <Details>".$error_details."</Details>
                    </Error>";
                    
                    // print($output);
                    echo $output;
                    
                    die();
                }else{

                    foreach($checkTicketNumber as $ticket){
                        $status = $ticket['status'];
                        $stake = $ticket['ticket_stake'];

                        if($status == 'paid'){
                            header("HTTP/1.0 400 Bad Request");
                            $error = 'ALREADY_PAID_OUT';
                            $error_details = '';

                            $output = "<Error>
                            <Code>".$error."</Code>
                            <Details>".$error_details."</Details>
                            </Error>";
                            
                            // print($output);
                            echo $output;
                            
                            die();
                        }else if($status == 'cancelled'){
                            header("HTTP/1.0 400 Bad Request");
                            $error = 'ALREADY_PROCESSED';
                            $error_details = '';

                            $output = "<Error>
                            <Code>".$error."</Code>
                            <Details>".$error_details."</Details>
                            </Error>";
                            
                            // print($output);
                            echo $output;
                            
                            die();
                        }else if($status == 'revert'){
                            header("HTTP/1.0 400 Bad Request");
                            $error = 'ALREADY_REVERTED';
                            $error_details = '';

                            $output = "<Error>
                            <Code>".$error."</Code>
                            <Details>".$error_details."</Details>
                            </Error>";
                            
                            // print($output);
                            echo $output;
                            
                            die();
                        }

                        if($Stake != $ticket['ticket_stake']){
                            header("HTTP/1.0 400 Bad Request");
                            $error = 'ILLEGAL_STAKE';
                            $error_details = '';

                            $output = "<Error>
                            <Code>".$error."</Code>
                            <Details>".$error_details."</Details>
                            </Error>";
                            
                            // print($output);
                            echo $output;
                            
                            die();
                        }
                    }

                    $insert = $this->rawInsert("update virtualsports_tickets set status = 'cancelled' where ticket_number = '$TicketNumber'");
                    $new_balance = $Balance + $stake;
                    $new_balance = number_format((float)$new_balance, 2, '.', '');

                
                    $update = $this->rawInsert("update profile_balance set balance='$new_balance' where profile_id='$PlayerId'");\

                    header("HTTP/1.0 200 OK");
            
                    $output = "<CancelTicketResponse>
                    <Balance>".$new_balance."</Balance>
                    <CurrencyCode>".$currency_code."</CurrencyCode>
                    </CancelTicketResponse>";
                    
                    // print($output);
                    echo $output;
                    
                    die();                  
                }
            }
        }else{
            header("HTTP/1.0 403 Forbidden");
            $error = '';
            $error_details = 'Invalid system credentials';

            $output = "<Error>
            <Code>".$error."</Code>
            <Details>".$error_details."</Details>
            </Error>";
            
            // print($output);
            echo $output;
            
            die();
        }


    }


    public function revertPaymentAction()
    {
        // Set DOc headers
        header('Content-Type: application/xml');

        if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
            //If it isn't, send back a 405 Method Not Allowed header.
            header($_SERVER["SERVER_PROTOCOL"]." 405 Method Not Allowed", true, 405);
            exit;
        }
        
        
        //Get the raw POST data from PHP's input stream.
        //This raw data should contain XML.
        $postData = trim(file_get_contents('php://input'));
        
        
        //Use internal errors for better error handling.
        libxml_use_internal_errors(true);
        
        
        //Parse the POST data as XML.
        $xml = simplexml_load_string($postData);
        
        
        //If the XML could not be parsed properly.
        if($xml === false) {
            //Send a 400 Bad Request error.
            header($_SERVER["SERVER_PROTOCOL"]." 400 Bad Request", true, 400);
            //Print out details about the error and kill the script.
            foreach(libxml_get_errors() as $xmlError) {
                echo $xmlError->message . "\n";
            }
            exit;
        }

        $systemId = $xml->Authorization->systemId;
        $systemPassword = $xml->Authorization->systemPassword;

        if($systemId == env('SYSTEM_ID') && $systemPassword == env('SYSTEM_PASSWORD')){
            // Agent Credentials

            $PlayerId = $xml->PlayerId;
            $SessionToken = $xml->SessionToken;
            $TicketNumber = $xml->TicketNumber;
            $Amount = $xml->Amount;           
            $Amount = number_format((float)$Amount, 4, '.', '');
            $CurrencyCode = $xml->CurrencyCode;
            $Jackpot = $xml->Jackpot;

            if($CurrencyCode != 'KES'){
                header("HTTP/1.0 400 Bad Request");
                $error = 'ILLEGAL_CURRENCY';
                $error_details = '';

                $output = "<Error>
                <Code>".$error."</Code>
                <Details>".$error_details."</Details>
                </Error>";
                
                // print($output);
                echo $output;
                
                die();
            }


            //var_dump($this->rawQueries("select * from profile_balance where profile_id='$PlayerId' limit 1"));
            // var_dump($PlayerId);

            $checkPlayer = $this->rawQueries("select * from profile where profile_id='$PlayerId' limit 1");
            if(empty($checkPlayer)){
                header("HTTP/1.0 400 Bad Request");
                $error = 'INVALID_PLAYER_ID';
                $error_details = '';

                $output = "<Error>
                <Code>".$error."</Code>
                <Details>".$error_details."</Details>
                </Error>";
                
                // print($output);
                echo $output;
                
                die();
            }

            $checkPlayerBalance = $this->rawQueries("select * from profile_balance where profile_id='$PlayerId' limit 1");

            if(empty($checkPlayerBalance)){
                
                $Balance = 0;
            }else{
                
                foreach($checkPlayerBalance as $Check){
                    $Balance = $Check['balance'];
                }   
                // var_dump($Stake);
                // var_dump($Balance);
                $ticket_id = $TicketNumber;
                $date_time = date('Y-m-d H:i:s');
                $currency_code = $CurrencyCode;

                // var_dump($Balance);

                $checkTicketNumber = $this->rawQueries("select * from virtualsports_tickets where ticket_number='$TicketNumber' and profile_id = '$PlayerId'");
                if(empty($checkTicketNumber)){
                    header("HTTP/1.0 400 Bad Request");
                    $error = 'ILLEGAL_TICKET_NUMBER';
                    $error_details = '';

                    $output = "<Error>
                    <Code>".$error."</Code>
                    <Details>".$error_details."</Details>
                    </Error>";
                    
                    // print($output);
                    echo $output;
                    
                    die();
                }else{

                    foreach($checkTicketNumber as $ticket){
                        $status = $ticket['status'];
                        $stake = $ticket['ticket_stake'];
                        $payout = $ticket['ticket_payout'];

                        if($status == 'revert' ){
                            header("HTTP/1.0 400 Bad Request");
                            $error = 'ALREADY_PROCESSED';
                            $error_details = '';

                            $output = "<Error>
                            <Code>".$error."</Code>
                            <Details>".$error_details."</Details>
                            </Error>";
                            
                            // print($output);
                            echo $output;
                            
                            die();
                        }else if($status == 'cancelled'){
                            header("HTTP/1.0 400 Bad Request");
                            $error = 'TICKET_CANCELLED';
                            $error_details = '';

                            $output = "<Error>
                            <Code>".$error."</Code>
                            <Details>".$error_details."</Details>
                            </Error>";
                            
                            // print($output);
                            echo $output;
                            
                            die();
                        }else if($status == 'open'){
                            header("HTTP/1.0 400 Bad Request");
                            $error = 'TICKET_OPEN';
                            $error_details = '';

                            $output = "<Error>
                            <Code>".$error."</Code>
                            <Details>".$error_details."</Details>
                            </Error>";
                            
                            // print($output);
                            echo $output;
                            
                            die();
                        }else{
                            $insert = $this->rawInsert("update virtualsports_tickets set status = 'revert' where ticket_number = '$TicketNumber'");
                            $new_balance = $Balance + $Amount;
                            $new_balance = number_format((float)$new_balance, 2, '.', '');

                        
                            $update = $this->rawInsert("update profile_balance set balance='$new_balance' where profile_id='$PlayerId'");\

                            header("HTTP/1.0 200 OK");
                    
                            $output = "<RevertPaymentResponse>
                            <Balance>".$new_balance."</Balance>
                            <CurrencyCode>".$currency_code."</CurrencyCode>
                            </RevertPaymentResponse>";
                            
                            // print($output);
                            echo $output;
                            
                            die();  
                        }


                    }

                                    
                }
            }
        }else{
            header("HTTP/1.0 403 Forbidden");
            $error = '';
            $error_details = 'Invalid system credentials';

            $output = "<Error>
            <Code>".$error."</Code>
            <Details>".$error_details."</Details>
            </Error>";
            
            // print($output);
            echo $output;
            
            die();
        }


    }




}