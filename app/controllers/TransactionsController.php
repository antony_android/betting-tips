<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

/**
 * Created by PhpStorm.
 * User: mxgel
 * Date: 10/03/2018
 * Time: 18:43
 */
class TransactionsController extends ControllerBase
{
    /**
     * Transactions
     */
    public function indexAction()
    {
        list($page, $limit, $skip) = $this->getPaginationParams();
        $user = $this->session->get('auth');

        if ($user) {
            /**$query = "SELECT   transaction.created,   id,   profile.msisdn,   if(iscredit = 1, 'DEBIT', 'CREDIT')       DB_CR,   reference,   amount,   (SELECT mpesa_amt    FROM mpesa_transaction    WHERE msisdn = profile.msisdn AND business_number = reference)    AS Deposit,   (SELECT amount    FROM withdrawal    WHERE      concat(withdrawal_id, '_', profile.msisdn) = transaction.reference AND      withdrawal.msisdn = profile.msisdn) AS withdrawal,   transaction.created_by FROM transaction   INNER   JOIN profile USING (profile_id) WHERE transaction.status = 'COMPLETE' AND profile.msisdn = ? ORDER BY transaction.created DESC LIMIT $skip, $limit";
            */
            $query = "select transaction.status, transaction.created, id, profile.msisdn, reference, amount,  (case      WHEN ((select mpesa_amt from mpesa_transaction where msisdn=profile.msisdn and business_number=transaction.reference) is not null  and transaction.iscredit=1 and transaction.status='COMPLETE' ) THEN 'DEPOSIT'     WHEN ((select amount from withdrawal where concat(withdrawal_id, '_', profile.msisdn) = transaction.reference and withdrawal.msisdn=profile.msisdn and transaction.status='COMPLETE') is not null) THEN 'WITHDRAWAL'     WHEN ((select bet_amount from bet where bet.bet_id=transaction.reference and transaction.profile_id = bet.profile_id and transaction.status='COMPLETE' and transaction.iscredit = 0 and transaction.status='COMPLETE') is not null) THEN 'BET'     WHEN ((select possible_win from bet where bet.bet_id=transaction.reference and transaction.profile_id = bet.profile_id and transaction.status='COMPLETE' and bet.status=5 and transaction.iscredit = 1 ) is not null) THEN 'WIN'  WHEN ((select bet_id from jackpot_bet where concat('jp[', bet_id, ']') = transaction.reference and transaction.status='COMPLETE') is not null) THEN 'JACKPOT BET'  WHEN (transaction.status='PENDING') THEN 'INCOMPLETE'  ELSE 'OTHER' END)as DB_CR, transaction.created_by from transaction inner join profile using(profile_id) where  profile.msisdn=? order by transaction.created DESC LIMIT $skip, $limit";

            $transactions = $this->rawQueries($query, [$user['mobile']]);

            $this->view->setVars([
                'transactions' => $transactions,
                'pages'        => $this->getResultPages(10, $limit),
                'page'         => $page,
                'winners' => $this->topWinners(),
            ]);
            $this->tag->setTitle(sprintf('%s Transactions', 'My'));
        } else {
            $this->flashSession->error($this->flashMessages('Login to access this page'));
            $this->response->redirect('login');
        }
    }
}
