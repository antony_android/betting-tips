<?php

/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */
class TermsController extends ControllerBase
{

    /**
     *
     */
    public function indexAction()
    {
        $this->view->setVars(["topLeagues" => $this->topLeagues(), 'winners'=>$this->topWinners()]);

        $this->tag->setTitle('Terms & Conditions');

    }

}

