<?php

/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */
class CompetitionController extends ControllerBase
{
    /**
     *
     */
    public function indexAction()
    {
        $id = $this->request->get('id', 'int');
        $sport_id = $this->request->get('sp', 'int');

        $keyword = $this->request->getPost('keyword', 'string');
        $bindings = [];
        if(!empty($id)) $bindings['competition_id'] = $id;
        if(!empty($sport_id)) $bindings['sport_id'] = $sport_id;
               
        $where = "";
        if(!empty($id)) $where .= " and c.competition_id=:competition_id ";
        if(!empty($sport_id)) $where .= " and c.sport_id = :sport_id ";
    
        if(!empty($keyword)){
            $bindings['keyword'] = "%$keyword%";
            $where .= " and (game_id like :keyword or home_team like :keyword or away_team like :keyword or competition_name like :keyword) ";
        }

        $this->session->set("previous_address", $_SERVER['REQUEST_URI']);
        
        $sCompetitions = $this->getCompetitions();
        list($page, $limit, $skip) = $this->getPaginationParams();

        $matches = $this->rawQueries("SELECT c.priority, (SELECT count(DISTINCT e.sub_type_id)
         FROM event_odd e INNER JOIN odd_type o ON (o.sub_type_id = e.sub_type_id AND 
         o.parent_match_id = e.parent_match_id) WHERE e.sub_type_id not in (select sub_type_id from prematch_disabled_market where status = 1) and e.parent_match_id = m.parent_match_id
          AND o.active = 1) AS side_bets, o.sub_type_id, 
          MAX(CASE WHEN o.odd_key = m.home_team THEN odd_value END) AS home_odd,
           MAX(CASE WHEN o.odd_key = 'draw' THEN odd_value END) AS neutral_odd,
            MAX(CASE WHEN o.odd_key = m.away_team THEN odd_value END) AS away_odd,
             MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', m.home_team, 'or',  'draw') 
             THEN odd_value END) AS double_chance_1x_odd, 
             MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', 'draw ' 'or',  m.away_team) THEN odd_value END)
              AS double_chance_x2_odd, MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', m.home_team, 'or',  
              m.away_team) THEN odd_value END) AS double_chance_12_odd, MAX(CASE WHEN o.odd_key =
               'under 2.5' THEN odd_value END) AS under_25_odd, MAX(CASE WHEN o.odd_key = 'over 2.5'
                THEN odd_value END) AS over_25_odd, m.game_id, m.match_id, m.start_time,
                 m.away_team, m.home_team, m.parent_match_id,c.competition_name,c.category 
                 FROM `match` m INNER JOIN event_odd o ON m.parent_match_id = o.parent_match_id
                  INNER JOIN competition c ON c.competition_id = m.competition_id INNER JOIN sport s
                   ON s.sport_id = c.sport_id WHERE m.start_time > now() AND m.start_time 
                   < now() + interval 1 month AND o.sub_type_id IN (1,10,18) AND m.status <> 3  
                   $where GROUP BY m.parent_match_id ORDER BY m.priority DESC, c.priority DESC , 
                   m.start_time LIMIT $skip,$limit", $bindings);

        $items = $this->rawQueries("select count(*) as total FROM `match` m INNER JOIN event_odd o ON m.parent_match_id = o.parent_match_id INNER JOIN competition c ON c.competition_id = m.competition_id INNER JOIN sport s ON s.sport_id = c.sport_id WHERE m.start_time > now() AND o.sub_type_id = 1 AND m.status <> 3  $where ", $bindings);

        $total = $items['0']['total'];

        $results = [];
        foreach ($matches as $match) {
            $results[(new DateTime($match['start_time']))->format($this->getDefaultDateFormat())][] = $match;
        }

        $theBetslip = $this->session->get("betslip");

        if(!empty($id)) {

            $theCompetition = $this->rawQueries("SELECT competition_name,competition_id,category, sport_name FROM competition inner join sport using(sport_id) WHERE competition_id=? LIMIT 1", [$id]);
            $title = $theCompetition['0']['sport_name'].' - '.$theCompetition['0']['competition_name'] . ", " . $theCompetition['0']['category'];
        }else{

            $theCompetition = $this->rawQueries("SELECT sport_name FROM sport WHERE sport_id=?", [$sport_id]);
            $title = $theCompetition['0']['sport_name'].' - '.'All Competitions';
        }
        $this->tag->setTitle($title);

        $this->view->setVars([
            'today'          => $results,
            'sCompetitions'  => $this->getCompetitions(),
            'theCompetition' => $theCompetition,
            'theBetslip'     => $theBetslip,
            'pages'          => $this->getResultPages(0, $limit),
            'total'          => $total,
            'topSports'      => $this->topSports(),
            'sportId' =>$sport_id,
            'keyword' =>$keyword,
            'title' => $title,
            'winners' => $this->topWinners(),
            'competitionId'=>$id,
        ]);

        $this->view->pick("competition/threeway");
    }

}
