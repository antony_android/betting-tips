<?php

/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */
class AutobetController extends ControllerBase
{

    /**
     * Results controller to get the results, outcome hardcoded to be changed later 
     */
    public function indexAction()
    {

        $activejP = $this->rawQueries("SELECT jackpot_event_id, jackpot_type FROM jackpot_event WHERE status = 'ACTIVE' AND jp_key='AUTO' ORDER BY 1 DESC LIMIT 1");
        
        if(empty($activejP)){

            $lastAutoID = $this->rawQueries("SELECT jackpot_event_id, jackpot_type FROM jackpot_event ORDER BY 1 DESC LIMIT 1");
            $lastID = $lastAutoID['0']['jackpot_event_id'];$jackpot_type = $jackpotID['0']['jackpot_type'];
            $lastAutoType = $lastAutoID['0']['jackpot_type'];
            $lastGames = $this->rawQueries("SELECT e.odd_key, (select oc.winning_outcome from outcome oc where oc.parent_match_id = m.parent_match_id and oc.sub_type_id=45 and oc.is_winning_outcome=1 and oc.live_bet=0) AS outcome, j.game_order AS pos, jackpot_match_id,e.sub_type_id, MAX(CASE WHEN e.odd_key = m.home_team THEN  odd_value END) AS home_odd, MAX(CASE WHEN e.odd_key = 'draw' THEN odd_value END)  AS neutral_odd, MAX(CASE WHEN e.odd_key = m.away_team THEN odd_value END) AS away_odd,   m.game_id, m.match_id, m.start_time, m.parent_match_id, m.away_team, m.home_team, c.competition_name,    c.category FROM jackpot_match j LEFT JOIN `match` m ON m.parent_match_id = j.parent_match_id     INNER JOIN competition c ON m.competition_id = c.competition_id LEFT JOIN event_odd e ON e.parent_match_id      = m.parent_match_id WHERE j.jackpot_event_id='$lastID' AND e.sub_type_id=1  GROUP BY j.parent_match_id ORDER BY game_order");
        }else{

            $jackpotID = $activejP['0']['jackpot_event_id'];
            $jackpot_type = $activejP['0']['jackpot_type'];
            $games = $this->rawQueries("select e.odd_key, j.game_order as pos, jackpot_match_id,e.sub_type_id, MAX(CASE WHEN e.odd_key = m.home_team THEN odd_value END) AS home_odd, MAX(CASE WHEN e.odd_key = 'draw' THEN odd_value END) AS neutral_odd, MAX(CASE WHEN e.odd_key = m.away_team THEN odd_value END) AS away_odd, m.game_id, m.match_id, m.start_time, m.parent_match_id, m.away_team, m.home_team, c.competition_name, c.category from jackpot_match j inner join `match` m on m.parent_match_id = j.parent_match_id INNER JOIN competition c ON m.competition_id = c.competition_id inner join event_odd e on e.parent_match_id = m.parent_match_id where j.jackpot_event_id='$jackpotID' and j.status='ACTIVE'  and e.sub_type_id=1 group by j.parent_match_id order by game_order");

        }

        $theBetslip[] = '';

        $betslip = $this->session->get("betslip");

        unset($theBetslip);

        foreach ($betslip as $slip) {
            if ($slip['bet_type'] == 'jackpot') {
                $theBetslip[$slip['match_id']] = $slip;
            }
        }

        $slipCountJ = sizeof($theBetslip);

        $this->tag->setTitle('Lucky Autobet - betpalace.co.ke');

        $this->view->setVars([
            "games"      => $games,
            'winners'=>$this->topWinners(),
            'slipCountJ' => $slipCountJ,
            'theBetslip' => $theBetslip,
            'jackpotID' => $jackpotID,
            'jackpot_type' => $jackpot_type,
            "lastGames"    => $lastGames,
            'lastAutoID' => $lastID,
            'lastAutoType' => $lastAutoType,
        ]);

    }

}

