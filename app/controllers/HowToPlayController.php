<?php

/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */
class HowToPlayController extends ControllerBase
{

    /**
     * Index
     */
    public function indexAction()
    {
        $this->tag->setTitle('How to Play');
        $this->view->pick("howtoplay/index");

        $this->view->setVars([
            'data' => json_decode(file_get_contents(__DIR__ . "/../storage/how-it-works-swahili.json")),
            'winners'=>$this->topWinners(),
        ]);
    }


}

