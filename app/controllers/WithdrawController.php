<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

/**
 * Class WithdrawController
 */
class WithdrawController extends ControllerBase
{

    /**
     *
     */
    public function indexAction()
    {
        $this->tag->setTitle('Online withdrawal');
        $this->view->setVars(['winners'=>$this->topWinners()]);
    }

    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function withdrawalAction()
    {
        $amount = $this->request->getPost('amount', 'int');

        if ($amount < 50) {
            $this->flashSession->error($this->flashMessages('Sorry, minimum withdraw amount is KES. 50.'));

            return $this->response->redirect('withdraw');
            $this->view->disable();
        } else if ($amount > 5000) {
            $this->flashSession->error($this->flashMessages('Sorry, maximum withdraw amount is KES. 5,000.'));

            return $this->response->redirect('withdraw');
            $this->view->disable();
        } else {
            $mobile = $this->session->get('auth')['mobile'];

            if (!$mobile) {
                $this->flashSession->error($this->flashMessages('Kindly login in to your account first'));

                return $this->response->redirect('withdraw');
            }

            $data = [
                'amount' => $amount,
                'msisdn' => $mobile,
            ];

            $exp = time() + 3600;

            $token = $this->generateToken($data, $exp);

            $transaction = "token=$token";

            $withdraw = $this->withdraw($transaction);

            $this->flashSession->error($this->flashMessages($withdraw));

            $this->response->redirect('withdraw');

            $this->view->disable();
        }
    }

}

