<?php

/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */
class TwowayController extends ControllerBase
{

    /**
     *
    */
    public function indexAction()
    {
        $id = $this->request->get('id', 'int');

        $this->session->set("previous_address", $_SERVER['REQUEST_URI']);

        $sport_id = $this->request->get('sp', 'int');
        if(empty($sport_id)) $sport_id = 80;

        $keyword = $this->request->getPost('keyword', 'string');
        list($page, $limit, $skip) = $this->getPaginationParams();

        $bindings = [];
        if(!empty($id)) $bindings['competition_id'] = $id;
        if(!empty($sport_id)) $bindings['sport_id'] = $sport_id;

        $where = "";
        if(!empty($id)) $where .= " and c.competition_id=:competition_id ";
        if(!empty($sport_id)) $where .= " and c.sport_id = :sport_id ";

    
        if(!empty($keyword)){
            $bindings['keyword'] = "%$keyword%";
            $where .= " and (game_id like :keyword or home_team like :keyword or away_team like :keyword or competition_name like :keyword) ";
        }

        $allowed_types = ['186' =>[
            'market'=>'Winner',
            'display' => '1, 2' ]
        ];

        if(array_key_exists($sport_id,
            $this->view->defaultDisplayIds)){
            $allowed_types = $this->view->defaultDisplayIds[$sport_id] ;
        }
        $types = implode(",", array_keys($allowed_types));
       
        $matches = $this->rawQueries("select c.priority, (select count(distinct e.sub_type_id) from event_odd e inner join odd_type o on o.sub_type_id = e.sub_type_id where o.parent_match_id = m.parent_match_id and o.active = 1) as side_bets, o.sub_type_id, 
            MAX(CASE WHEN o.odd_key=m.home_team  and special_bet_value='' THEN odd_value END) AS home_odd,  
            MAX(CASE WHEN o.odd_key = m.away_team and special_bet_value='' THEN odd_value END) AS away_odd, 
            MAX(CASE WHEN o.odd_key = m.home_team and special_bet_value=1 THEN odd_value END) AS 1st_set_winner1, 
            MAX(CASE WHEN o.odd_key = m.away_team and special_bet_value=1 THEN odd_value END) AS 1st_set_winner2, 
             MAX(CASE WHEN o.odd_key = m.home_team and special_bet_value=2 THEN odd_value END) AS 2st_set_winner1, 
            MAX(CASE WHEN o.odd_key = m.away_team and special_bet_value=2 THEN odd_value END) AS 2st_set_winner2, 
            m.game_id, m.match_id, m.start_time, m.away_team, m.home_team, m.parent_match_id,c.competition_name,
            c.category from `match` m inner join event_odd o on m.parent_match_id = o.parent_match_id 
            inner join competition c on c.competition_id = m.competition_id inner join sport s on 
            s.sport_id = c.sport_id where m.start_time > now() and o.sub_type_id in ($types) and 
            m.status <> 3 $where group by m.parent_match_id order by m.priority desc, c.priority desc, 
            m.start_time limit 0, 20", $bindings);

        $items = $this->rawQueries("select count(*) as total from `match` m inner join event_odd o on
         m.parent_match_id = o.parent_match_id inner join competition c on c.competition_id = 
         m.competition_id inner join sport s on s.sport_id = c.sport_id where m.start_time > 
         now() and o.sub_type_id = 186 and m.status <> 3 $where ", $bindings);

        $total = $items['0']['total'];

        $theBetslip = $this->session->get("betslip");

        if(!empty($id)) {

            $theCompetition = $this->rawQueries("SELECT competition_name,competition_id,category, sport_name FROM competition inner join sport using(sport_id) WHERE competition_id=? LIMIT 1", [$id]);
            $title = $theCompetition['0']['sport_name'].' - '.$theCompetition['0']['competition_name'] . ", " . $theCompetition['0']['category'];
        }else{

            $theCompetition = $this->rawQueries("SELECT sport_name FROM sport WHERE sport_id=?", [$sport_id]);
            $title = $theCompetition['0']['sport_name'].' - '.'All Competitions';
        }

        $this->tag->setTitle($title);

        $results = [];


        foreach ($matches as $day) {
            $results[(new DateTime($day['start_time']))->format($this->getDefaultDateFormat())][] = $day;
        }

        $vars = [
                'today'         => $results,
                'theBetslip'    => $theBetslip,
                'sCompetitions' =>$this->getCompetitions(),
                'sCompetition' => $theCompetition,
                'total'         => $total,
                'pages'         => $this->getResultPages($total, $limit),
                'page'          => $page,
                'topSports'      => $this->topSports(),
                'sportId' =>$sport_id,
                'keyword' =>$keyword,
                'title' => $title,
                'winners' => $this->topWinners(),
                'competitionId'=>$id,
                
            ];

        $this->view->setVars($vars);
        
       
        $this->view->pick("competition/index");
    } 

    /**
     *

    public function indexAction()
    {
       $id = $this->request->get('id', 'int');

       $sport_id = $this->request->get('sp', 'int');
       if(empty($sport_id)) $sport_id = 80;

       $keyword = $this->request->getPost('keyword', 'string');
       list($page, $limit, $skip) = $this->getPaginationParams();

       $bindings = [];
       if(!empty($id)) $bindings['competition_id'] = $id;
       if(!empty($sport_id)) $bindings['sport_id'] = $sport_id;

       $where = "";
       if(!empty($id)) $where .= " and c.competition_id=:competition_id ";
       if(!empty($sport_id)) $where .= " and c.sport_id = :sport_id ";

    
       if(!empty($keyword)){
            $bindings['keyword'] = "%$keyword%";
            $where .= " and (game_id like :keyword or home_team like :keyword or away_team like :keyword or competition_name like :keyword) ";
       }

       if(!empty($id)) {

            $theCompetition = $this->rawQueries("SELECT competition_name,competition_id,category, sport_name FROM competition inner join sport using(sport_id) WHERE competition_id=? LIMIT 1", [$id]);
            $title = $theCompetition['0']['sport_name'].' - '.$theCompetition['0']['competition_name'] . ", " . $theCompetition['0']['category'];
        }else{

            $theCompetition = $this->rawQueries("SELECT sport_name FROM sport WHERE sport_id=?", [$sport_id]);
            $title = $theCompetition['0']['sport_name'].' - '.'All Competitions';
        }

        // $title = $theCompetition['0']['sport_name'] .' - '.$theCompetition['0']['competition_name'].", ".$theCompetition['0']['category'];
        // $sport_id = $theCompetition['0']['sport_id'];

        $allowed_types = ['186' =>[
            'market'=>'Winner',
            'display' => '1, 2' ]
        ];

        if(array_key_exists($sport_id,
            $this->view->defaultDisplayIds)){
            $allowed_types = $this->view->defaultDisplayIds[$sport_id] ;
        }
        $types = implode(",", array_keys($allowed_types));

       $matches = $this->rawQueries("SELECT c.priority, (SELECT count(DISTINCT e.sub_type_id)
        FROM event_odd e INNER JOIN odd_type o ON (o.sub_type_id = e.sub_type_id AND 
        o.parent_match_id = e.parent_match_id) WHERE e.parent_match_id = m.parent_match_id 
        AND o.active = 1) AS side_bets, o.sub_type_id, MAX(CASE WHEN o.odd_key = m.home_team 
        THEN odd_value END) AS home_odd, MAX(CASE WHEN o.odd_key = 'draw' THEN odd_value END)
         AS neutral_odd, MAX(CASE WHEN o.odd_key = m.away_team THEN odd_value END) AS away_odd,
          MAX(CASE WHEN o.odd_key = concat(m.home_team, ' or ', m.away_team)  THEN odd_value END)
           AS double_chance_12_odd, MAX(CASE WHEN o.odd_key = concat('draw or ', m.away_team)
             THEN odd_value END) AS double_chance_x2_odd, MAX(CASE WHEN o.odd_key = concat(m.home_team,
              ' or draw')  THEN odd_value END) AS double_chance_1x_odd, 
              MAX(CASE WHEN o.odd_key = 'over 2.5' and o.sub_type_id=18  THEN odd_value END) 
              AS over_25_odd, MAX(CASE WHEN o.odd_key = 'under 2.5' and o.sub_type_id=18  THEN 
              odd_value END) AS under_25_odd,m.game_id, m.match_id, m.start_time, m.away_team,
               m.home_team, m.parent_match_id,c.competition_name,c.category FROM `match` m 
               INNER JOIN event_odd o ON m.parent_match_id = o.parent_match_id INNER JOIN competition c
                ON c.competition_id = m.competition_id INNER JOIN sport s ON s.sport_id = c.sport_id
                 WHERE c.competition_id=? AND m.start_time > now() AND o.sub_type_id in ($types) 
                 AND m.status <> 3 GROUP BY m.parent_match_id ORDER BY m.priority DESC, c.priority
                  DESC , m.start_time LIMIT 60", [
            $id,
        ]);

        $theCompetition = $this->rawQueries("select competition_name,competition_id,category from competition where competition_id='$id' limit 1");

        $theBetslip = $this->session->get("betslip");

        $this->tag->setTitle($title);

        $results = [];

        foreach ($matches as $day) {
            $results[(new DateTime($day['start_time']))->format($this->getDefaultDateFormat())][] = $day;
        }

        $this->view->setVars([
            'today'         => $results,
            'matches'        => $matches,
            'sCompetitions' =>$this->getCompetitions(),
            'sCompetition' => $theCompetition,
            'theBetslip'     => $theBetslip,
            'sportId'        => $sport_id,
            'keyword'        => $keyword,
            'pages'         => $this->getResultPages($total, $limit),
            'topSports'      => $this->topSports(),
            'title'          => $title,
            'competitionId'=>$id,
        ]);

        $this->view->pick("competition/index");
    }
    */
}
