<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;

class RegisterForm extends Form
{

    public function initialize($entity = null, $options = null)
    {

        // Name
        $mobile = new Text('mobile');
        $name->setLabel('Mobile number');
        $name->setFilters(array('numeric'));
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'Mobile number is required'
            ))
        ));

        $this->add($name);

        // Password
        $password = new Password('password');
        $password->setLabel('Password');
        $password->addValidators(array(
            new PresenceOf(array(
                'message' => 'Password is required'
            ))
        ));
        $this->add($password);

                // Name
        $mobile = new Text('age');
        $name->setLabel('Age');
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'You must be over 18 years'
            ))
        ));

                // Name
        $mobile = new Text('terms');
        $name->setLabel('Terms');
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'You must accept terms and conditions'
            ))
        ));

        // Confirm Password
        $repeatPassword = new Password('repeatPassword');
        $repeatPassword->setLabel('Repeat Password');
        $repeatPassword->addValidators(array(
            new PresenceOf(array(
                'message' => 'Confirmation password is required'
            ))
        ));
        $this->add($repeatPassword);
    }
}