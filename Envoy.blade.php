@servers(['web' => 'bikosports.co.tz'])

@task('deploy')
    cd bikosports.co.tz
    if [ -d latest ]; then
        rm -rf latest/
    fi
    git clone --depth=1 https://murwa:mX6qgel2014@bitbucket.org/rwafula/biko-web.git latest
    cd latest && rm -rf .git
    php -v
    composer update
    {{--yarn install--}}
    {{--yarn production--}}
    cd ..
    rm -rf current
    mv latest current
    cd current
    ln -s ../.env .env
    {{--rm -rf storage/--}}
    {{--ln -s /home/mpesa/default/storage/ /home/mpesa/default/current/storage--}}
    {{--sudo chgrp -R apache storage/ bootstrap/cache/--}}
    {{--sudo chmod -R g+rwx storage/ bootstrap/cache/--}}
    {{--php artisan migrate --force--}}
    {{--rm master.tar*--}}
@endtask
