/*
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

var HtmlWebpackPlugin = require('html-webpack-plugin');

var HtmlWebpackPluginConfig = new HtmlWebpackPlugin({

	//this config will replace with header-scripts.volt
	//template: __dirname + '/app/index.html',
	//filename: 'index.html',
	//inject: 'body'

	template: '../app/views/partials/footer-scripts.volt',
	filename: 'footer-scripts.volt',
	inject: 'react'
})

module.exports = {

	entry: [

		'./app/index.js'
	],
	output: {

		//path: __dirname + '/dist',
		path: './js',
		filename: "index_bundle.js"
	},
	module: {

		loaders: [

			{test: /\.js$/, exclude: /node_modules/, loader: "babel-loader"}
		]
	},
	plugins: [HtmlWebpackPluginConfig]
}