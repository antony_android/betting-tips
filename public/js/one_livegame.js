var markets_arr = [], prev_arr = [], arr_diff = [];
var polcount = 0;
var one_game = "";
var market = "";
var market_id = "";
var odd_key = "", odd_value = "", display = "", removed_market = "";
 var state = 0;
 var addme = "";

var _blink = function() {
  var elem = $(this);
  // count the blinks
  var count = 1;
  var intervalId = setInterval(function() {
    if (elem.css('visibility') == 'hidden') {
        elem.css('visibility', 'visible');
        // increment counter when showing to count # of blinks and stop when visible
        if (count++ === 2) {
            clearInterval(intervalId);
        }
    } else {
        elem.css('visibility', 'hidden');
    }    
  }, 200);
};


var updateClock = function(server_time, match_status) {
    var parts=[], min=0, sec =0, curtime=0;
    if(server_time_global == null){
        $('.match-time-real-counter').html(match_status + "<br/> <span class='center'>-:-</span>");
        
    }else{
       parts = server_time.split(':');
       $('.match-time-real-counter').html(match_status + "<br/><span class='center'>"+parts[0]+"'</span>");
    }
    
};

var card_html = function(card, count){
    var red = count==0 ? '<i class="fa fa-square red-card"></i>&nbsp;':'';
    var yellow = count==0 ? '<i class="fa fa-square yellow-card"></i>&nbsp;':'';
    var output = '';
    for(var i=0; i<count; i++){
        if(card == 1){
            output += red;
        }else{
            output += yellow;
        }

    }
    return output;
}

var homeUpdate = function(match_info){

      var match_time = match_info.match_time;
      var splitTime = "";
      var displayTime = "";

      if(match_time != "NaN"){

        splitTime = match_time.split(":");
        displayTime = splitTime[0]+"'";
      }else{
        splitTime = ["", ""]
        displayTime = "";
      }

      if(match_info.match_status == "Halftime"){

        displayTime = "-";
      }
      
      $('.live-score-wrapper .live-goals').html(match_info.score);
      $('#mobi-match-score').html(match_info.score);

      $('.live-score-div').html(displayTime);
      $('#mobi-match-time').html(displayTime);

      $('.live-match_status').html(match_info.match_status);
      $('#mobi-match-status').html(match_info.match_status);
      
      $('.home_red_card').html(match_info.home_red_card);
      $('#mobi-home-red-card').html(match_info.home_red_card);

      $('.home_yellow_card').html(match_info.home_yellow_card);
      $('#mobi-home-yellow-card').html(match_info.home_yellow_card);

      $('.away_red_card').html(match_info.away_red_card);
      $('#mobi-away-red-card').html(match_info.away_red_card);

      $('.away_yellow_card').html(match_info.away_yellow_card);
      $('#mobi-away-yellow-card').html(match_info.away_yellow_card);

      $('.home_corner').html(match_info.home_corner);
      $('#mobi-home-corner').html(match_info.home_corner);

      $('.away_corner').html(match_info.away_corner);
      $('#mobi-away-corner').html(match_info.away_corner);

}

var available_markets = function(){
    var ids = [];
    $('.an-mkt').each(function(){
        var id = $(this).attr('rel');

        $('button[value='+id+']').each(function(){
            var option = $(this).attr('custom');
            ids.push(option.toUpperCase());
        });

    });
   

    return ids;
};

var clean_d = function(data){
    return data.replace(/ /gi,'').replace(/[^A-Za-z0-9\-]/gi, '').replace(/-+/, '-');
};


var remove_stale_markets = function(amarkets, markets_options){
    //Remove stale markets
    $.each(amarkets, function (index, option) {
        console.log("Removing market "+index + " option "+option);
        if($.inArray(option.toUpperCase(), markets_options) == -1){
            $('button[custom="'+option+'"]').css("background", "red");
        }
    });

};

var add_new_market = function(market, custom, parent_match_id, blink, suspend, picked){
    var sub_type_id = market.sub_type_id;
    var m_suspend = market.odd_active !=  1|| market.odd_value == 'NaN'||market.eactive != 1;

    
    var html = '<button '+ ((suspend ||m_suspend)?'disabled="disabled"':'') + ' class="pick market-butt col-md-6 col-xs-6 odds '
            + market.match_id+' '+custom+' '+ ((suspend ||m_suspend)?'disabled':'') + ' '+(picked?'picked':'')+'" oddtype="'+market.name+'" '
            + 'parentmatchid="'+parent_match_id+'" bettype="live" '
            + 'hometeam="'+match_info.home_team+'" awayteam="'+match_info.away_team+'" '
            + 'oddvalue="'+market.odd_value+'" custom="'+custom+'" target="javascript:;" '
            + 'id="'+market.match_id+'" odd-key="'+market.odd_key+'" value="'+market.sub_type_id+'"'
            + 'special-value-value="'+market.special_bet_value+'" '
            + 'onClick="addBet(this.id,this.value,this.getAttribute(\'odd-key\'),this.getAttribute(\'custom\'),'
            + 'this.getAttribute(\'special-value-value\'),this.getAttribute(\'bettype\'),'
            + 'this.getAttribute(\'hometeam\'),this.getAttribute(\'awayteam\'),'
            + 'this.getAttribute(\'oddvalue\'),this.getAttribute(\'oddtype\'),'
            + 'this.getAttribute(\'parentmatchid\'))" title="'+market.display+'">'
            + '<span class="label label-inverse blueish">' 
                  +market.display
            + '</span>' 
            + '<span class="label label-inverse blueish odd-value">' 
                  +((suspend ||m_suspend) ? odds_lock_html: blink + market.odd_value)
            + '</span>' 
        + '</button>'
   
    return html;

}

var odds_up_html = '<span class="odd-dirrection" ><i class="fa fa-caret-up plus-odds-white" style="font-size:13px"></i></span>&nbsp;';
var odds_down_html = '<span class="odd-dirrection" ><i class="fa fa-caret-down minus-odds-white"  style="font-size:13px" ></i></span>&nbsp;';
var odds_neutral_html = '<span class="odd-dirrection" ><i class="neut-odds"  style="font-size:12px" ></i></span>&nbsp;';
var odds_lock_html = '<span class="odd-dirrection" ><i class="fa fa-lock" ></i></span>';
var amarkets = null;
var match_info =null;
var odds_lock_html = '<span class="odd-dirrection" ><i class="fa fa-lock" ></i></span>';

var ucwords = function(str) {
    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
        return $1.toUpperCase();
    });
}



var isFloat = function(num){
    return Number(num) == num && num % 1 != 0;
}


function pollOneGame(){

    var liveMatchesHTML = "";
    var active_match = $('#active-live-match').attr('rel');

    polcount++;

    
    $.post('livematch/fetchgame?id='+active_match, function(data) {

        var live_matches = data.liveMatches;
        var markets = data.subTypes; 
        match_info = data.matchInfo;

        var theBetslip = data.theBetslip;

        var totalOdd = data.totalOdd; 
        //update total odd on betslip
        $('table.bet-table tbody tr td b').html(totalOdd);


        var parent_match_id = match_info.parent_match_id;

        var suspend = (match_info.event_status != 'Live' || match_info.match_status == 'Ended' ||
                    match_info.match_status == 'Suspended'||match_info.match_status == 'Deactivated');

        homeUpdate(match_info);
        //amarkets = available_markets();

         /** update home team **/
        server_time_global = match_info.match_time;
        updateClock(match_info.match_time,match_info.match_status );
        
        var count = 0;
        jQuery.each(live_matches, function() {

            one_game = this;

            var server_mtime = "--";
            if(one_game.match_time) {
                server_mtime = one_game.match_time;
            }
            
            liveMatchesHTML += '<a class="'+ (count%2 == 0?'even-live':'') 
                + '" href="/livematch?id='+one_game.match_id+'">'
                + '<div class="one-live">'
                    + '<span style="padding:0;">'
                    + '<img height="15" src="'+ one_game.flag_url+'"/>&nbsp;' 
                    + '</span>'
                    + '<span class="live-header">Live </span>'
                    + '<span class="live-competition"> '+one_game.sport_name+'</span> <br/>'
                    + '<div class="cmp-d">'+one_game.category+' - '+one_game.competition_name+'</div>'
                    + '<span class="live-team">'+one_game.home_team+'</span>'
                    + ' <b>vs </b>'
                    + '<span class="live-team">'+one_game.away_team+'</span>'
                    + '<br/>'
                    + '<span class="'+one_game.parent_match_id+' gametime">'
                        + one_game.match_status+' &nbsp; '
                        +server_mtime+' &nbsp; ('+one_game.score+')'
                    + '</span>'
                + '</div>'
                + '</a>'
                + '<div style="border-bottom:1px solid #FFF;"></div>';

            count++;
        });

        if( !live_matches.length == 0 ){
            $("#live_matches").html(liveMatchesHTML);
        }else{
            $("#live_matches").html('<div class="empty-row col-md-12">No Event Found</div>');
        }
        $("#live_matches").fadeIn(2000);
 

    prev_arr = markets_arr;
        var retrieved_markets = [];
        var sub_type_id = '';
        var special_bet_value ='';
        var iteration = 0;
        var markets_output = '';
        if(markets.length == 0){
            markets_output = '<div class="no-data warning col-md-12 nopadding" >No open markets for this live game at the moment! </div>'
        }

        
         jQuery.each(theBetslip, function(index, slip){
            
            var delimite = slip.parent_match_id + slip.sub_type_id+slip.special_bet_value;
            
            var curvalue  = $('span[rel="'+delimite+'"]').html();
            var newvalue = slip.odd_value;
            var addme="";

            if ( curvalue != undefined ){
            
                curvalue = curvalue.trim();

               
                if(slip.odd_value == 1){
                    addme = '<span style="color:#cc0000; font-size:11px;">Market Disabled</span>';
                    state = 10;
                }else if(typeof curvalue != undefined && curvalue != ''){
                    curvalue = parseFloat(curvalue);

                    if(curvalue > slip.odd_value && curvalue > 1){
                        addme = '<span style="color:#cc0000; font-size:11px; display:block;">Odds reduced from '+curvalue+' to '+slip.odd_value+'</span>';
                        state = 0;
                    }else if(curvalue < slip.odd_value && curvalue > 1){
                        console.log("curvalue => " +curvalue + '<<<' + slip.odd_value);
                        addme = '<span style="color:#cc0000;font-size:11px;display:block;">Odds increased from '+curvalue+' to '+slip.odd_value+'</span>';
                        state = 0;
                    }else{
                        if(state == 10){
                            addme = '<span style="color:#cc0000; font-size:11px;"></span>';
                            state = 0;
                        }else{
                            state++;
                        }
                    }

                }
            
            }else{
                if(slip.odd_value == 1){
                    addme = '<span style="color:#cc0000; font-size:11px;">Market Disabled</span>';
                    state = 10;
                }else{
                    addme = "";
                }
            }

            $('span[rel="'+delimite+'"]').html(slip.odd_value + addme);
            
            if(slip.odd_value == 1){
                 $('span[rel="'+delimite+'"]').closest('.bet-option').css("background", "#f29f7a");
            }else{
                $('span[rel="'+delimite+'"]').closest('.bet-option').css("background", "");
            }

        });



        jQuery.each(markets, function() {

            market = this;
            var theMatch = theBetslip[market.match_id];
            var picked =false;
            odd_key = market.odd_key;
            odd_value = market.odd_value;
            display = capitalizeEachWord(market.display);
            var delimite = parent_match_id + market.sub_type_id+market.special_bet_value;

            if(theMatch !== null && theMatch !== undefined){
                if(theMatch.bet_pick==market.odd_key && theMatch.sub_type_id==market.sub_type_id
                     && theMatch.special_bet_value==market.special_bet_value){
                  picked = true;
                }
            }

            // retrieved_markets.push(delimite);

    
            if(sub_type_id != market.sub_type_id){
                iteration = 0;
                var marketName = ucwords(market.name); 
                markets_output += '<div class="col-sm-12 col-xs-12 top-matches header yellow an-mkt" '
                + 'rel="'+market.sub_type_id+'">' + marketName+'</div>';
            }
            
            sub_type_id = market.sub_type_id; 
            special_bet_value=market.special_bet_value; 
            
            
            if(sub_type_id == market.sub_type_id){
                var custom = clean_d(market.match_id+market.sub_type_id+market.odd_key+market.special_bet_value);
                var blink = '';
                if(prev_arr[custom] < odd_value){
                    blink = odds_up_html;
                }else if(prev_arr[custom] == odd_value){
                    blink = odds_neutral_html;
                }else{
                    blink = odds_down_html;
                }
               
                markets_output += add_new_market(market, custom, parent_match_id, blink, suspend, picked);

              
            }

             markets_arr[custom] = market.odd_value;



        });


        $('.live-match .match-detail').html(markets_output);
 

        $('.plus-odds-white').each(_blink);
        $('.minus-odds').each(_blink);
        $('.neut-odds').each(_blink);

        //setTimeout(pollOneGame,2000);


    })
    .done(pollOneGame)
    .fail(pollOneGame);

    polcount++;

}



$(document).ready(function(){
    pollOneGame();
});


function capitalizeEachWord(str) {
    return str.replace(/\w\S*/g, function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}