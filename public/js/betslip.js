/*
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

var seconds = 5,
    loader = $(".loader"),
    errorHolder = function () {
        return $(".betslip-error");
    },
    successHolder = function () {
        return $(".betslip-success");
    };

function hasTouch() {
    return 'ontouchstart' in document.documentElement
        || navigator.maxTouchPoints > 0
        || navigator.msMaxTouchPoints > 0;
}

$(window).scroll(function () {
    var holder = $('#slip-holder');
    if ($(document).scrollTop() > 50) {
        $('#shrink-header').addClass('shrink-header');
        holder.addClass('affix');
        if (!hasTouch()) {
            $('#right-generic').addClass('shrink-header');
        }
    } else {
        holder.removeClass('affix');
        $('#shrink-header').removeClass('shrink-header');
        if (!hasTouch()) {
            $('#right-generic').removeClass('shrink-header');
        }
    }
});


jQuery(document).ready(function ($) {
    $(".clickable-row").click(function () {
        window.document.location = $(this).data("href");
    });
});


$("#slip-button").click(function () {
    $("#slip-holder").slideDown("fast", function () {
    });

    $("#slip-holder").addClass('hahahaa');

});

$("#place_bet_button").click(function () {

    $("#slip-holder").addClass('hahahaa');

});


$("#slip-button-close").click(function () {
    $("#slip-holder").slideUp("fast", function () {
    });
});



function refreshSlip() {
    //refreshJackSlip();
    //window.onload();
    $.get("matches/betslip", {}, function (data) {
        var selector = $("#betslip");
        selector.animate({opacity: '0.8'});
        selector.html(data);
        selector.animate({opacity: '1'});
    }).done(function () {
        loader.css("display", "none");
    });
}


function refreshShopSlip() {
    window.onload();
    $.get("/shop/betslip", {}, function (data) {
        var selector = $("#betslip-shop");
        selector.animate({opacity: '0.8'});
        selector.html(data);
        selector.animate({opacity: '1'});
    }).done(function () {
        loader.css("display", "none");
    });
}

function refreshSlipWithMessage(message) {
    refreshJackSlip();
    window.onload();
    $.get("matches/betslip", {}, function (data) {
        var selector = $("#betslip");
        selector.animate({opacity: '0.8'});
        //data += '<div class="alert alert-danger alert-dismissible ss betslip-error" role="alert" style="display: block;">'+message+'</div>';
        selector.html(data);
        successHolder().html("");
        errorHolder().html(message.message);
        selector.animate({opacity: '1'});
        errorHolder().slideDown("slow");
        if(message.bet_amount != undefined){
            $('input#bet_amount').val(message.bet_amount);
        }

    }).done(function () {
        loader.css("display", "none");
    });
}

function refreshJackSlip() {
    $.post("matches/jackpot", {}, function (data) {
        $("#betslipJ").animate({opacity: '0.8'});
        $("#betslipJ").html(data);
        $("#betslipJ").animate({opacity: '1'});
    }).done(function () {
        loader.css("display", "none");
    });
}

function refreshPeerSlip() {

    $.post("matches/peer", {}, function (data) {
        $("#betslip-peer").animate({opacity: '0.8'});
        $("#betslip-peer").html(data);
        $("#betslip-peer").animate({opacity: '1'});
    }).done(function () {
        loader.css("display", "none");
    });
}

function refreshNewPeerSlip() {

    $.post("matches/peernew", {}, function (data) {
        
        $("#betslip-peer-new").animate({opacity: '0.8'});
        $("#betslip-peer-new").html(data);
        $("#betslip-peer-new").animate({opacity: '1'});
        $("#peer_invite").prop("readonly", false);
        $("#bet_amount").prop("readonly", false);
    }).done(function () {
        loader.css("display", "none");
    });
}

function refreshPingaPeerSlip() {

    $.post("matches/peernew", {}, function (data) {
        
        $("#betslip-peer-new").animate({opacity: '0.8'});
        $("#betslip-peer-new").html(data);
        $("#betslip-peer-new").animate({opacity: '1'});
        $("#peer_invite").prop("readonly", true);
        $(".peer-invite-friends").prop("readonly", true);
        $("#bet_amount").prop("readonly", true);
    }).done(function () {
        loader.css("display", "none");
    });
}

function refreshPeerWithMessage(message) {
    refreshJackSlip();
    window.onload();
    $.get("matches/peernew", {}, function (data) {
        var selector = $("#betslip-peer-new");
        selector.animate({opacity: '0.8'});
        data += '<div class="alert alert-danger alert-dismissible ss betslip-error" role="alert" style="display: block;">'+message+'</div>';
        selector.html(data);
        
        selector.animate({opacity: '1'});
    }).done(function () {
        loader.css("display", "none");
    });
}


function refreshSGRWithMessage(message) {
    refreshJackSlip();
    window.onload();
    $.get("matches/peernew", {}, function (data) {
        var selector = $("#betslip-sgr");
        selector.animate({opacity: '0.8'});
        data += '<div class="alert alert-danger alert-dismissible ss betslip-error" role="alert" style="display: block;">'+message+'</div>';
        selector.html(data);
        
        selector.animate({opacity: '1'});
    }).done(function () {
        loader.css("display", "none");
    });
}


function refreshAutobetSlip() {

    $.post("matches/autobet", {}, function (data) {
        
        $("#betslip-autobet").animate({opacity: '0.8'});
        $("#betslip-autobet").html(data);
        $("#betslip-autobet").animate({opacity: '1'});
    }).done(function () {
        loader.css("display", "none");
    });
}

function refreshSGRSlip(){

    window.onload();
    $.get("matches/sgr", {}, function (data) {
        var selector = $("#betslip-sgr");
        selector.animate({opacity: '0.8'});
        selector.html(data);
        selector.animate({opacity: '1'});
    }).done(function () {
        loader.css("display", "none");
    });
}

function refreshBingwaFour() {
    $.post("matches/correct", {}, function (data) {
        $("#betslipB").animate({opacity: '0.8'});
        $("#betslipB").html(data);
        $("#betslipB").animate({opacity: '1'});
    }).done(function () {
        loader.css("display", "none");
    });
}


function addShopBet(value, sub_type_id, odd_key, custom, special_bet_value, bet_type, home, away, odd, oddtype, parentmatchid, pos, live) {
    
    var self = this;
    if ($('[custom="' + custom + '"]').hasClass('picked')) {
        return removeShopMatch(value);
    }
    loader.slideDown("slow");

    $.post("add", {
        match_id: value,
        sub_type_id: sub_type_id,
        odd_key: odd_key,
        custom: custom,
        special_bet_value: special_bet_value,
        bet_type: bet_type,
        home: home,
        away: away,
        odd: odd,
        oddtype: oddtype,
        parentmatchid: parentmatchid,
        pos: pos
    }, function (data) {
            $("." + value).removeClass('picked');
            $(self).addClass('picked');
            $("." + custom).addClass('picked');
            refreshShopSlip();
            $(".shop-slip-counter").html(data.total);
    });
}

function addBet(value, sub_type_id, odd_key, custom, special_bet_value, bet_type, home, away, odd, oddtype, parentmatchid, pos, live) {
    var self = this;
    if ($('[custom="' + custom + '"]').hasClass('picked')) {
        return removeMatch(value);
    }
    loader.slideDown("slow");

    $.post("betslip/add", {
        match_id: value,
        sub_type_id: sub_type_id,
        odd_key: odd_key,
        custom: custom,
        special_bet_value: special_bet_value,
        bet_type: bet_type,
        home: home,
        away: away,
        odd: odd,
        oddtype: oddtype,
        parentmatchid: parentmatchid,
        pos: pos
    }, function (data) {
        $("." + value).removeClass('picked');
        $(self).addClass('picked');
        $("." + custom).addClass('picked');
        if (bet_type === 'jackpot') {
            refreshJackSlip();
        } else if (bet_type === 'autobet') {
            refreshAutobetSlip();
        } else {
            refreshSlip();
            $(".slip-counter").html(data.total);
        }
    });
}


function addSgrBet(value, sub_type_id, odd_key, custom, special_bet_value, bet_type, home, away, odd, oddtype, parentmatchid, pos, live) {
    var self = this;
    if ($('[custom="' + custom + '"]').hasClass('picked')) {
        return removeSGRMatch(value);
    }
    loader.slideDown("slow");

    $.post("betslip/addsgr", {
        match_id: value,
        sub_type_id: sub_type_id,
        odd_key: odd_key,
        custom: custom,
        special_bet_value: special_bet_value,
        bet_type: bet_type,
        product:"SGR",
        home: home,
        away: away,
        odd: odd,
        oddtype: oddtype,
        parentmatchid: parentmatchid,
        pos: pos
    }, function (data) {
        $("." + value).removeClass('picked');
        $(self).addClass('picked');
        $("." + custom).addClass('picked');
        refreshSGRSlip();
        $(".slip-counter").html(data.total);
    });
}

var previous_selected_peer = "";

function addPeerBet(value, sub_type_id, odd_key, custom, special_bet_value, bet_type, home, away, odd, oddtype, parentmatchid, pos) {
    
    var self = this;

    if ($('[custom="' + custom + '"]').hasClass('picked')) {

        return removePeerMatch(value);
    }

    loader.slideDown("slow");
    $.post("betslip/addnewpeer", {
        match_id: value,
        sub_type_id: sub_type_id,
        odd_key: odd_key,
        custom: custom,
        special_bet_value: special_bet_value,
        is_peer: 1,
        bet_type: bet_type,
        home: home,
        away: away,
        odd: odd,
        oddtype: oddtype,
        parentmatchid: parentmatchid,
        pos: pos
    }, function (data) {
        if(previous_selected_peer.length > 0){

            $('[custom="' + previous_selected_peer + '"]').removeClass('picked');
        }
        $(self).addClass('picked');
        $('[custom="' + custom + '"]').addClass('picked');
        previous_selected_peer = custom;
        refreshNewPeerSlip();
        $(".slip-counter").html(data.total);
    });
}

function pingaPeerBet(value, sub_type_id, odd_key, custom, special_bet_value, bet_type, home, away, odd, oddtype, parentmatchid, peer_bet_id, peer_msisdn, friend_pick, bet_amount) {
    
    var self = this;

    if ($('[custom="' + custom + '"]').hasClass('picked')) {

        return removePeerMatch(value);
    }

    loader.slideDown("slow");
    $.post("betslip/betagainst", {
        match_id: value,
        sub_type_id: 10,
        odd_key: odd_key,
        bet_amount:bet_amount,
        custom: custom,
        special_bet_value: special_bet_value,
        is_peer: 1,
        bet_type: bet_type,
        home: home,
        away: away,
        odd: odd,
        oddtype: oddtype,
        parentmatchid: parentmatchid,
        pos: 1,
        peer_bet_id:peer_bet_id, 
        peer_msisdn:peer_msisdn,
        friend_pick:friend_pick
    }, function (data) {

        if(previous_selected_peer.length > 0){

            $('[custom="' + previous_selected_peer + '"]').removeClass('picked');
        }
        $(self).addClass('picked');
        $('[custom="' + custom + '"]').addClass('picked');
        previous_selected_peer = custom;
        refreshPingaPeerSlip();
    });
}

function removeMatch(value) {
    loader.slideDown("slow");
    $.post("betslip/remove", {match_id: value}, function (data) {
        console.log(data);
        var counterEl = $(".slip-counter"),
            count = (data && data.count) ? data.count : (counterEl.html() - 1);
        counterEl.html(count);
        refreshSlip();
        //refreshJackSlip();
        $("." + value).removeClass('picked');
    });
}


function removeSGRMatch(value) {
    loader.slideDown("slow");
    $.post("betslip/removesgr", {match_id: value}, function (data) {
        console.log(data);
        var counterEl = $(".slip-counter"),
            count = (data && data.count) ? data.count : (counterEl.html() - 1);
        counterEl.html(count);
        refreshSGRSlip();
        $("." + value).removeClass('picked');
    });
}

function removePeerMatch(value) {
    loader.slideDown("slow");
    $.post("betslip/removepeer", {match_id: value}, function (data) {
        console.log(data);
        var counterEl = $(".peerslip-counter"),
            count = (data && data.count) ? data.count : (counterEl.html() - 1);
        counterEl.html(count);
        refreshPeerSlip();
        refreshNewPeerSlip();
        $("." + value).removeClass('picked');
    });
}


function removeShopMatch(value) {
    loader.slideDown("slow");
    $.post("/shop/remove", {match_id: value}, function (data) {
        console.log(data);
        var counterEl = $(".shop-slip-counter"),
            count = (data && data.count) ? data.count : (counterEl.html() - 1);
        counterEl.html(count);
        refreshSlip();
        $("." + value).removeClass('picked');
    });
}

function clearSlip() {
    loader.slideDown("slow");
    $.post("betslip/clearslip", {}, function (data) {
        $(".slip-counter").html(0);
        refreshSlip();
        $(".picked").removeClass('picked');
    });
}


function clearShopSlip() {
    loader.slideDown("slow");
    $.post("clearslip", {}, function (data) {
        $(".shop-slip-counter").html(0);
        refreshSlip();
        $(".picked").removeClass('picked');
    });
}

function clearPeerSlip() {
    loader.slideDown("slow");
    $.post("betslip/clearpeerslip", {}, function (data) {
        $(".slip-counter").html(0);
        refreshNewPeerSlip();
        refreshPingaPeerSlip();
        $(".picked").removeClass('picked');
    });
}

function clearSGRSlip() {
    loader.slideDown("slow");
    $.post("betslip/clearsgrslip", {}, function (data) {
        $(".slip-counter").html(0);
        refreshSGRSlip();
        $(".picked").removeClass('picked');
    });
}

function winnings() {
    var value = $("#bet_amount").val();
    var odds = $("#total_odd").val();
    var total = (value * odds)/1.2;
    var stakeaftertax = value/1.2;

    if(total > 70000){
        total = 70000;
    }
    var winnings = total,
    tax = 20 * (winnings-stakeaftertax) / 100,
    net = (winnings - tax);

    $("#stake-after-tax").html(stakeaftertax.toFixed(2));
    $("#pos_win").html(winnings.toFixed(2));
    $("#tax").html(tax.toFixed(2));
    $("#net-amount").html(net.toFixed(2));
}


function winningsM() {
    var value = $("#bet_amount_m").val();
    var odds = $("#total_odd_m").val();
    var totalWin = value * odds;
    var totalWin = Math.round(totalWin);
    $("#pos_win_m").html(totalWin);
}

function getPaymentAccount() {
    return (($('input[name="payment_method"]:checked').val() || 1) * 1) !== 1 ? 0 : 1;
}

function getMsisdn() {

    var msisdn= $('#msisdn').val();
    var _msisdn = $('#msisdn-mobile').val();
    if(msisdn==undefined ||msisdn=='undefined'||msisdn==null ||msisdn=="" ||msisdn.length==0){
        return _msisdn;
    }
    return msisdn; 
}

function validMsisdn(phoneNumber) {
    return /^(?:\+?(?:[1-9]{3})|0)?([6,7]([0-9]{8}))$/.test(phoneNumber);
}
function showSlipError(message) {
    loader.css("display", "none");
    return errorHolder().slideDown("slow").html(message);
}
function betSuccessAnalytics(bet_amount) {
    console.log('Send analytics for bet amount' + bet_amount);
    ga('send', {
        hitType: 'event',
        eventCategory: 'bet',
        eventAction: 'success',
        eventLabel: 'bet_success',
        eventValue: bet_amount,
        nonInteraction: true
    });
    dataLayer.push({
        'event': 'bet_success',
        'eventAmount': bet_amount
    });
    console.log('Analytics sent');
}



function sleep(delay) {
    var start = new Date().getTime();
    while (new Date().getTime() < start + delay);
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function autobet(event) {

    console.log("called auto bet " + event); 
    event.preventDefault();
    loader.slideDown("slow");
    
    var user_id = $("#user_id").val(),
        bet_amount = $("#bet_amount").val(),
        jackpot_type = $("#jackpot_type").val(),
        jackpot_id = $("#jackpot_id").val(),
        account = getPaymentAccount(),
        phoneNumber = getMsisdn();

    if (!user_id) {
        if (account) {
            loader.css("display", "none");
            return $("#quick-login").slideDown("slow");
        } else {
            // Here, we allow the user to bet anonymously
            // However, we'll still need their phone number
            if (!phoneNumber) {
                return showSlipError('Please provide your phone number');
            } else if (!validMsisdn(phoneNumber)) {
                return showSlipError("Please use a valid phone number");
            }
        }
    }

    var selections = [];
    var picks = ['1', 'X', '2'];

    var i;
    for (i = 0; i < 6; i++) {

        var pick = Math.floor(Math.random()*3);  
        selections.push(picks[pick]);
        $('.auto-pick'+i).html(picks[pick]);

        var match_id = $("#game"+i).attr("match_id");
        var hometeam = $("#game"+i).attr("hometeam");
        var oddtype = $("#game"+i).attr("oddtype");
        var special_bet_value = $("#game"+i).attr("special_bet_value");
        var bettype = $("#game"+i).attr("bettype");
        var awayteam = $("#game"+i).attr("awayteam");
        var odd_key = $("#game"+i).attr("odd_key");
        var oddvalue = (pick == 0)? $("#game"+i).attr("home_odd")
        :((pick == 1)?$("#game"+i).attr("neutral_odd"):$("#game"+i).attr("away_odd"));
        var parentmatchid = $("#game"+i).attr("parentmatchid");
        var pos = $("#game"+i).attr("pos");
        var jackpot_id = $("#game"+i).attr("jackpot_id");
        var jackpot_type = $("#game"+i).attr("jackpot_type");

        $.post("betslip/add", {
            match_id: match_id,
            sub_type_id: 1,
            odd_key: odd_key,
            special_bet_value: special_bet_value,
            bet_type: "jackpot",
            home: hometeam,
            away: awayteam,
            odd: oddvalue,
            jackpot_event_id: jackpot_id,
            jackpot_type: jackpot_type,
            oddtype: oddtype,
            parentmatchid: parentmatchid,
            pos: pos
        }, function (data) {
        
    });
        await sleep(1000);            

    }

    return $.post("betslip/betJackpot", {
        user_id: user_id,
        jackpot_type: jackpot_type,
        jackpot_id: jackpot_id,
        account: account,
        msisdn: phoneNumber
    }, function (data) {
        if (data.status_code == '421') {
            loader.css("display", "none");
            errorHolder().html(data.message);
            errorHolder().slideDown("slow");
        }

        if (data.status_code == '500') {
            loader.css("display", "none");
            errorHolder().html(data.message);
            errorHolder().slideDown("slow");
        }

        if (data.status_code == '201') {
            successHolder().slideDown("slow");
            successHolder().html(data.message);
            refreshJackSlip();
            refreshBingwaFour()
            $(".picked").removeClass('picked');
        }
    }, 'json');
}



function placeBet() {
    loader.slideDown("slow");
    var user_id = $("#user_id").val(),
        bet_amount = $("#bet_amount").val(),
        total_odd = $("#total_odd").val(),
        account = getPaymentAccount(),
        phoneNumber = getMsisdn();
    if (!user_id) {
        if (account) {
            loader.css("display", "none");
            return $("#quick-login").slideDown("slow");
        } else {
            // Here, we allow the user to bet anonymously
            // However, we'll still need their phone number
            if (!phoneNumber) {
                return showSlipError('Please provide your phone number');
            } else if (!validMsisdn(phoneNumber)) {
                return showSlipError("Please use a valid phone number");
            }
        }
    }
    if (bet_amount < 19) {
        return showSlipError("The minimum bet amount is KShs. 19");
    }
    // We good to go...
    // Hit the web...API (afterwards);
    $.post("betslip/placebet", {
        user_id: user_id,
        bet_amount: bet_amount,
        total_odd: total_odd,
        account: account,
        msisdn: phoneNumber
    }, function (data) {
        if (data.status_code === 421) {
            loader.css("display", "none");
            console.log('Status 421 .. calling refresh bet_slip');
            refreshSlipWithMessage(data);
        }

        if (data.status_code === 500) {
            loader.css("display", "none");
            successHolder().html("");
            errorHolder().html(data.message);
            errorHolder().slideDown("slow");
        }

        if (data.status_code === 201) {
            // var eventAmount = 2000;
            errorHolder().html("");
            successHolder().slideDown("slow").html(data.message);
            //betSuccessAnalytics(bet_amount);
            return clearSlip();
        }
    }, 'json');
}


function placeSGRBet() {
    loader.slideDown("slow");
    var user_id = $("#user_id").val(),
        bet_amount = $("#bet_amount").val(),
        total_odd = $("#total_odd").val(),
        account = getPaymentAccount(),
        phoneNumber = getMsisdn();
    if (!user_id) {
        if (account) {
            loader.css("display", "none");
            return $("#quick-login").slideDown("slow");
        } else {
            // Here, we allow the user to bet anonymously
            // However, we'll still need their phone number
            if (!phoneNumber) {
                return showSlipError('Please provide your phone number');
            } else if (!validMsisdn(phoneNumber)) {
                return showSlipError("Please use a valid phone number");
            }
        }
    }
    if (bet_amount < 19) {
        return showSlipError("The minimum bet amount is KShs. 19");
    }
    // We good to go...
    // Hit the web...API (afterwards);
    $.post("betslip/placesgrbet", {
        user_id: user_id,
        bet_amount: bet_amount,
        total_odd: total_odd,
        account: account,
        msisdn: phoneNumber
    }, function (data) {
        if (data.status_code === 421) {
            loader.css("display", "none");
            console.log('Status 421 .. calling refresh bet_slip');
            refreshSlipWithMessage(data);
        }

        if (data.status_code === 500) {
            loader.css("display", "none");
            successHolder().html("");
            errorHolder().html(data.message);
            errorHolder().slideDown("slow");
        }

        if (data.status_code === 201) {
            // var eventAmount = 2000;
            errorHolder().html("");
            successHolder().slideDown("slow").html(data.message);
            //betSuccessAnalytics(bet_amount);
            return clearSGRSlip();
        }
    }, 'json');
}


function placeShopBet() {
    loader.slideDown("slow");
    var user_id = $("#user_id").val(),
        bet_amount = $("#bet_amount").val(),
        total_odd = $("#total_odd").val(),
        account = getPaymentAccount(),
        phoneNumber = getMsisdn();
        console.log("method call success");
        console.log("data prepared id="+user_id+" bet amount="+bet_amount+" total odd="+total_odd+" account="+account+" phone="+phoneNumber);
    if (!user_id) {
        console.log("no user id ");
        if (account) {
            console.log("no user account");
            loader.css("display", "none");
            return $("#quick-login").slideDown("slow");
        } else {
            console.log("payment account found testing phone number");
            // Here, we allow the user to bet anonymously
            // However, we'll still need their phone number
            if (!phoneNumber) {
                console.log("no phone number");
                errorHolder().html("");
                successHolder().slideDown("slow").html('<span style="color:#EF4323">Please provide your phone number</span>');
                return;
            } else if (!validMsisdn(phoneNumber)) {
                console.log("invalid phone number");
                errorHolder().html("");
                successHolder().slideDown("slow").html('<span style="color:#EF4323">Please provide your phone number</span>');
                return;
            }
        }
    }
    if (bet_amount < 50) {
        console.log("Bet amount less than 50 bob");
        errorHolder().html("");
        successHolder().slideDown("slow").html('<span style="color:#EF4323">The minimum bet amount is KShs. 50</span>');
        return;
    }
    // We good to go...
    // Hit the web...API (afterwards);");
    console.log("pre-validation tests passed. submitting request to server");
    console.log("call to place bet initiated");
    $.post("/shop/placebet", {
        user_id: user_id,
        bet_amount: bet_amount,
        total_odd: total_odd,
        src:"internet",
        account: account,
        msisdn: phoneNumber
    }, function (data) {

        console.log("response code is "+data.status_code);
        if (data.status_code === 421) {
            loader.css("display", "none");
            console.log('Status 421 .. calling refresh bet_slip');
            refreshSlipWithMessage(data);
        }

        if (data.status_code === 500) {

            errorHolder().html("");
            successHolder().slideDown("slow").html('<span style="color:#EF4323">'+data.message+'</span>');
        }

        if (data.status_code === 201) {
            // var eventAmount = 2000;
            errorHolder().html("");
            successHolder().slideDown("slow").html(data.message);
            //betSuccessAnalytics(bet_amount);
            console.log("Bet ID "+data.bet_id);
            var url = "/shop/printbet?id="+data.bet_id;
            clearShopSlip();
            window.open(url, '_blank');
        }
    }, 'json');
}


function payShopBet(bet_id) {

    console.log("Bet ID is "+bet_id);
    if(confirm("Are you sure you want to pay this bet? Please note that this is irreversible.")){ 
        
        console.log("Proceeding to process");

        $.post("/shop/paybet", {
            bet_id: bet_id
        }, function (data) {

            console.log("Got status code "+data.status_code);
            if (data.status_code === 201) {
                loader.css("display", "none");
                successHolder().slideDown("slow").html(data.message);
                location.reload();
            }if (data.status_code === 421) {
                loader.css("display", "none");
                successHolder().html("");
                errorHolder().html(data.message);
                errorHolder().slideDown("slow");
            }
        }, 'json');

    } else{
        return false; 
    }
}


function placePeerBet() {

    loader.slideDown("slow");
    var user_id = $("#user_id").val(),
        bet_amount = $("#bet_amount").val(),
        is_peer = 1,
        account = getPaymentAccount(),
        peer_bet_id = $("#peer_bet_id").val(),
        bet_type = $("#bet_type").val(),
        phoneNumber = getMsisdn();
        total_odd = $("#total_odd_original").val();
        peer_msisdn = $("#peer_invite").val();

    if (!user_id) {
        if (account) {
            loader.css("display", "none");
            return $("#quick-login").slideDown("slow");
        } else {
            // Here, we allow the user to bet anonymously
            // However, we'll still need their phone number
            if (!phoneNumber) {
                return showSlipError('Please provide your phone number');
            } else if (!validMsisdn(phoneNumber)) {
                return showSlipError("Please use a valid phone number");
            }
        }
    }
    if (bet_amount < 19) {
        return showSlipError("The minimum bet amount is KShs. 19");
    }
    // We good to go...
    // Hit the web...API (afterwards);

    console.log("User ID: "+user_id);
    console.log("Bet Amount: "+bet_amount);
    console.log("Peer: "+is_peer);
    console.log("Account: "+account);
    console.log("Peer Bet ID: "+peer_bet_id);
    console.log("MSISDN: "+phoneNumber);
    console.log("Bet Type: "+bet_type);
    console.log("Peer MSISDN: "+peer_msisdn);

    $.post("betslip/placepeerbet", {
        user_id: user_id,
        bet_amount: bet_amount,
        total_odd: total_odd,
        account: account,
        is_peer: is_peer,
        bet_type: bet_type,
        peer_bet_id:peer_bet_id,
        peer_msisdn: peer_msisdn,
        msisdn: phoneNumber
    }, function (data) {
        if (data.status_code === 421) {
            loader.css("display", "none");
            console.log(data.message);
            refreshPeerWithMessage(data.message);
        }
        if (data.status_code === 321) {
            loader.css("display", "none");
            console.log(data.message);
            refreshPeerWithMessage(data.message);
        }

        if (data.status_code === 500) {
            loader.css("display", "none");
            errorHolder().html(data.message);
            errorHolder().slideDown("slow");
        }

        if (data.status_code === 201 || data.status_code === 200) {
            successHolder().slideDown("slow").html(data.message);
            return clearPeerSlip();
        }
    }, 'json');
}


function jackpotBet() {
    loader.slideDown("slow");
    var user_id = $("#user_id").val(),
        bet_amount = $("#bet_amount").val(),
        jackpot_type = $("#jackpot_type").val(),
        jackpot_id = $("#jackpot_id").val(),
        account = getPaymentAccount(),
        phoneNumber = getMsisdn();
    if (!user_id) {
        if (account) {
            loader.css("display", "none");
            return $("#quick-login").slideDown("slow");
        } else {
            // Here, we allow the user to bet anonymously
            // However, we'll still need their phone number
            if (!phoneNumber) {
                return showSlipError('Please provide your phone number');
            } else if (!validMsisdn(phoneNumber)) {
                return showSlipError("Please use a valid phone number");
            }
        }
    }
    return $.post("betslip/betJackpot", {
        user_id: user_id,
        jackpot_type: jackpot_type,
        jackpot_id: jackpot_id,
        account: account,
        msisdn: phoneNumber
    }, function (data) {
        if (data.status_code == '421') {
            loader.css("display", "none");
            errorHolder().html(data.message);
            errorHolder().slideDown("slow");
        }

        if (data.status_code == '500') {
            loader.css("display", "none");
            errorHolder().html(data.message);
            errorHolder().slideDown("slow");
        }

        if (data.status_code == '201') {
            successHolder().slideDown("slow");
            successHolder().html(data.message);
            refreshJackSlip();
            refreshBingwaFour()
            $(".picked").removeClass('picked');
        }
    }, 'json');
}

$(document).on('change', 'input[name="payment_method"]', function () {
    var selector = $('#wallet-phone-number');
    $('.bet-controls').addClass('hidden');
    if ($(this).val() * 1 !== 1) {
        return selector.slideDown('slow', function () {
            $(this).removeClass('hide');
            $('#bet-controls-bet').removeClass('hidden');
        });
    } else {
        if (!$("#user_id").val()) {
            $('#bet-controls-login').removeClass('hidden');
        } else {
            $('#bet-controls-bet').removeClass('hidden');
        }
    }
    return $('.betslip-error').slideUp('slow') && selector.slideUp('slow', function () {
            $(this).addClass('hide');
        });
});

jQuery(document).ready(function ($) {
    $(".clickable-row").click(function () {
        window.document.location = $(this).data("href");
    });
    /** Handle tabs on home page **/

    $(document).on( 'shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
        e.preventDefault();
      
        var url = $(this).attr("data-url");
        var href = this.hash;
        var pane = $(this);
        
        // ajax load from data-url
        $(href).load(url,function(result){      
            pane.tab('show');
        });
    });

    /** End handle tabs**/

    

});

    // function scroll(){
    //     var id_passed = 10;
    //     var element_ = document.getElementById("scroll-div");
    //     element_.style.marginLeft = "-100px";
    //     id_passed = id_passed - 10

    // }    
    // setInterval(scroll, 100);

    // var app = new Vue({
    //     el: '#app',
    //     data: {
    //       message: 'Hello Vue!'
    //     }
    //   })