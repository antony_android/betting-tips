/*
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

// Customise those settings

var divid = "betslip";
var url = "/matches/betslip";

var shopdivid = "betslip-shop";
var url_shop = "/shop/betslip";

var sgrdivid = "betslip-sgr";
var url_sgr = "/matches/sgr";

var divnp = "betslip-peer-new";
var urlnp = "/matches/peernew";

var divp = "betslip-peer";
var urlp = "/matches/peer";

var divj = "betslipJ";
var urlj = "/matches/jackpot";

var divb = "betslipB";
var urlb = "/matches/correct";

var divau = "betslip-autobet";
var urlau = "/matches/autobet";

var thediv = "matches";
var dataUrl = "/matches";

////////////////////////////////
//
// Refreshing the DIV
//
////////////////////////////////

var locations = window.location.href.split("/").pop();

function refreshdiv() {

// The XMLHttpRequest object

    var xmlHttp;
    try {
        xmlHttp = new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
    }
    catch (e) {
        try {
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
        }
        catch (e) {
            try {
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) {
                alert("Your browser does not support AJAX.");
                return false;
            }
        }
    }

    // Timestamp for preventing IE caching the GET request

    fetch_unix_timestamp = function () {
        return parseInt(new Date().getTime().toString().substring(0, 10))
    }

    var nocacheurl = url;

    // The code...

    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4) {
            var div = document.getElementById(divid);
            if (div) {
                div.innerHTML = xmlHttp.responseText;
            }
        }
    }
    xmlHttp.open("GET", nocacheurl, true);
    xmlHttp.send(null);
}

function refreshshopdiv() {

// The XMLHttpRequest object

    var xmlHttp;
    try {
        xmlHttp = new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
    }
    catch (e) {
        try {
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
        }
        catch (e) {
            try {
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) {
                alert("Your browser does not support AJAX.");
                return false;
            }
        }
    }

    // Timestamp for preventing IE caching the GET request

    fetch_unix_timestamp = function () {
        return parseInt(new Date().getTime().toString().substring(0, 10))
    }

    var nocacheurl = url_shop;

    // The code...

    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4) {
            var div = document.getElementById(shopdivid);
            if (div) {
                div.innerHTML = xmlHttp.responseText;
            }
        }
    }
    xmlHttp.open("GET", nocacheurl, true);
    xmlHttp.send(null);
}

function refreshData() {

// The XMLHttpRequest object

    var xHttp;
    try {
        xHttp = new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
    }
    catch (e) {
        try {
            xHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
        }
        catch (e) {
            try {
                xHttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) {
                alert("Your browser does not support AJAX.");
                return false;
            }
        }
    }

    var nocacheDataurl = dataUrl;

    // The code...

    xHttp.onreadystatechange = function () {
        if (xHttp.readyState == 4) {
            document.getElementById(thediv).innerHTML = xHttp.responseText;
        }
    }
    xHttp.open("GET", nocacheDataurl, true);
    xHttp.send(null);
}

function refreshSlipJ() {

// The XMLHttpRequest object

    var xHttp;
    try {
        xHttp = new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
    }
    catch (e) {
        try {
            xHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
        }
        catch (e) {
            try {
                xHttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) {
                alert("Your browser does not support AJAX.");
                return false;
            }
        }
    }

    var nocacheDataurl = urlj;

    // The code...

    xHttp.onreadystatechange = function () {
        if (xHttp.readyState === 4) {
            document.getElementById(divj).innerHTML = xHttp.responseText;
        }
    }
    xHttp.open("POST", nocacheDataurl, true);
    xHttp.send(null);
}

function refreshSlipP() {

// The XMLHttpRequest object

    var xHttp;
    try {
        xHttp = new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
    }
    catch (e) {
        try {
            xHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
        }
        catch (e) {
            try {
                xHttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) {
                alert("Your browser does not support AJAX.");
                return false;
            }
        }
    }

    var nocacheDataurl = urlp;

    // The code...

    xHttp.onreadystatechange = function () {
        if (xHttp.readyState === 4) {
            document.getElementById(divp).innerHTML = xHttp.responseText;
        }
    }
    xHttp.open("POST", nocacheDataurl, true);
    xHttp.send(null);
}

function refreshSlipNP() {

// The XMLHttpRequest object

    var xHttp;
    try {
        xHttp = new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
    }
    catch (e) {
        try {
            xHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
        }
        catch (e) {
            try {
                xHttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) {
                alert("Your browser does not support AJAX.");
                return false;
            }
        }
    }

    var nocacheDataurl = urlnp;

    // The code...

    xHttp.onreadystatechange = function () {
        if (xHttp.readyState === 4) {
            document.getElementById(divnp).innerHTML = xHttp.responseText;
        }
    }
    xHttp.open("POST", nocacheDataurl, true);
    xHttp.send(null);
}

function refreshSlipB() {

// The XMLHttpRequest object

    var xHttp;
    try {
        xHttp = new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
    }
    catch (e) {
        try {
            xHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
        }
        catch (e) {
            try {
                xHttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) {
                alert("Your browser does not support AJAX.");
                return false;
            }
        }
    }

    var nocacheDataurl = urlb;

    // The code...

    xHttp.onreadystatechange = function () {
        if (xHttp.readyState === 4) {
            document.getElementById(divb).innerHTML = xHttp.responseText;
        }
    }
    xHttp.open("GET", nocacheDataurl, true);
    xHttp.send(null);
}


function refreshSlipAutobet() {

    var xHttp;
    try {
        xHttp = new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
    }
    catch (e) {
        try {
            xHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
        }
        catch (e) {
            try {
                xHttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) {
                alert("Your browser does not support AJAX.");
                return false;
            }
        }
    }

    var nocacheDataurl = urlau;

    // The code...

    xHttp.onreadystatechange = function () {
        if (xHttp.readyState === 4) {
            document.getElementById(divau).innerHTML = xHttp.responseText;
        }
    }
    xHttp.open("GET", nocacheDataurl, true);
    xHttp.send(null);
}


function refreshSlipSGR() {

    var xHttp;
    try {
        xHttp = new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
    }
    catch (e) {
        try {
            xHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
        }
        catch (e) {
            try {
                xHttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) {
                alert("Your browser does not support AJAX.");
                return false;
            }
        }
    }

    var nocacheDataurl = url_sgr;

    // The code...

    xHttp.onreadystatechange = function () {
        if (xHttp.readyState === 4) {
            document.getElementById(sgrdivid).innerHTML = xHttp.responseText;
        }
    }
    xHttp.open("GET", nocacheDataurl, true);
    xHttp.send(null);
}

// Start the refreshing process

var seconds;
window.onload = function startrefresh() {
    refreshdiv();
    refreshshopdiv();
    if (locations === 'jackpot') {
        refreshSlipJ();
    }
    if (locations === 'correct') {
        refreshSlipB();
    }
    if (locations === 'rafiki') {
        refreshSlipNP();
    }
    if (locations === 'peerbets') {
        refreshSlipP();
    }
    if (locations === 'sgr') {
        refreshSlipSGR();
    }
    if (locations === 'autobet') {
        refreshSlipAutobet();
    }
}