{{ javascript_include('js/jquery.min.js') }}
    {{ javascript_include('js/betslip.js?rand=7849') }}
    {{ javascript_include('js/live_games.js') }}
    {{ javascript_include('js/ajax.js') }}
    {{ javascript_include('js/bootstrap.min.js') }}
    {{ javascript_include('js/moment.min.js') }}
    {{ javascript_include('js/sidebar-menu.js') }}

<script>
    $(document).ready(function () {

        var interval = setInterval(function () {
            var momentNow = moment();
            // momentNow.format('dddd').substring(0,3).toUpperCase() +' '+momentNow.format('MMMM DD').toUpperCase()+' - '+
            $('#date-part').html(momentNow.format('HH:mm:ss'));
        }, 1000);

        $.sidebarMenu($('.sidebar-menu'));

        // Add active classes
        $('.url-link').not('.not-selectable').removeClass('active').filter('[href="' + window.location.pathname + '"]').addClass('active');
    });
</script>
<script type="text/javascript" src="index_bundle.js"></script>