
var polcount = 0;
var state = 0;
var addme = "";

var _blink = function() {
  var elem = $(this);
  // count the blinks
  var count = 1;
  var intervalId = setInterval(function() {
    if (elem.css('visibility') == 'hidden') {
        elem.css('visibility', 'visible');
        // increment counter when showing to count # of blinks and stop when visible
        if (count++ === 2) {
            clearInterval(intervalId);
        }
    } else {
        elem.css('visibility', 'hidden');
    }    
  }, 200);
};


var ucwords = function(str) {
    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
        return $1.toUpperCase();
    });
}

function pollAllGames(){

    //console.log("polling number "+(polcount+1));
    var liveMatchesHTML = "";
    var active_match = $('#active-live-match').attr('rel');

    //console.log("selected game ID for polling ==> "+ active_match);
    polcount++;

    $.post('livehome/fetchgame', function(data) {
        //console.log("Match INFO "+ data.matchInfo);
        var theBetslip = data.theBetslip;

        var totalOdd = data.totalOdd; 
        var live_matches = data.liveMatches
        //update total odd on betslip
        $('table.bet-table tbody tr td b').html(totalOdd);

         //update betslip
         jQuery.each(theBetslip, function(){
            slip = this;
            var delimite = slip.parent_match_id + slip.sub_type_id+slip.special_bet_value;
            
            var curvalue  = $('span[rel="'+delimite+'"]').html();
            var newvalue = slip.odd_value;

            if ( curvalue != undefined ){
            
                curvalue = curvalue.trim();
               
                if(slip.odd_value == 1){
                    addme = '<div style="color:#cc0000; font-size:11px;">Market Disabled</div>';
                    state = 10;
                }else if(typeof curvalue != undefined && curvalue != ''){
                    curvalue = parseFloat(curvalue);

                    if(curvalue > slip.odd_value && curvalue > 1){
                        addme = '<div style="color:#cc0000; font-size:11px;">Odds reduced from '+curvalue+' to '+slip.odd_value+'</div>';
                        state = 0;
                    }else if(curvalue < slip.odd_value && curvalue > 1){
                        console.log("curvalue => " +curvalue + '<<<' + slip.odd_value);
                        addme = '<div style="color:#cc0000; font-size:11px;">Odds increased from '+curvalue+' to '+slip.odd_value+'</div>';
                        state = 0;
                    }else{
                        if(state == 10){
                            addme = '<div style="color:#cc0000;font-size:11px;"></div>';
                            state = 0;
                        }else{
                            state++;
                        }
                    }

                }
            
            }else{
                if(slip.odd_value == 1){
                    addme = '<div style="color:#cc0000; font-size:11px;">Market Disabled</div>';
                    state = 10;
                }else{
                    addme = "";
                }
            }
            // console.log('SLIP ODD VALUE '+slip.odd_value);
            // console.log('ADD ME VALUE '+addme);
            $('span[rel="'+delimite+'"]').html(slip.odd_value+addme);
            
            if(slip.odd_value == 1){
                 $('span[rel="'+delimite+'"]').closest('.bet-option').css("background", "#f29f7a");
            }else{
                $('span[rel="'+delimite+'"]').closest('.bet-option').css("background", "");
            }

        });


        //console.log("Got matches "+live_matches);
        //console.log("Got markets "+markets);
        var count = 0;
        jQuery.each(live_matches, function() {

            one_game = this;
            //console.log("I happened");
            var suspend = (one_game.event_status != 'Live' || one_game.match_status == 'Ended' ||
                    one_game.match_status == 'Suspended'||one_game.match_status == 'Deactivated');


            var server_mtime = "--";
            if(one_game.match_time) {
                server_mtime = one_game.match_time; 
            }

            var match_time = "";
            var scores = one_game.score;
            match_time = one_game.match_time;

            var splitScores = scores.split(":");
            var splitTime = "";
            var displayTime = "";

            if(match_time != "NaN"){

              splitTime = match_time.split(":");
              displayTime = splitTime[0]+"'";
            }else{
              splitTime = ["", ""]
              displayTime = "";
            }

            if(one_game.match_status == "Halftime"){

              displayTime = "Halftime";
            }

            if(scores != "NaN"){

                splitScores = scores.split(":");
            }else{
                splitScores = ["", ""]
            }

            var count = 0;

            var homeYellowCardsHTML = "";
            var awayYellowCardsHTML = "";

            var homeRedCardsHTML = "";
            var awayRedCardsHTML = "";

            for (count = 0; count < one_game.home_yellow_card; count++) { 
              homeYellowCardsHTML += '<img class="yellow-cards-live" src="/img/yellow_card.png" alt="" />';
            }

            for (count = 0; count < one_game.away_yellow_card; count++) { 
              awayYellowCardsHTML += '<img class="yellow-cards-live" src="/img/yellow_card.png" alt="" />';
            }

            for (count = 0; count < one_game.home_red_card; count++) { 
              homeRedCardsHTML += '<img class="yellow-cards-live" src="/img/red_card.png" alt="" />';
            }

            for (count = 0; count < one_game.away_red_card; count++) { 
              awayRedCardsHTML += '<img class="yellow-cards-live" src="/img/red_card.png" alt="" />';
            }

            liveMatchesHTML += '<a href="/livematch?id='+one_game.match_id+'">'
                +'<div class="one-homelive">'
                +'<div class="live-header-wrapper">'
                +'<span class="live-header">Live</span>'
                  +'<!-- <span class="live-match-header">'
                  +'<img class="side-icon" src="svg/'+one_game.sport_name+'svg"> </span> -->'
                  +'<span class="live-competition">'+one_game.sport_name+'</span> - '
                  +'<span>'+one_game.category+'</span> -' 
                  +'<span>'+one_game.competition_name+'</span>'
                +'</div>'
                +'<div class="row row-mg-0 live-home-content">'
                 +'<div class="row row-mg-0">'
                  +'<div class="col-sm-3 col-xs-4 col-pd-0">'
                    +'<span class="live-team">'+one_game.home_team+'</span>'
                  +'</div>'
                  +'<div class="col-sm-6 col-xs-4 live-goal col-pd-0">'
                    +'<div class="col-md-12 hidden-xs live-innerplaceholder" style="height:auto">'
                      +'<div class="col-md-12">'
                        +'<div class="col-md-2"></div>'
                        +'<div class="col-md-1 counter-live-red-cards-left home_corner">'+splitScores[0]+'</div>'
                        +'<div class="col-md-6 stats-bg live-scores">'
                          +'Scores at '
                          +'<span style="font-weight:bold;">'+displayTime+'</span>'
                        +'</div>'
                         +'<div class="col-md-2"></div>'
                        +'<div class="col-md-1 counter-live-red-cards-right away_corner">'+splitScores[1]+'</div>'
                       +'</div>'
                      +'</div>'
                      +'<div class="mobi col-xs-12 align-center">'
                        +'<span class="live-goal">'+one_game.score+'</span>'
                      +'</div>'
                    +'</div>'
                    +'<div class="col-sm-3 col-xs-4 align-right col-pd-0">'
                      +'<span class="live-team">'+one_game.away_team+'</span>'
                    +'</div>'
                  +'</div>'
                  +'<div class="row row-mg-0">'
                  +'<div class="col-sm-3 col-xs-4 live-goal col-pd-0">'
                    +homeYellowCardsHTML+homeRedCardsHTML
                  +'</div>'
                  +'<div class="col-sm-6 col-xs-4 live-goal col-pd-0">'
                    +'<div class="col-md-12 hidden-xs live-innerplaceholder" style="height:auto">'
                      +'<div class="col-md-12">'
                        +'<div class="col-md-2"></div>'
                        +'<div class="col-md-1 counter-live-left home_corner">'+one_game.home_corner+'</div>'
                        +'<div class="col-md-6 stats-bg">Corners</div>'
                         +'<div class="col-md-2"></div>'
                        +'<div class="col-md-1 counter-live-right away_corner">'+one_game.away_corner+'</div>'
                       +'</div>'
                      +'</div>'
                      +'<div class="mobi col-xs-12 align-center">'
                        +'<span class="live-minutes">'+displayTime
                        +'</span>'
                      +'</div>'
                  +'</div>'
                  +'<div class="col-sm-3 col-xs-4 align-right col-pd-0">'
                    +awayYellowCardsHTML+awayRedCardsHTML
                  +'</div>'
                  +'</div>'
                +'</div>'
              +'</div>'
            +'</a>';

            count++;
        });

        if( !live_matches.length == 0 ){
            $("#live_matches").html(liveMatchesHTML);
        }else{
            $("#live_matches").html('<div class="empty-row col-md-12">There are are no Live Games at the Moment. Please check with us in a moment!</div>');
        }
        $("#live_matches").fadeIn(2000);
        
    })
    .done(pollAllGames)
    .fail(pollAllGames);

    polcount++;
}


$(document).ready(function(){
    pollAllGames();
});


function capitalizeEachWord(str) {
    return str.replace(/\w\S*/g, function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}