/*
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

/** @jsx React.DOM */

var ProductsList = React.createClass({

  loadMatchesFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  getInitialState: function() {
    return {data: []};
  },

    componentDidMount: function() {
    this.loadMatchesFromServer();
  },

    render: function() {

        var products = this.state.data.map(function(match) {
            return (
              <li key={match.id}>
                <Product data={match} />
              </li>
            )
        });

        return (
          <ul className="clearfix">
            {products}
          </ul>
        );
    }
});
