/*
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

var CartMatch = React.createClass({
    getInitialState: function() {
      return {
        added: false,
      };
    },

    addToCart: function(pick, e) {
      if(!this.state.added) {
        // add
        this.props.data.pick=pick;
        $.publish('cart.added', this.props.data);
      }
      else {
        // remove
        $.publish('cart.removed', this.props.data.id);
      }

      this.setState({
        added: !this.state.added
      });
    },

    render: function() {
        // assign to props
        var data = this.props.data;

        return (
                <span className="cart-item__name">{data.home_team}</span>
                <span className="cart-item__price">{data.away_team} {data.away_odd}</span>
        );
    }
});
