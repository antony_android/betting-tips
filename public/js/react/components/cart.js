/*
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

/** @jsx React.DOM */

var CartMatch = React.createClass({

    render: function () {
        // assign to props
        var data = this.props.data;

        return (
            <div>
                <span className="cart-item__name">{data.home_team}</span>
                <span className="cart-item__price">{data.away_team} {data.away_odd}</span>
                <button className='btn btn-primary' onClick={this.props.removeItem.bind(this, data.id)}>remove</button>
            </div>
        );
    }
});

var Cart = React.createClass({

    getInitialState: function () {
        // also subscribe to product events here
        $.subscribe('cart.added', this.addItem);
        $.subscribe('cart.removed', this.removeItem);

        return {
            items: [],
            total: 0,
            currency: 'EUR'
        };
    },

    addItem: function (e, item) {
        this.state.items.push(item);
        this.forceUpdate();

        this.countTotal();
    },

    removeItem: function (itemId, e) {
        var itemIndexInArray;

        this.state.items.some(function (item, index) {
            if (item.id === itemId) {
                itemIndexInArray = index;
                return true;
            }
        });

        this.state.items.splice(itemIndexInArray, 1);
        this.forceUpdate();

        this.countTotal();
    },

    countTotal: function () {
        var total = 0;

        this.state.items.forEach(function (item, index) {
            total += item.away_odd;
        });

        this.setState({
            total: total
        })
    },

    render: function () {

        var items = this.state.items.map(function (item) {
            return (
                <li key={item.id} className="cart-item">
                    <CartMatch data={item} removeItem={this.removeItem.bind(this)}/>
                </li>
            )
        }, this);

        var body = (
            <ul>
                {items}
            </ul>
        );

        var empty = <div className="alert alert-info">Cart is empty</div>;

        return (
            <div className="panel panel-default">
                <div className="panel-body">
                    {items.length > 0 ? body : empty}
                    <div className="cart__total">Total: {this.state.total} {this.state.currency}</div>
                </div>
            </div>
        );
    }
});
