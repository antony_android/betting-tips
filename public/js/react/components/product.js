/*
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

/** @jsx React.DOM */

var Product = React.createClass({
    getInitialState: function() {
      return {
        added: false,
      };
    },

    addToCart: function(pick, e) {
      if(!this.state.added) {
        // add
        this.props.data.pick=pick;
        $.publish('cart.added', this.props.data);
      }
      else {
        // remove
        $.publish('cart.removed', this.props.data.id);
      }

      this.setState({
        added: !this.state.added
      });
    },

    render: function() {
        // assign to props
        var data = this.props.data;

        return (
          <div className="thumbnail">
            <img src={data.home_team} alt="product image" />
            <div className="caption clearfix">
              <h3><a href={data.url}>{data.away_team}</a></h3>
              <div className="product__price">{data.max_bet} {data.game_id}</div>
              <div className="product__button-wrap">
                <button value={data.away_team} className={this.state.added ? 'btn btn-danger' : 'btn btn-primary'} onClick={this.addToCart.bind(this,'home')}>
                  {this.state.added ? 'Remove' : data.away_odd}
                </button>
                <button className={this.state.added ? 'btn btn-danger' : 'btn btn-primary'} onClick={this.addToCart.bind(this,'away')}>
                  {this.state.added ? 'Remove' : data.away_odd}
                </button>
              </div>
            </div>
          </div>
        );
    }
});
