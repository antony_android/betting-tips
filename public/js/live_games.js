$(document).ready(function(){

	var odds_arr = [];
	var polcount = 0;

	var available_games = function(){
	var ids = [];
	$('button.live-game').each(function () {
	    ids.push($(this).attr("parentmatchid"));
	   });
	   return ids;
	};

	var odds_lock_html = '<span class="odd-dirrection" ><i class="fa fa-lock" ></i></span>';

	var clean_d = function(data){
	 return data.replace(/ /g,'').replace('/[^A-Za-z0-9\-]/', '').replace('/-+/', '-');
	};

	var _blink = function() {
	  var elem = $(this);
	  // count the blinks
	  var count = 1;
	  var intervalId = setInterval(function() {
	    if (elem.css('visibility') == 'hidden') {
	        elem.css('visibility', 'visible');
	        // increment counter when showing to count # of blinks and stop when visible
	        if (count++ === 5) {
	            clearInterval(intervalId);
	        }
	    } else {
	        elem.css('visibility', 'hidden');
	    }    
	  }, 200);
	};

	var remove_disabled_games = function(games){
	 	var not_available = [];
	 	var selected_ids = [];
	 	$.each(games, function(index, game){
	  	selected_ids.push(game.parent_match_id);
	 })
	 console.log("SELECTED IDS "+ selected_ids);

	  $.each(available_games(), function (index, parentmatchid) {
	     if($.inArray(parentmatchid, selected_ids) == -1){
	      //remove from DOM
	      console.log('Removing parent_match_id '+parentmatchid);
	      $('button[parentmatchid='+parentmatchid+']').closest('.col-sm-12.top-matches').remove();
	     }
	 });
	};

	var formatDate = function(template, date) {
	  var specs = 'YYYY:MM:DD:HH:mm:ss'.split(':');
	  date = new Date(date || Date.now() - new Date().getTimezoneOffset() * 6e4);
	  return date.toISOString().split(/[-:.TZ]/).reduce(function(template, item, i) {
	    return template.split(specs[i]).join(item);
	  }, template);
	};

	var create_display_button = function(one_game, suspend, market){
	 
		var market_name='', m_suspend=false, odd_value='', odd_key='', display_text='', button_text='';
		
		if(market == 1){
		   market_name = one_game.home_team;
		   odd_value = one_game.home_odd;
		   odd_key = one_game.home_team;
		   m_suspend =  one_game.home_odd_status !=  1;

		   button_text = '<button '
		         + ((suspend ||m_suspend)?'disabled="disabled"':'')
		         +' class="live-game home-team '+one_game.match_id+'  '+
		         clean_d(one_game.match_id+one_game.sub_type_id+market_name) +' ' 
		         + ((suspend ||m_suspend)?'disabled ':'') +'" '
		         +' hometeam="'+one_game.home_team+'" oddtype="1x2" bettype="live" ' 
		         +' awayteam="'+one_game.away_team+'" oddvalue="'+odd_value+'" '
		         +' target="javascript:;" odd-key="'+odd_key+'" '
		         +' parentmatchid="'+one_game.parent_match_id+'" id="'+one_game.match_id+'" '
		         +'custom="'+clean_d(one_game.match_id+one_game.sub_type_id+market_name)+'"' 
		         +'value="1" special-value-value="0" '
		         +'onclick="addBet(this.id,this.value,this.getAttribute(\'odd-key\'),'
		         +'this.getAttribute(\'custom\'),this.getAttribute(\'special-value-value\'),'
		         +'this.getAttribute(\'bettype\'),this.getAttribute(\'hometeam\'),'
		         +'this.getAttribute(\'awayteam\'),this.getAttribute(\'oddvalue\'),'
		         +'this.getAttribute(\'oddtype\'),this.getAttribute(\'parentmatchid\'))">';

		}else if(market == 2){
		   market_name = 'draw';
		   odd_value = one_game.neutral_odd;
		   odd_key = 'draw';
		   m_suspend =  one_game.neutral_odd_status !=  1;

		   button_text = '<button '
		         + ((suspend ||m_suspend)?'disabled="disabled"':'')
		         +' class="live-game draw '+one_game.match_id+'  '+
		         clean_d(one_game.match_id+one_game.sub_type_id+market_name) +' ' 
		         + ((suspend ||m_suspend)?'disabled ':'') +'" '
		         +' hometeam="'+one_game.home_team+'" oddtype="1x2" bettype="live" ' 
		         +' awayteam="'+one_game.away_team+'" oddvalue="'+odd_value+'" '
		         +' target="javascript:;" odd-key="'+odd_key+'" '
		         +' parentmatchid="'+one_game.parent_match_id+'" id="'+one_game.match_id+'" '
		         +'custom="'+clean_d(one_game.match_id+one_game.sub_type_id+market_name)+'"' 
		         +'value="1" special-value-value="0" '
		         +'onclick="addBet(this.id,this.value,this.getAttribute(\'odd-key\'),'
		         +'this.getAttribute(\'custom\'),this.getAttribute(\'special-value-value\'),'
		         +'this.getAttribute(\'bettype\'),this.getAttribute(\'hometeam\'),'
		         +'this.getAttribute(\'awayteam\'),this.getAttribute(\'oddvalue\'),'
		         +'this.getAttribute(\'oddtype\'),this.getAttribute(\'parentmatchid\'))">';

		}else if(market==3){
		   market_name = one_game.away_team;
		   odd_value = one_game.away_odd;
		   odd_key = one_game.away_team;
		   m_suspend =  one_game.away_odd_status !=  1;

		   button_text = '<button '
		         + ((suspend ||m_suspend)?'disabled="disabled"':'')
		         +' class="live-game away-team '+one_game.match_id+'  '+
		         clean_d(one_game.match_id+one_game.sub_type_id+market_name) +' ' 
		         + ((suspend ||m_suspend)?'disabled ':'') +'" '
		         +' hometeam="'+one_game.home_team+'" oddtype="1x2" bettype="live" ' 
		         +' awayteam="'+one_game.away_team+'" oddvalue="'+odd_value+'" '
		         +' target="javascript:;" odd-key="'+odd_key+'" '
		         +' parentmatchid="'+one_game.parent_match_id+'" id="'+one_game.match_id+'" '
		         +'custom="'+clean_d(one_game.match_id+one_game.sub_type_id+market_name)+'"' 
		         +'value="1" special-value-value="0" '
		         +'onclick="addBet(this.id,this.value,this.getAttribute(\'odd-key\'),'
		         +'this.getAttribute(\'custom\'),this.getAttribute(\'special-value-value\'),'
		         +'this.getAttribute(\'bettype\'),this.getAttribute(\'hometeam\'),'
		         +'this.getAttribute(\'awayteam\'),this.getAttribute(\'oddvalue\'),'
		         +'this.getAttribute(\'oddtype\'),this.getAttribute(\'parentmatchid\'))">';

		}else if(market == 4){

		   market_name = one_game.home_team+" or draw";
		   odd_value = one_game.double_chance_1x_odd;
		   odd_key = market_name;
		   m_suspend =  one_game.onex_odd_status !=  1;

		   button_text = '<button '
		         + ((suspend ||m_suspend)?'disabled="disabled"':'')
		         +' class="live-game onex-team '+one_game.match_id+'  '+
		         clean_d(one_game.match_id+one_game.sub_type_id+market_name) +' ' 
		         + ((suspend ||m_suspend)?'disabled ':'') +'" '
		         +' hometeam="'+one_game.home_team+'" oddtype="Double Chance" bettype="live" ' 
		         +' awayteam="'+one_game.away_team+'" oddvalue="'+odd_value+'" '
		         +' target="javascript:;" odd-key="'+odd_key+'" '
		         +' parentmatchid="'+one_game.parent_match_id+'" id="'+one_game.match_id+'" '
		         +'custom="'+clean_d(one_game.match_id+one_game.sub_type_id+market_name)+'"' 
		         +'value="1" special-value-value="0" '
		         +'onclick="addBet(this.id,this.value,this.getAttribute(\'odd-key\'),'
		         +'this.getAttribute(\'custom\'),this.getAttribute(\'special-value-value\'),'
		         +'this.getAttribute(\'bettype\'),this.getAttribute(\'hometeam\'),'
		         +'this.getAttribute(\'awayteam\'),this.getAttribute(\'oddvalue\'),'
		         +'this.getAttribute(\'oddtype\'),this.getAttribute(\'parentmatchid\'))">';

		}else if(market==5){

		   market_name = 'draw or '+one_game.away_team;
		   odd_value = one_game.double_chance_x2_odd;
		   odd_key = market_name;
		   m_suspend =  one_game.xtwo_odd_status !=  1;

		   button_text = '<button '
		         + ((suspend ||m_suspend)?'disabled="disabled"':'')
		         +' class="live-game xtwo-team '+one_game.match_id+'  '+
		         clean_d(one_game.match_id+one_game.sub_type_id+market_name) +' ' 
		         + ((suspend ||m_suspend)?'disabled ':'') +'" '
		         +' hometeam="'+one_game.home_team+'" oddtype="Double Chance" bettype="live" ' 
		         +' awayteam="'+one_game.away_team+'" oddvalue="'+odd_value+'" '
		         +' target="javascript:;" odd-key="'+odd_key+'" '
		         +' parentmatchid="'+one_game.parent_match_id+'" id="'+one_game.match_id+'" '
		         +'custom="'+clean_d(one_game.match_id+one_game.sub_type_id+market_name)+'"' 
		         +'value="1" special-value-value="0" '
		         +'onclick="addBet(this.id,this.value,this.getAttribute(\'odd-key\'),'
		         +'this.getAttribute(\'custom\'),this.getAttribute(\'special-value-value\'),'
		         +'this.getAttribute(\'bettype\'),this.getAttribute(\'hometeam\'),'
		         +'this.getAttribute(\'awayteam\'),this.getAttribute(\'oddvalue\'),'
		         +'this.getAttribute(\'oddtype\'),this.getAttribute(\'parentmatchid\'))">';

		}else if(market==6){

		   market_name = one_game.home_team+''+one_game.away_team;
		   odd_value = one_game.double_chance_12_odd;
		   odd_key = market_name;
		   m_suspend =  one_game.onetwo_odd_status !=  1;

		   button_text = '<button '
		         + ((suspend ||m_suspend)?'disabled="disabled"':'')
		         +' class="live-game onetwo-team '+one_game.match_id+'  '+
		         clean_d(one_game.match_id+one_game.sub_type_id+market_name) +' ' 
		         + ((suspend ||m_suspend)?'disabled ':'') +'" '
		         +' hometeam="'+one_game.home_team+'" oddtype="Double Chance" bettype="live" ' 
		         +' awayteam="'+one_game.away_team+'" oddvalue="'+odd_value+'" '
		         +' target="javascript:;" odd-key="'+odd_key+'" '
		         +' parentmatchid="'+one_game.parent_match_id+'" id="'+one_game.match_id+'" '
		         +'custom="'+clean_d(one_game.match_id+one_game.sub_type_id+market_name)+'"' 
		         +'value="1" special-value-value="0" '
		         +'onclick="addBet(this.id,this.value,this.getAttribute(\'odd-key\'),'
		         +'this.getAttribute(\'custom\'),this.getAttribute(\'special-value-value\'),'
		         +'this.getAttribute(\'bettype\'),this.getAttribute(\'hometeam\'),'
		         +'this.getAttribute(\'awayteam\'),this.getAttribute(\'oddvalue\'),'
		         +'this.getAttribute(\'oddtype\'),this.getAttribute(\'parentmatchid\'))">';

		}else if(market == 7){

		   market_name = "over 2.5";
		   odd_value = one_game.over_25_odd;
		   odd_key = market_name;
		   m_suspend =  one_game.over_25_odd_status !=  1;

		   button_text = '<button '
		         + ((suspend || m_suspend)?'disabled="disabled"':'')
		         +' class="live-game over25-team '+one_game.match_id+'  '+
		         clean_d(one_game.match_id+one_game.sub_type_id+market_name) +' ' 
		         + ((suspend ||m_suspend)?'disabled':'') +'" '
		         +' hometeam="'+one_game.home_team+'" oddtype="Totals Over/Under" bettype="live" ' 
		         +' awayteam="'+one_game.away_team+'" oddvalue="'+odd_value+'" '
		         +' target="javascript:;" odd-key="'+odd_key+'" '
		         +' parentmatchid="'+one_game.parent_match_id+'" id="'+one_game.match_id+'" '
		         +'custom="'+clean_d(one_game.match_id+one_game.sub_type_id+market_name)+'"' 
		         +'value="1" special-value-value="2.5" '
		         +'onclick="addBet(this.id,this.value,this.getAttribute(\'odd-key\'),'
		         +'this.getAttribute(\'custom\'),this.getAttribute(\'special-value-value\'),'
		         +'this.getAttribute(\'bettype\'),this.getAttribute(\'hometeam\'),'
		         +'this.getAttribute(\'awayteam\'),this.getAttribute(\'oddvalue\'),'
		         +'this.getAttribute(\'oddtype\'),this.getAttribute(\'parentmatchid\'))">';

		}else if(market == 8){

		   market_name = "under 2.5";
		   odd_value = one_game.under_25_odd;
		   odd_key = market_name;
		   m_suspend =  one_game.under_25_odd_status !=  1;

		   button_text = '<button '
		         + ((suspend || m_suspend)?'disabled="disabled"':'')
		         +' class="live-game under25-team '+one_game.match_id+'  '+
		         clean_d(one_game.match_id+one_game.sub_type_id+market_name) +' ' 
		         + ((suspend ||m_suspend)?'disabled ':'') +'" '
		         +' hometeam="'+one_game.home_team+'" oddtype="Totals Over/Under" bettype="live" ' 
		         +' awayteam="'+one_game.away_team+'" oddvalue="'+odd_value+'" '
		         +' target="javascript:;" odd-key="'+odd_key+'" '
		         +' parentmatchid="'+one_game.parent_match_id+'" id="'+one_game.match_id+'" '
		         +'custom="'+clean_d(one_game.match_id+one_game.sub_type_id+market_name)+'"' 
		         +'value="1" special-value-value="2.5" '
		         +'onclick="addBet(this.id,this.value,this.getAttribute(\'odd-key\'),'
		         +'this.getAttribute(\'custom\'),this.getAttribute(\'special-value-value\'),'
		         +'this.getAttribute(\'bettype\'),this.getAttribute(\'hometeam\'),'
		         +'this.getAttribute(\'awayteam\'),this.getAttribute(\'oddvalue\'),'
		         +'this.getAttribute(\'oddtype\'),this.getAttribute(\'parentmatchid\'))">';

		}

		else{
		  return '';
		}

		  if(market == 1){

		  	m_suspend =  one_game.home_odd_status !=  1;

		  	if(m_suspend || suspend) {

		  		display_text = odds_lock_html;
		  	}else {

		  		display_text = '<span class="odds-btn-label mobile-view">HOME<br/></span>'
                 +'<span class="theodds" id="'+one_game.match_id+'_homeodd">'+odd_value+'</span>';
		  	}

		  }else if(market == 2){

		  	m_suspend =  one_game.draw_odd_status != 1;

		  	if(m_suspend || suspend) {
		  		
		  		display_text = odds_lock_html;
		  	}else {

		  		display_text = '<span class="odds-btn-label mobile-view">DRAW<br/></span>'
                 +'<span class="theodds" id="'+one_game.match_id+'_neutralodd">'+odd_value+'</span>';
		  	}

		  }else if(market == 3){

		  	m_suspend =  one_game.away_odd_status != 1;
		  	if(m_suspend || suspend) {
		  		
		  		display_text = odds_lock_html;
		  	}else {

		  		display_text = '<span class="odds-btn-label mobile-view">AWAY<br/></span>'
                 +'<span class="theodds" id="'+one_game.match_id+'_awayodd">'+odd_value+'</span>';
		  	}

		  }else if(market == 4){

		  	m_suspend =  one_game.onex_odd_status !=  1;
		  	if(m_suspend || suspend) {
		  		
		  		display_text = odds_lock_html;
		  	}else {

		  		display_text = '<span class="odds-btn-label mobile-view">1 OR X<br/></span>'
                 +'<span class="theodds" id="'+one_game.match_id+'_1xodd">'+odd_value+'</span>';
		  	}

		  }else if(market == 5){

		  	m_suspend =  one_game.xtwo_odd_status != 1;
		  	if(m_suspend || suspend) {
		  		
		  		display_text = odds_lock_html;
		  	}else {

		  		display_text = '<span class="odds-btn-label mobile-view">X OR 2<br/></span>'
                 +'<span class="theodds" id="'+one_game.match_id+'_x2odd">'+odd_value+'</span>';
		  	}

		  }else if(market == 6){

		  	m_suspend =  one_game.onetwo_odd_status != 1;
		  	if(m_suspend || suspend) {
		  		
		  		display_text = odds_lock_html;
		  	}else {

		  		display_text = '<span class="odds-btn-label mobile-view">1 OR 2<br/></span>'
                 +'<span class="theodds" id="'+one_game.match_id+'_12odd">'+odd_value+'</span>';
		  	}

		  }else if(market == 7){

		  	m_suspend =  one_game.over_25_odd_status !=  1;
		  	if(m_suspend || suspend) {
		  		
		  		display_text = odds_lock_html;
		  	}else {

		  		display_text = '<span class="odds-btn-label mobile-view">OVER 2.5<br/></span>'
                 +'<span class="theodds" id="'+one_game.match_id+'_over25odd">'+odd_value+'</span>';
		  	}

		  }else if(market == 8){

		  	m_suspend =  one_game.under_25_odd_status !=  1;

		  	if(m_suspend || suspend) {
		  		
		  		display_text = odds_lock_html;
		  	}else {

		  		display_text = '<span class="odds-btn-label mobile-view">UNDER 2.5<br/></span>'
                 +'<span class="theodds" id="'+one_game.match_id+'_under25odd">'+odd_value+'</span>';
		  	}
		  	
		  }else{
		  	display_text = "";
		  }

	 	return button_text+display_text+'</button>';

	};


	function doPoll(){

		var games, one_game, match_id, home_odd, neutral_odd, away_odd,
			 x1_odd, x2_odd, one2_odd, over25_odd, under25_odd;

		var prev_home_odd, prev_neutral_odd, prev_away_odd,
			 prev_x1_odd, prev_x2_odd, prev_one2_odd, prev_over25_odd,
			  prev_under25_odd, match_time,start_time;

		polcount++;

	    $.post('live/fetchgames', function(data) {

	        games = data.games;

	        jQuery.each(games, function() {

	        	one_game = this;

	        	console.log("Got game "+one_game);
			  	match_id = one_game.match_id;
			  	match_time = one_game.match_time;
			  	if(match_time != null){
				  	match_time = match_time.split(":");
				  	match_time = match_time[0]+"'";
				  }else{
				  	match_time = "";
				  }
			  	
			    var suspend = (one_game.event_status != 'Live' || one_game.match_status == 'Ended' ||
	       		one_game.match_status == 'Suspended');
	       		start_time = new Date(one_game.start_time);

		  		/* missing game on rendered games */
	      		if($.inArray(one_game.parent_match_id, available_games()) == -1 && polcount > 2){
	      			console.log("parent_match_id found in available_games .."+one_game.parent_match_id);

	      			var new_item_append_html =  '<div class="col-sm-12 top-matches hidden-overflow">'
	      			+'<div class="col-sm-4 col-xs-11 p-0"><div class="compt-detail mobile-view"> GAME ID:'
	      			+'<span class="game-id">'+one_game.game_id+'</span>'+one_game.category+' '
	      			+one_game.competition_name+' | ' +start_time.getHours()+":"+start_time.getMinutes()
                +'</div><div class="compt-detail web-element p-3-left">'
                    +'<div class="col-sm-12 p-0">'+one_game.category+' '+one_game.competition_name+'</div>'
                    +'<div class="col-sm-3 p-0">ID: <span class="game-id">'+one_game.game_id+'</div>'
                    +'<div class="col-sm-9 web-games p-0">'+one_game.home_team+'</div>'
                    +'<div class="col-sm-12 pt-5"></div><div class="col-sm-3 p-0">'
                    +start_time.getHours()+":"+start_time.getMinutes()+'</div><div class="col-sm-9 web-games p-0">'
                    +one_game.away_team+'</div></div><div class="compt-teams hidden-lg">'
                    +one_game.home_team+'<span class="mobile-view">VS</span>'+one_game.away_team
                    +'</div><div class="compt-teams hidden-lg">'
                    +'<div class="col-xs-4 live-match-status">'+one_game.match_status+'</div>'
                    +'<div class="col-xs-4 live-match-time">'+match_time+'</div>'
                    +'<div class="col-xs-4 live-match-score">'+one_game.score+'</div>'
                    +'</div><div class="col-xs-1 events-odd pad mobile-view">'
                +'<a class="side" href="livematch?id='+one_game.match_id+'">'
                +one_game.side_bets+'</a></div></div>'
	            +'<div class="col-sm-2 col-xs-12 match-div-col mgr-5 p-0" >'
	            +'<div class="col-sm-4 col-xs-4 p-0 match-left-col">'
	                	+create_display_button(one_game, suspend, 1) + ' '
	            +'</div><div class="col-sm-4 col-xs-4 events-odd match-div-col match-mid-col">'
	                	+create_display_button(one_game, suspend, 2) + ' '
	            +'</div><div class="col-sm-4 col-xs-4 match-div-col match-right-col" style="padding:0;">'
	                	+create_display_button(one_game, suspend,3)  + ' '
	            +'</div></div><div class="col-sm-2 col-xs-12 match-div-col mgr-5 p-0" >'
		        +'<div class="col-sm-4 col-xs-4 p-0 match-left-col">'
	                	+create_display_button(one_game, suspend,4)  + ' '
	            +'</div><div class="col-sm-4 col-xs-4 events-odd match-div-col match-mid-col" style="">'
	                	+create_display_button(one_game, suspend,5)  + ' '
	            +' </div><div class="col-sm-4 col-xs-4 match-div-col match-right-col" style="padding:0;">'
	                	+create_display_button(one_game, suspend,6)  + ' '
	            +'</div></div><div class="col-sm-2 col-xs-12 match-div-col over-under mgr-5  p-0" >'
	            +'<div class="col-sm-6 col-xs-6 p-0 match-left-col">'    	
		                +create_display_button(one_game, suspend,7)  + ' '
		        +'</div><div class="col-sm-6 col-xs-6 events-odd match-div-col match-right-col" style="padding:0;">'
		                +create_display_button(one_game, suspend,8)  + ' '
		        +'</div></div><div class="col-sm-1 events-odd pad web-element">'
		                        +'<a class="side" href="livematch?id='+one_game.match_id+'">+'
				+one_game.side_bets+'</a></div><div class="col-sm-12 hidden-xs">'
				    +'<div class="col-sm-4 col-xs-11 p-0"></div>'
                    +'<div class="col-sm-2 col-xs-12 match-div-col mgr-5 p-0 live-match-status">'+one_game.match_status+'</div>'
                    +'<div class="col-sm-2 col-xs-12 match-div-col mgr-5  p-0 live-match-time">'+match_time+'</div>'
                    +'<div class="col-sm-2 col-xs-12 match-div-col over-under mgr-5 p-0 live-match-score">'+one_game.score+'</div>'
                    +'</div></div>';
	           
	                $('.middle-content').prepend(new_item_append_html);
	                
	            }else if(polcount > 2 ){

			  		console.log("Home odd "+odds_arr[match_id+"_homeodd"]);
			  		console.log("Existing odd "+one_game.home_odd);

			  		var homeBtn = $('button#'+match_id + '.home-team');
				    var drawBtn = $('button#'+match_id + '.draw');
				    var awayBtn = $('button#'+match_id + '.away-team');

				    var onexBtn = $('button#'+match_id + '.onex-team');
				    var xtwoBtn = $('button#'+match_id + '.xtwo-team');
				    var onetwoBtn = $('button#'+match_id+'.onetwo-team');

				    var overBtn = $('button#'+match_id + '.over25-team');
				    var underBtn = $('button#'+match_id +'.under25-team');

					if(suspend || one_game.home_odd_status != 1){
				       homeBtn.attr("disabled","disabled");
				       homeBtn.addClass('disabled');
				       $("#"+match_id+"_homeodd").html(odds_lock_html);
				       console.log("Home button disable run over");
			     	 }else{
				       homeBtn.removeAttr("disabled");
				       homeBtn.removeClass('disabled');

				  		if(odds_arr[match_id+"_homeodd"] < one_game.home_odd){

				  			$("#"+match_id+"_homeodd").html('<span><img class="plus-odds-white" src="/img/up-icon.png"/> </span>'+one_game.home_odd);

				  		}else if(odds_arr[match_id+"_homeodd"] == one_game.home_odd){

				  			// alert(match_id+"_homeodd");
				  			$("#"+match_id+"_homeodd").html('<span><img class="neutral-odds-white" src="/img/neutral.jpg"/> </span>'+one_game.home_odd);
				  		}else{

				  			$("#"+match_id+"_homeodd").html('<span><img class="minus-odds-white" src="/img/l-down.png"/> </span>'+one_game.home_odd);
				  		}
				  	}

				  	if(suspend || one_game.neutral_odd_status != 1){
				       drawBtn.attr("disabled","disabled");
				       drawBtn.addClass('disabled');
				       $("#"+match_id+"_neutralodd").html(odds_lock_html);
				       console.log("Draw button disable run over");
			     	 }else{
				        drawBtn.removeAttr("disabled");
				        drawBtn.removeClass('disabled');

				  		if(odds_arr[match_id+"_neutralodd"] < one_game.neutral_odd){

				  			$("#"+match_id+"_neutralodd").html('<span><img class="plus-odds-white" src="/img/up-icon.png"/> </span>'+one_game.neutral_odd);

				  		}else if(odds_arr[match_id+"_neutralodd"] == one_game.neutral_odd){

				  			$("#"+match_id+"_neutralodd").html('<span><img class="neutral-odds-white" src="/img/neutral.jpg"/> </span>'+one_game.neutral_odd);
				  		}else{

				  			$("#"+match_id+"_neutralodd").html('<span><img class="minus-odds-white" src="/img/l-down.png"/> </span>'+one_game.neutral_odd);
				  		}
				  	}

				  	if(suspend || one_game.away_odd_status != 1){
				       awayBtn.attr("disabled","disabled");
				       awayBtn.addClass('disabled');
				       $("#"+match_id+"_awayodd").html(odds_lock_html);
				       console.log("Away button disable run over");
			     	 }else{
				        awayBtn.removeAttr("disabled");
				        awayBtn.removeClass('disabled');
				  		if(odds_arr[match_id+"_awayodd"] < one_game.away_odd){

				  			$("#"+match_id+"_awayodd").html('<span><img class="plus-odds-white" src="/img/up-icon.png"/> </span>'+one_game.away_odd);

				  		}else if(odds_arr[match_id+"_awayodd"] == one_game.away_odd){

				  			$("#"+match_id+"_awayodd").html('<span><img class="neutral-odds-white" src="/img/neutral.jpg"/> </span>'+one_game.away_odd);
				  		}else{

				  			$("#"+match_id+"_awayodd").html('<span><img class="minus-odds-white" src="/img/l-down.png"/> </span>'+one_game.away_odd);
				  		}
				  	}

				  	if(suspend || one_game.double_chance_1x_odd_status != 1){
				       onexBtn.attr("disabled","disabled");
				       onexBtn.addClass('disabled');
				       $("#"+match_id+"_1xodd").html(odds_lock_html);
				       console.log("One X button disable run over");
			     	 }else{
				        onexBtn.removeAttr("disabled");
				        onexBtn.removeClass('disabled');
				  		if(odds_arr[match_id+"_1xodd"] < one_game.double_chance_1x_odd){

				  			$("#"+match_id+"_1xodd").html('<span><img class="plus-odds-white" src="/img/up-icon.png"/> </span>'+one_game.double_chance_1x_odd);

				  		}else if(odds_arr[match_id+"_1xodd"] == one_game.double_chance_1x_odd){

				  			$("#"+match_id+"_1xodd").html('<span><img class="neutral-odds-white" src="/img/neutral.jpg"/> </span>'+one_game.double_chance_1x_odd);
				  		}else{

				  			$("#"+match_id+"_1xodd").html('<span><img class="minus-odds-white" src="/img/l-down.png"/> </span>'+one_game.double_chance_1x_odd);
				  		}
				  	}

				  	if(suspend || one_game.double_chance_x2_odd_status != 1){
				       xtwoBtn.attr("disabled","disabled");
				       xtwoBtn.addClass('disabled');
				       $("#"+match_id+"_x2odd").html(odds_lock_html);
				       console.log("X two button disable run over");
			     	 }else{
				        xtwoBtn.removeAttr("disabled");
				        xtwoBtn.removeClass('disabled');
				  		if(odds_arr[match_id+"_x2odd"] < one_game.double_chance_x2_odd){

				  			$("#"+match_id+"_x2odd").html('<span><img class="plus-odds-white" src="/img/up-icon.png"/> </span>'+one_game.double_chance_x2_odd);

				  		}else if(odds_arr[match_id+"_x2odd"] == one_game.double_chance_x2_odd){

				  			$("#"+match_id+"_x2odd").html('<span><img class="neutral-odds-white" src="/img/neutral.jpg"/> </span>'+one_game.double_chance_x2_odd);
				  		}else{

				  			$("#"+match_id+"_x2odd").html('<span><img class="minus-odds-white" src="/img/l-down.png"/> </span>'+one_game.double_chance_x2_odd);
				  		}
				  	}

				  	if(suspend || one_game.double_chance_12_odd_status != 1){
				       onetwoBtn.attr("disabled","disabled");
				       onetwoBtn.addClass('disabled');
				       $("#"+match_id+"_12odd").html(odds_lock_html);
				       console.log("1 two button disable run over");
			     	 }else{
				        onetwoBtn.removeAttr("disabled");
				        onetwoBtn.removeClass('disabled');
				  		if(odds_arr[match_id+"_12odd"] < one_game.double_chance_12_odd){

				  			$("#"+match_id+"_12odd").html('<span><img class="plus-odds-white" src="/img/up-icon.png"/> </span>'+one_game.double_chance_12_odd);

				  		}else if(odds_arr[match_id+"_12odd"] == one_game.double_chance_12_odd){

				  			$("#"+match_id+"_12odd").html('<span><img class="neutral-odds-white" src="/img/neutral.jpg"/> </span>'+one_game.double_chance_12_odd);
				  		}else{

				  			$("#"+match_id+"_12odd").html('<span><img class="minus-odds-white" src="/img/l-down.png"/> </span>'+one_game.double_chance_12_odd);
				  		}
				  	}

				  	if(suspend || one_game.over_25_odd_status != 1){
				       overBtn.attr("disabled","disabled");
				       overBtn.addClass('disabled');
				       $("#"+match_id+"_over25odd").html(odds_lock_html);
				       console.log("1 two button disable run over");
			     	 }else{
				        overBtn.removeAttr("disabled");
				        overBtn.removeClass('disabled');
				  		if(odds_arr[match_id+"_over25odd"] < one_game.over_25_odd){

				  			$("#"+match_id+"_over25odd").html('<span><img class="plus-odds-white" src="/img/up-icon.png"/> </span>'+one_game.over_25_odd);

				  		}else if(odds_arr[match_id+"_over25odd"] == one_game.over_25_odd){

				  			$("#"+match_id+"_over25odd").html('<span><img class="neutral-odds-white" src="/img/neutral.jpg"/> </span>'+one_game.over_25_odd);
				  		}else{

				  			$("#"+match_id+"_over25odd").html('<span><img class="minus-odds-white" src="/img/l-down.png"/> </span>'+one_game.over_25_odd);
				  		}
				  	}

				  	if(suspend || one_game.under_25_odd_status != 1){
				       underBtn.attr("disabled","disabled");
				       underBtn.addClass('disabled');
				       $("#"+match_id+"_under25odd").html(odds_lock_html);
				       console.log("1 two button disable run over");
			     	 }else{
				        underBtn.removeAttr("disabled");
				        underBtn.removeClass('disabled');
				  		if(odds_arr[match_id+"_under25odd"] < one_game.under_25_odd){

				  			$("#"+match_id+"_under25odd").html('<span><img class="plus-odds-white" src="/img/up-icon.png"/> </span>'+one_game.under_25_odd);

				  		}else if(odds_arr[match_id+"_under25odd"] == one_game.under_25_odd){

				  			$("#"+match_id+"_under25odd").html('<span><img class="neutral-odds-white" src="/img/neutral.jpg"/> </span>'+one_game.under_25_odd);
				  		}else{

				  			$("#"+match_id+"_under25odd").html('<span><img class="minus-odds-white" src="/img/l-down.png"/> </span>'+one_game.under_25_odd);
				  		}
				  	}
			  	}

			  	odds_arr[match_id+"_homeodd"] = one_game.home_odd;
			  	odds_arr[match_id+"_neutralodd"] = one_game.neutral_odd;
			  	odds_arr[match_id+"_awayodd"] = one_game.away_odd;
			  	odds_arr[match_id+"_1xodd"] = one_game.double_chance_1x_odd;
			  	odds_arr[match_id+"_x2odd"] = one_game.double_chance_x2_odd;
			  	odds_arr[match_id+"_12odd"] = one_game.double_chance_12_odd;
			  	odds_arr[match_id+"_over25odd"] = one_game.over_25_odd;
			  	odds_arr[match_id+"_under25odd"] = one_game.under_25_odd;

			});

			$("#live-match-status").html(one_game.match_status);
			$("#live-match-time").html(match_time);
			$("#live-match-score").html(one_game.match_score);
			
			remove_disabled_games(games);

			$('.plus-odds-white').each(_blink);
	        $('.minus-odds-white').each(_blink);
	        $('.neutral-odds-white').each(_blink);

	        setTimeout(doPoll, 5000);
	    });

	    polcount++;
	}

	doPoll();

});