var polcount = 0;
var odd_value = "";
var state = 0;
var addme = "";


var clean_d = function(data){
    return data.replace(/ /gi,'').replace(/[^A-Za-z0-9\-]/gi, '').replace(/-+/, '-');
};

var ucwords = function(str) {
    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
        return $1.toUpperCase();
    });
}


var isFloat = function(num){
    return Number(num) == num && num % 1 != 0;
}


function pollOneGame(){

    polcount++;
    
    $.post('betmobile/fetchslip', function(data) {

        if(data.has_live == 0){

            console.log("Betslip has no live bet. Just slept and came back "+data.has_live);               
            return true;
        }

        console.log("Some live bets were  found and need revalidation here "+data.has_live);

        var theBetslip = data.betslip;

        var totalOdd = data.totalOdd; 

        var bet_amount =  $('#bet_amount').val();
        console.log("Bet amount "+bet_amount);
        var possible_win = bet_amount * totalOdd;

        console.log("Possible win raw "+possible_win);
     
        $('#pos_win').html(parseFloat(possible_win).toFixed(2));
        
        var count = 0;

        jQuery.each(theBetslip, function(index, slip){

            if(slip.parent_match_id == undefined){

                addme = '<span style="color:#cc0000; font-size:11px;">Market Disabled</span>';
                state = 10;
                return true;
            }

            var parent_match_id = slip.parent_match_id;
            
            var delimite = slip.parent_match_id + slip.sub_type_id+slip.special_bet_value;

            console.log("Delimite is "+delimite);
            
            var curvalue  = $('span[rel="'+delimite+'"]').html();

            console.log("Got current odds as "+curvalue);

            var newvalue = slip.odd_value;
            var addme="";

            console.log("Got updated odds as "+curvalue);

            if ( curvalue != undefined ){
            
                curvalue = curvalue.trim();
               
                if(slip.odd_value == 1){
                    addme = '<span style="color:#cc0000; font-size:11px;">Market Disabled</span>';
                    state = 10;
                }else if(typeof curvalue != undefined && curvalue != ''){
                    curvalue = parseFloat(curvalue);

                    if(curvalue > slip.odd_value && curvalue > 1){
                        console.log("Now updating odds with a negative value "+slip.odd_value);
                        addme = '<span style="color:#cc0000; font-size:11px; display:block;">Odds reduced from '+curvalue+' to '+slip.odd_value+'</span>';
                        state = 0;
                    }else if(curvalue < slip.odd_value && curvalue > 1){
                        console.log("curvalue => " +curvalue + '<<< Positive update ' + slip.odd_value);
                        addme = '<span style="color:#cc0000;font-size:11px;display:block;">Odds increased from '+curvalue+' to '+slip.odd_value+'</span>';
                        state = 0;
                    }else{
                        if(state == 10){
                            addme = '<span style="color:#cc0000; font-size:11px;"></span>';
                            state = 0;
                        }else{
                            state++;
                        }
                    }

                }
            
            }else{
                if(slip.odd_value == 1){
                    console.log("Now disabling the market");
                    addme = '<span style="color:#cc0000; font-size:11px;">Market Disabled</span>';
                    state = 10;
                }else{
                    addme = "";
                }
            }

            $('span[rel="'+delimite+'"]').html(slip.odd_value + addme);
            
            if(slip.odd_value == 1){
                 $('span[rel="'+delimite+'"]').closest('.bet-option').css("background", "#f29f7a");
            }else{
                $('span[rel="'+delimite+'"]').closest('.bet-option').css("background", "");
            }

        });

    })
    .done(pollOneGame)
    .fail(pollOneGame);

    polcount++;

}


$(document).ready(function(){
    pollOneGame();
});


function capitalizeEachWord(str) {
    return str.replace(/\w\S*/g, function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}