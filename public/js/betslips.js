/*
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

var seconds = 5;

$(window).scroll(function () {
    if ($(document).scrollTop() > 50) {
        $('#slip-holder').addClass('affix');
        $('#shrink-header').addClass('shrink-header');
    } else {
        $('#slip-holder').removeClass('affix');
        $('#shrink-header').removeClass('shrink-header');
    }
});


jQuery(document).ready(function ($) {
    $(".clickable-row").click(function () {
        window.document.location = $(this).data("href");
    });
});


$("#slip-button").click(function () {
    $("#slip-holder").slideDown("fast", function () {
    });
});

$("#slip-button-close").click(function () {
    $("#slip-holder").slideUp("fast", function () {
    });
});


function refreshSlip() {
    $.post("betslip", {}, function (data) {
        $("#betslip").animate({opacity: '0.8'});
        $("#betslip").html(data);
        $("#betslip").animate({opacity: '1'});
    }).done(function () {
        $(".loader").css("display", "none");
    });
}

function refreshPeerSlip() {
    $.post("betslip", {}, function (data) {
        $("#betslip-peer").animate({opacity: '0.8'});
        $("#betslip-peer").html(data);
        $("#betslip-peer").animate({opacity: '1'});
    }).done(function () {
        $(".loader").css("display", "none");
    });
}

function fbPurchase() {
    fbq('track', 'Purchase', {
        content_type: 'product', //either 'product' or 'product_group'
        content_ids: [user_id], //array of one or more product ids in the page
        value: 50, //REQUIRED
        currency: 'KES' //REQUIRED
    });
}

function fbDeposit() {
    fbq('track', 'AddPaymentInfo', {
        content_type: 'product', //either 'product' or 'product_group'
        content_ids: [user_id], //array of one or more product ids in the page
        value: 50, //REQUIRED
        currency: 'KES' //REQUIRED
    });
}

function refreshJackSlip() {
    $.post("matches/jackpot", {}, function (data) {
        $("#betslipJ").animate({opacity: '0.8'});
        $("#betslipJ").html(data);
        $("#betslipJ").animate({opacity: '1'});
    }).done(function () {
        $(".loader").css("display", "none");
    });
}

function refreshBingwaFour() {
    $.post("matches/correct", {}, function (data) {
        $("#betslipB").animate({opacity: '0.8'});
        $("#betslipB").html(data);
        $("#betslipB").animate({opacity: '1'});
    }).done(function () {
        $(".loader").css("display", "none");
    });
}

function addBet(value, sub_type_id, odd_key, custom, special_bet_value, bet_type, home, away, odd, oddtype, parentmatchid, pos) {
    $(".loader").slideDown("slow");
    $.post("betslip/add", {
        match_id: value,
        sub_type_id: sub_type_id,
        odd_key: odd_key,
        custom: custom,
        special_bet_value: special_bet_value,
        bet_type: bet_type,
        home: home,
        away: away,
        odd: odd,
        oddtype: oddtype,
        parentmatchid: parentmatchid,
        pos: pos
    }, function (data) {
        var game_id = value;
        $("." + game_id).removeClass('picked');
        $("." + custom).addClass('picked');
        if (bet_type == 'jackpot') {
            refreshJackSlip();
        } else if (bet_type == 'bingwafour') {
            refreshBingwaFour();
        } else {
            refreshSlip();
            $(".slip-counter").html(data.total);
        }
    });
}

function removeMatch(value) {
    $(".loader").slideDown("slow");
    $.post("betslip/remove", {match_id: value}, function (data) {
        refreshSlip();
        refreshJackSlip();
        $("." + value).removeClass('picked');
    });
}

function clearSlip(value) {
    $(".loader").slideDown("slow");
    $.post("betslip/clearslip", {}, function (data) {
        refreshSlip();
        $(".picked").removeClass('picked');
    });
}

function winnings() {
    var value = $("#bet_amount").val();
    var odds = $("#total_odd").val();
    var total = value * odds,
        winnings = total - value,
        tax = 18 * winnings / 100,
        net = winnings - tax;
    $("#pos_win").html(Math.round(winnings));
    $("#tax").html(Math.round(tax));
    $("#net-amount").html(Math.round(net));
}


function winningsM() {
    var value = $("#bet_amount_m").val();
    var odds = $("#total_odd_m").val();
    var total = value * odds,
        winnings = total - value,
        tax = 18 * winnings / 100,
        net = winnings - tax;
    $("#pos_win_m").html(Math.round(winnings));
    $("#tax").html(Math.round(tax));
    $("#net-amount").html(Math.round(net));
}

function placeBet() {
    $(".loader").slideDown("slow");
    var user_id = $("#user_id").val();
    var bet_amount = $("#bet_amount").val();
    var total_odd = $("#total_odd").val();
    if (!user_id) {
        $(".loader").css("display", "none");
        $("#quick-login").slideDown("slow");
    } else if (bet_amount < 20) {
        $(".loader").css("display", "none");
        $(".betslip-error").html("The minimum bet amount is KES. 20");
        $(".betslip-error").slideDown("slow");
    }
    else {
        $.post("betslip/placebet", {user_id: user_id, bet_amount: bet_amount, total_odd: total_odd}, function (data) {
            if (data.status_code == '421') {
                $(".loader").css("display", "none");
                $(".betslip-error").html(data.message);
                $(".betslip-error").slideDown("slow");
            }

            if (data.status_code == '500') {
                $(".loader").css("display", "none");
                $(".betslip-error").html(data.message);
                $(".betslip-error").slideDown("slow");
            }

            if (data.status_code == '201') {
                $(".betslip-success").slideDown("slow");
                $(".betslip-success").html(data.message);
                refreshSlip();
                $(".picked").removeClass('picked');
                fbq('track', 'Purchase', {
                    content_type: 'product', //either 'product' or 'product_group'
                    content_ids: [user_id], //array of one or more product ids in the page
                    value: 50, //REQUIRED
                    currency: 'KES' //REQUIRED
                });
            }
        }, 'json');
    }
}

function jackpotBet() {
    $(".loader").slideDown("slow");
    var user_id = $("#user_id").val();
    var bet_amount = $("#bet_amount").val();
    var jackpot_type = $("#jackpot_type").val();
    var jackpot_id = $("#jackpot_id").val();

    if (!user_id) {
        $(".loader").css("display", "none");
        $("#quick-login").slideDown("slow");
    }
    else {
        $.post("betslip/betJackpot", {
            user_id: user_id,
            jackpot_type: jackpot_type,
            jackpot_id: jackpot_id
        }, function (data) {
            if (data.status_code == '421') {
                $(".loader").css("display", "none");
                $(".betslip-error").html(data.message);
                $(".betslip-error").slideDown("slow");
            }

            if (data.status_code == '500') {
                $(".loader").css("display", "none");
                $(".betslip-error").html(data.message);
                $(".betslip-error").slideDown("slow");
            }

            if (data.status_code == '201') {
                $(".betslip-success").slideDown("slow");
                $(".betslip-success").html(data.message);
                refreshJackSlip();
                refreshBingwaFour()
                $(".picked").removeClass('picked');

                if (jackpot_type = 13) {
                    fbq('track', 'InitiateCheckout', {
                        content_type: 'product', //either 'product' or 'product_group'
                        content_ids: [user_id], //array of one or more product ids in the page
                        value: 50, //REQUIRED
                        currency: 'KES' //REQUIRED
                    });
                }

            }
        }, 'json');
    }
}