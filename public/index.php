<?php
/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */

$host = gethostname();
if ($host == "pr-web-2" || $host == "pr-web-3") {
    #ini_set('session.save_handler', 'redis');
    #ini_set('session.save_path', 'tcp://pr-memcache:6379?auth=phpredis');
}

//error_reporting(E_ALL);

define('APP_PATH', realpath('..'));

try {
    /**
     * Handle the request
     */

    include APP_PATH . "/vendor/autoload.php";

    /**
     * Read the configuration
     */
    $config = include APP_PATH . "/app/config/config.php";

    /**
     * Read auto-loader
     */
    include APP_PATH . "/app/config/loader.php";

    /**
     * Read services
     */
    include APP_PATH . "/app/config/services.php";

    $loader = new \Phalcon\Loader();
    $loader->registerDirs([
        APP_PATH . "/app/traits/",
    ])->register();

    $application = new \Phalcon\Mvc\Application($di);

    $html = $application->handle()->getContent();
    echo env('APP_DEBUG') ? $html : (new \voku\helper\HtmlMin())->minify($html);

} catch (\Exception $e) {
    if (env('APP_DEBUG') == "true") {
        //echo $e->getMessage() . '<br>';
        //echo '<pre>' . $e->getTraceAsString() . '</pre>';
    } else {
        echo "<head><title>Page Not Found - mabet.bet</title></head><body style='background-color: #030928;background:url(img/error-cover.jpg)'><link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'><div style='
    max-width: 600px;
    display: block;
    margin: 100px auto;
    text-align: center;
    color: #e0e0e0;
    font-size: 20px;
    font-family: Montserrat, sans-serif;
'>
<a href='http://betpalace.co.ke'><img style='max-width: 180px;' src='img/logo.png' /></a>
<p><b style='font-size: 50px;
    line-height: 60px;
    display: block;'>Sorry</b> <br/>Looks like something went wrong on our end.<br/> Please head back to the Homepage
</p>
    <a href='http://betpalace.co.ke' class='error-404' style='background: #6e2f44; color: #ffffff; border-radius: 10px; padding:5px 15px;'>Home</a>
</div>

</body>";
    }



}
